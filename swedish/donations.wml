#use wml::debian::template title="Hur du donerar till Debianprojektet" MAINPAGE="true"
#use wml::debian::translation-check translation="77730b1058b58e76cb29910f6acf353ec5bf9847"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#spi">Software in the Public Interest</a></li>
  <li><a href="#debianfrance">Debian France</a></li>
  <li><a href="#debianch">debian.ch</a></li>
  <li><a href="#debian">Debian</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Tack till alla donatorer för
deras stöd till Debian med utrustning eller tjänster:
<a href="https://db.debian.org/machines.cgi">hosting och hårdvarusponsorer</a>,
<a href="mirror/sponsors">speglingssponsorer</a>,
<a href="partners/">utvecklings- och tjänstpartners</a>.<p>
</aside>

<p>
Donationer hanteras av
<a href="$(HOME)/devel/leader">Debians projektledare</a> (DPL)
och tillåter Debian att ha
<a href="https://db.debian.org/machines.cgi">maskiner</a>,
<a href="https://wiki.debian.org/Teams/DSA/non-DSA-HW">annan hårdvara</a>,
domäner, kryptografiska certifikat,
<a href="https://www.debconf.org">Debiankonferensen</a>,
<a href="https://wiki.debian.org/MiniDebConf">Debian mini-konferenser</a>,
<a href="https://wiki.debian.org/Sprints">utvecklingssprintar</a>,
närvaro vid andra evenemang, osv.
</p>


<p id="default">
Olika <a href="https://wiki.debian.org/Teams/Treasurer/Organizations">organisationer</a>
förvaltar Debians tillgångar och tar emot donationer för Debians räkning.
Denna sida listar olika sätt att donera till Debianprojektet.
Det lättaste sättet att donera till Debian är via PayPal till
<a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest</a>,
en icke vinstdrivande organisation som förvaltar tillgångar för Debians räkning.
</p>

<p style="text-align:center"><button type="button">
<span class="fab fa-paypal fa-2x">
</span> <a
href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">Donera via PayPal</a>
</button></p>

<aside class="light">
  <span class="fas fa-gifts fa-5x"></span>
</aside>

<table>
<tr>
<th>Organisation</th>
<th>Metoder</th>
<th>Kommentarer</th>
</tr>
<tr>
<td><a href="#spi"><acronym title="Software in the Public Interest">SPI</acronym></a></td>
<td>
 <a href="#spi-paypal">PayPal</a>,
 <a href="#spi-click-n-pledge">Click &amp; Pledge</a>,
 <a href="#spi-cheque">check</a> (USD/CAD),
 <a href="#spi-other">övrigt</a>
</td>
<td>USA, skattebefriat ideellt</td>
</tr>
<tr>
<td><a href="#debianfrance">Debian France</a></td>
<td>
	<a href="#debianfrance-bank">Banköverföring</a>,
	<a href="#debianfrance-paypal">PayPal</a>
</td>
<td>Frankrike, skattebefriat ideellt</td>
</tr>
<tr>
<td><a href="#debianch">debian.ch</a></td>
<td>
 <a href="#debianch-bank">Banköverföring</a>,
 <a href="#debianch-other">Övrigt</a>
</td>
<td>Schweiz, ideellt</td>
</tr>
<tr>
<td><a href="#debian">Debian</a></td>
<td>
 <a href="#debian-equipment">Utrustning</a>,
 <a href="#debian-time">Tid</a>,
 <a href="#debian-other">Övrigt</a>
</td>
<td></td>
</tr>

# Template:
#<tr>
#<td><a href="#"><acronym title=""></acronym></a></td>
#<td>
# <a href="#"></a>,
# <a href="#"></a> (allows recurring donations),
# <a href="#cheque"></a> (CUR)
#</td>
#<td>, tax-exempt non-profit</td>
#</tr>

</table>

<h2 id="spi">Software in the Public Interest</h2>

<p>
<a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest, Inc.</a>
är ett skattebefriat ideellt företag baserat i USA, som grundades 1997 av personer
från Debian för att hjälpa organisationer relaterade till fri mjukvara och
fri hårdvara.
</p>

<h3 id="spi-paypal">PayPal</h3>
<p>
Enstaka och återkommande donationer kan göras via
<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">SPI-sidan</a>
på PayPal-webbplatsen.
För att göra en återkommande donation,
kryssa i rutan "upprepa betalningen (Månadsvis)".
</p>


<h3 id="spi-click-n-pledge">Click &amp; Pledge</h3>

<p>
Enstaka och återkommande donationer kan göras via
<a href="https://co.clickandpledge.com/advanced/default.aspx?wid=34115">SPI-sidan</a>
på Click &amp; Pledge-webbsidan.
För att göra en återkommande donation,
välj hur ofta du vill donera till höger,
skrolla ner till "Debian Project Donation",
mata in summan du vill donera,
klicka på "Add to cart" och
gå igenom resten av processen.
</p>


<h3 id="spi-cheque">Check</h3>

<p>
Donationer kan göras via check eller money order i
<abbr title="US dollars">USD</abbr> och
<abbr title="Canadian dollars">CAD</abbr>.
Vänligen sätt Debian i meddelandefältet och skicka checken till SPI på
adressen som listas på <a href="https://www.spi-inc.org/donations/">SPIs donationssida</a>.
</p>

<h3 id="spi-other">Övrigt</h4>

<p>
Donationer via banköverföring och andra metoder finns även tillgängliga.
För några delar av världen kan det vara lättare att göra en donation
till en av Software in the Public Interests partnerorganisationer.
För mer information, vänligen besök
<a href="https://www.spi-inc.org/donations/">SPIs donationssida</a>.
</p>

<h2 id="debianfrance">Debian Frankrike</h2>

<p>
<a href="https://france.debian.net/">Debian France Association</a> är en
organisation registrerad i Frankrike under <q>lagen från 1901</q>, för att
stödja och främja Debianprojektet i Frankrike.
</p>

<h3 id="debianfrance-bank">Banköverföring</h3>
<p>
Donationer via banköverföring finns tillgängliga till bankkontot som listas
på <a href="https://france.debian.net/soutenir/#compte">Debian Frances
donationssida</a>. Kvitton för dessa donationer för tillgängliga om du
skickar e-post till <a
href="mailto:donation@france.debian.net">donation@france.debian.net</a>.
</p>

<h3 id="debianfrance-paypal">PayPal</h3>
<p>
Donationer kan skickas via sidan för
<a href="https://france.debian.net/galette/plugins/paypal/form">Debian France PayPal</a>.
Dessa kan riktas specifikt till Debian France, eller till Debianprojektet i
allmänhet.
</p>

<h2 id="debianch">debian.ch</h2>

<p>
<a href="https://debian.ch/">debian.ch</a> grundades för att representera
Debianprojektet i Schweiz och Furstendömet Lichtenstein.
</p>

<h3 id="debianch-bank">Banköverföring</h3>

<p>
Donationer via banköverföring från både Schweiziska och internationella
banker finns tillgängliga till bankkontona som listas på
<a href="https://debian.ch/">debian.ch's webbplats</a>.
</p>

<h3 id="debianch-other">Övrigt</h3>

<p>
Donationer via andra metoder kan genomföras genom att kontakta donationsadressen
som listas på <a href="https://debian.ch/">debian.ch's webbplats</a>.
</p>


# Template:
#<h3 id=""></h3>
#
#<p>
#</p>
#
#<h4 id=""></h4>
#
#<p>
#</p>

<h2 id="debian">Debian</h2>

<p>
Debian har möjligheten att acceptera direkta <a
href="#debian-equipment">utrustningsdonationer</a> men inte <a
href="#debian-other">andra</a> donationer vid denna tidpunkt.
</p>

<a name="equipment_donations"></a>
<h3 id="debian-equipment">Utrustning och tjänster</h3>

<p>
Debian förlitar sig även på utrustningsdonationer och tjänster från
individer, företag, universitet osv för att hålla Debian anslutet till
världen.</p>

<p>Om ditt företag har några overksamma maskiner eller utrustning över
(hårddiskar, SCSI-controllers, nätverkskort, osv) som ligger och skräpar, var
vänlig och överväg att donera dessa till Debian. Vänligen kontakta vår
<a href="mailto:hardware-donations@debian.org">delegat för hårdvarudonationer</a>
för detaljer.</p>

<p>Debian underhåller en <a href="https://wiki.debian.org/Hardware/Wanted">lista på hårdvara som
önskas</a> för olika tjänster och grupper inom projektet.</p>

<h3 id="debian-time">Tid</h3>

<p>
Det finns många sätt att <a href="$(HOME)/intro/help">hjälpa Debian</a>
med din privata eller arbetstid.
</p>

<h3 id="debian-other">Övrigt</h3>

<p>
Debian kan inte ta emot någon kryptovaluta vid detta tillfälle men vi
undersöker möjligheter att kunna ta emot den här sortens donationer.
# Brian Gupta requested we discuss this before including it:
#If you have cryptocurrency to donate, or insights to share, please
#get in touch with <a href="mailto:madduck@debian.org">Martin f. krafft</a>.
</p>

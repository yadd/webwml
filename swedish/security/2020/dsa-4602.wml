#use wml::debian::translation-check translation="4c0996e191dd68b6dfbadfee3c048714d4cfa961" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i Xen hypervisor, vilket kunde
resultera i överbelastning, utökning av rättigheter gäst-till-värd eller
informationsläckage.</p>

<p>Utöver detta tillhandahåller denna uppdatering lindringar för spekulativa
sidokanalangreppet <q>TSX Asynchronous Abort</q>. För ytterligare information
var vänlig se
<a href="https://xenbits.xen.org/xsa/advisory-305.html">https://xenbits.xen.org/xsa/advisory-305.html</a></p>

<p>För den gamla stabila utgåvan (Stretch) har dessa problem rättats i
i version 4.8.5.final+shim4.10.4-1+deb9u12. Notera att detta kommer att
vara den sista säkerhetsuppdateringen för Xen i den gamla stabila
distributionen; uppströmsstöd för 4.8.x-grenen avslutades i slutet av
December 2019. Om du är beroende av säkerhetsstöd för din Xen-installation
rekommenderas en uppdatering till den stabila utgåvan (Buster).</p>

<p>För den stabila utgåvan (Buster) har dessa problem rättats i
version 4.11.3+24-g14b62ab3e5-1~deb10u1.</p>

<p>Vi rekommenderar att ni uppgraderar era xen-paket.</p>

<p>För detaljerad säkerhetsstatus om xen vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/xen">https://security-tracker.debian.org/tracker/xen</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4602.data"

#use wml::debian::template title="寄付"
#use wml::debian::translation-check translation="77730b1058b58e76cb29910f6acf353ec5bf9847"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#spi">Software in the Public Interest</a></li>
  <li><a href="#debianfrance">Debian France</a></li>
  <li><a href="#debianch">debian.ch</a></li>
  <li><a href="#debian">Debian</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debianの備品やサービスをサポートするべく寄附してくれたみなさんに感謝します:
<a href="https://db.debian.org/machines.cgi">ホスティングやハードウェアスポンサー</a>, <a href="mirror/sponsors">公式ミラーのスポンサー</a>, <a href="partners/">開発およびサービスのパートナー</a>
</p>
</aside>

<p>
寄付は <a href="$(HOME)/devel/leader">Debian プロジェクトリーダー</a>
(DPL) により管理され、それにより Debian が<a
href="https://db.debian.org/machines.cgi">マシン</a>や<a
href="https://wiki.debian.org/Teams/DSA/non-DSA-HW">その他のハードウェア</a>、
ドメイン、各種証明書の保持、<a href="https://www.debconf.org">Debian
カンファレンス</a>や <a href="https://wiki.debian.org/MiniDebConf">Debian
ミニカンファレンス</a>、<a
href="https://wiki.debian.org/Sprints">開発スプリント</a>、
その他のイベントへの参加、その他のことができるようになります。
</p>

<p id="default">
様々な<a href="https://wiki.debian.org/Teams/Treasurer/Organizations">組織</a>が
Debian の信託により資産を保持し、Debian のために寄付を受け取っています。
このページでは、Debianプロジェクトに寄付するための異なる方法を紹介しています。
信託の下 Debian の資産を保持している非営利組織である <a
href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest</a>
に PayPal 経由で寄付するのが最も簡単な Debian への寄付方法です。
</p>

<p style="text-align:center"><button type="button"><span class="fab fa-paypal fa-2x"></span> <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">PayPal 経由で寄附</a></button></p>

<aside class="light">
  <span class="fas fa-gifts fa-5x"></span>
</aside>

<table>
<tr>
<th>組織</th>
<th>方法</th>
<th>摘要</th>
</tr>
<tr>
<td><a href="#spi"><acronym title="Software in the Public Interest">SPI</acronym></a></td>
<td>
 <a href="#spi-paypal">PayPal</a>、
 <a href="#spi-click-n-pledge">Click &amp; Pledge</a>、
 <a href="#spi-cheque">Cheque</a>、
 <a href="#spi-other">その他</a>
</td>
<td>米国、非営利非課税</td>
</tr>
<tr>
<td><a href="#debianfrance">Debian France</a></td>
<td>
 <a href="#debianfrance-bank">電信送金</a>、
 <a href="#debianfrance-paypal">PayPal</a>
</td>
<td>フランス、非営利非課税</td>
</tr>
<tr>
<td><a href="#debianch">debian.ch</a></td>
<td>
 <a href="#debianch-bank">電信送金</a>、
 <a href="#debianch-other">その他</a>
</td>
<td>スイス、非営利</td>
</tr>
<tr>
<td><a href="#debian">Debian</a></td>
<td>
 <a href="#debian-equipment">備品</a>、
 <a href="#debian-time">時間</a>、
 <a href="#debian-other">その他</a>
</td>
<td></td>
</tr>

# Template:
#<tr>
#<td><a href="#"><acronym title=""></acronym></a></td>
#<td>
# <a href="#"></a>,
# <a href="#"></a> (allows recurring donations),
# <a href="#cheque"></a> (CUR)
#</td>
#<td>, tax-exempt non-profit</td>
#</tr>

</table>

<h2 id="spi">Software in the Public Interest</h3>

<p>
<a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest, Inc.</a>
は Debian の人によりフリーソフトウェア/ハードウェアを支援するため 
1997 年に創設されたアメリカ合衆国を本拠とする非営利非課税の企業です。
</p>

<h3 id="spi-paypal">PayPal</h3>

<p>
<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">SPIページ</a> 
から、PayPalウェブサイト上で一度限りおよび定期的な寄附ができます。
定期的な寄附をするためには、<em>Make this a monthly donation</em>にチェックを入れてください。
</p>

<h3 id="spi-click-n-pledge">Click &amp; Pledge</h3>

<p>
Click &amp; Pledge ウェブサイトの<a
href="https://co.clickandpledge.com/advanced/default.aspx?wid=34115">SPIページ</a>から、
一度限り及び定期的な寄付ができます。
定期的に寄付するには、右側で頻度を選択して<q>Debian Project Donation</q>
までスクロールし、寄付額を入力して<q>ADD TO CART</q>をクリックし、
残りのプロセスを進めます。
</p>

<h3 id="spi-cheque">Cheque</h3>

<p>
cheque か
米ドルまたはカナダドル建ての郵便為替を経由して寄付できます。<a
href="https://www.spi-inc.org/donations/">SPIの寄付のページ</a>
に記載されているアドレス宛に、メモ欄に Debian と記入してSPIに送ってください。
</p>

<h3 id="spi-other">その他</h3>

<p>
電信送金や他の方法による寄付も可能です。国や地域によっては
Software in the Public Interest のパートナー組織宛ての方が容易かもしれません。
さらなる詳細については<a
href="https://www.spi-inc.org/donations/">SPIの寄付のページ</a>を見てください。
</p>

<h2 id="debianfrance">Debian France</h2>

# http://www.jil.go.jp/foreign/labor_system/2004_8/france_01.html
<p>
<a href="https://france.debian.net/">Debian France Association</a>
はフランスで Debian プロジェクトをサポート、促進するために創設された組織で、
<q>1901年法</q>に基づいて登録されています。
</p>

<h3 id="debianfrance-bank">電信送金</h3>
<p>
<a href="https://france.debian.net/soutenir/#compte">Debian France
の寄付のページ</a>に掲載されている銀行口座への電信送金により寄付できます。
<a href="mailto:donation@france.debian.net">donation@france.debian.net</a>
宛てにメールを送ると寄付の受領証を受け取れます。
</p>

<h3 id="debianfrance-paypal">PayPal</h3>
<p>
<a href="https://france.debian.net/galette/plugins/paypal/form">Debian
France の PayPal のページ</a>から寄付できます。Debian France
宛てにするか Debian プロジェクト全体宛てにするか選択できます。
</p>

<h2 id="debianch">debian.ch</h2>

<p>
<a href="https://debian.ch/">debian.ch</a> はスイスとリヒテンシュタイン侯国で
Debian プロジェクトの代理を務めるために創設されました。
</p>

<h3 id="debianch-bank">電信送金</h3>

<p>
<a href="https://debian.ch/">debian.ch ウェブサイト</a>に掲載されている
銀行口座宛にスイス及び国際銀行から電信送金で寄付できます。
</p>

<h3 id="debianch-other">その他</h3>

<p>
<a href="https://debian.ch/">debian.ch
ウェブサイト</a>に掲載されている宛先に連絡を取ることで他の方法による寄付も可能です。
</p>

# Template:
#<h3 id=""></h3>
#
#<p>
#</p>
#
#<h4 id=""></h4>
#
#<p>
#</p>

<h2 id="debian">Debian</h2>

<p>
Debian は<a
href="#debian-equipment">備品</a>の寄付を直接受け取ることができますが、<a
href="#debian-other">その他</a>の寄付を受け取ることは現在のところできません。
</p>

<h3 id="debian-equipment">備品やサービス</h3>

<p>
Debian
と世界との接続を維持するための備品やサービスの寄付も個人や企業、大学等に頼っています。
</p>

<p>もしあなたの会社で、使用されていないマシンやスペアの機材
(ハードディスク、SCSIコントローラ、ネットワークカードなど)
がありましたら、それを Debian に寄付することをご考慮ください。詳細については<a
href="mailto:hardware-donations@debian.org">ハードウェア寄付係</a>までご連絡ください。</p>

<p>Debian は、プロジェクト内部のさまざまなサービスやグループのために、<a
href="https://wiki.debian.org/Hardware/Wanted">望まれるハードウェアのリスト</a>を管理しています。</p>

<h3 id="debian-time">時間</h3>

<p>
個人や仕事の時間を使って <a href="$(HOME)/intro/help">Debian
を支援</a>する方法は多数あります。
</p>

<h3 id="debian-other">その他</h3>

<p>
Debian が現時点で受け入れられる仮想通貨はありませんが、
この方法での寄付に対応できる方法を調査中です。
</p>

#use wml::debian::template title="Debian 13 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="cc8d41f2c05e7bd0e9797313797722d700f10194"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Problemas conhecidos</toc-add-entry>
<toc-add-entry name="security">Problemas de segurança</toc-add-entry>

<p>A equipe de segurança do Debian emite atualizações para pacotes na versão
estável (stable), nos quais eles(as) identificaram problemas relacionados à
segurança. Por favor consulte as <a href="$(HOME)/security/">páginas de segurança</a>
para obter informações sobre quaisquer problemas de segurança identificados no
<q>trixie</q>.</p>

<p>Se você usa o APT, adicione a seguinte linha ao <tt>/etc/apt/sources.list</tt>
para poder acessar as atualizações de segurança mais recentes:</p>

<pre>
  deb http://security.debian.org/ trixie-security main contrib non-free non-free-firmware
</pre>

<p>Depois disso execute <kbd>apt update</kbd> seguido por
<kbd>apt upgrade</kbd>.</p>

<toc-add-entry name="pointrelease">PLançamentos pontuais</toc-add-entry>

<p>Às vezes, no caso de vários problemas críticos ou atualizações de segurança,
a versão já lançada é atualizada. Geralmente, as atualizações são indicadas
como lançamentos pontuais.</p>

<!-- <ul>
  <li>The first point release, 13.1, was released on
      <a href="$(HOME)/News/2017/FIXME">FIXME</a>.</li>
</ul> -->

<ifeq <current_release_trixie> 13.0 "

<p>Ainda não há lançamentos pontuais para o Debian 13.</p>" "

<p>Consulte o <a
href="http://http.us.debian.org/debian/dists/trixie/ChangeLog">\
ChangeLog</a>
para obter detalhes sobre alterações entre 13 e <current_release_trixie/>.</p>"/>

<p>As correções na versão estável (stable) lançada geralmente passam por
um longo período de teste antes de serem aceitas no repositório.
No entanto, essas correções estão disponíveis no repositório
<a href="http://ftp.debian.org/debian/dists/trixie-proposed-updates/">\
dists/trixie-proposed-updates</a> de qualquer espelho do repositório Debian.</p>

<p>Se você usar o APT para atualizar seus pacotes, poderá instalar as
atualizações propostas adicionando a seguinte linha ao
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# Atualizações propostas para a versão 13
  deb http://deb.debian.org/debian trixie-proposed-updates main contrib non-free
</pre>

<p>Depois disso, execute <kbd>apt update</kbd> seguido por
<kbd>apt upgrade</kbd>.</p>

<toc-add-entry name="installer">Sistema de instalação</toc-add-entry>

<p>
Para obter informações sobre erratas e atualizações para o sistema de
instalação, consulte a página
<a href="debian-installer/">informações de instalação</a>
</p>

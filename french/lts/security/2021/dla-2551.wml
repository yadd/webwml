#use wml::debian::translation-check translation="2151728ae4d6df0107aaa0dbd6fa9ca8a128c0d9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes ont été découverts dans slirp, un émulateur SLIP/PPP utilisant
un compte d’interpréteur pour réseau commuté.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7039">CVE-2020-7039</a>

<p>Dû à une mauvaise gestion de la mémoire, un dépassement de tampon de tas ou
d’autres accès hors limites peuvent se produire. Cela peut conduire à un déni de
service ou à une exécution possible de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8608">CVE-2020-8608</a>

<p>Prévention d’une vulnérabilité de dépassement de tampon due à un usage
incorrect des valeurs renvoyées par snprintf.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:1.0.17-8+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets slirp.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de slirp, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/slirp">\
https://security-tracker.debian.org/tracker/slirp</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2551.data"
# $Id: $

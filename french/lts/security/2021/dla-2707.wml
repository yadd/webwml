#use wml::debian::translation-check translation="a3072c3b6f47f324dfd468f4e3feed1d30c1cf16" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème de sécurité a été découvert dans sogo.</p>

<p>SOGo ne valide pas la signature de n’importe quelle assertion SAML qu’il
reçoit. N’importe quel intervenant ayant accès au réseau pourrait usurper
l'identité d'un utilisateur lorsque SAML est la méthode d’authentification.</p>

<p>Pour Debian 9 « Stretch », ce problème a été corrigé dans
la version 3.2.6-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sogo.</p>

<p>ATTENTION ! Si vous utilisez l’authentification SAML, utilisez sogo-tool
pour supprimer immédiatement les sessions d’utilisateur et obliger tous les
utilisateurs à passer par la page de connexion :</p>

<p>sogo-tool -v expire-sessions 1</p>
<p>systemctl restart memcached</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sogo, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sogo">\
https://security-tracker.debian.org/tracker/sogo</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2707.data"
# $Id: $

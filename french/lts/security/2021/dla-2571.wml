#use wml::debian::translation-check translation="a8e04bbcb89a312a789fc2c81d84933c7c0ec37e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été découverts dans openvswitch, un commutateur
Ethernet virtuel à base logicielle, utilisable en production et multicouche.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35498">CVE-2020-35498</a>

<p>Des attaques par déni de service, dans lesquelles des paquets réseau
contrefaits pouvaient faire que la recherche de paquets ignore les champs
d’en-têtes réseau pour les couches 3 et 4. Les paquets réseau contrefaits étaient
des paquets IPv4 ou IPv6 ordinaires avec des remplissages Ethernet au-dessus
de 255 octets. Cela provoquait l'esquive de l’analyse des champs d’en-tête après
la couche 2 par les tests de validité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27827">CVE-2020-27827</a>

<p>Attaques par déni de service en utilisant des paquets LLDP contrefaits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17206">CVE-2018-17206</a>

<p>Problème de lecture excessive de tampon lors du décodage d’action BUNDLE.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17204">CVE-2018-17204</a>

<p>Échec d’assertion dû à une absence de validation d’information (groupe,
type et commande) dans le décodeur OF1.5.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9214">CVE-2017-9214</a>

<p>Lecture excessive de tampon provoquée par un dépassement d'entier non signé par
le bas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8011">CVE-2015-8011</a>

<p>Un dépassement de tampon dans la fonction lldp_decode dans
daemon/protocols/lldp.c dans lldpd avant la version 0.8.0 permet à des
attaquants distants de provoquer un déni de service (plantage du démon) et
éventuellement d’exécuter du code arbitraire à l’aide de vecteurs impliquant
une vaste gestion d’adresses et de limites TLV.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.6.10-0+deb9u1. Cette version est une nouvelle version de l’amont.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openvswitch.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openvswitch, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openvswitch">\
https://security-tracker.debian.org/tracker/openvswitch</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2571.data"
# $Id: $

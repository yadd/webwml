#use wml::debian::translation-check translation="e81af41ad0ae993c6a5df59a56f8ccf7a645a5f8" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un problème a été decouvert dans libzip, une bibliothèque pour lire,
créer et modifier des archives zip. Des archives ZIP contrefaites
pourraient permettre à des attaquants distants de provoquer un déni de
service dû à un échec d'allocation de mémoire par un mauvais traitement
d'enregistrements EOCD.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.1.2-1.1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libzip.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libzip, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libzip">\
https://security-tracker.debian.org/tracker/libzip</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2858.data"
# $Id: $

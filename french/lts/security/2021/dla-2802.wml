#use wml::debian::translation-check translation="c2089414f364c0f41baa7d17b5741fc58aaa784b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans elfutils, une collection
d’utilitaires pour gérer des objets ELF.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16062">CVE-2018-16062</a>

<p>dwarf_getaranges dans dwarf_getaranges.c dans libdw permettait un déni
de service (lecture hors limites de tampon de tas) à l'aide d'un fichier
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16402">CVE-2018-16402</a>

<p>libelf/elf_end.c permettait de provoquer un déni de service (double
libération de zone de mémoire et plantage d'application) parce qu'il
tentait deux fois de décompresser.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18310">CVE-2018-18310</a>

<p>Un déréférencement d'adresse mémoire non valable dans libdwfl permettait
un déni de service (plantage de l'application) au moyen d'un fichier
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18520">CVE-2018-18520</a>

<p>Une utilisation de mémoire après libération dans des fichiers ELF ar
récursifs permettait un déni de service (plantage d'application) à l'aide
d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18521">CVE-2018-18521</a>

<p>Une division par zéro dans arlib_add_symbols() permettait un déni de
service (plantage d'application) à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7150">CVE-2019-7150</a>

<p>Une erreur de segmentation pourrait se produire parce que
dwfl_segment_report_module() ne vérifiait pas si les données dynamiques
issues d'un fichier core étaient tronquées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7665">CVE-2019-7665</a>

<p>Les notes du fichier core de type NT_PLATFORM devaient contenir une
chaîne terminée par zéro ce qui permettait un déni de service (plantage
d'application) à l'aide d'un fichier contrefait.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 0.168-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets elfutils.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de elfutils, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/elfutils">\
https://security-tracker.debian.org/tracker/elfutils</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2802.data"
# $Id: $

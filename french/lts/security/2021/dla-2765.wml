#use wml::debian::translation-check translation="b8171411925029743d299ff55f71734e505755a2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans mupdf.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10246">CVE-2016-10246</a>

<p>Un dépassement de tampon dans la fonction main dans jstest_main.c permet à
des attaquants distants de provoquer un déni de service (écriture hors limites)
à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10247">CVE-2016-10247</a>

<p>Un dépassement de tampon dans la fonction my_getline dans jstest_main.c
permet à des attaquants distants de provoquer un déni de service (écriture hors
limites) à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6060">CVE-2017-6060</a>

<p>Un dépassement de pile dans jstest_main.c permet à des attaquants distants
d’avoir un impact non précisé à l'aide d'une image contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10289">CVE-2018-10289</a>

<p>Boucle infinie dans la fonction fz_skip_space du fichier pdf/pdf-xref.c.
Un opposant distant peut exploiter cette vulnérabilité pour provoquer un
déni de service à l'aide d'un fichier pdf contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000036">CVE-2018-1000036</a>

<p>Plusieurs fuites de mémoire dans l'analyseur PDF peuvent permettre à un
attaquant de provoquer un déni de service (fuite de mémoire) à l'aide d'un
fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-19609">CVE-2020-19609</a>

<p>Écrasement de tampon basé sur le tas dans la fonction tiff_expand_colormap()
lors de l’analyse des fichiers TIFF permettant à des attaquants de provoquer un
déni de service.</p>


<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 1.14.0+ds1-4+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mupdf.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mupdf,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mupdf">\
https://security-tracker.debian.org/tracker/mupdf</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2765.data"
# $Id: $

#use wml::debian::translation-check translation="e15d3a8516a2aeccf0b1878192963393000f7e32" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été corrigées dans unbound, un
résolveur DNS mettant en cache, récursif et validant. La prise en charge du serveur DNS
Unbound a été reprise, les sources peuvent être trouvées dans le paquet source
unbound-1.9</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12662">CVE-2020-12662</a>

<p>Unbound contrôlait insuffisamment le volume de messages réseau, c'est-à-dire
une vulnérabilité <q>NXNSAttack</q>. Cela est provoqué par des sous-domaines
aléatoires dans le NSDNAME dans les enregistrements NS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12663">CVE-2020-12663</a>

<p>Unbound comportait une boucle infinie à l'aide de réponses DNS mal formées
reçues de serveurs amont.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28935">CVE-2020-28935</a>

<p>Unbound comportait une vulnérabilité locale qui pouvait permettre une
attaque locale par lien symbolique. Lors de l’écriture d’un fichier PID, unbound
créait le fichier s’il n’existait pas ou ouvrait un fichier existant. Si le
fichier existait déjà, il suivait les liens symboliques s’il advenait que le
fichier était un lien symbolique plutôt qu’un fichier normal.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.9.0-2+deb10u2~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets unbound1.9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de unbound1.9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/unbound1.9">\
https://security-tracker.debian.org/tracker/unbound1.9</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2556.data"
# $Id: $

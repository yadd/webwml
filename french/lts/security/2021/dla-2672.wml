#use wml::debian::translation-check translation="e73863d11c9b3ddd3da22573112a1fb94ed89a85" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans imagemagick.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27751">CVE-2020-27751</a>

<p>Un défaut a été découvert dans MagickCore/quantum-export.c. Un attaquant
soumettant un fichier contrefait, traité par ImageMagick, pourrait déclencher
un comportement non défini sous la forme de valeurs en dehors de l’intervalle de
type <q>unsigned long long</q> ainsi qu’un exposant de décalage qui soit trop
grand pour le type 64 bits. Cela est susceptible d’avoir un impact sur la
disponibilité de l’application, mais pourrait éventuellement causer d’autres
problèmes relatifs à un comportement indéfini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20243">CVE-2021-20243</a>

<p>Un défaut a été découvert dans MagickCore/resize.c. Un attaquant soumettant
un fichier contrefait, traité par ImageMagick, pourrait déclencher un
comportement indéfini sous la forme d’une division mathématique par zéro.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20245">CVE-2021-20245</a>

<p>Un défaut a été découvert dans coders/webp.c. Un attaquant soumettant
un fichier contrefait, traité par ImageMagick, pourrait déclencher un
comportement indéfini sous la forme d’une division mathématique par zéro.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20309">CVE-2021-20309</a>

<p>Une division par zéro dans WaveImage() de MagickCore/visual-effects.c
pourrait déclencher un comportement indéfini à l'aide d'un fichier image
contrefait soumis à une application utilisant ImageMagick.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20312">CVE-2021-20312</a>

<p>Un dépassement d'entier dans WriteTHUMBNAILImage de coders/thumbnail.c
pourrait déclencher un comportement indéfini à l'aide d'un fichier image
contrefait soumis par un attaquant et traité par une application utilisant
ImageMagick.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20313">CVE-2021-20313</a>

<p>Fuite potentielle de chiffrement lorsque le calcul des signatures est
possible dans TransformSignature.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 8:6.9.7.4+dfsg-11+deb9u13.</p>

<p>Nous vous recommandons de mettre à jour vos paquets imagemagick.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de imagemagick, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/imagemagick">\
https://security-tracker.debian.org/tracker/imagemagick</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2672.data"
# $Id: $

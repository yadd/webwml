#use wml::debian::translation-check translation="23b8451b261b3af732fb953e8819105a1a0765d0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il existait une vulnérabilité par lecture excessive de tampon basé sur le tas
dans expat, une bibliothèque d’analyse d’XML.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15903">CVE-2019-15903</a>

<p>Dans libexpat avant 2.2.8, une entrée XML contrefaite pourrait tromper
l’analyseur en changeant l’analyse du DTD en analyse du document de manière trop
hâtive. Un appel consécutif à <tt>XML_GetCurrentLineNumber</tt> (ou
<tt>XML_GetCurrentColumnNumber</tt>) aboutirait alors à une lecture hors limites
de tampon basé sur le tas.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.1.0-6+deb8u6.</p>
<p>Nous vous recommandons de mettre à jour vos paquets expat.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1912.data"
# $Id: $

#use wml::debian::translation-check translation="c9149cd8797347070b76c08c80d7859e09025c0c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans le navigateur web
Firefox de Mozilla, qui pouvaient éventuellement aboutir à l’exécution de code
arbitraire.</p>

<p>Debian suit la prise en charge à long terme des publications (ESR) de
Firefox. La prise en charge de la série 102.x a cessé, aussi, depuis cette
mise à jour, les publications 115.x sont suivies.</p>

<p>Entre les versions 102.x et 115.x, Firefox a incorporé un certain nombre de
mises à jour de fonctionnalités. Pour plus d’informations, veuillez consulter
la page
<a href="https://www.mozilla.org/en-US/firefox/115.0esr/releasenotes/">https://www.mozilla.org/en-US/firefox/115.0esr/releasenotes/</a>.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 115.3.0esr-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firefox-esr.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de firefox-esr,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/firefox-esr">\
https://security-tracker.debian.org/tracker/firefox-esr</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3587.data"
# $Id: $

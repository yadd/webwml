#use wml::debian::translation-check translation="9e57d228bd3c1a27be2e4764e858fd5a1b1f1cb7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une autre régression a été découverte dans Netatalk, le service « Apple
Filing Protocol », introduite dans le correctif
<a href="https://security-tracker.debian.org/tracker/CVE-2022-23123">CVE-2022-23123</a>.
Elle impactait un sous-ensemble d’utilisateurs ayant certaines métadonnées dans
leurs fichiers partagés. Le problème menait à un plantage inévitable et rendait
netatalk inutilisable avec les volumes partagés.</p>

<p>Indépendamment, un correctif est fourni pour sauvegarder des fichiers MS
Office dans tout autre volume partagé.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 3.1.12~ds-3+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets netatalk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de netatalk,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/netatalk">\
https://security-tracker.debian.org/tracker/netatalk</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3426-3.data"
# $Id: $

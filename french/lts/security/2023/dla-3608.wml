#use wml::debian::translation-check translation="8afbe3a00a5793e3d8438b46c790fa3ca5eb8340" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que la mise à jour de freerdp2 (voir la DLA-3606-1) créait
un bogue dans vinagre, qui casse les connexions RDP avec comme symptômes des
écrans bloqués ou noirs.
<p><b>Remarque</b> : sha256 est désormais utilisé au lieu de sha1 pour les
certificats des empreintes numériques. Cela invalide tous les hôtes dans le
fichier known_hosts2 de FreeRDP, $HOME/.config/freerdp/known_hosts2.
En cas de problèmes avec la connexion, essayer de supprimer ce fichier.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 3.22.0-6+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets vinagre.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de vinagre,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/vinagre">\
https://security-tracker.debian.org/tracker/vinagre</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3608.data"
# $Id: $

#use wml::debian::translation-check translation="395e4b9f734bf0c5e37a6b9e828a96e30b36bab4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans sssd, un ensemble de démons
pour gérer l’accès à des répertoires distants et de mécanismes
d’authentification, qui pouvaient conduire à une élévation des privilèges.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16838">CVE-2018-16838</a>

<p>Il a été découvert que quand GPO (Group Policy Objects) ne sont pas lisibles
par SSSD à cause de permissions trop strictes du côté serveur, SSSD permettait
à tous les utilisateurs authentifiés de se connecter au lieu de refuser
l’accès.</p>

<p>Un nouveau réglage booléen <code>ad_gpo_ignore_unreadable</code> (par défaut
à <code>False</code>) est introduit pour les environnements où les attributs
dans le <code>groupPolicyContainer</code> ne sont pas lisibles et le changement
des permissions des objets GPO n’est pas possible ou désirable. Consulter
<code>sssd-ad(5)</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3811">CVE-2019-3811</a>

<p>Il a été découvert que si un utilisateur était configuré sans répertoire
home, alors <code>sssd(8)</code> renvoies <code>/</code> (c’est-à-dire, le
répertoire racine) au lieu de la chaine vide (signifiant pas de répertoire home).
Cela pouvait impacter des services qui restreignaient l’accès au système de
fichiers de l’utilisateur qu’à partir de leur répertoire à travers un
<code>chroot()</code> ou de manière similaire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3621">CVE-2021-3621</a>

<p>Il a été découvert que la commande <code>sssctl(8)</code> était vulnérable
à une injection de commande d’interpréteur à l’aide des sous-commandes
<code>logs-fetch</code> et <code>cache-expire</code>.</p>

<p>Ce défaut pouvait permettait à un attaquant d’amener l’utilisateur root à
exécuter une commande <code>sssctl(8)</code> contrefaite pour l'occasion,
par exemple à l’aide sudo, dans le but d’obtenir les droits du
superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4254">CVE-2022-4254</a>

<p>Il a été découvert que <code>libsss_certmap</code> échouait à nettoyer les
données de certificat dans les filtres LDAP.</p>

<p>PKINIT autorise un client de s’authentifier auprès de KDC en utilisant un
certificat X.509 et la clé privée correspondante, plutôt qu’avec une phrase de
passe ou un fichier keytab. Les règles de mappage sont utilisées pour faire
correspondre le certificat présenté durant une requête d’authentification
PKINIT au certificat principal correspondant. Cependant, il a été découvert que
le filtre de mappage était vulnérable à une injection de filtre LDAP. Comme le
résultat des recherches est influencé par les valeurs dans le certificat pouvant
être contrôlé par un attaquant, ce défaut pouvait permettre à celui-ci de
contrôler le compte de l’administrateur, conduisant à une prise de contrôle
complète du domaine.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.16.3-3.2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sssd.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sssd,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sssd">\
https://security-tracker.debian.org/tracker/sssd</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3436.data"
# $Id: $

#use wml::debian::translation-check translation="bfea3cb2ed255efbcf5efa370b4cc579490a224e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité potentielle d’injection
LDAP dans Bouncy Castle, une bibliothèque de chiffrement pour Java. Durant le
processus de validation de certificat, bouncycastle utilisait <q>Subject name</q>
de certificat dans un filtre de recherche LDAP sans aucun échappement.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-33201">CVE-2023-33201</a>

<p>Bouncy Castle pour Java avant la version 1.74 était sujet à une vulnérabilité
d’injection LDAP. Cette vulnérabilité affectait seulement les applications qui
utilisaient CertStore de LDAP à partir de Bouncy Castle pour valider les
certificats X.509. Durant le processus de validation du certificat, Bouncy
Castle insérait le <q>Subject Name</q> de certificat dans un filtre de recherche
LDAP sans aucun échappement, conduisant à une vulnérabilité d’injection LDAP.</p></li>
</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.60-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bouncycastle.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3514.data"
# $Id: $

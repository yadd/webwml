#use wml::debian::translation-check translation="2074507438f8d3a39ad2f8b50d8f85001981f664" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été découverte dans phpCAS, une bibliothèque cliente de
système d’authentification (CAS) en PHP, qui pouvait permettre à un attaquant
d’obtenir un accès à un compte de victime sur un service CASifié sans que
celle-ci en ait connaissance et visitait un site web de l’attaquant tout en
étant connectée au même serveur CAS.</p>

<p>Le correctif pour cette vulnérabilité nécessite un changement important de
l’API dans php-cas et la mise à niveau des logiciels utilisant cette
bibliothèque.</p>

<p>Pour Buster, tous les paquets dans les dépôts de Debian qui utilisent php-cas
ont été mis à niveau, cependant une configuration manuelle supplémentaire est
attendue car php-cas a besoin d’une information de site supplémentaire, l’URL de
base du service, pour fonctionner. Les DLA pour leurs paquets respectifs auront
une information supplémentaire ainsi que les fichiers NEWS des paquets.</p>

<p>Pour les logiciels de tierces parties utilisant php-cas, veuillez noter que
l’amont a fourni les instructions suivantes pour mettre à niveau ce logiciel [1].</p>

<p>phpCAS a désormais besoin d’un argument d’URL de base de service
supplémentaire lors de la construction de la classe cliente. Il accepte
n’importe quel argument de :</p>

<p>1. Une chaine d’URL de base de service. La découverte d’URL de service
utilisera toujours ce nom de serveur (protocole, nom d’hôte et numéro de port)
sans utiliser aucun nom d’hôte de service externe.
2. Un tableau de chaines d’URL de base de service. La découverte d’URL de
service vérifiera en se servant de cette liste avant d’utiliser l’URL de base
auto-découvert. Sans correspondance, le premier URL dans le tableau sera utilisé
par défaut. Cette option est utile si votre site web PHP est accessible depuis
plusieurs domaines sans nom canonique ou par HTTP et HTTPS.
3. Une classe mettant en œuvre CAS_ServiceBaseUrl_Interface. Si vous avez
besoin de personnaliser le comportement de la découverte d’URL de base, vous
pouvez utiliser une classe qui met en œuvre l’interface.</p>

<p>La construction de la classe cliente est habituellement faite avec
phpCAS::client().</p>

<p>Par exemple, en utilisant la première possibilité :
phpCAS::client(CAS_VERSION_2_0, $cas_host, $cas_port, $cas_context);
peut devenir :
phpCAS::client(CAS_VERSION_2_0, $cas_host, $cas_port, $cas_context, "https://casified-service.example.org:8080");</p>


<p>Détails de la vue :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39369">CVE-2022-39369</a>

<p>La bibliothèque phpCAS utilise les en-têtes HTTP pour déterminer l’URL de
service utilisé pour valider les tickets. Cela permettait à un attaquant de
contrôler l’en-tête de l’hôte et utiliser un ticket valable accordé pour
n’importe quel service autorisé dans le même domaine SSO (serveur CAS) pour
authentifier le service protégé par phpCAS. Selon les réglages du registre du
serveur CAS de service, dans le pire des cas, cela peut être n’importe quel autre
URL de service (si les URL autorisés sont configurés à <q>^(https)://.*</q>) ou
cela peut être strictement limité aux services connus ou autorisés dans la même
fédération SSO si une validation de service d’URL est appliquée.</p>

<p>[1] <a href="https://github.com/apereo/phpCAS/blob/f3db27efd1f5020e71f2116f637a25cc9dbda1e3/docs/Upgrading#L1C1-L1C1">https://github.com/apereo/phpCAS/blob/f3db27efd1f5020e71f2116f637a25cc9dbda1e3/docs/Upgrading#L1C1-L1C1</a>.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.3.6-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php-cas.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de php-cas,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/php-cas">\
https://security-tracker.debian.org/tracker/php-cas</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3485.data"
# $Id: $

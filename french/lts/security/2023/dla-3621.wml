#use wml::debian::translation-check translation="528b8f95d928998f32ec0453ade47525504a1d01" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans nghttp2, une implémentation
du protocole HTTP/2.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11080">CVE-2020-11080</a>

<p>Un déni de service pouvait être causé par une grande charge HTTP/2 de trame
SETTINGS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-44487">CVE-2023-44487</a>

<p>Un déni de service pouvait être causé en réinitialisant de nombreux flux
HTTP/2 rapidement. Cela a été largement observé depuis août.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.36.0-2+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nghttp2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de nghttp2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/nghttp2">\
https://security-tracker.debian.org/tracker/nghttp2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3621.data"
# $Id: $

#use wml::debian::translation-check translation="9b750ca0266c0e14a5c5115854a1464c28b6c328" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une attaque potentielle par déni de service
(DoS) dans le serveur SIP de téléphonie Kamailio. Cela était provoqué parce que
le serveur Kamailio gérait incorrectement les requêtes <code>INVITE</code> avec
des champs dupliqués.</p>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27507">CVE-2020-27507</a>

<p>Le serveur SIP Kamailio avant la version 5.5.0 gérait incorrectement les
requêtes INVITE avec des champs dupliqués et des paramètres tag excessivement
longs, conduisant à un dépassement de tampon qui plantait le serveur ou,
éventuellement, avaient un impact non précisé.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 5.2.1-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets kamailio.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3438.data"
# $Id: $

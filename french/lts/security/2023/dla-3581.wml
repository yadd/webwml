#use wml::debian::translation-check translation="f48fb716560df61a769fdd23846c5652c4721d69" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de dépassement de tampon a été découverte dans FLAC, un
codec audio sans pertes et libre, dans la fonction bitwriter_grow_. Ce défaut
pouvait permettre à des attaquants distants d’exécuter du code arbitraire à
l’aide d’une entrée contrefaite pour l'occasion dans l’encodeur.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.3.2-3+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets flac.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de flac,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/flac">\
https://security-tracker.debian.org/tracker/flac</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3581.data"
# $Id: $

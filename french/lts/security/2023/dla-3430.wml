#use wml::debian::translation-check translation="c7eaa1151c7252648b73f0dae1c57ee9b0c2151a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’une absence de nettoyage des entrées dans cups-filters,
lors de l’utilisation du dorsal <q>Backend Error Handler</q> (beh) pour créer
une imprimante réseau accessible, pouvait aboutir à l’exécution de commandes
arbitraires.</p>


<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.21.6-5+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cups-filters.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cups-filters,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cups-filters">\
https://security-tracker.debian.org/tracker/cups-filters</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3430.data"
# $Id: $

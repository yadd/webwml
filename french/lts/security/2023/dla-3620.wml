#use wml::debian::translation-check translation="ab843eb96518b31338b8b80cc7ae39f7295f1cb3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans poppler, une bibliothèque de
rendu de PDF.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-23804">CVE-2020-23804</a>

<p>Dépassement de pile dans XRef::readXRefTable().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-37050">CVE-2022-37050</a>

<p>Plantage dans PDFDoc::savePageAs().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-37051">CVE-2022-37051</a>

<p>Plantage dans l’outil pdfunite.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.71.0-5+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets poppler.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de poppler,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/poppler">\
https://security-tracker.debian.org/tracker/poppler</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3620.data"
# $Id: $

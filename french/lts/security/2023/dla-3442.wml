#use wml::debian::translation-check translation="c6295eb2f31a64c181c7688461a1a2b04f16cab6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Alvaro Muñoz du <q>GitHub Security Lab</q> a découvert seize façons
d’exploiter une vulnérabilité de script intersite dans nbconvert, un outil et
une bibliothèque utilisés pour convertir des <q>notebooks</q> dans divers autres
formats à l’aide de patrons Jinja.</p>

<p>Lors de l’utilisation de nbconvert pour créer une version HTML notebook
contrôlable par l’utilisateur, il était possible d’injecter du HTML arbitraire
qui pouvait conduire à une vulnérabilité de script intersite (XSS) si ces
notebooks HTML étaient servis par un serveur web sans
<code>Content-Security-Policy</code> serrée (par exemple, nbviewer).</p>

<ol>
<li> GHSL-2021-1013: XSS dans <code>notebook.metadata.language_info.pygments_lexer</code> ;</li>
<li> GHSL-2021-1014: XSS dans <code>notebook.metadata.title</code> ;</li>
<li> GHSL-2021-1015: XSS dans <code>notebook.metadata.widgets</code> ;</li>
<li> GHSL-2021-1016: XSS dans <code>notebook.cell.metadata.tags</code> ;</li>
<li> GHSL-2021-1017: XSS dans les cellules de données de sortie <code>text/html</code> ;</li>
<li> GHSL-2021-1018: XSS dans les cellules de données de sortie <code>image/svg+xml</code> ;</li>
<li> GHSL-2021-1019: XSS dans <code>notebook.cell.output.svg_filename</code> ;</li>
<li> GHSL-2021-1020: XSS dans les cellules de données de sortie <code>text/markdown</code> ;</li>
<li> GHSL-2021-1021: XSS dans les cellules de données de sortie <code>application/javascript</code> ;</li>
<li> GHSL-2021-1022: XSS dans output.metadata.filenames <code>image/png</code> et <code>image/jpeg</code> ;</li>
<li> GHSL-2021-1023: XSS dans les cellules de données de sortie <code>image/png</code> et <code>image/jpeg</code> ;</li>
<li> GHSL-2021-1024: XSS dans output.metadata.width/height <code>image/png</code> et <code>image/jpeg</code> ;</li>
<li> GHSL-2021-1025: XSS dans les cellules de données de sortie <code>application/vnd.jupyter.widget-state+json</code> ;</li>
<li> GHSL-2021-1026: XSS dans les cellules de données de sortie <code>application/vnd.jupyter.widget-view+json</code> ;</li>
<li> GHSL-2021-1027: XSS dans les cellules raw ;</li>
<li> GHSL-2021-1028: XSS dans les cellules markdown.</li>
</ol>

<p>Certaines de ces vulnérabilités, précisement GHSL-2021-1017, -1020, -1021 et -1028,
sont réellement des choix de conception où les cellules <code>text/html</code>,
<code>text/markdown</code>, <code>application/javascript</code> et markdown
pouvaient permettre une exécution de code arbitraire JavaScript. Ces
vulnérabilités sont par conséquent laissées ouvertes par défaut, mais les
utilisateurs peuvent désormais exclure et enlever tous les éléments JavaScript
à l'aide d'une nouvelle option <code>HTMLExporter</code> <code>sanitize_html</code>.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 5.4-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nbconvert.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de nbconvert,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/nbconvert">\
https://security-tracker.debian.org/tracker/nbconvert</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3442.data"
# $Id: $

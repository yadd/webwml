#use wml::debian::translation-check translation="0cefe58a3181a750261466da85fb42fb7466fafa" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans yajl, un analyseur JSON et
un petit générateur de validation JSON, écrit en ANSI C, qui éventuellement
pouvaient provoquer une corruption de mémoire ou déni de service.</p>

<p>Le <a href="https://security-tracker.debian.org/tracker/CVE-20117-16516">CVE-20117-16516</a>
a été déjà corrigé dans la DLA-3478, mais le correctif a été découvert incomplet
car il ne corrigeait pas une fuite de mémoire. Cette mise à jour corrige ce
problème.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16516">CVE-2017-16516</a>

<p>Quand un fichier contrefait JSON était fourni à yajl, le processus pouvait
planter avec SIGABRT dans la fonction yajl_string_decode dans yajl_encode.c.
Cela pouvait éventuellement aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24795">CVE-2022-24795</a>

<p>Les branches 1.x et 2.x de <q>yajl</q> contenaient un dépassement d'entier
qui conduisait à une corruption de mémoire de tas lors du traitement d’entrées
importantes (~2Go).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-33460">CVE-2023-33460</a>

<p>Une fuite de mémoire existait dans yajl 2.1.0 lors de l’utilisation de la
fonction yajl_tree_parse, qui, éventuellement, causait un épuisement de mémoire
dans le serveur et un plantage.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.1.0-3+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets yajl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de yajl,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/yajl">\
https://security-tracker.debian.org/tracker/yajl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3492.data"
# $Id: $

#use wml::debian::translation-check translation="af90a4117386efca897b68f2f27fc35fd6f8c940" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>SoX est un utilitaire en ligne de commande qui peut convertir divers formats
de fichier audio dans d’autres formats. Il peut aussi appliquer divers effets
à ces fichiers audio lors de la conversion.</p>

<p>Sox était vulnérable à une division par zéro en lisant un fichier
Creative Voice (.voc) spécialement contrefait dans la fonction
read_samples. Ce défaut pouvait conduire à un déni de service.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 14.4.2+git20190427-1+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sox.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sox,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sox">\
https://security-tracker.debian.org/tracker/sox</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3527.data"
# $Id: $

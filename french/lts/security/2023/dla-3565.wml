#use wml::debian::translation-check translation="d8d84357521b5fe1123191662fea60ef1d2072a7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Loofah, une bibliothèque
de Ruby pour la transformation et le nettoyage d’HTML/XML. Un attaquant pouvait
lancer des attaques par script intersite (XSS) et de déni de service (DoS)
à l’aide de documents HTML/XML contrefaits.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23514">CVE-2022-23514</a>

<p>Une expression rationnelle inefficace était susceptible d’un retour sur trace
excessif lors du nettoyage de certains attributs SVG. Cela pouvait conduire à
un déni de service à travers une consommation de ressources du CPU.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23515">CVE-2022-23515</a>

<p>Script intersite à l’aide du type de média image/svg+xml dans des URI de
données.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23516">CVE-2022-23516</a>

<p>Loofah utilisait la récursion pour le nettoyage de sections CDATA, le rendant
susceptible à un épuisement de pile et la levée d’une exception
SystemStackError. Cela pouvait conduire à un déni de service à travers une
consommation de ressources du CPU.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.2.3-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-loofah.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-loofah,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-loofah">\
https://security-tracker.debian.org/tracker/ruby-loofah</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3565.data"
# $Id: $

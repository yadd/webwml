#use wml::debian::translation-check translation="b08f698a6c38cbcd8a40339e924e351ae63b02fe" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que syslog-ng, un démon de journalisation du système,
avait des problèmes de dépassement d'entier et de tampon hors limites.
Cela pouvait permettre à un attaquant distant de provoquer un déni de service à
l’aide d’entrées syslog contrefaites.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 3.19.1-5+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets syslog-ng.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de syslog-ng,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/syslog-ng">\
https://security-tracker.debian.org/tracker/syslog-ng</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3348.data"
# $Id: $

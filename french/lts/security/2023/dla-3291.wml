#use wml::debian::translation-check translation="cb488de11d927d25ddf68f457a3c55a12a2f38be" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que node-object-path, un module de Node.js pour accéder
aux propriétés enfouies d’objet en utilisant des chemins séparés par des points,
était vulnérable à une pollution de prototype.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3805">CVE-2021-3805</a>

<p>Vulnérabilité de pollution de prototype dans les fonctions <code>del()</code>,
<code>empty()</code>, <code>push()</code> et <code>insert()</code> lors de
l’utilisation du mode <q>inherited props</q> (par exemple, quand une nouvelle
instance <code>object-path</code> est créée avec l’option
<code>includeInheritedProps</code> réglée à <code>true</code> ou lors de
l’utilisation de l’instance par défaut <code>withInheritedProps</code>).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23434">CVE-2021-23434</a>

<p>Une vulnérabilité de confusion de type pouvait conduire à un contournement
du correctif
<a href="https://security-tracker.debian.org/tracker/CVE-2020-15256">CVE-2020-15256</a> \
quand les composants path utilisés dans le paramètre path sont des tableaux, car
l’opérateur <code>===</code> renvoie toujours <code>false</code> quand le type
d’opérandes est différent.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.11.4-2+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-object-path.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de node-object-path,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/node-object-path">\
https://security-tracker.debian.org/tracker/node-object-path</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3291.data"
# $Id: $

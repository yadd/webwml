#use wml::debian::translation-check translation="885662a683533b54571acd4c5c22ff98d278563a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs failles ont été trouvées dans libyang, une boîte à outils d’analyse
pour le langage de modélisation YANG d’IETF. Des doubles libérations de zone de
mémoire, des accès non autorisés en mémoire et des déréférencements de pointeur
NULL pouvaient provoquer un déni de service ou éventuellement une exécution de
code.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.16.105+really1.0-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libyang.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libyang,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libyang">\
https://security-tracker.debian.org/tracker/libyang</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3572.data"
# $Id: $

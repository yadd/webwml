#use wml::debian::translation-check translation="7059017d8f81e708177eac24fd373c30b0bd5ef8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web WebKitGTK.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42826">CVE-2022-42826</a>

<p>Francisco Alonso a découvert que le traitement de contenu web malveillant
pouvait conduire à l’exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23517">CVE-2023-23517</a>

<p>YeongHyeon Choi, Hyeon Park, SeOk JEON, YoungSung Ahn, JunSeo Bae
et Dohyun Lee ont découvert que le traitement de contenu web malveillant
pouvait conduire à l’exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23518">CVE-2023-23518</a>

<p>YeongHyeon Choi, Hyeon Park, SeOk JEON, YoungSung Ahn, JunSeo Bae
and Dohyun Lee ont découvert que le traitement de contenu web malveillant
pouvait conduire à l’exécution de code arbitraire.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.38.4-2~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3308.data"
# $Id: $

#use wml::debian::translation-check translation="60906ad69fd5485a27692477f6c222ec435707a6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans exempi, une implémentation
d’XMP (Extensible Metadata Platform).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-18651">CVE-2020-18651</a>

<p>Une vulnérabilité de dépassement de tampon a été découverte dans la fonction
ID3_Support::ID3v2Frame::getFrameValue qui permettait à des attaquants distants
de provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-18652">CVE-2020-18652</a>

<p>Une vulnérabilité de dépassement de tampon a été découverte dans
WEBP_Support.cpp qui permettait à des attaquants distants de provoquer un déni
de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36045">CVE-2021-36045</a>

<p>Une vulnérabilité de lecture hors limites a été découverte qui pouvait
conduire à une divulgation arbitraire de mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36046">CVE-2021-36046</a>

<p>Une vulnérabilité de corruption de mémoire a été découverte qui
pouvait aboutir potentiellement à l’exécution de code arbitraire dans le
contexte de l’utilisateur actuel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36047">CVE-2021-36047</a>

<p>Une vulnérabilité de validation incorrecte d’entrée a été découverte qui
pouvait aboutir potentiellement à l’exécution de code arbitraire dans le
contexte de l’utilisateur actuel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36048">CVE-2021-36048</a>

<p>Une validation incorrecte d’entrée a été découverte qui
pouvait aboutir potentiellement à l’exécution de code arbitraire dans le
contexte de l’utilisateur actuel.</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36050">CVE-2021-36050</a>

<p>Une vulnérabilité de dépassement de tampon a été découverte qui pouvait
aboutir potentiellement à l’exécution de code arbitraire dans le
contexte de l’utilisateur actuel.</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36051">CVE-2021-36051</a>

<p>Une vulnérabilité de dépassement de tampon a été découverte qui pouvait
aboutir potentiellement à l’exécution de code arbitraire dans le
contexte de l’utilisateur actuel.</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36052">CVE-2021-36052</a>

<p>Une vulnérabilité de corruption de mémoire a été découverte qui pouvait
aboutir potentiellement à l’exécution de code arbitraire dans le
contexte de l’utilisateur actuel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36053">CVE-2021-36053</a>

<p>Une vulnérabilité de lecture hors limites a été découverte qui pouvait
conduire à une divulgation arbitraire de mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36054">CVE-2021-36054</a>

<p>une vulnérabilité de dépassement de tampon a été découverte qui pouvait
éventuellement aboutir à un déni de service local d’application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36055">CVE-2021-36055</a>

<p>Une vulnérabilité d’utilisation de mémoire après libération a été découverte
qui pouvait aboutir à l’exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36056">CVE-2021-36056</a>

<p>Une vulnérabilité de dépassement de tampon a été découverte qui pouvait
aboutir potentiellement à l’exécution de code arbitraire dans le
contexte de l’utilisateur actuel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36057">CVE-2021-36057</a>

<p>Une vulnérabilité de condition <q>write-what-where</q> a été découverte,
apparaissant lors du processus d’allocation de mémoire pour l’application.
Cela pouvait aboutir à la non concordance des fonctions de gestion de mémoire,
conduisant à un déni de service d’application locale dans le contexte de
l’utilisateur actuel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36058">CVE-2021-36058</a>

<p>Une vulnérabilité de dépassement d’entier a été découverte qui pouvait
éventuellement aboutir à un déni de service au niveau de l’application dans le
contexte de l’utilisateur actuel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36064">CVE-2021-36064</a>

<p>Une vulnérabilité de dépassement de capacité de tampon par le bas a été
découverte qui pouvait aboutir potentiellement à l’exécution de code arbitraire
dans le contexte de l’utilisateur actuel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39847">CVE-2021-39847</a>

<p>Une vulnérabilité de dépassement de pile a été découverte qui pouvait
aboutir potentiellement à l’exécution de code arbitraire dans le contexte de
l’utilisateur actuel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40716">CVE-2021-40716</a>

<p>Une vulnérabilité de lecture hors limites a été découverte qui pouvait
conduire à la divulgation de mémoire sensible.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40732">CVE-2021-40732</a>

<p>Une vulnérabilité de déréférencement de pointeur NULL a été découverte
qui pouvait aboutir à une fuite de données à partir de certains emplacements
de mémoire et un déni de service local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42528">CVE-2021-42528</a>

<p>Une vulnérabilité de déréférencement de pointeur NULL a été découverte
lors de l’analyse d’un fichier contrefait pour l'occasion. Un attaquant
non authentifié pouvait exploiter cette vulnérabilité pour réaliser un déni de
service dans le contexte de l’utilisateur actuel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42529">CVE-2021-42529</a>

<p>Une vulnérabilité de dépassement de pile a été découverte qui pouvait
aboutir potentiellement à l’exécution de code arbitraire dans le
contexte de l’utilisateur actuel.</p></li>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42530">CVE-2021-42530</a>

<p>Une vulnérabilité de dépassement de pile a été découverte qui pouvait
aboutir potentiellement à l’exécution de code arbitraire dans le
contexte de l’utilisateur actuel.</p></li>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42531">CVE-2021-42531</a>

<p>Une vulnérabilité de dépassement de pile a été découverte qui pouvait
aboutir potentiellement à l’exécution de code arbitraire dans le
contexte de l’utilisateur actuel.</p></li>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42532">CVE-2021-42532</a>

<p>Une vulnérabilité de dépassement de pile a été découverte qui pouvait
aboutir potentiellement à l’exécution de code arbitraire dans le
contexte de l’utilisateur actuel.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.5.0-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets exempi.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de exempi,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/exempi">\
https://security-tracker.debian.org/tracker/exempi</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3585.data"
# $Id: $

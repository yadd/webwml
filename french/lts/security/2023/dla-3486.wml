#use wml::debian::translation-check translation="2074507438f8d3a39ad2f8b50d8f85001981f664" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le paquet source ocsinventory-server a été mis à niveau pour corriger la
modification d’API dans php-cas à cause du
<a href="https://security-tracker.debian.org/tracker/CVE-2022-39369">CVE-2022-39369</a>, consulter DLA 3485-1 pour plus de détails.</p>

<p>CAS est un mécanisme d’authentification facultatif dans le paquet binaire
ocsinventory-reports, et si utilisé, ocsinventory-reports ne fonctionnera plus
s’il n’est pas reconfiguré.</p>

<p>Il nécessite que l’URL de base du service à authentifier soit configuré.</p>

<p>Pour ocsinventory-reports, cela est fait avec la variable
$cas_service_base_url dans le fichier
the /usr/share/ocsinventory-reports/backend/require/cas.config.php</p>

<p>Attention : malgré cette mise à niveau, ocsreports-server ne devrait être
utilisé que dans des environnement fiables et sécurisés.</p>


<p>Pour Debian 10 <q>Buster</q>, cette mise à jour est disponible jusqu’à
la version 2.5+dfsg1-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ocsinventory-server.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ocsinventory-server,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ocsinventory-server">\
https://security-tracker.debian.org/tracker/ocsinventory-server</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3486.data"
# $Id: $

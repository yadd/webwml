#use wml::debian::translation-check translation="44e410e4968d702ea87dc692f4bd6676419b75f8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilité de sécurité ont été trouvées dans Jetty, un serveur
web basé sur Java et un moteur de servlets.</p>

<p>La classe org.eclipse.jetty.servlets.CGI est devenue obsolète. Son
utilisation n’est plus sûre. Les développeurs amont de Jetty recommandent
d’utiliser Fast CGI à la place. Voir aussi
<a href="https://security-tracker.debian.org/tracker/CVE-2023-36479">CVE-2023-36479</a>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-26048">CVE-2023-26048</a>

<p>Dans les versions affectées, les servlets avec prise en charge multipart
(par exemple, annotés <q>@MultipartConfig</q>) qui appelaient
<q>HttpServletRequest.getParameter()</q> ou <q>HttpServletRequest.getParts()</q>
pouvaient provoquer un <q>OutOfMemoryError</q> quand le client envoyait une
requête multipart avec une partie ayant un nom, mais pas de nom de fichier
et un contenu très important. Cela arrivait même avec le réglage par défaut
de <q>fileSizeThreshold=0</q> qui diffusait le contenu entier de la partie sur
le disque.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-26049">CVE-2023-26049</a>

<p>Une analyse non standard de cookie dans Jetty pouvait permettre à un
attaquant d’introduire clandestinement des cookies dans d’autres cookies, ou
de provoquer un comportement inattendu en altérant le mécanisme d’analyse de
cookie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-40167">CVE-2023-40167</a>

<p>Avant cette version, Jetty acceptait le caractère <q>+</q> en traitant la
valeur content-length dans le champ d’en-tête HTTP/1. Cela était plus permissif
que ce qui était autorisé par la RFC et les autres serveurs rejetaient
systématiquement de telles requêtes avec des réponses 400. Aucun scénario
d’exploitation de cela n’est connu, mais il est concevable que ce trafic de
requête pouvait aboutir si jetty était utilisé en combinaison avec un serveur
qui ne fermait pas la connexion après l’envoi d’une telle réponse 400.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-36479">CVE-2023-36479</a>

<p>Les utilisateurs de CgiServlet avec une structure particulière de commande
pouvaient avoir une mauvaise commande exécutée. Si un utilisateur envoyait une
requête à un servlet org.eclipse.jetty.servlets.CGI pour un binaire avec une
espace dans son nom, le servlet protégeait la commande en l’enveloppant dans
des guillemets. Cette commande enveloppée, plus un préfixe de commande
facultatif, était alors exécutée à travers un appel à Runtime.exec. Si le nom
originel du binaire fourni par l’utilisateur contenait un guillemet suivi par
une espace, la ligne de commande résultante contenait plusieurs caractères
génériques au lieu d’un seul.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 9.4.16-0+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jetty9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de jetty9,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/jetty9">\
https://security-tracker.debian.org/tracker/jetty9</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3592.data"
# $Id: $

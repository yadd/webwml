#use wml::debian::translation-check translation="c80881896d195f73282cbe11fb85f9dc37411777" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans php-dompdf un convertisseur
HTML vers PDF, conforme à CSS 2.1, écrit en PHP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3838">CVE-2021-3838</a>

<p>php-dompdf était vulnérable à une désérialisation de données non fiables en
utilisation la désérialisation PHAR (phar://) avec un URL pour image.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2400">CVE-2022-2400</a>

<p>php-dompdf était vulnérable à un contrôle externe de nom de fichier en
contournant une vérification d’accès non autorisée.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.6.2+dfsg-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php-dompdf.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de php-dompdf,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/php-dompdf">\
https://security-tracker.debian.org/tracker/php-dompdf</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3495.data"
# $Id: $

#use wml::debian::translation-check translation="0ad79430eb437a687d2d4987a07a002623a98bbf" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilité de sécurité ont été trouvées dans symfony, un
cadriciel PHP pour des applications web et un ensemble de composants PHP
réutilisables, qui pouvaient conduire à une divulgation d'informations ou
une usurpation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21424">CVE-2021-21424</a>

<p>James Isaac, Mathias Brodala et Laurent Minguet ont découvert qu’il était
possible de dénombrer les utilisateurs sans les permissions adéquates à cause
de messages d’exception différents selon que l’utilisateur existait ou non.
Il était alors possible d’énumérer les utilisateurs en utilisant une attaque
temporelle, en comparant le temps écoulé pour l’authentification d’un
utilisateur existant ou non existant.</p>

<p>403s sont désormais renvoyées que l’utilisateur existe ou pas si un
utilisateur correspond à un utilisateur réel ou si l’utilisateur n’existe pas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24894">CVE-2022-24894</a>

<p>Soner Sayakci a découvert que quand le système de cache HTTP de Symfonym
était activé, l’en-tête de réponse pouvait être stocké dans un en-tête
<code>Set-Cookie</code> et renvoyé à d’autres clients, par conséquent permettant
à un attaquant de récupérer la session de la victime.</p>

<p>Le constructeur <code>HttpStore</code> désormais prend un paramètre contenant
une liste d’en-têtes privés qui sont retirés des en-têtes de réponse HTTP. La
valeur par défaut pour ce paramètre est <code>Set-Cookie</code>, mais elle peut
être écrasée ou étendue par l’application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24895">CVE-2022-24895</a>

<p>Marco Squarcina a découvert que des jetons CSRF n’étaient pas supprimés
lors de la connexion, ce qui pouvait permettre à des attaquants sur le même site
de contourner le mécanisme de protection CSRF en réalisant une attaque similaire
à une fixation de session.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 3.4.22+dfsg-2+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets symfony.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de symfony,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/symfony">\
https://security-tracker.debian.org/tracker/symfony</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3493.data"
# $Id: $

#use wml::debian::translation-check translation="6905cebd4b023c07bd448e67501fdb50a86f0bc4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Denys Klymenko a découvert une vulnérabilité de script intersite (XSS) dans
Roundcube, une solution web personnalisable de messagerie et basée sur AJAX pour
les serveurs IMAP. Elle pouvait permettre à un attaquant distant de charger du
code arbitraire JavaScript à l'aide d'un message text/html de courriel avec un
document SVG contrefait.</p>

<p>Ce problème est dû à une correction incomplète pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2020-16145">CVE-2020-16145</a>.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.3.17+dfsg.1-1~deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets roundcube.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de roundcube,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/roundcube">\
https://security-tracker.debian.org/tracker/roundcube</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3630.data"
# $Id: $

#use wml::debian::translation-check translation="13784d5198b6dce9f87d3c90e1e12a4ef087d070" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans zabbix,
une solution de supervision réseau, permettant éventuellement une énumération
d’utilisateurs, un script intersite ou une contrefaçon de requête intersite.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15132">CVE-2019-15132</a>

<p>Zabbix jusqu’à la version 4.4.0alpha1 permettait une énumération
d’utilisateurs. Avec des requêtes de connexion, il était possible d’énumérer
les noms d’utilisateur en se basant sur la variabilité des réponses de serveur
(par exemple, des messages <q>Login name ou password is incorrect</q> ou
<q>No permissions for system access</q>, ou simplement le blocage pendant
quelques secondes). Cela affectait api_jsonrpc.php et index.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15803">CVE-2020-15803</a>

<p>Zabbix avant les versions 3.0.32rc1, 4.x avant 4.0.22rc1, 4.1.x jusqu’à 4.4.x
avant 4.4.10rc1 et 5.x avant 5.0.2rc1 permettait des XSS stockés dans le Widget
URL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27927">CVE-2021-27927</a>

<p>Dans Zabbix, versions 4.0.x avant 4.0.28rc1, 5.0.0alpha1 avant 5.0.10rc1,
5.2.x avant 5.2.6rc1 et 5.4.0alpha1 avant 5.4.0beta2, le contrôleur
d’authentification CControllerpdate n’avait pas de mécanisme de protection
CSRF. Le code dans ce contrôleur appelait diableSIDValidation à l’intérieur de
la méthode init(). Un attaquant n’avait pas à connaitre les identifiants de
connexion d’un utilisateur, mais devait connaitre l’URL correct de Zabbix et
les informations de contact d’un utilisateur existant avec des privilèges
suffisants.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24349">CVE-2022-24349</a>

<p>Un utilisateur authentifié pouvait créer un lien avec une charge XSS
réfléchie pour des pages d’actions et l’envoyer à d’autres utilisateurs. Le
code malveillant avait accès aux mêmes objets que le reste de la page et
pouvait faire des modifications arbitraires dans le contenu de la page affichée
d’une victime. Cette attaque pouvait être mise en œuvre à l’aide d’un piratage
psychologique et l’expiration d’un certain nombre de facteurs — un attaquant
devait avoir un accès autorisé à l’interface de Zabbix et autoriser une connexion
réseau entre un serveur malveillant et un ordinateur de victime, appréhender
l’infrastructure attaquée, être reconnu par la victime comme un administrateur
et utiliser un canal de communication de confiance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24917">CVE-2022-24917</a>

<p>Un utilisateur authentifié pouvait créer un lien avec du code Javascript
réfléchi pour une page de services et l’envoyer à d’autres utilisateurs. La
charge pouvait être exécutée seulement avec une valeur de jeton CSRF connue
de la victime, qui est modifiée périodiquement et qui est difficile à prédire.
Le code malveillant avait accès aux mêmes objets que le reste de la page web et
pouvait faire des modifications arbitraires dans le contenu de la page de la
victime, affichée lors d’un piratage psychologique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24919">CVE-2022-24919</a>

<p>Un utilisateur authentifié pouvait créer un lien avec du code Javascript
réfléchi pour une page de graphismes et l’envoyer à d’autres utilisateurs. La
charge pouvait être exécutée seulement avec une valeur de jeton CSRF connue
de la victime, qui est modifiée périodiquement et qui est difficile à prédire.
Le code malveillant avait accès aux mêmes objets que le reste de la page web et
pouvait faire des modifications arbitraires dans le contenu de la page de la
victime, affichée lors d’un piratage psychologique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-35229">CVE-2022-35229</a>

<p>Un utilisateur authentifié pouvait créer un lien avec du code Javascript
réfléchi à l’intérieur pour une page de détection et l’envoyer à d’autres
utilisateurs. La charge pouvait être exécutée seulement avec une valeur de
jeton CSRF connue de la victime, qui est modifiée périodiquement et qui est
difficile à prédire.

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-35230">CVE-2022-35230</a>

<p>Un utilisateur authentifié pouvait créer un lien avec du code Javascript
réfléchi à l’intérieur pour une page de graphismes et l’envoyer à d’autres
utilisateurs. La charge pouvait être exécutée seulement avec une valeur de
jeton CSRF connue de la victime, qui est modifiée périodiquement et qui est
difficile à prédire.
</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1:4.0.4+dfsg-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets zabbix.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de zabbix,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/zabbix">\
https://security-tracker.debian.org/tracker/zabbix</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3390.data"
# $Id: $

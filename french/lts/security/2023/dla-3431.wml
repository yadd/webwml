#use wml::debian::translation-check translation="5a7c79ef1ec672b855283af9599597fde220a816" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été corrigées dans sqlite (version 2). Elles
pouvaient permettre à des utilisateurs locaux d’obtenir des informations
sensibles, de causer un déni de service (plantage d'application) ou d’avoir un
impact non précisé.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6153">CVE-2016-6153</a>

<p>sqlite mettait en œuvre improprement l’algorithme de recherche de répertoire
temporaire. Cela pouvait permettre à des utilisateurs locaux d’obtenir des
informations sensibles, de causer un déni de service (plantage d'application)
ou d’avoir un impact non précisé en exploitant l’utilisation du répertoire de
travail en cours pour des fichiers temporaires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8740">CVE-2018-8740</a>

<p>Les bases de données, dont le schéma est corrompu en utilisant une déclaration
CREATE TABLE AS, pouvaient provoquer un déréférencement de pointeur NULL.</p>


<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.8.17-15+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sqlite.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sqlite,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sqlite">\
https://security-tracker.debian.org/tracker/sqlite</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3431.data"
# $Id: $

#use wml::debian::translation-check translation="91b5185043ef7a3ca32b6bf476cfd69172475d23" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans svgpp : une
bibliothèque C++ pour l’analyse et le rendu de fichiers SVG (Scalable Vector
Graphics).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44960">CVE-2021-44960</a>

<p>La fonction XMLDocument::getRoot dans la fonction renderDocument gérait
l’objet XMLDocument incorrectement. Plus précisément, elle renvoyait un pointeur
NULL lors de la seconde déclaration if, aboutissant à une référence de pointeur
NULL après la fonction renderDocument.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6245">CVE-2019-6245</a>
et <a href="https://security-tracker.debian.org/tracker/CVE-2019-6247">CVE-2019-6247</a>
<p>Un problème a été découvert dans Anti-Grain Geometry (AGG) dans la fonction
agg::cell_aa::not_equal. Puisque svgpp est une bibliothèque d’en-tête seulement,
le problème est uniquement transitif en théorie. En conséquence, seul un
durcissement de version de dépendance a été ajouté dans le fichier de contrôle.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.2.3+dfsg1-6+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets svgpp.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de svgpp,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/svgpp">\
https://security-tracker.debian.org/tracker/svgpp</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3376.data"
# $Id: $

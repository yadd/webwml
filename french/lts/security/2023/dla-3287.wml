#use wml::debian::translation-check translation="ae8be054da264fc499381ae635ac8ee2d4edaf00" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été trouvées dans lemonldap-ng, un système
OpenID-Connect, CAS et SAML, compatible Web-SSO, qui pouvaient aboutir à une
divulgation d'informations ou une usurpation d’identité.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16093">CVE-2020-16093</a>

<p>Maxime Besson a découvert que LemonLDAP::NG avant la version 2.0.9 ne
vérifiait pas la validité du certificat X.509 par défaut lors de la connexion
à des dorsaux LDAP distants, parce que la configuration par défaut du module
Net::LDAPS pour Perl était utilisée.</p>

<p>Cette mise à jour change le comportement par défaut pour requérir une
validation X.509 par rapport à l’ensemble
<code>/etc/ssl/certs/ca-certificates.crt</code> de la distribution. Le
comportement antérieur peut être rétabli en exécutant
<code>/usr/share/lemonldap-ng/bin/lemonldap-ng-cli set ldapVerify none</code>.</p>

<p>Si un dorsal de session est défini à
Apache::Session::LDAP ou Apache::Session::Browseable::LDAP, alors la correction
entière implique la mise à niveau du module Apache::Session correspondant
(libapache-session-ldap-perl respectivement libapache-session-browseable-perl)
à la version 0.4-1+deb10u1 (ou ⩾ 0.5) respectivement à la
version 1.3.0-1+deb10u1 (ou ⩾ 1.3.8). Consultez les annonces relatives
<a href="dla-3284">DLA-3284-1</a> et <a href="dla-3285">DLA-3285-1</a> pour plus
de détails.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-37186">CVE-2022-37186</a>

<p>Mickael Bride a découvert que, sous certaines conditions, la session
demeurait valable sur les gestionnaires après avoir été détruite sur le
portail.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.0.2+ds-7+deb10u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets lemonldap-ng.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de lemonldap-ng,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/lemonldap-ng">\
https://security-tracker.debian.org/tracker/lemonldap-ng</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3287.data"
# $Id: $

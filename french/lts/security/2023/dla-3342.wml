#use wml::debian::translation-check translation="09be4b4ae3542465746139f8efc10b838aa604c4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs défauts ont été trouvés dans freeradius, un serveur RADIUS de
haute performance et hautement configurable.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41859">CVE-2022-41859</a>

<p>Dans freeradius, la fonction EAP-PWD compute_password_element() divulguait
des informations sur le mot de passe, ce qui permettait à un attaquant de
réduire substantiellement la taille de l’attaque par dictionnaire hors-ligne.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41860">CVE-2022-41860</a>

<p>Dans freeradius, quand un solliciteur EAP-SIM soumettait une option SIM
inconnue, le serveur essayait de rechercher cette option dans les dictionnaires
internes. Cette recherche échouait, mais le code SIM n’était pas vérifié pour
cet échec. À la place il déréférençait un pointeur NULL et provoquait le
plantage du serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41861">CVE-2022-41861</a>

<p>Un client RADIUS malveillant ou un serveur personnel pouvait soumettre un
attribut mal formé qui provoquait le plantage du serveur.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 3.0.17+dfsg-1.1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets freeradius.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de freeradius,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/freeradius">\
https://security-tracker.debian.org/tracker/freeradius</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3342.data"
# $Id: $

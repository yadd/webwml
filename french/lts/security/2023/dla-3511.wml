#use wml::debian::translation-check translation="9532a05bba4df58161df2b52ce20fa1414b644ac" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Tavis Ormandy a découvert que, sous des circonstances particulières liées à
la microarchitecture, un registre de vecteur dans les processeurs <q>Zen 2</q>
pouvait ne pas être mis à zéro correctement. Ce défaut permettait à un attaquant
de divulguer le contenu du registre à travers des processus concurrents, des
hyper flux et des clients virtualisés.</p>

<p>Pour plus de détails, veuillez consulter
<a href="https://lock.cmpxchg8b.com/zenbleed.html">https://lock.cmpxchg8b.com/zenbleed.html</a>
<a href="https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8">https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8</a>.</p>

<p>La version initiale du microcode d'AMD ne fournit des mises à jour que
pour les processeurs EPYC de deuxième génération : divers processeurs Ryzen sont
aussi affectés, mais les mises à jour ne sont pas encore disponibles. Des
correctifs seront fournis dans une mise à jour ultérieure dès qu'ils seront
publiés.</p>

<p>Pour plus de détails particuliers et les dates prévues, veuillez consulter la
page
<a href="https://www.amd.com/en/resources/product-security/bulletin/amd-sb-7008.html">https://www.amd.com/en/resources/product-security/bulletin/amd-sb-7008.html</a>.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 3.20230719.1+deb10u1. De plus, la mise à jour fournit un correctif pour
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9836">CVE-2019-9836</a>.</p>

<p>Nous vous recommandons de mettre à jour vos paquets amd64-microcode.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de amd64-microcode,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/amd64-microcode">\
https://security-tracker.debian.org/tracker/amd64-microcode</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3511.data"
# $Id: $

#use wml::debian::translation-check translation="5a189592c02a6dcf05fba5aae083f430787956c9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Gregor Kopf de Secfault Security GmbH a découvert que HSQLDB, un moteur de
base de données SQL en Java, permettait l’exécution de commandes fallacieuses de
script dans les fichiers .script et .log. Hsqldb gère un mot clé <q>SCRIPT</q>
qui est normalement utilisé pour enregistrer une entrée de commandes par
l’administrateur pour produire un tel script. En combinaison avec LibreOffice,
un attaquant pouvait fabriquer un odb contenant un fichier <q>database/script</q>
qui lui-même contenait une commande SCRIPT où le contenu du fichier pouvait
être écrit dans un nouveau fichier dont l’emplacement était déterminé par
l’attaquant.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.8.0.10+dfsg-10+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets hsqldb1.8.0.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de hsqldb1.8.0,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/hsqldb1.8.0">\
https://security-tracker.debian.org/tracker/hsqldb1.8.0</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3468.data"
# $Id: $

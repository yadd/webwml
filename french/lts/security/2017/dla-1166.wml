#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une vulnérabilité d’exécution de code à distance a été découverte dans tomcat7.</p>

<p>Lorsque HTTP PUT était activé (par exemple, à l’aide du réglage du paramètre
d’initialisation en lecture seule du servlet par défaut à false), il
était possible de téléverser un fichier JSP sur le serveur à l'aide d'une
requête contrefaite pour l'occasion. Ce JSP pourrait alors être requis et tout
code qu’il contenait serait exécuté par le serveur.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 7.0.28-4+deb7u16.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat7.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1166.data"
# $Id: $

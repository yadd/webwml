#use wml::debian::translation-check translation="5a92f5ba5c86bcac9b588a3e7656db8f48163007" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10208">CVE-2016-10208</a>

<p>Sergej Schumilo et Ralf Spenneberg ont découvert qu’un système de fichiers
ext4 contrefait pourrait déclencher une corruption de mémoire lors de son
montage. Un utilisateur pouvant fournir un périphérique ou une image de système
de fichiers à monter pourrait utiliser cela pour un déni de service (plantage ou
corruption de données) ou éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8824">CVE-2017-8824</a>

<p>Mohamed Ghannam a découvert que l'implémentation de DCCP ne gérait pas
correctement les ressources quand une socket est déconnectée et
reconnectée, menant éventuellement à une utilisation de mémoire après
libération. Un utilisateur local pourrait utiliser cela pour un déni de
service (plantage ou corruption de données) ou éventuellement pour une
augmentation de droits. Sur les systèmes qui n'ont pas déjà chargé le
module dccp, cela peut être atténué en le désactivant :
<code>echo >> /etc/modprobe.d/disable-dccp.conf install dccp false</code></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8831">CVE-2017-8831</a>

<p>Pengfei Wang a découvert que pilote de capture vidéo saa7164 relit les
données d’un périphérique PCI après sa validation. Un utilisateur physiquement
présent capable de relier un périphérique PCI spécialement conçu pourrait
utiliser cela pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12190">CVE-2017-12190</a>

<p>Vitaly Mayatskikh a découvert que la couche block ne comptait pas
correctement les références de page pour des E/S brutes à partir de l’espace
utilisateur. Cela pourrait être exploité par une VM cliente sur un périphérique
SCSI hôte pour un déni de service (épuisement de mémoire) ou éventuellement pour
une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13080">CVE-2017-13080</a>

<p>Une vulnérabilité a été découverte dans le protocole WPA2 qui pourrait
conduire à une réinstallation du même GTK (Group Temporal Key), ce qui
réduit substantiellement la sécurité du chiffrement wifi. C’est un des problèmes
connus de manière collective comme <q>KRACK</q>.</p>

<p>Les mises à jour des GTK sont habituellement gérées par les paquets wpa, où
ce problème a été déjà corrigé (DLA-1150-1). Cependant, quelques périphériques
wifi peuvent être encore actifs et mettent à jour de manière autonome les GTK
alors que le système est à l’arrêt. Le noyau doit aussi faire une vérification
et ignorer la réinstallation de clefs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14051">CVE-2017-14051</a>

<p><q>shqking</q> a signalé que le pilote d’hôte qla2xxx SCSI ne validait pas
correctement les E/S pour l’attribut sysfs <q>optrom</q> des périphériques qu’il
créait. Il est très peu probable que cela ait un impact de sécurité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15115">CVE-2017-15115</a>

<p>Vladis Dronov a signalé que l’implémentation de SCTP ne gérait pas correctement
« peel-off »  d’une association vers un autre espace de noms. Cela conduit à une
utilisation de mémoire après libération qu’un utilisateur local peut exploiter
pour un déni de service (plantage ou corruption de données) ou éventuellement
pour une élévation des privilèges. Sur les systèmes n’ayant pas déjà le module
sctp chargé, cela peut être atténué en le désactivant :
<code>echo &gt;&gt; /etc/modprobe.d/disable-sctp.conf install sctp false</code></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15265">CVE-2017-15265</a>

<p>Michael23 Yu a signalé une situation de compétition dans le sous-système de
séquençage d’ALSA impliquant la création et la suppression de ports. Cela
pourrait conduire à une utilisation de mémoire après libération. Un utilisateur
local avec accès à un périphérique séquenceur ALSA peut utiliser cela pour un
déni de service (plantage ou perte de données) ou éventuellement pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15299">CVE-2017-15299</a>

<p>Eric Biggers a découvert que le sous-système KEYS ne gérait pas la mise à jour
correctement d’une clef non instanciée, conduisant à un déréférencement de
pointeur NULL. Un utilisateur local peut utiliser cela pour un déni de service
(plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15649">CVE-2017-15649</a>

<p><q>nixioaming</q> a signalé a situation de compétition dans l’implémentation
de socket de paquet (AF_PACKET) impliquant le ré-attachement à un groupe fanout.
Cela pourrait conduire à une utilisation de mémoire après libération. Un
utilisateur local avec la capacité CAP_NET_RAW peut utiliser cela pour un déni
de service (plantage ou corruption de données) ou éventuellement pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15868">CVE-2017-15868</a>

<p>Al Viro a découvert que l'implémentation du protocole d'encapsulation de
réseau Bluebooth (BNEP) ne validait pas le type de la seconde socket passée
à la fonction BNEPCONNADD ioctl(), ce qui pourrait conduire à une
corruption de mémoire. Un utilisateur local doté de la capacité
CAP_NET_ADMIN peut utiliser cela pour un déni de service (plantage ou
corruption de données) ou éventuellement pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16525">CVE-2017-16525</a>

<p>Andrey Konovalov a signalé que l’implémentation de console série USB ne
gérait pas la déconnexion correctement de périphériques série inhabituels,
conduisant à utilisation de mémoire après libération. Un problème similaire a
été découvert dans le cas où la configuration d’une console série échoue. Un
utilisateur physiquement présent avec un périphérique USB spécialement conçu
peut utiliser cela pour provoquer un déni de service (plantage ou corruption de
données) ou éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16527">CVE-2017-16527</a>

<p>Andrey Konovalov a signalé que le périphérique de mixage sonore USB ne
supprimait pas correctement l’E/S dans le cas où il échouait à sonder un
périphérique. Cela pourrait conduire à une utilisation de mémoire après
libération. Un utilisateur physiquement présent avec un périphérique USB
spécialement conçu peut utiliser cela pour provoquer un déni de service
(plantage ou corruption de données) ou éventuellement pour une élévation des
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16529">CVE-2017-16529</a>

<p>Andrey Konovalov a signalé que pilote USB audio ne validait pas entièrement
la longueur des descripteurs. Cela pourrait conduire à une lecture excessive de
tampon. Un utilisateur physiquement présent avec un périphérique USB
spécialement conçu peut utiliser cela pour provoquer un déni de service
(plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16531">CVE-2017-16531</a>

<p>Andrey Konovalov a signalé que le noyau USB ne validait pas la longueur des
IAD. Cela pourrait conduire à une lecture excessive de tampon excessive. Un
utilisateur physiquement présent avec un périphérique USB spécialement conçu
peut utiliser cela pour provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16532">CVE-2017-16532</a>

<p>Andrey Konovalov a signalé que le pilote de test USB ne gérait pas pas
correctement les périphériques avec des combinaisons particulières de
terminaisons. Un utilisateur physiquement présent avec un périphérique USB
spécialement conçu peut utiliser cela pour provoquer un déni de service
(plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16533">CVE-2017-16533</a>

<p>Andrey Konovalov a signalé que le pilote USB HID ne validait pas complètement
la longueur des descripteurs. Cela pourrait conduire à une lecture excessive de
tampon. Un utilisateur physiquement présent avec un périphérique USB
spécialement conçu peut utiliser cela pour provoquer un déni de service
(plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16535">CVE-2017-16535</a>

<p>Andrey Konovalov a signalé que le pilote central USB ne validait pas
complètement la longueur des descripteurs BOS. Cela pourrait conduire à une
lecture excessive de tampon. Un utilisateur physiquement présent avec un
périphérique USB spécialement conçu peut utiliser cela pour provoquer un déni de
service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16536">CVE-2017-16536</a>

<p>Andrey Konovalov a signalé que le pilote de capture vidéo cx231xx ne validait
pas complètement la configuration de la terminaison du périphérique. Cela
pourrait conduire à un déréférencement de NULL. Un utilisateur physiquement
présent avec un périphérique USB spécialement conçu peut utiliser cela pour
provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16537">CVE-2017-16537</a>

<p>Andrey Konovalov a signalé que le pilote imon RC ne validait pas complètement
la configuration de l’interface de périphérique. Cela pourrait conduire à un
déréférencement de NULL. Un utilisateur physiquement présent avec un
périphérique USB spécialement conçu peut utiliser cela pour provoquer un déni de
service (plantage).</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16643">CVE-2017-16643</a>

<p>Andrey Konovalov a signalé que le pilote de tablette gtco ne validait
pas complètement la longueur des descripteurs. Cela pourrait conduire à une
lecture excessive de tampon. Un utilisateur physiquement présent avec un
périphérique USB spécialement conçu peut utiliser cela pour provoquer un déni de
service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16649">CVE-2017-16649</a>

<p>Bjørn Mork a trouvé que le pilote de réseau cdc_ether ne validait pas la
longueur maximale de segment du périphérique, éventuellement conduisant à une
division par zero. Un utilisateur physiquement présent avec un
périphérique USB spécialement conçu peut utiliser cela pour provoquer un déni de
service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16939">CVE-2017-16939</a>

<p>Mohamed Ghannam a signalé (à travers le programme SecuriTeam Secure
Disclosure de Beyond Security) que l'implémentation d'IPsec (xfrm) ne
gérait pas correctement certaines causes d'échec lors du vidage
d'informations de politique à travers netlink. Un utilisateur local doté de
la capacité CAP_NET_ADMIN peut utiliser cela pour un déni de service
(plantage ou corruption de données) ou éventuellement pour une augmentation
de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000407">CVE-2017-1000407</a>

<p>Andrew Honig a signalé que l'implémentation de KVM pour les processeurs
Intel permettait un accès direct au port d'E/S 0x80 de l'hôte, ce qui n'est
généralement pas sûr. Sur certains systèmes, cela permet à une VM cliente
de provoquer un déni de service (plantage) de l'hôte.</p></li>
</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.2.96-2. Cette version inclut aussi des corrections de bogue
issues de l'amont, des versions antérieures jusqu'à la version 3.2.96. Elle corrige
aussi quelques régressions causées par le correctif pour
<a href="https://security-tracker.debian.org/tracker/CVE-2017-1000364">CVE-2017-1000364</a>,
qui était inclus dans la DLA-993-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1200.data"
# $Id: $

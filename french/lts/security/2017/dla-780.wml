#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Grégoire Scano"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans libav :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7424">CVE-2016-7424</a>

<p>La fonction put_no_rnd_pixels8_xy2_mmx dans x86/rnd_template.c dans
libav 11.7 et antérieur permet à des attaquants distants de provoquer
un déni de service (déréférencement de pointeur NULL et plantage)
à l'aide d'un fichier MP3 contrefait.</p></li>

<li>(Pas de CVE attribué)

<p>Le codec h264 est vulnérable à divers plantages dus à des libérations
non valables, des listes doublement chaînées corrompues ou des lectures
hors limites.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 6:0.8.19-0+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-780.data"
# $Id: $

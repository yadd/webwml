#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Ming :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6358">CVE-2018-6358</a>

<p>une vulnérabilité de dépassement de tampon basé sur le tas dans la fonction
printDefineFont2 (util/listfdb.c). Des attaquants distants peuvent exploiter
cette vulnérabilité pour provoquer un déni de service à l'aide d'un fichier swf
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7867">CVE-2018-7867</a>

<p>une vulnérabilité de dépassement de tampon basé sur le tas dans la fonction
getString (util/decompile.c) durant un sprintf RegisterNumber. Des attaquants
distants peuvent exploiter cette vulnérabilité pour provoquer un déni de service
à l'aide d'un fichier swf contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7868">CVE-2018-7868</a>

<p>une vulnérabilité de lecture hors limites de tampon basé sur le tas dans la
fonction getName (util/decompile.c) pour des données CONSTANT8. Des attaquants
distants peuvent exploiter cette vulnérabilité pour provoquer un déni de service
à l'aide d'un fichier swf contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7870">CVE-2018-7870</a>

<p>un déréférencement d’adresse mémoire non valable dans la fonction getString
(util/decompile.c) pour des données CONSTANT16. Des attaquants distants peuvent
exploiter cette vulnérabilité pour provoquer un déni de service à l'aide d'un
fichier swf contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7871">CVE-2018-7871</a>

<p>une vulnérabilité de lecture hors limites de tampon basé sur le tas dans la
fonction getName (util/decompile.c) pour des données CONSTANT16. Des attaquants
distants peuvent exploiter cette vulnérabilité pour provoquer un déni de service
à l'aide d'un fichier swf contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7872">CVE-2018-7872</a>

<p>un déréférencement d’adresse mémoire non valable dans la fonction getName
(util/decompile.c) pour des données  CONSTANT16a. Des attaquants distants
peuvent exploiter cette vulnérabilité pour provoquer un déni de service à l'aide
d'un fichier swf contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7875">CVE-2018-7875</a>

<p>une vulnérabilité de lecture hors limites de tampon basé sur le tas dans la
fonction getName (util/decompile.c) pour des données CONSTANT8. Des attaquants
distants peuvent exploiter cette vulnérabilité pour provoquer un déni de service
à l'aide d'un fichier swf contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9165">CVE-2018-9165</a>

<p>la fonction pushdup (util/decompile.c) réalise une copie superficielle
d’éléments String (au lieu d’une copie profonde), permettant une modification
simultanée de plusieurs éléments de la pile, ce qui indirectement rend la
bibliothèque vulnérable à un déréférencement de pointeur NULL dans getName
(util/decompile.c). Des attaquants distants peuvent exploiter cette
vulnérabilité pour provoquer un déni de service à l'aide d'un fichier swf
contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.4.4-1.1+deb7u8.</p>
<p>Nous vous recommandons de mettre à jour vos paquets ming.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1343.data"
# $Id: $

#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux défauts ont été découverts dans mailman, un gestionnaire de liste
de diffusion basé sur le web.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-0618">CVE-2018-0618</a>

<p>Toshitsugu Yoneyama de Mitsui Bussan Secure Directions, Inc. a découvert que
mailman est prédisposé à un défaut de script intersite permettant à un
détendeur de liste malveillant d’injecter des scripts dans la page listinfo,
à cause d’une entrée non vérifiée dans le champ host_name.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13796">CVE-2018-13796</a>

<p>Hammad Qureshi a découvert une vulnérabilité d’usurpation de contenu avec des
messages non valables de nom de liste dans l’interface web.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1:2.1.18-2+deb8u3.</p>
<p>Nous vous recommandons de mettre à jour vos paquets mailman.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1442.data"
# $Id: $

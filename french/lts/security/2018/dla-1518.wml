#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux vulnérabilités ont été découvertes dans polarssl, une bibliothèque
SSL/TLS et de chiffrement légère (de nos jours continuée sous le nom de mbedtls)
qui pourraient aboutir à récupérer du texte simple à l’aide d’attaques par canal
auxiliaire.</p>

<p>Deux autres vulnérabilités mineures ont été découvertes dans polarssl qui
pourraient aboutir des erreurs de dépassement de capacité.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-0497">CVE-2018-0497</a>

<p>Comme protection contre l’attaque « Lucky Thirteen », le code TLS pour le
déchiffrement CBC dans le mode encrypt-then-MAC réalise des calculs MAC
supplémentaires pour compenser les variations de longueurs de message dues au
remplissage. Le montant de ces ajouts à réaliser était basé sur l’hypothèse que
le gros du temps est passé dans le traitement de bloc de 64 octets, ce qui est
correct pour la plupart des hachages mais pas pour SHA-384. Cela corrige le
montant du travail supplémentaire pour SHA-384 (et SHA-512, actuellement non
utilisé dans TLS ainsi que MD2 quoique personne ne devrait s’en soucier).</p>

<p>C’est un correctif de régression pour ce qui corrigeait
<a href="https://security-tracker.debian.org/tracker/CVE-2013-0169">CVE-2013-0169</a>
.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-0498">CVE-2018-0498</a>

<p>La base pour les attaques de la famille Lucky 13 est pour un attaquant d’être
capable de faire la distinction entre les remplissages valables (long) TLS-CBC
et ceux non valables. Puisque notre code règle padlen = 0 pour les remplissages
non valables, la longueur de l’entrée de la fonction HMAC donne l’information
sur cela.</p>

<p>L’information sur cette longueur (modulo la taille de bloc MD/SHA) peut être
déduite du montant de remplissage MD/SHA utilisé (cela est différent du
remplissage TLS-CBC). Si le remplissage MD/SHA est lu d’un tampon (statique), un
attaquant local pourrait obtenir l’information sur le montant utilisé à l'aide
d’une attaque de cache visant ce tampon.</p>

<p>Débarrassons nous de ce tampon. Maintenant le seul tampon utilisé est
celui interne de MD/SHA qui est toujours entièrement lu par la fonction
process().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9988">CVE-2018-9988</a>

<p>Empêchement de dépassement de capacité lors de la vérification de limites et
ajout de la vérification de limites avant la lecture de longueur dans
ssl_parse_server_key_exchange().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9989">CVE-2018-9989</a>

<p>Empêchement de dépassement de capacité lors de la vérification de limites et
ajout de la vérification de limites avant la lecture de longueur dans
ssl_parse_server_psk_hint()</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.3.9-2.1+deb8u4.</p>
<p>Nous vous recommandons de mettre à jour vos paquets polarssl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1518.data"
# $Id: $

#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Les vulnérabilités suivantes ont été découvertes dans la version de
Wireshark pour Wheezy :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5350">CVE-2016-5350</a>

<p>Le dissecteur de SPOOLS pourrait entrer dans une boucle infinie</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5351">CVE-2016-5351</a>

<p>Le dissecteur IEEE 802.11 pourrait planter</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5353">CVE-2016-5353</a>

<p>Le dissecteur UMTS FP pourrait planter</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5354">CVE-2016-5354</a>

<p>Certains dissecteurs USB pourrait planter</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5355">CVE-2016-5355</a>

<p>L'analyseur de fichier Toshiba pourrait planter</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5356">CVE-2016-5356</a>

<p>L'analyseur de fichier CoSine pourrait planter</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5357">CVE-2016-5357</a>

<p>L'analyseur de fichier NetScreen pourrait planter</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5359">CVE-2016-5359</a>

<p>Le dissecteur WBXML pourrait entrer dans une boucle infinie</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.12.1+g01b65bf-4+deb8u6~deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wireshark.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-538.data"
# $Id: $

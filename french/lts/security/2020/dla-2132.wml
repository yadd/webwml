#use wml::debian::translation-check translation="24a355a24119c20247e94866e14d05fb547c939c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème lorsque des permissions par
défaut pour un stockage de cookies HTTP pouvaient permettre à des attaquants
locaux de lire des identifiants privés dans <tt>libzypp</tt>, une bibliothèque
de gestion de paquets qui permet aux applications, telles que YaST, zypper et
l’implémentation openSUSE/SLEKit de PackageKit, de fonctionner</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18900">CVE-2019-18900</a>

<p>Une vulnérabilité de permissions par défaut incorrectes dans libzypp de CaaS
Platform 3.0, Linux serveur Enterprise 12, Linux serveur Enterprise 15 de SUSE
permettait à des attaquants locaux de lire un stockage de cookies utilisé par
libzypp, en exposant des cookies privés. Ce problème affecte libzypp versions
avant 16.21.2-27.68.1 de CaaS Platform 3.0,  libzypp versions avant 16.21.2-2.45.1
de Linux serveur Enterprise 12 et Linux Enterprise Server version 15 17.19.0-3.34.1
de SUSE.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 14.29.1-2+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libzypp.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2132.data"
# $Id: $

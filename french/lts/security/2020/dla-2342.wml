#use wml::debian::translation-check translation="02e7db96b436e0a9137b3807e1e76d2c7c8b0e59" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans libjackson-json-java,
un moteur JSON pour Java.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7525">CVE-2017-7525</a>

<p>Vulnérabilité de sécurité du désérialiseur de Jackson.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15095">CVE-2017-15095</a>

<p>Blocage de davantage de types JDK de la désérialisation polymorphique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10172">CVE-2019-10172</a>

<p>Vulnérabilités d’entité externe XML.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.9.2-8+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libjackson-json-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libjackson-json-java, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libjackson-json-java">https://security-tracker.debian.org/tracker/libjackson-json-java</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2342.data"
# $Id: $

#use wml::debian::translation-check translation="407b54477579bffe1564acf60007d932cf8c7c62" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité d’entité externe XML
dans <tt>log4net</tt>, une API de connexion pour la CLI (Common Language
Infrastructure) ECMA, quelques fois appelée « Mono ».</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1285">CVE-2018-1285</a>

<p>Apache log4net avant la version 2.0.8 ne désactivait pas les entités externes
XML lors de l’analyse de fichiers de configuration de log4net. Cela pourrait
permettre des attaques basées sur des entités externes dans les applications
qui acceptent des fichiers de configuration arbitraires d’utilisateurs.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.2.10+dfsg-6+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets log4net.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2211.data"
# $Id: $

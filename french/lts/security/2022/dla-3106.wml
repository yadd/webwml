#use wml::debian::translation-check translation="3d563557d6f54d2abe185c3e34b31770a10a2400" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait une vulnérabilité de divulgation d'identifiant dans
python-oslo-utils, un ensemble d'utilitaires utilisés par OpenStack.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0718">CVE-2022-0718</a>

<p>Un défaut a été découvert dans python-oslo-utils. Du fait d'une analyse
incorrecte, les mots de passe contenant un guillemet double ( " )
provoquent un masquage incorrect dans les journaux de débogage, faisant que
tout élément du mot de passe après le guillemet double est en texte clair.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans la version
3.36.5-0+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-oslo.utils.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3106.data"
# $Id: $

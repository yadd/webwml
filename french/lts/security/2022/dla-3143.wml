#use wml::debian::translation-check translation="c5e1feb6900c9bb244f4c044d740c5ebf6e83395" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait une vulnérabilité potentielle de déni de service dans
strongswan, une solution de VPN IPsec.</p>

<p>Strongswan pouvait effectuer des requêtes sur des URL possédant des
certificats non sûrs, et cela pouvait éventuellement mener à une attaque
par déni de service en bloquant le processus du récupérateur.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40617">CVE-2022-40617</a></li>
</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans la version
5.7.2-1+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets strongswan.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3143.data"
# $Id: $

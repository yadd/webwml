#use wml::debian::translation-check translation="e41e40a3d8eab77b1a8c72846da0f610db005cec" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème a été découvert dans ujson, un encodeur et décodeur ultra
rapide de JSON pour Python.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45958">CVE-2021-45958</a>

<p>Un dépassement de tampon de tas dans Buffer_AppendIndentUnchecked (appelé
par encode) a été détecté. Son exploitation peut, par exemple, utiliser une
grande quantité d'indentations.</p>


<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1.35-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ujson.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ujson, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ujson">\
https://security-tracker.debian.org/tracker/ujson</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2929.data"
# $Id: $

#use wml::debian::translation-check translation="49064cdf7fe29043a324b81efaac1f1c376dc2d7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans cURL, une
bibliothèque de transfert par URL. Ces défauts peuvent permettre à des
attaquants distants d'obtenir des informations sensibles, de divulguer des
données d'en-têtes d'authentification ou de cookie ou de faciliter une
attaque par déni de service.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans la
version 7.64.0-4+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets curl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de curl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3085.data"
# $Id: $

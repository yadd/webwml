#use wml::debian::translation-check translation="197feebb6584b4887789267745679b668769fe5d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Le correctif pour traiter le
<a href="https://security-tracker.debian.org/tracker/CVE-2023-44487">CVE-2023-44487</a>
(attaque de type <q>Rapid Reset</q>) était incomplet et provoquait une
régression lors d'une utilisation d'entrées/sorties asynchrones (par défaut
pour NIO et NIO2). Les trames DATA doivent être incluses lors du calcul de
la surcharge HTTP/2 pour assurer que les connexions ne se sont pas
terminées prématurément.</p>

<p>Pour la distribution oldstable (Bullseye), ce problème a été corrigé
dans la version 9.0.43-2~deb11u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tomcat9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tomcat9">\
https://security-tracker.debian.org/tracker/tomcat9</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5522.data"
# $Id: $

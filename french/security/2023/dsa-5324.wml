#use wml::debian::translation-check translation="bec50c222c13d6f647da3a5732aebd366b942db9" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, un déni de service ou des
fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2873">CVE-2022-2873</a>

<p>Zheyu Ma a découvert qu'un défaut d'accès mémoire hors limites dans le
contrôleur d'hôte iSMT SMBus 2.0 d'Intel peut avoir pour conséquences un
déni de service (plantage du système).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3545">CVE-2022-3545</a>

<p>Le pilote Netronome Flow Processor (NFP) contenait un défaut
d'utilisation de mémoire après libération dans area_cache_get(). Cela
pouvait avoir pour conséquences un déni de service ou l'exécution de code
arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3623">CVE-2022-3623</a>

<p>Une situation de compétition lors de la requête d'une page hugetlb
CONT-PTE/PMD peut avoir pour conséquences un déni de service ou une fuite
d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4696">CVE-2022-4696</a>

<p>Une vulnérabilité d'utilisation de mémoire après libération a été
découverte dans le sous-système io_uring.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36280">CVE-2022-36280</a>

<p>Une vulnérabilité d'écriture hors limites a été découverte dans le
pilote vmwgfx qui peut permettre à un utilisateur local non privilégié de
provoquer un déni de service (plantage du système).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41218">CVE-2022-41218</a>

<p>Hyunwoo Kim a signalé un défaut d'utilisation de mémoire après
libération dans le sous-système Media DVB-core provoqué par des
concurrences de refcount qui peut permettre à un utilisateur local de
provoquer un déni de service ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-45934">CVE-2022-45934</a>

<p>Un dépassement d'entier a été découvert dans l2cap_config_req() dans le
sous-système Bluetooth qui peut permettre à un attaquant proche
physiquement de provoquer un déni de service (plantage du système).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-47929">CVE-2022-47929</a>

<p>Frederick Lawler a signalé un déréférencement de pointeur NULL dans le
sous-système de contrôle de trafic permettant à un utilisateur non
privilégié de provoquer un déni de service en définissant une configuration
de contrôle de trafic contrefaite pour l'occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0179">CVE-2023-0179</a>

<p>Davide Ornaghi a découvert des calculs incorrects lors de la
récupération de bits d'en-tête de VLAN dans le sous-système netfilter,
permettant à un utilisateur local de divulguer des adresses de pile et de
tas ou éventuellement une élévation locale de privilèges à ceux de
super-utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0266">CVE-2023-0266</a>

<p>Un défaut d'utilisation de mémoire après libération dans le sous-système
son, dû à une absence de verrouillage, peut avoir pour conséquences un déni
le service ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0394">CVE-2023-0394</a>

<p>Kyle Zeng a découvert un défaut de déréférencement de pointeur NULL dans
rawv6_push_pending_frames() dans le sous-système réseau permettant à un
utilisateur local de provoquer un déni de service (plantage du système).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23454">CVE-2023-23454</a>

<p>Kyle Zeng a signalé que l'ordonnanceur réseau Class Based Queueing (CBQ)
était prédisposé à un déni de service du fait de l'interprétation des
résultats de la classification avant la vérification du code de retour de
la classification.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23455">CVE-2023-23455</a>

<p>Kyle Zeng a signalé que l'ordonnanceur réseau ATM Virtual Circuits (ATM)
était prédisposé à un déni de service du fait de l'interprétation des
résultats de la classification avant la vérification du code de retour de
la classification.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 5.10.162-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5324.data"
# $Id: $

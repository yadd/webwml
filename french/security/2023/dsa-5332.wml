#use wml::debian::translation-check translation="b2b613bd1658a976f257f8135be174a831859e23" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans Git, un système de gestion
de versions distribué. Un attaquant peut déclencher l'exécution de code
distant, amener des utilisateurs locaux à exécuter des commandes
arbitraires, divulguer des informations sur le système de fichiers local et
et contourner l'interpréteur restreint.</p>

<p>Cette mise à jour inclut deux modifications de comportement qui peuvent
affecter certaines configurations :  — Git s'arrête quand une traversée de
répertoires modifie la propriété du répertoire pour un utilisateur
différent de l'utilisateur actuel lors de la recherche d'un répertoire git
de premier niveau. Un utilisateur peut créer une exception à ce
comportement en utilisant la nouvelle configuration de safe.directory.
— L'option par défaut de protocol.file.allow est passée de
&quot;always&quot; à &quot;user&quot;.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 1:2.30.2-1+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets git.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de git, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/git">\
https://security-tracker.debian.org/tracker/git</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5332.data"
# $Id: $

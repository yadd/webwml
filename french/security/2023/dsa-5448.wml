#use wml::debian::translation-check translation="3cda56bfefce1a9b5c6dac4138b94ab37510b63f" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, un déni de service ou des
fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2124">CVE-2023-2124</a>

<p>Kyle Zeng, Akshay Ajayan et Fish Wang ont découvert que l'absence de
vérification des métadonnées pouvaient avoir pour conséquences un déni de
service ou une possible élévation de privilèges lors du montage d'une
image disque XFS corrompue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2156">CVE-2023-2156</a>

<p>L'implémentation du protocole RPL IPv6 dans le noyau Linux ne gérait pas
correctement les données fournies par l'utilisateur avec pour
conséquence une assertion déclenchable. Un attaquant distant non
authentifié peut tirer avantage de ce défaut pour un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2269">CVE-2023-2269</a>

<p>Zheng Zhang a signalé qu'un traitement incorrect du verrouillage dans
l'implémentation du mappage de périphérique peut avoir pour conséquence un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3090">CVE-2023-3090</a>

<p>L'absence d'initialisation dans l'implémentation de réseau ipvlan
pouvait conduire à une vulnérabilité d'écriture hors limites, avec pour
conséquences un déni de service ou éventuellement l'exécution de code
arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3212">CVE-2023-3212</a>

<p>Yang Lan a découvert que l'absence de validation dans le système de
fichiers GFS2 pouvait avoir pour conséquence un déni de service à l'aide
d'un déréférencement de pointeur NULL lors du montage d'un système de
fichiers GFS2 mal formé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3268">CVE-2023-3268</a>

<p>Un accès mémoire hors limites dans relayfs pouvait avoir pour
conséquences un déni de service ou une fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3269">CVE-2023-3269</a>

<p>Ruihan Li a découvert qu'un traitement de verrou incorrect pour accéder
et mettre à jour des zones de mémoire virtuelle (VMA) pouvait avoir pour
conséquence une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3390">CVE-2023-3390</a>

<p>Un défaut d'utilisation de mémoire après libération dans le sous-système
netfilter, provoqué par le traitement incorrect d'un chemin d'erreur,
pouvait avoir pour conséquences un déni de service ou une élévation de
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31084">CVE-2023-31084</a>

<p>Le pilote DVB Core ne gérait pas correctement le verrouillage de
certains événements, permettant à un utilisateur local de provoquer un déni
de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32250">CVE-2023-32250</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2023-32254">CVE-2023-32254</a></p>

<p>Quentin Minster a découvert deux situations de compétition dans KSMBD,
un serveur du noyau qui implémente le protocole SMB3, qui pouvaient avoir
pour conséquences un déni de service ou éventuellement l'exécution de code
arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-35788">CVE-2023-35788</a>

<p>Hangyu Hua a découvert une vulnérabilité d'écriture hors limites dans le
classificateur Flower qui pouvait avoir pour conséquences un déni de
service ou l'exécution de code arbitraire.</p></li>

</ul>

<p>Pour la distribution stable (Bookworm), ces problèmes ont été corrigés
dans la version 6.1.37-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5448.data"
# $Id: $

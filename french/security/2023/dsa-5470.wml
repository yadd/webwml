#use wml::debian::translation-check translation="eaf022ce9622a4eda9dd45f8101f64db075c5af3" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans python-werkzeug, une
collection d'utilitaires pour les applications WSGI.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23934">CVE-2023-23934</a>

<p>Werkzeug ne gérait pas correctement l'analyse de cookies sans nom, ce
qui pouvait permettre d'espionner d'autres cookies.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-25577">CVE-2023-25577</a>

<p>Werkzeug pouvait analyser un nombre illimité de parties, dont des
parties de fichier. Cela pouvait avoir pour conséquence un déni de service.</p></li>

</ul>

<p>Pour la distribution oldstable (Bullseye), ces problèmes ont été
corrigés dans la version 1.0.1+dfsg1-2+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-werkzeug.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python-werkzeug,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python-werkzeug">\
https://security-tracker.debian.org/tracker/python-werkzeug</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5470.data"
# $Id: $

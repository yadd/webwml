#use wml::debian::translation-check translation="7fbf113ef094837f72d2bdb71154488accfe2afb" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le serveur de courrier
électronique Dovecot.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12100">CVE-2020-12100</a>

<p>La réception d'un courrier électronique avec des parties MIME
profondément imbriquées mène à un épuisement de ressource lorsque Dovecot
tente de l'analyser.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12673">CVE-2020-12673</a>

<p>L'implémentation de NTLM de Dovecot ne vérifie pas correctement la
taille du tampon de message, ce qui mène à une plantage lors de la lecture
d'une allocation antérieure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12674">CVE-2020-12674</a>

<p>L'implémentation du mécanisme RPA de Dovecot accepte des messages de
longueur nulle, ce qui mène à un plantage d'insertion plus tard.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 1:2.3.4.1-5+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets dovecot.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de dovecot, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/dovecot">\
https://security-tracker.debian.org/tracker/dovecot</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4745.data"
# $Id: $

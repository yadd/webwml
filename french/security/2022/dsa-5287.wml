#use wml::debian::translation-check translation="928673d8ddad60984a8ca39812f17cf93166e398" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Heimdal, une
implémentation de Kerberos 5 qui vise à être compatible avec Kerberos du
MIT.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3671">CVE-2021-3671</a>

<p>Joseph Sutton a découvert que le KDC de Heimdal ne validait pas la
présence du nom du serveur dans le TGS-REQ avant le déréférencement. Cela
peut avoir pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44758">CVE-2021-44758</a>

<p>Heimdal est prédisposé à un déréférencement de pointeur NULL dans les
accepteurs quand un jeton SPNEGO initial n'a pas de mécanisme acceptable.
Cela peut avoir pour conséquence un déni de service pour une application
de serveur qui utilise SPNEGO.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3437">CVE-2022-3437</a>

<p>Plusieurs défauts de dépassement de tampon et des fuites de durée non
constante ont été découverts lors de l'utilisation de 1DES, 3DES ou RC4
(arcfour).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41916">CVE-2022-41916</a>

<p>Un accès mémoire hors limites a été découvert quand Heimdal normalise
en Unicode. Cela peut avoir pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42898">CVE-2022-42898</a>

<p>Des dépassements d'entier dans l'analyse de PAC peut avoir pour
conséquences un déni de service pour les KDC de Heimdal ou éventuellement
pour les serveurs Heimdal.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44640">CVE-2022-44640</a>

<p>Le compilateur ASN.1 de Heimdal génère du code qui permet des encodages
DER contrefaits pour l'occasion pour invoquer une libération non valable
dans la structure décodée après une erreur de décodage. Cela peut avoir
pour conséquence une exécution de code à distance dans le KDC de Heimdal.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 7.7.0+dfsg-2+deb11u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets heimdal.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de heimdal, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/heimdal">\
https://security-tracker.debian.org/tracker/heimdal</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5287.data"
# $Id: $

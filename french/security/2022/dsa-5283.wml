#use wml::debian::translation-check translation="a4a2fd361bb79ac6a6e7dfb3abc7ed376bf063c6" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs défauts ont été découverts dans jackson-databind, une
bibliothèque JSON rapide et puissante pour Java.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36518">CVE-2020-36518</a>

<p>Une exception de dépassement de pile de Java et un déni de service à
l'aide d'une grande profondeur d'objets imbriqués.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42003">CVE-2022-42003</a>

<p>Un épuisement de ressources dans jackson-databind de FasterXML peut
survenir à cause de l'absence de vérification dans les désérialiseurs de
valeur de primitive pour éviter l'imbrication profonde de tableau
d'enveloppes lorsque la fonction UNWRAP_SINGLE_VALUE_ARRAYS est activée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42004">CVE-2022-42004</a>

<p>Un épuisement de ressources dans jackson-databind de FasterXML peut
survenir à cause de l'absence de vérification dans
BeanDeserializerBase.deserializeFromArray pour éviter l'utilisation de
tableaux profondément imbriqués. Une application n'est vulnérable qu'avec
certains choix personnalisés pour la désérialisation.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2.12.1-1+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jackson-databind.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de jackson-databind,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/jackson-databind">\
https://security-tracker.debian.org/tracker/jackson-databind</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5283.data"
# $Id: $

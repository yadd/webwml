#use wml::debian::template title="Interpréter le code de conduite de Debian" BARETITLE=true
#use wml::debian::translation-check translation="8ec27f98071e8313b1a32572c961a2417fc0f36f" maintainer="Jean-Pierre Giraud"
{#meta#:
<meta name="keywords" content="code of conduct, coc">
:#meta#}

<h2>Commentaires généraux</h2>

<p>L'objectif de ce document est de fournir quelques explications et exemples sur
la manière dont le <a href="$(HOME)/code_of_conduct">code de conduite</a> (CdC)
est interprété au sein du projet Debian. Si vous vous posez d'autres questions,
vous pouvez vous adresser à l'équipe en charge de la communauté (Community Team
– community@debian.org). Si vous vous inquiétez de savoir si ce que vous pensez
faire pourrait enfreindre le CdC, veuillez aussi vous adresser à l'équipe en
charge de la communauté.</p>

<p>L'objectif du code de conduite est de créer un espace communautaire où chacun
se sent à l'aise. Cela nous aide à entretenir une communauté de contributeurs
enthousiastes à participer à Debian et à nous aider à atteindre nos objectifs de
créer et entretenir Debian. La communauté Debian est tout à la fois un groupe
d'ami(e)s travaillant sur un projet et un groupe de collègues remplissant leurs
tâches. Debian est autant un vaste groupement social qu'un projet technique.</p>

<p>Le but du code de conduite et des efforts pour son application est d'aider
les gens à s'aligner sur les valeurs partagées que Debian a adoptées en tant que
communauté à travers le processus de résolution générale. Vous devez suivre et
respecter le code de conduite pour participer à Debian. Vous n'êtes pas obligé
d'être parfait – tout le monde peut commettre des erreurs ou être dans un
mauvais jour – l'objectif de l'application du code de conduite est d'aider à
mieux agir. C'est tout ce qu'on vous demande : faites de votre mieux pour
traiter vos amis et collègues avec considération. Le code de conduite s'applique
à toutes les activités entreprises dans le cadre de Debian et celle que vous
menez comme représentant du projet Debian. Des sanctions telles qu'un
bannissement (temporaire ou permanent) ou la perte de statut peuvent aussi être
prises si vos activités en dehors de Debian peuvent affecter le projet ou créer
un espace non sûr ou néfaste dans Debian.</p>

<p>Ce texte est un <strong>document évolutif</strong>. Il pourra changer avec le
temps parce que ce qui est considéré comme normal et idéal, autant dans Debian
qu'en dehors, évolue. Le code de conduite a été ratifié en 2014. Il n'a pas
changé depuis lors, alors que les attentes et les bonnes pratiques dans les
communautés du logiciel libre et à code source ouvert, et dans l'univers de la
technologie en général, ont, elles, évolué.</p>

<h2>1. Soyez respectueux</h2>

<p><strong>Dans un projet de la taille de Debian, il y aura inévitablement des
gens avec qui vous serez en désaccord ou avec qui vous trouverez difficile de
coopérer. Acceptez le mais, pour autant, restez respectueux. Le désaccord avec
les actes ou les opinions de quelqu'un n'est pas une excuse pour un mauvais
comportement ou des attaques personnelles. Une communauté dans laquelle les gens
se sentent menacés n'est pas une communauté saine.</strong></p>

<p>Tous les membres de la communauté, et tout le monde en général, méritent
qu'on les respecte. Dans Debian, le respect n'est pas quelque chose qui se
gagne, c'est quelque chose que mérite chacun des membres de la communauté,
indépendamment de son âge, de son genre, de sa taille, de son éducation, de son
appartenance ethnique ou d'autres facteurs.</p>

<p>Debian est une communauté ouverte. Toutes les cultures et croyances sont
bienvenues et admises aussi longtemps qu'elles ne nuisent pas à d'autres. Vos
propres aspirations ou votre contexte culturel ne sont pas une excuse pour
violer le CdC ou pour être irrespectueux envers une autre personne de la
communauté ou dans votre rôle comme membre de la communauté Debian. Debian
possède sa propre culture. Lorsque vous travaillez avec des contributeurs Debian
ou ses utilisateurs, veuillez respecter les règles de Debian et représenter
Debian positivement.</p>

<p>Dans Debian, les personnes viennent de milieux culturels différents, ont des
expériences diverses et pourraient ne pas parler couramment ou ne pas être à
l'aise avec la langue d'une discussion donnée. Cela signifie qu'il est
important de présumer les meilleures intentions de l'interlocuteur (voir plus
bas) et d'être compréhensif sur les différences dans les styles de
communication. Cela ne veut pas dire cependant qu'il est acceptable de
communiquer de façon inappropriée intentionnellement ou de ne pas adapter votre
style de communication pour être conforme aux normes de la communauté une fois
qu'elles ont été abordées.</p>

<h3>Exemples de comportements irrespectueux</h3>

<p>Voici une liste <strong>non exhaustive</strong> d'exemples de comportements
irrespectueux :</p>

<ul>
  <li>grossièreté à l'encontre d'une personne ou de son travail ;</li>
  <li>insulter quelqu'un du fait de son âge, son handicap, son genre, sa
      sexualité, sa corpulence, sa religion, sa nationalité, sa race, son
      appartenance ethnique, sa caste, sa tribu, son éducation, son type de
      contribution ou son statut dans Debian ou dans le logiciel libre et à
      source ouverte, en utilisant une de ces caractéristiques comme insulte 
      ou  en proférantfaire des déclarations désobligeantes sur un groupe ;</li>
  <li>utilisation intentionnelle du mauvais pronom ou nom (par exemple le
      <q>morinommage</q>) d'un individu ;</li>
  <li>Communication agressive ou répétée avec quelqu'un après qu'il a demandé
      d'arrêter :</li>
  <li>aucun effort de bonne foi pour arriver à un accord avec une personne ou
      pour changer des comportements qui vont à l'encontre des valeurs de
      Debian.</li>
</ul>

<h2>2. Présumer de la bonne foi</h2>

<p><strong>Les contributeurs Debian utilisent des moyens variés pour atteindre
notre objectif commun de produire un système d'exploitation libre : la manière
de faire d'un autre peut être différente de la vôtre. Présumez que les autres
personnes travaillent de façon collaborative pour atteindre cet objectif. Notez
que beaucoup de nos contributeurs n'ont pas l'anglais comme langue maternelle
ou peuvent être issus de milieux culturels différents.</strong></p>

<p><strong>Debian est un projet global.</strong> Debian comprend des personnes
de différents milieux, expériences, style de communication et normes
culturelles. De ce fait, il est particulièrement important de présumer de la
bonne foi. Cela signifie présumer, à un niveau raisonnable, que votre
interlocuteur n'a pas l'intention de vous blesser ou de vous insulter.</p>

<p>Pour présumer de la bonne foi, nous devons aussi agir de bonne foi. Cela
signifie aussi que vous devriez présumer que les gens font de leur mieux et que
vous ne devez pas les blesser ou les insulter. Ennuyer quelqu’un de façon
délibérée dans Debian n'est pas acceptable.</p>

<p>Présumer de la bonne foi concerne la communication, le comportement et les
contributions. Cela signifie présumer que tout contributeur, quelle que soit sa
contribution, fait au mieux de ses capacités et le fait avec intégrité.</p>

<h3>Exemples de comportements de mauvaise foi</h3>

<p>Encore une fois, la liste suivante <strong>n'est pas exhaustive</strong> :</p>

<ul>
  <li>troller ;</li>
  <li>présumer que quelqu'un trolle ;</li>
  <li>utiliser des phrases comme <q>Je sais que tu n'es pas stupide</q> ou <q>Tu
      ne peux pas avoir fait cela intentionnellement, donc tu dois vous devez
      être stupide</q> ;</li>
  <li>dénigrer les contributions de quelqu'un ;</li>
  <li>susciter un antagonisme avec d'autres – <q>toucher la corde sensible de
      quelqu'un</q> pour provoquer.</li>
</ul>

<h2>3. Être collaboratif</h2>

<p><strong>Debian est un projet vaste et complexe ; il y a toujours plus à
apprendre dans Debian. C'est bien de demander de l'aide quand vous en avez
besoin. De même, offrir son aide devrait être envisagé dans le contexte de
notre projet commun d'améliorer Debian.</strong></p>

<p><strong>Quand vous faites quelque chose au bénéfice du projet, soyez prêts à
expliquer aux autres comment cela fonctionne de sorte qu'ils puissent s'appuyer
sur votre travail pour le rendre encore plus efficace.</strong></p>

<p>Nos contributions aident les autres contributeurs, le projet et nos
utilisateurs. Nous travaillons de façon ouverte dans l'idée que quiconque
souhaite contribuer devrait pouvoir le faire, dans les limites du raisonnable.
Chacun dans Debian vient d'un contexte différent et possède des compétences
diverses. Cela signifie que vous devriez être positif et constructif et, quand
c'est possible, apporter votre assistance, vos avis ou des conseils. Nous
accordons de la valeur au consensus, cependant, il y a des moments où des
décisions démocratiques seront prises ou des orientations seront données par
les personnes qui souhaitent et sont capable d'entreprendre une tâche.</p>

<p>Les différentes équipes utilisent des outils variés et ont des règles de
collaboration différentes. Cela peut être des choses comme des réunions de
suivi hebdomadaires, des notes partagées ou des processus de relecture de code.
Le simple fait que les choses ont fonctionné d'une certaine manière ne signifie
pas que c'est la meilleure ou la seule manière de les réaliser, et les équipes
devraient être ouvertes à la discussion sur de nouvelles méthodes de
collaboration.</p>

<p>Être collaboratif comprend aussi la présomption de bonne foi (voir plus
haut) que les autres sont également collaboratifs plutôt qu'ils sont <q>contre
vous</q> ou vous ignorent.</p>

<p>Une bonne collaboration est plus précieuse que des compétences techniques.
Être un bon contributeur technique ne rend pas acceptable d'être un membre
nuisible de la communauté.</p>

<h3>Exemples de mauvaise collaboration</h3>

<ul>
  <li>refuser d'adopter les règles de contribution d'une équipe ;</li>
  <li>insulter d'autres contributeurs ou collègues ;</li>
  <li>refuser de travailler avec d'autres à moins que cela ne présente une
      menace pour votre sécurité ou votre bien-être.</li>
</ul>

<h2>4. Essayer d'être concis</h2>

<p><strong>N'oubliez pas que ce que vous écrivez une fois sera lu par des
centaines de personnes. Rédiger un message court signifie que les gens pourront
comprendre la conversation d'une façon aussi efficace que possible. Quand une
longue explication s'avère nécessaire, pensez à ajouter un résumé.</strong></p>

<p><strong>Essayez d'apporter de nouveaux arguments à une conversation de telle
sorte que chaque message ajoute quelque chose de particulier au fil, en vous
souvenant que le reste du fil contient toujours les autres messages avec les
arguments qui ont déjà été développés.</strong></p>

<p><strong>Essayez de ne pas vous écarter du sujet, surtout dans les
conversations qui sont déjà très longues.</strong></p>

<p>Certains sujets ne sont pas appropriés dans Debian, dont les sujets
controversés de nature politique ou religieuse. Debian constitue un
environnement de collègues autant que d'amis. Les échanges collectifs publics
devraient être respectueux, pas hors sujet et professionnels. L'utilisation d'un
langage concis et accessible est d'autant plus importante que de nombreux
contributeurs de Debian n'ont pas l'anglais comme langue maternelle et que
l'essentiel des communications au sein du projet se font en anglais. Il est
important d'être clair et explicite, et quand c'est possible, d'expliquer ou
d'éviter les expressions idiomatiques. (Remarque : l'utilisation d'idiomes, par
exemple, n'est pas une violation du code de conduite. Les éviter est simplement
une bonne pratique dans un souci de clarté en général.)</p>

<p>Il n'est pas toujours facile d'exprimer le sens et le ton au moyen d'un texte
ou entre les cultures. Avoir l'esprit ouvert, présumer des bonnes intentions de
l'autre et essayer de le faire sont les choses les plus importantes dans une
conversation.</p>

<h2>5. Être transparent</h2>

<p><strong>La plupart de moyens de communication dans Debian permettent à la
fois des communications publiques et privées. Comme il est stipulé au paragraphe
trois du contrat social de Debian, vous devriez privilégier l'utilisation de
méthodes de communication publiques pour les messages liés à Debian, à moins que
cela ne concerne un sujet sensible.</strong></p>

<p><strong>Cela s'applique aux messages d'aide ou d'assistance relatifs à
Debian également ; non seulement une requête d'assistance publique a beaucoup
plus de chance de recevoir une réponse, mais cela assure que si des réponses
erronées sont données par inadvertance à vos questions, elles seront plus
facilement repérées et corrigées.</strong></p>

<p>Il est important de garder le plus possible de communications publiques.
Beaucoup de listes de diffusion sont ouvertes à tous ou leurs archives sont
accessibles au public. Les archives peuvent être enregistrées et stockées par
des sources extérieures à Debian (par exemple par Internet Archivel). Il faut
considérer que ce qui a été dit sur une liste de diffusion de Debian est
<q>permanent</q>. Beaucoup de gens conservent aussi l'historique des
conversations IRC..</p>

<h3>Vie privée</h3>

<p>Les conversations privées dans le contexte du projet sont toujours
considérées comme soumises au code de conduite. Il est recommandé de signaler
si quelque chose d'exprimé dans une conversation privée est inapproprié ou
met en danger (voir plus haut pour des exemples).</p>

<p>En même temps, il est important de respecter les conversations privées,
elles ne devraient pas être partagées sauf pour des questions de sécurité.
Certains lieux, comme la liste de diffusion debian-private, relèvent de cette
catégorie.</p>

<h2>6. En cas de problèmes</h2>

<p><strong>Même si tous les participants devraient adhérer à ce code de
conduite, nous sommes conscients que tout le monde peut parfois être dans un
mauvais jour ou ne pas connaître certaines recommandations du code de conduite.
Quand cela se produit, vous pouvez leur répondre et pointer le code de conduite.
Ces messages peuvent être publics ou privés selon ce qui est le plus approprié.
Néanmoins, que le message soit public ou non, il doit toujours renvoyer à la
partie pertinente du code de conduite ; en particulier, il ne doit pas être
grossier ou irrespectueux. Présumez de la bonne foi ; il est plus probable que
les participants soient inconscients de leur mauvais comportement plutôt qu'ils
n'aient intentionnellement essayé de dégrader la qualité de la discussion.</strong></p>

<p><strong>Les contrevenants sérieux ou récurrents se verront interdire
temporairement ou de façon permanente de communiquer à travers les outils de
communication de Debian. Les plaintes devront être envoyées (en privé) aux
administrateurs du canal de communication de Debian en question. Pour trouver
l'information pour contacter ces administrateurs, veuillez consulter la page
sur la structure organisationnelle de Debian.</strong></p>

<p>L'objectif du code de conduite est de fournir aux personnes des indications
sur la manière de faire en sorte que Debian demeure une communauté accueillante.
Les gens devraient se sentir les bienvenus pour participer comme ils sont et non
pas comme d'autres souhaiteraient qu'ils soient. Le code de conduite de Debian
expose les grandes lignes de ce que les gens devraient faire plutôt que de la
façon dont ils devraient ne pas se comporter. Ce document fournit un aperçu de
la façon dont l'équipe en charge de la communauté interprète le code de
conduite. Les différents membres de la communauté Debian peuvent interpréter ce
document différemment. Ce qui est le plus important c'est que les gens se
sentent à l'aise, en sécurité et bienvenus dans Debian. Indépendamment du fait
que la situation soit mentionnée précisément dans ce document, si quelqu'un se
sent mal à l'aise, dans l'insécurité ou pas le bienvenu, il devrait s'adresser à
l'<a href="https://wiki.debian.org/Teams/Community">équipe en charge de la
communauté</a>.</p>

<p>Si quelqu'un s'inquiète d'avoir commis quelque chose d'inapproprié ou de
songer à faire quelque chose qui pourrait être inapproprié, il est aussi
encouragé à saisir l'équipe en charge de la communauté.</p>

<p>Vu la taille de Debian, l'équipe en charge de la communauté ne peut
surveiller et, de fait, ne surveille pas de façon proactive la totalité des
communications, bien que parfois ses membres peuvent les voir en passant. Ainsi
il est important que la communauté Debian collabore avec l'équipe en charge de
la communauté.</p>

<h2>Manquements au code de conduite</h2>

<p>Personne n'est censé être parfait en permanence. Commettre une erreur
n'est pas la fin du monde, toutefois cela peut avoir pour conséquence que
quelqu'un demande une amélioration. Dans Debian, on s'attend à des efforts
sincères pour bien faire et pour faire mieux encore. Des manquements répétés
au code de conduite peuvent aboutir à des mesures de rétorsion ou des
restrictions à l'interaction avec la communauté comprenant, entre autres :</p>

<ul>
  <li>avertissements officiels ;</li>
  <li>bannissement temporaire ou permanent des listes de diffusion, des canaux
      IRC ou d'autres moyens de communication ;</li>
  <li>retrait temporaire ou permanent de droits et privilèges ;</li>
  <li>ou rétrogradation temporaire ou permanente du statut dans le projet
      Debian.</li>
</ul>

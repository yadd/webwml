#use wml::debian::cdimage title="Télécharger les images de CD/DVD de Debian par HTTP ou par FTP" BARETITLE=true
#use wml::debian::translation-check translation="f08ae1f306b187336e0875b70e7657516951b554" maintainer="Jean-Paul Guillonneau"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
# Translators:
# Willy Picard, 2002-2004.
# Nicolas Bertolissio, 2004.
# Thomas Huriaux, 2004-2006.
# Frédéric Bothamy, 2007.
# Simon Paillard, 2007, 2009.
# Thomas Péteul, 2009, 2010.
# David Prévot, 2011.
# Baptiste Jammet, 2015-2016.

<div class="tip">
<p><strong>Veuillez ne pas télécharger les images de CD ou de DVD avec votre
navigateur comme vous le faites pour d'autres fichiers&nbsp;!</strong>
Si votre téléchargement échoue, la plupart des navigateurs ne vous
permettent pas de reprendre le téléchargement à partir de l'endroit où celui-ci
a été interrompu.</p>
</div>

<p>Veuillez utiliser à la place un outil qui permet la reprise des
téléchargements interrompus communément appelé <q>gestionnaire de
téléchargement</q>. Il existe de nombreux greffons de navigateur pour réaliser
cela. Vous pouvez aussi installer un programme indépendant. Sous Linux/Unix,
vous pouvez utiliser <a href="https://aria2.github.io/">aria2</a>,
<a href="https://sourceforge.net/projects/dfast/">wxDownload Fast</a>, ou (en ligne de
commande) <q><tt>wget&nbsp;-c&nbsp;</tt><em>URL</em></q> ou
<q><tt>curl&nbsp;-C&nbsp;-&nbsp;-L&nbsp;-O&nbsp;</tt><em>URL</em></q>.
De nombreuses autres possibilités existent et sont répertoriées sur la page
<a href="https://en.wikipedia.org/wiki/Comparison_of_download_managers">\
Comparaison de gestionnaires de téléchargement</a> (en anglais).
</p>

<p>Les images suivantes de Debian peuvent être téléchargées&nbsp;:</p>

<ul>

  <li><a href="#stable">images officielles des CD ou DVD de
  la distribution <q>stable</q></a>&nbsp;;</li>

  <li><a href="https://cdimage.debian.org/cdimage/weekly-builds/">images
  officielles des CD ou DVD de la distribution <q>testing</q> (<em>régénérées
  une fois par semaine</em>)</a>.</li>

</ul>

<p>Voir également :</p>
<ul>

  <li>la <a href="#mirrors">liste complète des miroirs <tt>debian-cd/</tt></a> ;</li>

  <li>la page <a href="../netinst/">Installation par le réseau</a> pour des
  images <q>d’installation par le réseau</q> ;</li>

  <li>la page <a href="$(DEVEL)/debian-installer/">Installation avec
  l’installateur Debian</a> pour des images de la distribution <q>testing</q>.</li>

</ul>

<hr/>

<h2><a name="stable">Images officielles des CD ou DVD de la
distribution <q>stable</q></a></h2>

<p>Pour installer Debian sur une machine sans connexion Internet, il est
possible d'utiliser des images de CD (700&nbsp;Mo chacune) ou des
images de DVD (4,4&nbsp;Go chacune). Téléchargez le fichier de
l'image du premier CD ou DVD, gravez-le en utilisant un
logiciel de gravure de CD/DVD (ou une clef USB pour les architectures
i386 et amd64), puis redémarrez à partir de ce média.</p>

<p>Le <strong>premier</strong> disque (CD ou DVD) contient
tous les fichiers nécessaires pour installer un système Debian
standard.<br/>
Pour éviter des téléchargements inutiles, veuillez <strong>ne
pas</strong> télécharger d'autres fichiers d'image de CD ou
DVD à moins d'avoir besoin des paquets de ces images.</p>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>

<p>Les liens suivants pointent vers des fichiers image d'une taille
n'excédant pas 700&nbsp;Mo afin qu'ils puissent être gravés sur des
médias CD-R(W)&nbsp;:</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>

<p>Les liens suivants pointent vers des fichiers image d'une taille
n'excédant pas 4,7&nbsp;Go afin qu'ils puissent être gravés sur des
disques DVD-R/DVD+R ou des médias similaires&nbsp;:</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>Assurez-vous de lire la documentation avant de faire l'installation.
<strong>Si vous ne devez lire qu'un seul document</strong> avant
l'installation, veuillez lire notre
<a href="$(HOME)/releases/stable/i386/apa">guide d'installation</a>, un
parcours rapide du processus d'installation. D'autres documentations
utiles&nbsp;:</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">le manuel d'installation</a>,
    les instructions détaillées d'installation&nbsp;;</li>
<li><a href="https://wiki.debian.org/DebianInstaller">la documentation de
    l'installateur Debian</a>, y compris la FAQ avec des questions et
    réponses récurrentes&nbsp;;</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">les
    errata de l'installateur Debian</a>, la liste des problèmes connus
    dans l'installateur.</li>
</ul>

<hr />

<h2><a name="mirrors">Miroirs répertoriés de l'archive <q>debian-cd</q></a></h2>

<p>Veuillez noter que <strong>certains miroirs ne sont pas à jour</strong>
&mdash;&nbsp;avant de télécharger quoi que ce soit, veuillez vérifier que le
numéro de version des images soit le même que l'un de ceux
<a href="../#latest">sur ce site</a>&nbsp;!
En outre, veuillez noter que beaucoup de sites ne sont pas des miroirs
de la totalité des ensembles d'images (en particulier, des images
de DVD), en raison de l'espace nécessaire.</p>

<p><strong>En cas de doute, utilisez le
<a href="https://cdimage.debian.org/debian-cd/">serveur principal des images
de CD</a> en Suède</strong>.

<p>Souhaitez-vous proposer les images de CD de Debian sur votre
miroir&nbsp;? Si oui, veuillez consulter les
<a href="../mirroring/">instructions concernant la mise en place d'un
miroir d'images de CD</a>.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"

#use wml::debian::template title="Portages"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="acad0e189af43d486b586d3628b2f3afa4b55523" maintainer="Jean-Paul Guillonneau"
#use wml::debian::toc

# Translators:
# Thomas Marteau, 2001-2007.
# Jean-Édouard Babin, 2008.
# Guillaume Delacour, 2009.
# David Prévot, 2011-2014
# Jean-Paul Guillonneau 2015-2023

<toc-display/>


<toc-add-entry name="intro">Introduction</toc-add-entry>

<p>
Comme vous le savez probablement, <a href="https://www.kernel.org/">Linux</a>
n’est qu'un noyau. Et pendant longtemps, le noyau Linux n'a fonctionné que sur
les machines Intel x86, à partir du 386.
</p>

<p>
Cependant, cela n'est plus vrai ! Le noyau Linux est porté sur un nombre
croissant d'architectures. Et la distribution Debian est portée en conséquence
vers toutes ces architectures. En général, c'est un processus au
démarrage difficile (car il faut faire en sorte que la libc et l'éditeur de liens
dynamiques fonctionnent correctement), suivi d'un travail long et répétitif
consistant à compiler tous nos paquets sur l'architecture cible.
</p>

<p>
Debian est un système d'exploitation, et pas un noyau (en fait, c'est plus
qu'un système d'exploitation vu qu'il inclut des milliers d'applications).

Par conséquent, même si la plupart des portages sont basés sur Linux,
d'autres portages sont basés sur les noyaux FreeBSD, NetBSD et Hurd.
</p>

<div class="important">
<p>
Cette page est en perpétuelle évolution. Tous les
portages n'ont pas encore de pages, et la plupart ont un site extérieur. Nous
nous efforçons de collecter des renseignements sur tous les portages, pour les
reproduire sur le site Debian.
D’autres portages peuvent être <a href="https://wiki.debian.org/CategoryPorts">listés</a>
sur le wiki.
</p>
</div>


<toc-add-entry name="portlist-released">Liste des portages officiels</toc-add-entry>

<p>
Ces portages concernent les architectures prises en charge officiellement par le
projet Debian, et font partie de la publication officielle ou feront partie
d’une prochaine publication.
</p>


<table class="tabular" summary="">
<tbody>
<tr>
<th>Portage</th>
<th>Architecture</th>
<th>Description</th>
<th>Ajout</th>
<th>État</th>
</tr>

<tr>
<td><a href="amd64/">amd64</a></td>
<td>PC 64 bits (amd64)</td>
<td>
Portage sur l'architecture à base de processeurs 64 bits x86 pour prendre en
charge les espaces utilisateur 32 bits et 64 bits pour cette architecture. Ce
portage fait fonctionner les systèmes à base de processeur AMD 64 bits Opteron,
Athlon et Sempron ainsi que les processeurs d’Intel avec prise en charge
d’Intel 64, dont les Pentium D et plusieurs séries Xeon et Core.
</td>
<td>4.0</td>
<td><a href="$(HOME)/releases/stable/amd64/release-notes/">publié</a></td>
</tr>

<tr>
<td><a href="arm/">arm64</a></td>
<td>ARM 64 bits (AArch64)</td>
<td>Portage sur l’architecture ARM 64 bits avec la nouvelle version 8 du jeu
d’instructions 64 bits (appelée AArch64) pour les processeurs tels que X-Gene
d’Applied Micro, Seattle d’AMD et ThunderX de Cavium.</td>
<td>8</td>
<td><a href="$(HOME)/releases/stable/arm64/release-notes/">publié</a></td>
</tr>

<tr>
<td><a href="arm/">armel</a></td>
<td>EABI ARM</td>
<td>Portage sur l’architecture ARM 32 bits petit-boutiste utilisant l’ABI
Embedded, qui prend en charge les CPU ARM compatibles avec le jeu d’instructions
v5te. Ce portage n’utilise pas d’unité de calcul en virgule flottante (FPU).</td>
<td>5.0</td>
<td><a href="$(HOME)/releases/stable/armel/release-notes/">publié</a></td>
</tr>

<tr>
<td><a href="arm/">armhf</a></td>
<td>ARM avec unité de calcul en virgule flottante</td>
<td>Portage sur l’architecture ARM 32 bits petit-boutiste pour les cartes et
périphériques intégrant une unité de calcul en virgule flottante (FPU) et
d’autres fonctionnalités modernes des CPU d’ARM. Ce portage nécessite au moins
un CPU ARMv7 CPU avec prise en charge de la virgule flottante Thumb-2 et
VFPv3-D16.</td>
<td>7.0</td>
<td><a href="$(HOME)/releases/stable/armhf/release-notes/">publié</a></td>
</tr>
<tr>
<td><a href="i386/">i386</a></td>
<td>PC 32 bits (i386)</td>
<td>Portage sur les processeurs 32 bits x86, sur lesquels Linux a été développé
initialement pour les processeurs Intel&nbsp;386, d'où le surnom. Debian
fonctionne sur tout système basé sur un processeur IA-32 fabriqué par Intel
(incluant toutes les séries Pentium ainsi que les tout derniers Core Duo en mode
32 bits), AMD (incluant K6, toutes les séries Athlon et les séries Athlon64 en
mode 32 bits), Cyrix ou par d’autres fabricants.</td>
<td>1.1</td>
<td><a href="$(HOME)/releases/stable/i386/release-notes/">publié</a></td>
</tr>

<tr>
<td><a href="https://wiki.debian.org/mips64el">mips64el</a></td>
<td>MIPS (mode 64 bits petit-boutiste)</td>
<td>
Portage sur l’ABI N64 petit-boutiste pour l’ISA MIPS64r1 et la virgule flottante
matérielle.</td>
<td>9</td>
<td><a href="$(HOME)/releases/stable/mips64el/release-notes/">publié</a></td>
</tr>

<tr>
<td><a href="powerpc/">ppc64el</a></td>
<td>POWER7+, POWER8</td>
<td>Portage sur l’architecture POWER 64 bits petit-boutiste en utilisant la
nouvelle ABI ELFv2 de Open Power.</td>
<td>8</td>
<td><a href="$(HOME)/releases/stable/ppc64el/release-notes/">publié</a></td>
</tr>

<tr>
<td><a href="https://wiki.debian.org/RISC-V">riscv64</a></td>
<td>RISC-V (64 bits petit-boutiste)</td>
<td>Portage pour <a href="https://riscv.org/">RISC-V</a> 64 bits petit-boutiste,
une ISA libre/code source ouvert.</td>
<td>13</td>
<td>en test</td>
</tr>

<tr>
<td><a href="s390x/">s390x</a></td>
<td>System z</td>
<td>Portage pour l’espace utilisateur 64 bits pour ordinateurs centraux IBM
System z.</td>
<td>7.0</td>
<td><a href="$(HOME)/releases/stable/s390x/release-notes/">publié</a></td>
</tr>
</tbody>
</table>


<toc-add-entry name="portlist-unreleased">Liste des autres portages</toc-add-entry>

<p>
Ces portages sont soit des développements en cours pour être finalement
publiés comme architecture officielle, soit des portages ayant été
officiellement pris en charge mais ayant cessé d’être publiés pour cause d’échec
à la qualification ou d’un intérêt limité pour les développeurs, ou des portages
sur lesquels plus personne ne travaille et qui sont listés pour un intérêt
historique.
</p>

<p>
Ces portages, lorsqu’ils sont encore entretenus activement, sont disponibles
dans l’infrastructure <url "https://www.ports.debian.org/">.
</p>

<div class="tip">

<p>
Il existe des images d’installation non officielles disponibles pour plusieurs
portages dans <url "https://cdimage.debian.org/cdimage/ports"/>.
Ces images sont entretenues par les équipes de portage de Debian correspondantes.
</p>
</div>

<table class="tabular" summary="">
<tbody>
<tr>
<th>Portage</th>
<th>Architecture</th>
<th>Description</th>
<th>Ajout</th>
<th>Abandon</th>
<th>État</th>
<th>Remplacé par</th>
</tr>

<tr>
<td><a href="alpha/">alpha</a></td>
<td>Alpha</td>
<td>Portage sur l’architecture Alpha 64 bits RISC.</td>
<td>2.1</td>
<td>6.0</td>
<td>en cours</td>
<td>-</td>
</tr>

<tr>
<td><a href="arm/">arm</a></td>
<td>OABI ARM</td>
<td>Portage sur l’architecture ARM en utilisant l’ancienne ABI.</td>
<td>2.2</td>
<td>6.0</td>
<td>abandonné</td>
<td>armel</td>
</tr>

<tr>
<td><a href="https://web.archive.org/web/20130326061253/http://avr32.debian.net/">avr32</a></td>
<td>Atmel 32 bits RISC</td>
<td>Portage sur l'architecture RISC 32 bits de Atmel (AVR32).</td>
<td>-</td>
<td>-</td>
<td>abandonné</td>
<td>-</td>
</tr>

<tr>
<td><a href="hppa/">hppa</a></td>
<td>HP PA-RISC</td>
<td>Portage sur l’architecture PA-RISC d’Hewlett-Packard.</td>
<td>3.0</td>
<td>6.0</td>
<td>en cours</td>
<td>-</td>
</tr>

<tr>
<td><a href="hurd/">hurd-i386</a></td>
<td>32-bit PC (i386)</td>
<td>Portage sur le système d’exploitation GNU Hurd, pour les processeurs 32 bits
x86.</td>
<td>-</td>
<td>-</td>
<td>en cours</td>
<td>-</td>
</tr>

<tr>
<td><a href="hurd/">hurd-amd64</a></td>
<td>PC 64 bits (amd64)</td>
<td>Portage sur le système d’exploitation GNU/Hurd, pour les processeurs x86
64 bits. Il prend en charge seulement les systèmes 64 bits, pas ceux sur 32 bits à
l’aide du 64 bits.
</td>
<td>-</td>
<td>-</td>
<td>en cours</td>
<td>-</td>

<tr>
<td><a href="ia64/">ia64</a></td>
<td>Intel Itanium IA-64</td>
<td>Portage sur la première architecture 64 bits d'Intel. Note : à ne pas
confondre avec les dernières extensions 64 bits d'Intel pour les processeurs
Pentium 4 et Celeron, architecture appelée Intel 64 ; pour cette dernière,
voir le portage AMD64.</td>
<td>3.0</td>
<td>8</td>
<td>en cours</td>
<td>-</td>
</tr>

<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-amd64</a></td>
<td>PC 64 bits (amd64)</td>
<td>Portage avec le noyau de FreeBSD utilisant la glibc. Il a été publié comme
premier portage non Linux de Debian comme aperçu technologique.</td>
<td>6.0</td>
<td>8</td>
<td><a href="https://lists.debian.org/debian-devel/2023/05/msg00306.html">abandonné</a></td>
<td>-</td>
</tr>

<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-i386</a></td>
<td>PC 32 bits (i386)</td>
<td>Portage avec le noyau de FreeBSD utilisant la glibc. Il a été publié comme
premier portage non Linux de Debian comme aperçu technologique.</td>
<td>6.0</td>
<td>8</td>
<td><a href="https://lists.debian.org/debian-devel/2023/05/msg00306.html">abandonné</a></td>
<td>-</td>
</tr>

<tr>
<td><a href="http://www.linux-m32r.org/">m32r</a></td>
<td>M32R</td>
<td>Portage pour le microprocesseur RISC 32&nbsp;bits de Renesas Technology.</td>
<td>-</td>
<td>-</td>
<td>abandonné</td>
<td>-</td>
</tr>

<tr>
<td><a href="m68k/">m68k</a></td>
<td>Motorola 68k</td>
<td>Portage sur la série de processeurs Motorola 68k, en particulier la série
de stations de travail Sun3, les ordinateurs personnels Apple Macintosh et les
ordinateurs personnels Atari et Amiga.</td>
<td>2.0</td>
<td>4.0</td>
<td>en cours</td>
<td>-</td>
</tr>

<tr>
<td><a href="mips/">mips</a></td>
<td>MIPS (mode gros-boutiste)</td>
<td>Portage sur l’architecture MIPS (gros-boutiste) utilisée sur les machines
SGI.</td>
<td>3.0</td>
<td>11</td>
<td><a href="https://lists.debian.org/debian-release/2019/08/msg00582.html">abandonné</a></td>
<td>-</td>
</tr>

<tr>
<td><a href="mips/">mipsel</a></td>
<td>MIPS (mode petit-boutiste)</td>
<td>Portage sur l’architecture MIPS (petit-boutiste) utilisée sur Digital
DECstations. </td>
<td>3.0</td>
<td>13</td>
<td><a href="https://lists.debian.org/debian-devel-announce/2023/09/msg00000.html">abandonné</a></td>
<td>-</td>
</tr>

<tr>
<td><a href="netbsd/">netbsd-i386</a></td>
<td>32-bit PC (i386)</td>
<td>Portage avec le noyau NetBSD et la libc, pour les processeurs 32 bits x86.</td>
<td>-</td>
<td>-</td>
<td>abandonné</td>
<td>-</td>
</tr>

<tr>
<td><a href="netbsd/">netbsd-alpha</a></td>
<td>Alpha</td>
<td>Portage avec le noyau NetBSD et la libc, pour les processeurs Alpha 64 bits.</td>
<td>-</td>
<td>-</td>
<td>abandonné</td>
<td>-</td>
</tr>

<tr>
<td><a href="https://web.archive.org/web/20150905061423/http://or1k.debian.net/">or1k</a></td>
<td>OpenRISC 1200</td>
<td>Portage sur le CPU au code source ouvert <a href="https://openrisc.io/">OpenRISC</a> 1200.</td>
<td>-</td>
<td>-</td>
<td>abandonné</td>
<td>-</td>
</tr>

<tr>
<td><a href="powerpc/">powerpc</a></td>
<td>Motorola/IBM PowerPC</td>
<td>Portage pour de nombreux modèles Apple Macintosh PowerMac et les machines
à architecture ouverte CHRP et PReP.</td>
<td>2.2</td>
<td>9</td>
<td>en cours</td>
<td>-</td>
</tr>


<tr>
<td><a href="https://wiki.debian.org/PowerPCSPEPort">powerpcspe</a></td>
<td>PowerPC Signal Processing Engine</td>
<td>Portage sur le matériel « Signal Processing Engine » présent sur les
processeurs faible consommation FreeScale 32 bits et « e500 » d’IBM.</td>
<td>-</td>
<td>-</td>
<td>abandonné</td>
<td>-</td>
</tr>

<tr>
<td><a href="s390/">s390</a></td>
<td>S/390 et zSeries</td>
<td>Portage sur les serveurs IBM S/390.</td>
<td>3.0</td>
<td>8</td>
<td>abandonné</td>
<td>s390x</td>
</tr>

<tr>
<td><a href="sparc/">sparc</a></td>
<td>Sun SPARC</td>
<td>Portage pour les stations de travail UltraSPARC de Sun ainsi que sur leurs
successeurs avec l’architecture sun4.</td>
<td>2.1</td>
<td>8</td>
<td>abandonné</td>
<td>sparc64</td>
</tr>

<tr>
<td><a href="https://wiki.debian.org/Sparc64">sparc64</a></td>
<td>SPARC 64 bits</td>
<td>Portage sur les processeurs 64 bits SPARC.</td>
<td>-</td>
<td>-</td>
<td>en cours</td>
<td>-</td>
</tr>

<tr>
<td><a href="https://wiki.debian.org/SH4">sh4</a></td>
<td>SuperH</td>
<td>Portage sur les processeurs SuperH d'Hitachi. Le processeur à code source
ouvert <a href="https://j-core.org/">J-Core</a> est aussi pris en charge.</td>
<td>-</td>
<td>-</td>
<td>en cours</td>
<td>-</td>
</tr>

<tr>
<td><a href="https://wiki.debian.org/X32Port">x32</a></td>
<td>PC 64 bits avec des pointeurs 32 bits</td>
<td>Portage sur l’ABI amd64/x86_64 x32 utilisant le jeu d’instructions d’AMD,
mais avec des pointeurs 32 bits, pour combiner l’ensemble de grands registres de
cet ISA avec la faible empreinte en mémoire et cache découlant des pointeurs
32 bits.</td>
<td>-</td>
<td>-</td>
<td>en cours</td>
<td>-</td>
</tr>
</tbody>
</table>

<div class="note">
<p>Parmi les noms d'ordinateurs et de processeurs cités plus haut, beaucoup sont
des marques déposées par leurs fabricants.</p>
</div>

#use wml::debian::template title="Programme de Jonathan Carter" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"
#use wml::debian::translation-check translation="3f89b50160fb33b73e8c02981fc7018172d2dc62" maintainer="Jean-Pierre Giraud"

<DIV class="main">
<TABLE CLASS="title">
<TR><TD>
<BR>
<H1 CLASS="titlemain"><BIG><B>Jonathan Carter</B></BIG><BR>
    <SMALL>Programme de DPL</SMALL><BR>
    <SMALL>14 mars 2023</SMALL>
</H1>
<H3>
<A HREF="mailto:jcc@debian.org"><TT>jcc@debian.org</TT></A><BR>
<A HREF="https://jonathancarter.org"><TT>https://jonathancarter.org</TT></A><BR>
<A HREF="https://wiki.debian.org/highvoltage"><TT>https://wiki.debian.org/highvoltage</TT></A>
</H3>
</TD></TR>
</TABLE>


<H2 CLASS="section">1. Introduction</H2>

<P>Bonjour, mon nom est Jonathan Carter, connu aussi comme <EM>highvoltage</EM>,
avec comme pseudonyme dans Debian <EM>jcc</EM>, et je suis candidat à un
quatrième mandat de DPL.</P>

<P>Endosser le rôle de DPL pendant trois mandats a été un peu comme des montagnes
russes, avec beaucoup de défis et pleins de réussites pour le projet, y compris
une pandémie qui nous a affectés de bien des façons, la publication de Debian 11
(Bullseye) (qui s'est transformée en grand succès !), quatre résolutions
générales et l'arrivée (ou le retour) de 64 développeurs Debian comme membres
du projet.
</P>


<H2 CLASS="section">2. Pourquoi suis-je candidat au poste de DPL (une fois de plus)</H2>

<P>Quand j'ai été élu la première fois, je souhaitais apporter au projet une
impression de stabilité et de normalité, après que nous avions traversé des
problèmes conflictuels durant les années précédentes. Je savais que cela serait
dur, dans la mesure où certaines blessures étaient profondes, et où, au-delà de
Debian, que beaucoup de choses dans le monde échappent à notre contrôle.</P>

<P>Je crois que beaucoup de grands progrès ont été réalisés pour rendre le
projet Debian bien plus stable, et beaucoup de gens ont travaillé dur pour y
parvenir, mais la raison pour laquelle je souhaite la stabilité n'est pas
seulement pour elle-même. La stabilité s'accompagne de sécurité et d'espace
pour croître et je crois que Debian est maintenant à un stade où elle est prête
à croître et à se développer vers quelque chose de plus grand.</P>

<P>Il n'y a rien de mieux pour vous préparer au poste de DPL que d'avoir déjà
exercé quelques mandats de DPL. Je comprends vraiment bien Debian, et je
comprends aussi nos défis, j'ai appris comment mieux travailler avec les membres
de notre projet, toutes choses qui aident à identifier et à résoudre les
éléments bloquants, et même si je crois que beaucoup dans Debian pourraient être
de bons DPL, je crois que mes compétences, mon expérience, ma patience et ma
compréhension constituent un excellent mélange pour contribuer à guider Debian
durant l'année à venir.</P>


<H2 CLASS="section">3. Programme</H2>

<H3 CLASS="subsection"> 3.1. Aider Debian à se distinguer par elle-même dans
l'écosystème GNU/Linux </H3>

<P>Qui n'a jamais entendu la phrase « Debian est la distribution que les autres
distributions utilisent comme socle » ? C'est un constat très factuel, il y a
beaucoup de systèmes très réputés basés sur Debian, mais assurément Debian ne
saurait être <em>seulement</em> cela. Dans ma vie personnelle, j'utilise Debian
sur mes ordinateurs domestiques, mes serveurs et mes lecteurs multimédias, et
pour ces mêmes fonctions au travail. Il y a longtemps j'ai appris les éléments
de base pour configurer Debian, et depuis, je ne veux pas imaginer un monde où
je n'aurais plus Debian. C'est un <em>excellent</em> système d'exploitation
polyvalent (et pour ceux qui jouent au bingo du DPL, c'est fait... certains
pourraient même le qualifier de <em>système d'exploitation universel !</em>).
Mais ce n'est pas la perception qu'en a le reste du monde. Récemment, j'ai
entendu par hasard quelqu'un qui expliquait ce qu'est Debian, l'appelant une
« distribution de référence », signifiant qu'elle était bonne mais que personne
ne l'utilisait directement. En pratique, ce n'est pas très différent que de
dire « Debian est la distribution sur laquelle des autres distributions sont
basées », mais je dois admettre que cela m'a un peu blessé de l'entendre
exprimé de cette manière.</P>

<P>Je crois que les problèmes qui se dressent devant Debian l'empêchant d'être
aussi populaire qu'elle le mérite peuvent être corrigés, et que le prochain
cycle de développement après la publication de Debian 12 sera un moment idéal
pour faire des efforts encore plus grands dans cette direction que nous ne
l'avons déjà fait.</P>

<P><B>3.1.1. Identifier et corriger les problèmes principaux</B> </P>

<P>Pour Debian 12, nous avons voté <a
href="https://www.debian.org/vote/2022/vote_003">l'inclusion des microprogrammes
non libres</a> sur les médias d'installation par défaut. Bien que les
microprogrammes non libres restent en soi un gros problème, je considère que
c'est un grand progrès pour faciliter, pour les utilisateurs, l'installation de
Debian sur une machine nue (particulièrement dans la mesure où un nombre
croissant d'ordinateurs portables n'ont pas de port Ethernet et un nombre
toujours croissant de serveurs ont besoin d'un microprogramme pour que leurs
ports Ethernet fonctionnent). Je suis très content que nous ayons fait cela,
c'était probablement <em>le</em> problème numéro un qui empêchait une plus
vaste adoption.</P>

<P>Mais je pense que nous pouvant et nous devrions faire d'avantage. J'aimerais
que collectivement nous identifiions les problèmes les plus importants qui
donnent du fil à retordre aux utilisateurs, leur trouvions des solutions
potentielles et si cela semble possible en faire un objectif de publication. Je
vais lister plus loin deux exemples tirés de mes propres difficultés, mais ce ne
sont que des exemples et ce vote du DPL n'est pas un vote pour passer outre un
responsable de paquet de quelque manière que ce soit, mais je les liste pour
vous donner une idée de ce dont je parle.</P>

<P><B>Sur les systèmes de bureau, exécuter fsck automatiquement quand un système
de fichiers rencontre un problème.</B> De façon régulière, un contact m'envoie
un courriel ou un message Signal avec une photo de son ordinateur portable, de
sa machine de bureau ou de son serveur avec une invite de commande BusyBox
avec le message suivant disant quelque chose comme « filesystem check failed
for /dev/sda2 ». Alors je deviens un héros qui leur dit « entrez 'fsck -y
/dev/sda2' puis redémarrez ! ». Je peux comprendre que, sur certains serveurs,
on puisse souhaiter un comportement plus prudent et intervenir manuellement,
mais sur un système de bureau, la réponse à l'utilisateur va presque toujours
être d'effectuer une vérification du système de fichiers, aussi pourquoi ne pas
le faire automatiquement ? L'incident peut toujours être rapporté à
l'utilisateur d'une autre manière après la correction du système de fichiers.
</P>

<P><B>Laisser dpkg reprendre automatiquement sur les systèmes de bureau.</B> Un
autre problème d'écran noir au démarrage survient quand le gestionnaire
d'affichage ne démarre pas parce qu'il n'a pas encore fini d'être configuré.
Dans ce cas, la réponse est presque toujours « Pressez ctrl+alt+f2,
connectez-vous, entrez 'sudo dpkg --configure -a' puis exécutez 'apt upgrade'
(ou tout ce qui installait le matériel) à nouveau ». Chaque fois qu'apt vous dit
d'exécuter 'dpkg --configure -a', la réponse sur les systèmes de bureau est
certainement presque toujours d'entrer cela, aussi, pourquoi apt ne peut-il pas
gérer cela automatiquement pour l'utilisateur ? (Et, idéalement nous devrions
avoir un mécanisme qui vérifie si cela est nécessaire au moment du démarrage.)
</P>

<P>Ce sont juste deux de <em>mes</em> bêtes noires, mais j'aimerais que nous,
comme projet, nous identifions et acceptions de corriger certains des problèmes
principaux qui affectent régulièrement nos utilisateurs. Certains problèmes
affectent durement nos utilisateurs, mais ils devraient être relativement
triviaux à corriger. Nous ne devrions pas laisser ce type de bogues perdurer
dans encore une autre publication.</P>

<P><B>3.1.2. Rendre Debian enthousiasmante pour la communauté.</B> Nous
possédons une grande communauté d'utilisateurs, de contributeurs et de
développeurs occasionnels. Nous avons des problèmes dans tous ces cas. Pour les
utilisateurs, nous avons vraiment besoin que se constituent des groupes locaux.
J'avais relevé cela auparavant, bien que la pandémie ait entravé le développement
de cette idée. Je pense que c'est le moment de formaliser d'avantage le soutien
aux groupes locaux, dans la mesure où ils aident à distribuer le matériel
promotionnel, à créer du battage médiatique, à fournir de l'aide, et parfois,
quand nous avons vraiment de la chance, ils peuvent assumer des tâches
importantes comme soumettre une candidature pour une DebConf. Les groupes locaux
sont essentiels pour Debian et nous devons mieux les soutenir. Pour les
contributeurs occasionnels, nous avons besoin d'une meilleure voie pour les
aider à devenir plus impliqués dans Debian. Pour les développeurs Debian, je
pense que nous avons besoin de trouver plus de moyens pour nous soutenir
mutuellement. Je pense que, trop souvent, un développeur bosse dur pendant des
semaines (voire des mois) submergé par des problèmes sans grande aide, pour le
plus grand bien de tous, et même s'il est plaisant d'être un héros, j'aimerais
voir plus de processus et de procédures pour soutenir les développeurs que de
juste remplir un bogue de demande d'aide (RFH — « Request for Help »).</P>

<P><B>3.1.3. Réduire le temps d'attente dans la file NEW.</B> D'abord,
j'aimerais remercier l'équipe FTP pour leurs prouesses durant les deux derniers
cycles de publication. À chaque moment décisif, ils ont fait ce qu'on attendait
d'eux en réduisant la file NEW. Néanmoins, leur « bande passante » est très
limitée et à certains moments, le temps d'attente a été beaucoup plus long que ce
qui convient à beaucoup. Une des suggestions dans le passé a été de rendre publics
les paquets présents dans la file pour qu'il soit plus facile pour des experts
occasionnels de pointer des problèmes et faire un rejet rapide (réduisant le
temps d'attente des développeurs et libérant du temps pour les réviseurs de
l'équipe FTP). D'après ce que je comprends, le problème est que l'équipe FTP est
préoccupée par de possibles problèmes de copyright dans les téléversements dans
la mesure où certaines personnes pourraient les traiter comme une certaine forme
d'archive de paquet. Pendant ces trois dernières années, nous avons signé
quelques mandats à des juristes qui peuvent nous aider sur des problèmes de
copyright. J'espère que l'équipe FTP sera ouverte à la discussion avec un
juriste spécialisé dans le copyright qui pourrait nous donner un peu plus
d'orientations sur ce sujet. Jusqu'à présent, nous publions les paquets sur
mentors.debian.net et cela n'a pas été un problème, mais je pense que si nous
pouvons obtenir de solides avis juridiques, plutôt que nous limiter à des
suppositions, cela pourrait tranquilliser certains esprits.</P>


<H3 CLASS="subsection"> 3.2. Financement du développement </H3>

<P><B>3.2.1. Ouverture.</B>
Jusqu'à maintenant, 2023 a été la pire de ces dernières années sur le plan de
l'ouverture. Nous avons été dépendants pour l'essentiel du « Google Summer of
Code » et de « Outreachy » pour l'ouverture, et avoir ces organismes externes
pour aider à gérer cela a été formidable. Cette année, nous n'avons pas été
sélectionnés pour le GSoC dans la mesure où la liste de notre projet était trop
courte. Pour ce qui concerne Outreachy, le programme est devenu plutôt coûteux.
Je communiquerai sur le sujet sur la liste debian-project dans les semaines
à venir, mais le projet est de révoquer la délégation actuelle en charge de
l'ouverture et de la remplacer par une nouvelle équipe de développeurs Debian
qui construira un programme interne d'ouverture où nous pourrons probablement
faire un meilleur usage avec le même montant des fonds.
</P>

<P><B>3.2.2. Bourses.</B>
Nous devrions être en mesure de trouver un moyen de payer des contributeurs pour
réaliser des tâches bien définies. Dans le passé, un précédent DPL a fait une
tentative pour rémunérer des développeurs Debian, mais malheureusement cela a
été si mal exécuté que les DPL futurs devraient plutôt éviter purement et
simplement le problème. Je crois que nous pourrions avoir un système où il
serait possible de marquer des rapports de bogue que nous ne serions pas capables
de prendre en charge en tant que bénévoles avec une étiquette appropriée, et une
fois que l'ampleur du travail a été détaillée pour un rapport de bogue, complétée
d'une estimation de temps, alors nous pourrions offrir une récompense pour ce
bogue ou quelqu'un de qualifié pourrait tenter de le résoudre.
</P>

<H2 class="section"> 4. En conclusion </H2>

<P>Certaines des réflexions que j'ai exposées ne sont volontairement pas trop
figées ou précises, mon objectif en tant que DPL est de mettre en vigueur les
souhaits du projet et pas le contraire. Je vous invite à partager vos réflexions
sur la liste <a href="https://lists.debian.org/debian-vote/">debian-vote</a>
pendant la <a href="https://www.debian.org/vote/2023/vote_001">\
période de campagne</a> afin que vous puissiez contribuer à élaborer le prochain
mandat de DPL.</P>

<P>Merci d'avoir pris le temps de lire mon programme. Cette année, je me
présente sans opposant, ce qui signifie que mon travail est un peu plus
difficile pour vous convaincre de voter. <em>Venez voter</em>, je vous en prie !</P>


<H2> A. Journal des modifications </H2>

<P> Les versions de ce programme sont gérées dans un <a href="https://salsa.debian.org/jcc/dpl-platform">dépôt Git.</a> </P>

<UL>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/5.0.0">5.0.0</a> : Nouveau programme pour les élections 2023 du DPL.</LI>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/5.0.1">5.0.1</a> : correction d'erreurs mineures.</LI>
</UL>

<BR>

</DIV>

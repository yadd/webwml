#use wml::debian::translation-check translation="4924e09e5cb1b4163d7ec7161488354e4167b24c" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 11.5</define-tag>
<define-tag release_date>2022-09-10</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la cinquième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans ce
document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de security.debian.org
n'auront pas beaucoup de paquets à mettre à jour et la plupart des mises à jour
de security.debian.org sont comprises dans cette mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections importantes
aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction avahi "Correction de l'affichage d'URL contenant le caractère « &amp; » dans avahi-discover ; pas de désactivation du nettoyage des temporisations lors du nettoyage de <q>watch</q> ; correction de plantages de pointeur NULL lors des tentatives de résolution de noms d'hôte mal formatés [CVE-2021-3502]">
<correction base-files "Mise à jour de /etc/debian_version pour la version 11.5">
<correction cargo-mozilla "Nouveau paquet source pour prendre en charge la construction des nouvelles versions de firefox-esr et de thunderbird">
<correction clamav "Nouvelle version amont stable">
<correction commons-daemon "Correction de détection de JVM">
<correction curl "Rejet des cookies avec <q>control bytes</q> [CVE-2022-35252]">
<correction dbus-broker "Correction d'échec d'assertion lors de la déconnexion de groupes de pairs ; correction d'une fuite de mémoire ; correction d'un déréférencement de pointeur NULL [CVE-2022-31213]">
<correction debian-installer "Reconstruction avec proposed-updates ; passage de l'ABI de Linux à la version 5.10.0-18">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates ; passage de l'ABI de Linux à la version 5.10.0-18">
<correction debian-security-support "Mise à jour de l'état de prise en charge de divers paquets">
<correction debootstrap "Assurance que les chroots non merged-usr peuvent continuer à être créés pour les chroots de versions plus anciennes et de buildd">
<correction dlt-daemon "Correction d'un problème de double libération de mémoire [CVE-2022-31291]">
<correction dnsproxy "Écoute de localhost par défaut plutôt que de 192.168.168.1 qui peut être indisponible">
<correction dovecot "Corrections de possibles problèmes de sécurité quand deux entrées de configuration passdb existent avec les mêmes configurations de pilote et de paramètres [CVE-2022-30550]">
<correction dpkg "Correction de la gestion du fichier de configuration removal-on-upgrade, de fuite de mémoire dans la gestion de remove-on-upgrade ; Dpkg::Shlibs::Objdump : correction d'apply_relocations pour fonctionner avec les symboles versionnés ; ajout de la prise en charge du processeur ARCv2 ; plusieurs mises à jour et corrections pour dpkg-fsys-usrunmess">
<correction fig2dev "Correction d'un problème de double libération de mémoire [CVE-2021-37529], d'un problème de déni de service [CVE-2021-37530] ; plus de mauvais placement des images eps incorporées">
<correction foxtrotgps "Correction de plantage en assurant que les fils sont toujours non référencés">
<correction gif2apng "Correction de dépassements de tampon de tas [CVE-2021-45909 CVE-2021-45910 CVE-2021-45911]">
<correction glibc "Correction de dépassements de tampon vers le bas ou vers le haut, dus à un décalage d'entier, dans getcwd() [CVE-2021-3999] ; correction de plusieurs dépassements dans les fonctions avec caractères larges ; ajout de quelques fonctions de chaîne optimisées d'EVEX pour corriger un problème de performance (jusqu'à 40 %) avec les processeurs Skylake-X ; grantpt rendu utilisable après un fork multi-threadé ; assurance que la protection de vtable de libio est activée">
<correction golang-github-pkg-term "Correction de construction avec les nouveaux noyaux Linux">
<correction gri "Utilisation de <q>ps2pdf</q> à la place de <q>convert</q> pour convertir de PS à PDF">
<correction grub-efi-amd64-signed "Nouvelle version amont">
<correction grub-efi-arm64-signed "Nouvelle version amont">
<correction grub-efi-ia32-signed "Nouvelle version amont">
<correction grub2 "Nouvelle version amont">
<correction http-parser "Plus de réglage F_CHUNKED dans un nouveau champ d'en-tête Transfer-Encoding, corrigeant un problème potentiel de dissimulation de requête HTTP [CVE-2020-8287]">
<correction ifenslave "Correction des configurations d'interfaces agrégées">
<correction inetutils "Correction d'un problème de dépassement de tampon [CVE-2019-0053], d'un problème d'épuisement de pile, de la gestion de réponses FTP PASV [CVE-2021-40491], d'un problème de déni de service [CVE-2022-39028]">
<correction knot "Correction du repli d'IXFR à AXFR avec dnsmasq">
<correction krb5 "Utilisation de SHA256 comme empreinte CMS de Pkinit">
<correction libayatana-appindicator "Compatibilité fournie aux programmes qui dépendent de libappindicator">
<correction libdatetime-timezone-perl "Mise à jour des données incluses">
<correction libhttp-daemon-perl "Amélioration de la gestion de l'en tête Content-Length [CVE-2022-31081]">
<correction libreoffice "Prise en charge d'EUR dans la localisation .hr ; ajout du taux de conversion HRK&lt;-&gt;EUR dans Calc et dans Euro Wizard ; corrections de sécurité [CVE-2021-25636 CVE-2022-26305 CVE-2022-26306 CVE-2022-26307] ; correction d'un blocage lors de l'accès aux carnets d'adresses d'Evolution">
<correction linux "Nouvelle version amont stable">
<correction linux-signed-amd64 "Nouvelle version amont stable">
<correction linux-signed-arm64 "Nouvelle version amont stable">
<correction linux-signed-i386 "Nouvelle version amont stable">
<correction llvm-toolchain-13 "Nouveau paquet source pour prendre en charge la construction des nouvelles versions de firefox-esr et de thunderbird">
<correction lwip "Correction de problèmes de dépassement de tampon [CVE-2020-22283 CVE-2020-22284]">
<correction mokutil "Nouvelle version amont pour permettre la gestion de SBAT">
<correction node-log4js "Pas de création de fichiers lisibles par tous par défaut [CVE-2022-21704]">
<correction node-moment "Correction d'un problème de déni de service basé sur les expressions rationnelles [CVE-2022-31129]">
<correction nvidia-graphics-drivers "Nouvelle version amont ; corrections de sécurité [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-legacy-390xx "Nouvelle version amont ; corrections de sécurité [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-tesla-450 "Nouvelle version amont ; corrections de sécurité [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-tesla-470 "Nouvelle version amont ; corrections de sécurité [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-settings "Nouvelle version amont ; correction de construction croisée">
<correction nvidia-settings-tesla-470 "Nouvelle version amont ; correction de construction croisée">
<correction pcre2 "Correction de problèmes de lecture hors limites [CVE-2022-1586 CVE-2022-1587]">
<correction postgresql-13 "Interdiction du remplacement par les scripts d'extension d'objets qui n'appartiennent pas déjà à l'extension [CVE-2022-2625]">
<correction publicsuffix "Mise à jour des données incluses">
<correction rocksdb "correction d'instruction interdite sur arm64">
<correction sbuild "Buildd::Mail : prise en charge de l'en-tête Subject: MIME encodé, et copie de l'en-tête Content-Type: lors du réacheminement de courriel">
<correction systemd "Abandon de la copie empaquetée de linux/if_arp.h, correction d'échec de construction avec des nouveaux en-têtes du noyau ; prise en charge de la détection des clients Hyper-V d'ARM64 ; détection d'instances OpenStack en tant que KVM sur arm">
<correction twitter-bootstrap4 "Installation effective des fichiers CSS map">
<correction tzdata "Mise à jour des données de fuseau horaire pour l'Iran et le Chili">
<correction xtables-addons "Prise en charge à la fois des anciennes et nouvelles versions de security_skb_classify_flow()">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2022 5175 thunderbird>
<dsa 2022 5176 blender>
<dsa 2022 5177 ldap-account-manager>
<dsa 2022 5178 intel-microcode>
<dsa 2022 5179 php7.4>
<dsa 2022 5180 chromium>
<dsa 2022 5181 request-tracker4>
<dsa 2022 5182 webkit2gtk>
<dsa 2022 5183 wpewebkit>
<dsa 2022 5184 xen>
<dsa 2022 5185 mat2>
<dsa 2022 5187 chromium>
<dsa 2022 5188 openjdk-11>
<dsa 2022 5189 gsasl>
<dsa 2022 5190 spip>
<dsa 2022 5191 linux-signed-amd64>
<dsa 2022 5191 linux-signed-arm64>
<dsa 2022 5191 linux-signed-i386>
<dsa 2022 5191 linux>
<dsa 2022 5192 openjdk-17>
<dsa 2022 5193 firefox-esr>
<dsa 2022 5194 booth>
<dsa 2022 5195 thunderbird>
<dsa 2022 5196 libpgjava>
<dsa 2022 5197 curl>
<dsa 2022 5198 jetty9>
<dsa 2022 5199 xorg-server>
<dsa 2022 5200 libtirpc>
<dsa 2022 5201 chromium>
<dsa 2022 5202 unzip>
<dsa 2022 5203 gnutls28>
<dsa 2022 5204 gst-plugins-good1.0>
<dsa 2022 5205 ldb>
<dsa 2022 5205 samba>
<dsa 2022 5206 trafficserver>
<dsa 2022 5207 linux-signed-amd64>
<dsa 2022 5207 linux-signed-arm64>
<dsa 2022 5207 linux-signed-i386>
<dsa 2022 5207 linux>
<dsa 2022 5208 epiphany-browser>
<dsa 2022 5209 net-snmp>
<dsa 2022 5210 webkit2gtk>
<dsa 2022 5211 wpewebkit>
<dsa 2022 5213 schroot>
<dsa 2022 5214 kicad>
<dsa 2022 5215 open-vm-tools>
<dsa 2022 5216 libxslt>
<dsa 2022 5217 firefox-esr>
<dsa 2022 5218 zlib>
<dsa 2022 5219 webkit2gtk>
<dsa 2022 5220 wpewebkit>
<dsa 2022 5221 thunderbird>
<dsa 2022 5222 dpdk>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction evenement "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction php-cocur-slugify "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction php-defuse-php-encryption "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction php-dflydev-fig-cookies "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction php-embed "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction php-fabiang-sasl "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction php-markdown "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction php-raintpl "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction php-react-child-process "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction php-react-http "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction php-respect-validation "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction php-robmorgan-phinx "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction ratchet-pawl "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction ratchet-rfc6455 "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction ratchetphp "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction reactphp-cache "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction reactphp-dns "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction reactphp-event-loop "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction reactphp-promise-stream "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction reactphp-promise-timer "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction reactphp-socket "Non maintenu ; seulement nécessaire pour movim déjà supprimé">
<correction reactphp-stream "Non maintenu ; seulement nécessaire pour movim déjà supprimé">

</table>

<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.

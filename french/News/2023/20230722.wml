#use wml::debian::translation-check translation="1f51232f1964140bbf71ecfd1fc7bdcbd252e744" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 12.1</define-tag>
<define-tag release_date>2023-07-22</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>Bookworm</define-tag>
<define-tag revision>12.1</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la première mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans ce
document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions actuelles en utilisant un miroir Debian à jour.
</p>

<p>
Les personnes qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette mise
à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections importantes
aux paquets suivants :

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction aide "Gestion correcte de la création de l'utilisateur système ; correction du traitement du répertoire enfant lors de correspondance équivalente">
<correction autofs "Correction d'un blocage lors de l'utilisation de LDAP authentifié par Kerberos">
<correction ayatana-indicator-datetime "Correction de la lecture des sons d'alarme personnalisés">
<correction base-files "Mise à jour pour la version 12.1">
<correction bepasty "Correction du rendu des envois de texte">
<correction boost1.81 "Ajout à libboost-json1.81-dev de la dépendance manquante à libboost-json1.81.0">
<correction bup "Restauration correcte des ACL POSIX">
<correction context "Activation du socket dans mtxrun de ConTeXt">
<correction cpdb-libs "Correction d'une vulnérabilité de dépassement de tampon [CVE-2023-34095]">
<correction cpp-httplib "Correction d'un problème d'injection de CRLF [CVE-2023-26130]">
<correction crowdsec "Correction du fichier acquis.yaml par défaut pour inclure également la source de données journalctl, limitée à l'unité ssh.service, assurant que l'acquisition fonctionne même sans le fichier auth.log traditionnel ; assurance qu'une source de données non valable ne fait pas émettre d'erreur par le moteur">
<correction cups "Corrections de sécurité : utilisation de mémoire après libération [CVE-2023-34241] ; dépassement de tampon de tas [CVE-2023-32324]">
<correction cvs "Configuration du chemin complet vers ssh">
<correction dbus "Nouvelle version amont stable ; correction d'un problème de déni de service [CVE-2023-34969] ; plus de tentative pour prendre en compte DPKG_ROOT, en restaurant la copie de /etc/machine-id de systemd de préférence à la création d'un identifiant de machine entièrement nouveau">
<correction debian-installer "Passage de l'ABI du noyau Linux à la version 6.1.0-10 ; reconstruction avec proposed-updates">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction desktop-base "Retrait des alternatives à Emerald lors de la désinstallation du paquet">
<correction dh-python "Réintroduction de « Breaks+Replaces » dans python2 nécessaire pour aider apt dans certains scénarios de mise à niveau">
<correction dkms "Ajout de « Breaks » sur les paquets *-dkms obsolètes et incompatibles">
<correction dnf "Correction de la constante PYTHON_INSTALL_DIR par défaut de DNF">
<correction dpdk "Nouvelle version amont stable">
<correction exim4 "Correction de l'analyse d'arguments pour l'expansion ${run } ; correction du renvoi par ${srs_encode ..} d'un résultat incorrect tous les 1024 jours">
<correction fai "Correction de la durée de vie des adresses IP">
<correction glibc "Correction d'un dépassement de tampon dans gmon ; correction d'un blocage dans (__check_pf) de getaddrinfo avec une annulation différée ; correction de la prise en charge de y2038 dans strftime sur les architectures 32 bits ; correction d'un cas particulier d'analyse de /etc/gshadow qui peut renvoyer de mauvais pointeurs provoquant des erreurs de segmentation dans les applications ; correction d'un blocage dans system() lorsqu'il est appelé de façon concurrente par plusieurs threads ; cdefs : définition de limites des macros de renforcement pour __FORTIFY_LEVEL &gt; 0 pour prendre en charge les anciens compilateurs C90">
<correction gnome-control-center "Nouvelle version amont de correction de bogues">
<correction gnome-maps "Nouvelle version amont de correction de bogues">
<correction gnome-shell "Nouvelle version amont de correction de bogues">
<correction gnome-software "Nouvelle version amont ; correction de fuites de mémoire">
<correction gosa "avertissement silencieux sur l'obsolescence de PHP 8.2 ; correction d'un modèle manquant dans le thème par défaut ; correction du style de table ; correction de l'utilisation de debugLevel &gt; 0">
<correction groonga "Correction de liens de la documentation">
<correction guestfs-tools "Correction de sécurité [CVE-2022-2211]">
<correction indent "Restauration de la macro ROUND_UP et ajustement de la taille initiale du tampon">
<correction installation-guide "Activation de la traduction en indonésien">
<correction kanboard "Correction de l'injection malveillante d'étiquettes HTML dans le DOM [CVE-2023-32685] ; correction du référencement indirect d'objet basé sur les paramètres menant à l'exposition de fichiers privés [CVE-2023-33956] ; correction de l'absence de contrôles d'accès [CVE-2023-33968, CVE-2023-33970] ; correction d'un problème de script intersite (XSS) stocké dans la fonctionnalité « Task External Link » [CVE-2023-33969]">
<correction kf5-messagelib "Recherche des sous-clés également">
<correction libmatekbd "Correction de fuites de mémoire">
<correction libnginx-mod-http-modsecurity "Reconstruction du binaire avec pcre2">
<correction libreoffice "Nouvelle version amont de correction de bogues">
<correction libreswan "Correction d'un problème potentiel de déni de service [CVE-2023-30570]">
<correction libxml2 "Correction d'un problème de déréférencement de pointeur NULL [CVE-2022-2309]">
<correction linux "Nouvelle version amont stable ; netfilter : nf_tables : genmask plus ignoré lors de la recherche d'une chaîne par identifiant [CVE-2023-31248], accès hors limites évité dans nft_byteorder_eval [CVE-2023-35001]">
<correction linux-signed-amd64 "Nouvelle version amont stable ; netfilter : nf_tables : genmask plus ignoré lors de la recherche d'une chaîne par identifiant [CVE-2023-31248], accès hors limites évité dansnft_byteorder_eval [CVE-2023-35001]">
<correction linux-signed-arm64 "Nouvelle version amont stable ; netfilter : nf_tables: genmask plus ignoré lors de la recherche d'une chaîne par identifiant [CVE-2023-31248], accès hors limites évité dans nft_byteorder_eval [CVE-2023-35001]">
<correction linux-signed-i386 "Nouvelle version amont stable ; netfilter : nf_tables : genmask plus ignoré lors de la recherche d'une chaîne par identifiant [CVE-2023-31248], accès hors limites évité dans nft_byteorder_eval [CVE-2023-35001]">
<correction mailman3 "Suppression d'une tâche cron redondante ; gestion de l'ordre des services quand MariaDB est présent">
<correction marco "Titre de fenêtre correct quand elle est propriété du superutilisateur">
<correction mate-control-center "Correction de plusieurs fuites de mémoire">
<correction mate-power-manager "Correction de plusieurs fuites de mémoire">
<correction mate-session-manager "Correction de plusieurs fuites de mémoire ; moteurs de Clutter autres que x11 autorisés">
<correction multipath-tools "Chemins sous-jacents cachés de LVM ; échec du service initial évité sur les nouvelles installations">
<correction mutter "Nouvelle version amont de correction de bogues">
<correction network-manager-strongswan "Construction du composant éditeur avec prise en charge de GTK 4">
<correction nfdump "Renvoie de succès au démarrage ; correction d'une erreur de segmentation dans l'analyse d'option">
<correction nftables "Correction de régression dans le format de listing de set">
<correction node-openpgp-seek-bzip "Correction de l'installation de fichiers dans le paquet seek-bzip">
<correction node-tough-cookie "Correction d'un problème de pollution de prototype [CVE-2023-26136]">
<correction node-undici "Corrections de sécurité : en-tête HTTP <q>Host</q> protégé contre les injections de CLRF [CVE-2023-23936] ; possible déni de service par expression rationnelle (ReDoS) à l'encontre de Headers.set et Headers.append [CVE-2023-24807]">
<correction node-webpack "Correction de sécurité (objets interdomaines) [CVE-2023-28154]">
<correction nvidia-cuda-toolkit "Mise à jour de l'openjdk-8-jre empaqueté">
<correction nvidia-graphics-drivers "Nouvelle version amont stable ; corrections de sécurité [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-graphics-drivers-tesla "Nouvelle version amont stable ; corrections de sécurité [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-graphics-drivers-tesla-470 "Nouvelle version amont stable ; corrections de sécurité [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-modprobe "Nouvelle version amont de correction de bogues">
<correction nvidia-open-gpu-kernel-modules "Nouvelle version amont stable ; corrections de sécurité [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-support "Ajout de « Breaks » sur des paquets incompatibles provenant de Bullseye">
<correction onionshare "Correction de l'installation d'outils du bureau">
<correction openvpn "Correction d'une fuite de mémoire et de pointeur bancal (possible vecteur de plantage)">
<correction pacemaker "Correction d'une régression dans l'ordonnanceur de ressources">
<correction postfix "Nouvelle version amont de correction de bogues ; correction de <q>postfix set-permissions</q>">
<correction proftpd-dfsg "Pas d'activation de sockets de type inetd à l'installation">
<correction qemu "Nouvelle version amont stable ; correction de la non disponibilité des périphériques USB pour HVM domUS de XEN ; 9pfs : ouverture de fichiers spéciaux évitée [CVE-2023-2861] ; correction de problèmes de ré-entrée dans le contrôleur LSI [CVE-2023-0330]">
<correction request-tracker5 "Correction des liens vers la documentation">
<correction rime-cantonese "Trier les mots et les caractères par fréquence">
<correction rime-luna-pinyin "Installation de données pinyin de schéma manquantes">
<correction samba "Nouvelle version amont stable ; génération des pages de manuel assurée durant la construction ; activation de la possibilité de stocker les tickets kerberos dans le trousseau du noyau ; correction de problèmes de construction sur armel et mipsel ; correction de problèmes de connexion ou de confiance avec Windows avec les mises à jour de Windows 2023-07">
<correction schleuder-cli "Correction de sécurité (échappement de valeur)">
<correction smarty4 "Correction d'un problème d'exécution de code arbitraire [CVE-2023-28447]">
<correction spip "Divers problèmes de sécurité ; correction de sécurité (filtrage des données d'authentification)">
<correction sra-sdk "Correction de l'installation de fichiers dans libngs-java">
<correction sudo "Correction du format de journal d'événements">
<correction systemd "Nouvelle version amont de correction de bogues">
<correction tang "Correction d'une situation de compétition lors de la création ou la rotation de clés [CVE-2023-1672]">
<correction texlive-bin "Désactivation de socket par défaut dans luatex [CVE-2023-32668] ; installation possible sur i386">
<correction unixodbc "Ajout de « Breaks+Replaces » avec odbcinst1debian1">
<correction usb.ids "Mise à jour des données incluses">
<correction vm "Désactivation de la compilation en code objet">
<correction vte2.91 "Nouvelle version amont de correction de bogues">
<correction xerial-sqlite-jdbc "Utilisation d'un UUID comme identifiant de connexion [CVE-2023-32697]">
<correction yajl "Correction de sécurité de fuite de mémoire ; correction d'un problème de déni de service [CVE-2017-16516] et d'un problème de dépassement d'entier [CVE-2022-24795]">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2023 5423 thunderbird>
<dsa 2023 5425 php8.2>
<dsa 2023 5427 webkit2gtk>
<dsa 2023 5428 chromium>
<dsa 2023 5429 wireshark>
<dsa 2023 5430 openjdk-17>
<dsa 2023 5432 xmltooling>
<dsa 2023 5433 libx11>
<dsa 2023 5434 minidlna>
<dsa 2023 5435 trafficserver>
<dsa 2023 5436 hsqldb1.8.0>
<dsa 2023 5437 hsqldb>
<dsa 2023 5439 bind9>
<dsa 2023 5440 chromium>
<dsa 2023 5443 gst-plugins-base1.0>
<dsa 2023 5444 gst-plugins-bad1.0>
<dsa 2023 5445 gst-plugins-good1.0>
<dsa 2023 5446 ghostscript>
<dsa 2023 5447 mediawiki>
<dsa 2023 5448 linux-signed-amd64>
<dsa 2023 5448 linux-signed-arm64>
<dsa 2023 5448 linux-signed-i386>
<dsa 2023 5448 linux>
<dsa 2023 5449 webkit2gtk>
<dsa 2023 5450 firefox-esr>
<dsa 2023 5451 thunderbird>
</table>



<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>


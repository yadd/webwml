#use wml::debian::template title="포트"
#use wml::debian::translation-check translation="acad0e189af43d486b586d3628b2f3afa4b55523" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::toc

<toc-display/>

<toc-add-entry name="intro">소개</toc-add-entry>
<p>
 여러분 대부분이 아는 바와 같이, <a href="https://www.kernel.org/">리눅스</a>는
 그냥 커널입니다. 오래동안 리눅스 커널은 386에서부터 시작해서 오직 인텔 x86 계열
 컴퓨터에서만 동작했습니다.
</p>
<p>

 그러나 어떤 의미로든 이제 더 이상은 사실이 아닙니다. 리눅스 커널은 여러
 아키텍쳐에 포팅되었고, 그 아키텍쳐 목록은 매우 크고 계속 커지고 있습니다.
 그리고 그 뒤를 따라 우리는 데비안 배포판을 이들 아키텍처에 포팅했습니다.
 일반적으로, 이것은 처음에는 끈끈하게 시작하다가 (일단 libc와 동적 링커가 부드럽게 동작하면),
 그 다음에는 패키지를 새로운 아키텍쳐에서 다시 컴파일하도록 계속 시도하는,
 시작과 비교하면 (오래 걸리는 경우) 반복 작업입니다.
</p>
<p>

 데비안은 운영 체제(OS)이며, 커널이 아닙니다. (실제로 데비안은 OS 이상인데
 왜냐면 그것이 수천 개의 응용 프로그램을 포함하고 있기 때문입니다.) 그러므로,
 대부분의 데비안 포트는 리눅스 기반이지만, FreeBSD, NetBSD 그리고 Hurd 커널에
 기반한 포트도 있습니다.
</p>

<div class="important">
<p>

 이 페이지는 진행 중입니다. 모든 포트가 페이지가 있지는 않고, 대부분은 외부
 사이트에 있습니다. 우리는 모든 포트의 정보를 모으고, 데비안 웹 사이트를 통해
 미러하려고 하고 있습니다.
 더 많은 포트가 <a href="https://wiki.debian.org/CategoryPorts">위키에 나열</a>되었을
 수 있습니다.
</p>
</div>

<toc-add-entry name="portlist-released">공식 포트 목록</toc-add-entry>

<p>
이 포트는 데비안 프로젝트가 공식적으로 지원하는 아키텍쳐로,
공식 릴리스에 포함되거나, 다음 릴리스에 포함될 예정입니다.
</p>

<table class="tabular" summary="">
<tbody>
<tr>
<th>포트</th>
<th>아키텍처</th>
<th>설명</th>
<th>추가</th>
<th>상태</th>
</tr>
<tr>
<td><a href="amd64/">amd64</a></td>
<td>64-bit PC (amd64)</td>
<td>Port to 64-bit x86 processors, to support both 32-bit and 64-bit userland.
The port supports AMD's 64-bit Opteron, Athlon and Sempron
processors, and Intel's processors with Intel 64 support, including the
Pentium D and various Xeon and Core series.</td>
<td>4.0</td>
<td><a href="$(HOME)/releases/stable/amd64/release-notes/">released</a></td>
</tr>
<tr>
<td><a href="arm/">arm64</a></td>
<td>64-bit ARM (AArch64)</td>
<td>Port to the 64-bit ARM architecture with the new version 8 64-bit
instruction set (called AArch64), for processes such as the
Applied Micro X-Gene, AMD Seattle and Cavium ThunderX.</td>
<td>8</td>
<td><a href="$(HOME)/releases/stable/arm64/release-notes/">released</a></td>
</tr>
<tr>
<td><a href="arm/">armel</a></td>
<td>EABI ARM</td>
<td>Port to the 32-bit little-endian ARM architecture using the Embedded ABI,
that supports ARM CPUs compatible with the v5te instruction set.
This port does not take advantage of floating-point units (FPU).</td>
<td>5.0</td>
<td><a href="$(HOME)/releases/stable/armel/release-notes/">released</a></td>
</tr>
<tr>
<td><a href="arm/">armhf</a></td>
<td>Hard Float ABI ARM</td>
<td>Port to the 32-bit little-endian ARM architecture for boards and devices
that ship with a floating-point unit (FPU), and other modern ARM CPU features.
This port requires at least an ARMv7 CPU with Thumb-2 and VFPv3-D16
floating point support.</td>
<td>7.0</td>
<td><a href="$(HOME)/releases/stable/armhf/release-notes/">released</a></td>
</tr>
<tr>
<td><a href="i386/">i386</a></td>
<td>32-bit PC (i386)</td>
<td>Port to 32-bit x86 processors, where Linux was originally
developed for the Intel 386 processors, hence the short name. Debian
supports all IA-32 processors, made by Intel (including all Pentium
series and recent Core Duo machines in 32-bit mode), AMD (K6, all Athlon
series, Athlon64 series in 32-bit mode), Cyrix and other
manufacturers.</td>
<td>1.1</td>
<td><a href="$(HOME)/releases/stable/i386/release-notes/">released</a></td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/mips64el">mips64el</a></td>
<td>MIPS (64-bit little-endian mode)</td>
<td>
Port to the little-endian N64 ABI for the MIPS64r1 ISA and
hardware floating-point.
</td>
<td>9</td>
<td><a href="$(HOME)/releases/stable/mips64el/release-notes/">released</a></td>
</tr>
<tr>
<td><a href="powerpc/">ppc64el</a></td>
<td>POWER7+, POWER8</td>
<td>Port for the 64-bit little-endian POWER architecture,
using the new Open Power ELFv2 ABI.</td>
<td>8</td>
<td><a href="$(HOME)/releases/stable/ppc64el/release-notes/">released</a></td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/RISC-V">riscv64</a></td>
<td>RISC-V (64-bit little endian)</td>
<td>Port for 64-bit little-endian <a href="https://riscv.org/">RISC-V</a>,
a free/open ISA.</td>
<td>13</td>
<td>testing</td>
</tr>
<tr>
<td><a href="s390x/">s390x</a></td>
<td>System z</td>
<td>Port to the 64-bit userland for IBM System z mainframes.</td>
<td>7.0</td>
<td><a href="$(HOME)/releases/stable/s390x/release-notes/">released</a></td>
</tr>
</tbody>
</table>

<toc-add-entry name="portlist-other">List of other ports</toc-add-entry>

<p>
These ports are either work in progress efforts that intend to eventually
be promoted into officially released architectures, ports that were once
officially supported but stopped being released because failed the release
qualification or had limited developer interest, or ports that are no
longer worked on and are listed for historical interest.
</p>

<p>
These ports, when they are still actively maintained, are available on
the <url "https://www.ports.debian.org/"> infrastructure.
</p>

<div class="tip">
<p>
 There are non-official installation images available for some of the following ports in
 <url "https://cdimage.debian.org/cdimage/ports"/>.
 Those images are maintained by the corresponding Debian Port Teams.
</p>
</div>

<table class="tabular" summary="">
<tbody>
<tr>
<th>포트</th>
<th>아키텍처</th>
<th>설명</th>
<th>추가 버전</th>
<th>제외 버전</th>
<th>상태</th>
<th>후속</th>
</tr>
<tr>
<td><a href="alpha/">alpha</a></td>
<td>Alpha</td>
<td>Port to the 64-bit RISC Alpha architecture.</td>
<td>2.1</td>
<td>6.0</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="arm/">arm</a></td>
<td>OABI ARM</td>
<td>Port to the ARM architecture using the old ABI.
</td>
<td>2.2</td>
<td>6.0</td>
<td>dead</td>
<td>armel</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20130326061253/http://avr32.debian.net/">avr32</a></td>
<td>Atmel 32-bit RISC</td>
<td>Port to Atmel's 32-bit RISC architecture, AVR32. </td>
<td>-</td>
<td>-</td>
<td>dead</td>
<td>-</td>
</tr>
<tr>
<td><a href="hppa/">hppa</a></td>
<td>HP PA-RISC</td>
<td>Port to Hewlett-Packard's PA-RISC architecture.
</td>
<td>3.0</td>
<td>6.0</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="hurd/">hurd-i386</a></td>
<td>32-bit PC (i386)</td>
<td>
Port to the GNU Hurd operating system, for the 32-bit x86 processors.
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="hurd/">hurd-amd64</a></td>
<td>64-bit PC (amd64)</td>
<td>
Port to the GNU Hurd operating system, for the 64-bit x86 processors.
It supports only 64-bit, not 32-bit along 64-bit.
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="ia64/">ia64</a></td>
<td>Intel Itanium IA-64</td>
<td>Port to Intel's
first 64-bit architecture. Note: this should not be confused with the
latest Intel 64-bit extensions for Pentium 4 and Celeron processors,
called Intel 64; for these, see the amd64 port.
</td>
<td>3.0</td>
<td>8</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-amd64</a></td>
<td>64-bit PC (amd64)</td>
<td>Port to the kernel of FreeBSD using the glibc.
It was released as the first non-Linux port of Debian as a technology preview.
</td>
<td>6.0</td>
<td>8</td>
<td><a href="https://lists.debian.org/debian-devel/2023/05/msg00306.html">dead</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-i386</a></td>
<td>32-bit PC (i386)</td>
<td>Port to the kernel of FreeBSD using the glibc.
It was released as the first non-Linux port of Debian as a technology preview.
</td>
<td>6.0</td>
<td>8</td>
<td><a href="https://lists.debian.org/debian-devel/2023/05/msg00306.html">dead</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="http://www.linux-m32r.org/">m32</a></td>
<td>M32R</td>
<td>Port to the 32-bit RISC microprocessor of Renesas Technology.</td>
<td>-</td>
<td>-</td>
<td>dead</td>
<td>-</td>
</tr>
<tr>
<td><a href="m68k/">m68k</a></td>
<td>Motorola 68k</td>
<td>Port to the Motorola 68k series of processors — in
particular, the Sun3 range of workstations, the Apple Macintosh personal
computers, and the Atari and Amiga personal computers.</td>
<td>2.0</td>
<td>4.0</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="mips/">mips</a></td>
<td>MIPS (big-endian mode)</td>
<td>Port to
the MIPS architecture which is used in (big-endian) SGI machines.
<td>3.0</td>
<td>11</td>
<td><a href="https://lists.debian.org/debian-release/2019/08/msg00582.html">dead</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="mips/">mipsel</a></td>
<td>MIPS (little-endian mode)</td>
<td>Port to
the MIPS architecture which is used in (little-endian) Digital DECstations.
</td>
<td>3.0</td>
<td>13</td>
<td><a href="https://lists.debian.org/debian-devel-announce/2023/09/msg00000.html">dead</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="netbsd/">netbsd-i386</a></td>
<td>32-bit PC (i386)</td>
<td>
Port to the NetBSD kernel and libc, for the 32-bit x86 processors.
</td>
<td>-</td>
<td>-</td>
<td>dead</td>
<td>-</td>
</tr>
<tr>
<td><a href="netbsd/">netbsd-alpha</a></td>
<td>Alpha</td>
<td>
Port to the NetBSD kernel and libc, for the 64-bit Alpha processors.
</td>
<td>-</td>
<td>-</td>
<td>dead</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20150905061423/http://or1k.debian.net/">or1k</a></td>
<td>OpenRISC 1200</td>
<td>Port to the <a href="https://openrisc.io/">OpenRISC</a> 1200 open source CPU.</td>
<td>-</td>
<td>-</td>
<td>dead</td>
<td>-</td>
</tr>
<tr>
<td><a href="powerpc/">powerpc</a></td>
<td>Motorola/IBM PowerPC</td>
<td>Port for many of
the Apple Macintosh PowerMac models, and CHRP and PReP open
architecture machines.</td>
<td>2.2</td>
<td>9</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/PowerPCSPEPort">powerpcspe</a></td>
<td>PowerPC Signal Processing Engine</td>
<td>
Port to the "Signal Processing Engine" hardware present on low-power 32-bit FreeScale and IBM "e500" CPUs.
</td>
<td>-</td>
<td>-</td>
<td>dead</td>
<td>-</td>
</tr>
<tr>
<td><a href="s390/">s390</a></td>
<td>S/390 and zSeries</td>
<td>Port to IBM S/390 servers.</td>
<td>3.0</td>
<td>8</td>
<td>dead</td>
<td>s390x</td>
</tr>
<tr>
<td><a href="sparc/">sparc</a></td>
<td>Sun SPARC</td>
<td>Port for the Sun
UltraSPARC series of workstations, as well as some of their successors
in the sun4 architectures.
</td>
<td>2.1</td>
<td>8</td>
<td>dead</td>
<td>sparc64</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/Sparc64">sparc64</a></td>
<td>64-bit SPARC</td>
<td>
Port to the 64-bit SPARC processors.
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/SH4">sh4</a></td>
<td>SuperH</td>
<td>
Port to Hitachi SuperH processors. Also supports the open source
<a href="https://j-core.org/">J-Core</a> processor.
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/X32Port">x32</a></td>
<td>64-bit PC with 32-bit pointers</td>
<td>
Port to the amd64/x86_64 x32 ABI, which uses the amd64 instruction set but
with 32-bit pointers, to combine the larget register set of that ISA with
the smaller memory and cache footprint resulting from 32-bit pointers.
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
</tbody>
</table>



<div class="note">
<p>Many of the above computer and processor
names are trademarks and registered trademarks of their manufacturers.
</p>
</div>

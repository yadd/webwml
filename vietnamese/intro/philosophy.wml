#use wml::debian::template title="Triết lý sống của chúng tôi: Tại sao chúng tôi làm thế và chúng tôi làm như thế nào" MAINPAGE="true"

#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e" maintainer="Trần Ngọc Quân"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freedom">Sứ mệnh của chúng tôi: Tạo ra một Hệ điều hành tự do</a></li>
    <li><a href="#how">Giá trị của chúng tôi: Cộng đồng Debian hoạt động như thế nào</a></li>
  </ul>
</div>

<h2><a id="freedom">Sứ mệnh của chúng tôi: Tạo ra một Hệ điều hành tự do</a></h2>

<p>Dự án Debian là tập hợp các cá nhân, chia sẻ nhau cùng một mục đích chung:
Chúng tôi muốn tạo ra một hệ điều hành tự do, tự cho cho tất cả mọi người.
Giờ đây, khi sử dụng từ "free", chúng tôi không có ý nói về tiền bạc,
thay vào đó, chúng tôi nói về phần mềm <em>tự do</em>.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-unlock-alt fa-2x"></span> <a href="free">Định nghĩa phần mềm tự do của chúng tôi</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.gnu.org/philosophy/free-sw">Đọc tuyên bố của Tổ chức phần mềm tự do</a></button></p>

<p>Có thể bạn đang thắc mắc tại sao nhiều người lại chọn dành nhiều giờ đồng hồ
để viết phần mềm, đóng gói và bảo trì nó cẩn thận,
chỉ để cho đi mà không tính phí? Chà,
có rất nhiều lý do, sau đây là một số lý do:</p>

<ul>
  <li>Một số người chỉ đơn giản thích giúp đỡ người khác và đóng góp vào một dự án phần mềm tự do là một cách tuyệt vời để thực hiện điều này.</li>
  <li>Nhiều nhà phát triển viết chương trình để tìm hiểu thêm về máy tính, các kiến ​​trúc và ngôn ngữ lập trình khác nhau.</li>
  <li>Một số nhà phát triển đóng góp để coi đó như một lời "cảm ơn" vì tất cả phần mềm miễn phí tuyệt vời mà họ đã nhận được từ những người khác.</li>
  <li>Nhiều người trong giới hàn lâm tạo ra phần mềm miễn phí để chia sẻ kết quả nghiên cứu của họ.</li>
  <li>Các công ty cũng giúp duy trì phần mềm miễn phí: để tác động đến cách ứng dụng phát triển hoặc triển khai các tính năng mới một cách nhanh chóng.</li>
  <li>Tất nhiên, hầu hết các nhà phát triển Debian đều tham gia, vì họ nghĩ rằng điều đó thật thú vị!</li>
</ul>

<p>Mặc dù chúng tôi tin vào phần mềm tự do, nhưng chúng tôi tôn trọng việc mọi người đôi khi
phải cài đặt phần mềm không tự do trên máy của họ - cho dù
họ có muốn thế hay không. Chúng tôi đã quyết định hỗ trợ những người dùng này, bất cứ
khi nào có thể. Ngày càng có nhiều gói cài đặt phần mềm không tự do
trên hệ thống Debian.</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Chúng tôi cam kết với Phần mềm tự do và chúng tôi đã chính thức hóa cam kết đó trong tài liệu: <a href="$(HOME)/social_contract">Khế ước xã hội</a> của chúng tôi</p>
</aside>

<h2><a id="how">Giá trị của chúng tôi: Cộng đồng Debian hoạt động như thế nào</a></h2>

<p>Dự án Debian có hàng ngàn <a
href="people">nhà phát triển và người đóng góp</a> hoạt động tích cực trải rộng trên <a
href="$(DEVEL)/developers.loc">khắp thế giới</a>. Dự án ở tầm cỡ này
cần được <a href="organization">cấu trúc tổ chức</a> một cách cẩn thận.
Vì vậy, nếu bạn thắc mắc về cách dự án Debian hoạt động
và liệu cộng đồng Debian có các quy tắc và hướng dẫn hay không,
hãy xem các tuyên bố sau:</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><a href="$(DEVEL)/constitution">Tuyên ngôn Debian</a>: <br>
          Tài liệu này mô tả cơ cấu tổ chức và giải thích cách dự án Debian đưa ra các quyết định chính thức.</li>
        <li><a href="../social_contract">Khế ước xã hội và Nguyên tắc về phần mềm tự do</a>: <br>
          Khế ước xã hội và Nguyên tắc về phần mềm tự do Debian (DFSG) là một phần của khế ước này, nó mô tả cam kết của chúng tôi đối với phần mềm tự do và cộng đồng phần mềm tự do.</li>
        <li><a href="diversity">Tuyên bố đa dạng:</a> <br>
          Dự án Debian hoan nghênh và khuyến khích mọi người tham gia, bất kể bạn đánh giá bản thân như thế nào hay người khác nhìn nhận bạn ra sao.</li>
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
        <li><a href="../code_of_conduct">Quy tắc ứng xử:</a> <br>
          Chúng tôi đã áp dụng quy tắc ứng xử cho những người tham gia vào các bó thư, kênh IRC, v.v..</li>
        <li><a href="../doc/developers-reference/">Tham khảo của nhà phát triển:</a> <br>
          Tài liệu này cung cập một cái nhìn tổng thể về các sản phẩm khuyến nghị và các nguồn tài nguyên sẵn có cho những nhà phát triển và bảo trì Debian.</li>
        <li><a href="../doc/debian-policy/">Chính sách Debian:</a> <br>
          Sổ tay hướng dẫn mô tả các yêu cầu chính sách đối với bản phân phối Debian, ví dụ: cấu trúc và nội dung của kho nén Debian, các yêu cầu kỹ thuật mà mọi gói phải đáp ứng để được đưa vào, v.v..</li>
      </ul>
    </div>
  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas fa-code fa-2x"></span> Bên trong Debian: <a href="$(DEVEL)/">Góc phát triển</a></button></p>

# templates webwml Catalan template.
# Copyright (C) 2002, 2003, 2004, 2005 Free Software Foundation, Inc.
# Jordi Mallach <jordi@debian.org>, 2002, 2003.
# Guillem Jover <guillem@debian.org>, 2004-2007, 2018-2019.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml\n"
"PO-Revision-Date: 2020-09-27 17:31+0200\n"
"Last-Translator: Guillem Jover <guillem@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Data"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Línia temporal"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "Resum"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Nominacions"

#: ../../english/template/debian/votebar.wml:25
msgid "Withdrawals"
msgstr "Retirades"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Debat"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Plataformes"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Proponent"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "Proponent de la proposta A"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "Proponent de la proposta B"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "Proponent de la proposta C"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "Proponent de la proposta D"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "Proponent de la proposta E"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "Proponent de la proposta F"

#: ../../english/template/debian/votebar.wml:55
msgid "Proposal G Proposer"
msgstr "Proponent de la proposta G"

#: ../../english/template/debian/votebar.wml:58
msgid "Proposal H Proposer"
msgstr "Proponent de la proposta H"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Persones que secunden"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "Persones que secunden la proposta A"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "Persones que secunden la proposta B"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "Persones que secunden la proposta C"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "Persones que secunden la proposta D"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "Persones que secunden la proposta E"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "Persones que secunden la proposta F"

#: ../../english/template/debian/votebar.wml:82
msgid "Proposal G Seconds"
msgstr "Persones que secunden la proposta G"

#: ../../english/template/debian/votebar.wml:85
msgid "Proposal H Seconds"
msgstr "Persones que secunden la proposta H"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Oposició"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Text"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "Proposta A"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "Proposta B"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "Proposta C"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "Proposta D"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "Proposta E"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "Proposta F"

#: ../../english/template/debian/votebar.wml:112
msgid "Proposal G"
msgstr "Proposta G"

#: ../../english/template/debian/votebar.wml:115
msgid "Proposal H"
msgstr "Proposta H"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Opcions"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Proponent de l'esmena"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Persones que secunden l'esmena"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Text de l'esmena"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "Proponent de l'esmena A"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "Persones que secunden l'esmena A"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "Text de l'esmena A"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "Proponent de l'esmena B"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "Persones que secunden l'esmena B"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "Text de l'esmena B"

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr "Proponent de l'esmena C"

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr "Persones que secunden l'esmena C"

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr "Text de l'esmena C"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Esmenes"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Actes"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Requeriments de majoria"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Dades i estadístiques"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Quòrum"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Discussió mínima"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Butlleta"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Fòrum"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Resultat"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "En&nbsp;espera&nbsp;de&nbsp;patrocinadors"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "En&nbsp;discussió"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "Votació&nbsp;en&nbsp;curs"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Decidida"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "Retirada"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Altres"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Pàgina&nbsp;principal&nbsp;de&nbsp;votacions"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Com"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Enviar&nbsp;proposta"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Esmenar&nbsp;una&nbsp;proposta"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Donar&nbsp;suport&nbsp;a&nbsp;una&nbsp;proposta"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Llegir un resultat"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Votar"

#use wml::debian::template title="Πληροφορίες κυκλοφορίας της έκδοσης Debian &ldquo;bullseye&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="4c728c46ce7c43bebb50236f2439e9bc75f33a36" maintainer="galaxico"

<p>Το Debian <current_release_bullseye> κυκλοφόρησε στις <a href="$(HOME)/News/<current_release_newsurl_bullseye/>"><current_release_date_bullseye></a>.
<ifneq "11.0" "<current_release>"
  "Debian 11.0 was initially released on <:=spokendate('2021-08-14'):>."
/>
The release included many major
changes, described in
our <a href="$(HOME)/News/2021/20210814">press release</a> and
the <a href="releasenotes">Release Notes</a>.</p>

<p><strong>Το Debian 11 αντικαταστήθηκε από την έκδοση 
<a href="../bookworm/">Debian 12 (<q>bookworm</q>)</a>.
#Security updates have been discontinued as of <:=spokendate('2023-06-10'):>.
</strong></p>

# <p><strong>However, bullseye benefits from Long Term Support (LTS) until
# the end of August 2026. The LTS is limited to i386, amd64, armel, armhf and arm64.
# All other architectures are no longer supported in bullseye.
# For more information, please refer to the <a
# href="https://wiki.debian.org/LTS">LTS section of the Debian Wiki</a>.
# </strong></p>

<p>Για να αποκτήσετε και να εγκαταστήσετε το Debian, δείτε την σελίδα 
<a href="debian-installer/">πληροφορίες Εγκατάστασης</a> και τον
<a href="installmanual">Οδηγό Εγκατάστασης</a>. Για να αναβαθμίσετε από μια
παλιότερη έκδοση του Debian, δείτε τις οδηγίες στις
<a href="releasenotes">Σημειώσεις της Έκδοσης</a>.</p>

### Activate the following when LTS period starts.
#<p>Architectures supported during Long Term Support:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Υπολογιστικές αρχιτεκτονκές που υποστηρίζονται στην αρχική κυκλοφορία του bullseye:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Σε αντίθεση με τις επιθυμίες μας, ίσως υπάρχουν μερικά προβλήματα στην έκδοση, έστω και αν 
έχει αναγγελθεί ως <em>σταθερή</em>. Έχουμε ετοιμάσει 
<a href="errata">μια λίστα με τα σημαντικότερα γνωστά προβλήματα</a>, και πάντα, βέβαια, μπορείτε να
<a href="../reportingbugs">αναφέρετε περισσότερα προβλήματα</a> σε μας.</p>

<p>Τέλος, αλλά πολύ σημαντικό, έχουμε φτιάξει μια λίστα <a href="credits">ατόμων που αξίζουν τα εύσημα</a> για την πραγματοποίηση της κυκλοφορίας αυτής της έκδοσης.</p>

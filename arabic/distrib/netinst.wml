#use wml::debian::template title="تثبيت دبيان من خلال الإنترنت" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83"

<p>
تتطلب هذه الطريقة توفر اتصال إنترنت <em>خلال</em> عملية التثبيت.
خلافا للطرق الأخرى ستنزِّل بيانات أقل وفقا لحتياجاتك. كل من الاتصال
السلكي واللاسلكي مدعومان. بطاقات ISDN الداخلية <em>ليست</em> مدعومة للأسف.
</p>
<p>هناك ثلاث خيارات للتثبيت من الشبكة:</p>

<toc-display />
<div class="line">
<div class="item col50">

<toc-add-entry name="smallcd">
أقراص مدمجة صغيرة أو مفاتيح الـ USB
</toc-add-entry>

<p>
الملفات التالية هي صور أقراص.
اختر البُنية المناسبة لمعالجك أدناه.
</p>

<stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>لمزيد من التفاصيل، يرجى مراجعة: <a href="../CD/netinst/">التثبيت الشبكي من القرص القياسي</a></p>

<div class="line">
<div class="item col50">

<toc-add-entry name="verysmall">
أقراص مدمجة جد صغيرة و مفاتيح الـ USB مخصصة إلخ.
</toc-add-entry>

<p>
يمكنك تنزيل بعض ملفات الصور صغيرة الحجم، مناسبة
لمفاتيح الـ USB والأجهزة المماثلة، يجب حرقها على الوسيط
والإقلاع من خلاله لبدء التثبيت.
</p>

<p>
هناك بعض الاختلافات بين البنُى فيما يخص دعم التثبيت من الأقراص الصغيرة.
</p>

<p>
لمزيد من التفاصيل، يُرجى الرجوع إلى
<a href="$(HOME)/releases/stable/installmanual">كُتيّب التثبيت الخاص بالبُنية</a>،
وخاصة الفصل
<q>الحصول على وسائط تثبيت النظام</q>.
</p>

<p>
هذه روابط ملفات الصور المتوفرة (لمزيد من المعلومات، يرجى قراءة الملف MANIFEST).
</p>

<stable-verysmall-images />
</div>
<div class="item col50 lastcol">

<toc-add-entry name="netboot">الإقلاع من الشبكة</toc-add-entry>

<p>
يمكنك إعداد خادوم TFTP و DHCP (أو BOOTP أو RARP) على شبكتك المحلية
ليعمل كوسيط للتثبيت. إن كان بيوس (BIOS) الأجهزة العميلة يدعم هذه الميزة،
يمكنك إذن إقلاع نظام تثبيت دبيان من خلال الشبكة
(باستخدام PXE و TFTP) ثم المضي قُدما في تثبيت ما تبقى من دبيان.
</p>

<p>
ليست كل الأجهزة تدعم الإقلاع من الشبكة.
لا يوصى بهذه الطريقة للمستخدمين المبتدئين بسبب الإعدادات الإضافية التي تتطلبها.
</p>

<p>
لمزيد من التفاصيل، يرجى الرجوع إلى
<a href="$(HOME)/releases/stable/installmanual">كُتيّب التثبيت الخاص بالبُنية</a>،
وخاصة الفصل
<q>إعداد الملفات للإقلاع من خلال الشبكة باستخدام TFTP</q>.
</p>
<p>
هذه روابط لملفات الصور (لمزيد من المعلومات، يرجى قراءة الملف MANIFEST):
</p>

<stable-netboot-images />
</div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
إن كان في نظامك عتاد <strong>يتطلب تحميل فيرموير غير حر</strong> مع تعريف الجهاز، يمكنك استخدام أحد
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
أرشيفات الحزم الخاصة بالفيرمويرات الشائعة</a> أو تنزيل صورة <strong>غير رسمية</strong>
تتضمن هذه الفيرمويرات <strong>غير الحرة</strong>.
تعليمات كيفية استخدام هذه الأرشيفات بالإضافة
لمعلومات حول كيفية تحميل الفيرموير خلال التثبيت متوفرة في
<a href="../releases/stable/amd64/ch06s04">دليل التثبيت</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">صور تثبيت غير رسمية للتوزيعة <q>المستقرة</q> تتضمن الفيرمويرات</a>
</p>
</div>

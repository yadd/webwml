<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in dwarfutils, a tool and
library for reading/consuming and writing/producing DWARF debugging
information. The Common Vulnerabilities and Exposures project
identifies the following issues:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8538">CVE-2015-8538</a>

    <p>A specially crafted ELF file can cause a segmentation fault.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8750">CVE-2015-8750</a>

    <p>A specially crafted ELF file can cause a NULL pointer
    dereference.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2050">CVE-2016-2050</a>

    <p>Out-of-bounds write</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2091">CVE-2016-2091</a>

    <p>Out-of-bounds read</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5034">CVE-2016-5034</a>

    <p>Out-of-bounds write</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5036">CVE-2016-5036</a>

    <p>Out-of-bounds read</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5038">CVE-2016-5038</a>

    <p>Out-of-bounds read</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5039">CVE-2016-5039</a>

    <p>Out-of-bounds read</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5042">CVE-2016-5042</a>

    <p>A specially crafted DWARF section can cause an infinite loop,
    reading from increasing memory addresses until the application
    crashes.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
20120410-2+deb7u2.</p>

<p>We recommend that you upgrade your dwarfutils packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-669.data"
# $Id: $

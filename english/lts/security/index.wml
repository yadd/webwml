#use wml::debian::template title="LTS Security Information" GEN_TIME="yes"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<toc-display/>

<toc-add-entry name="keeping-secure">Keeping your Debian LTS system secure</toc-add-entry>

<p>In order to receive the latest Debian LTS security advisories, subscribe to
the <a href="https://lists.debian.org/debian-lts-announce/">\
debian-lts-announce</a> mailing list.</p>

<p>For more information about security issues in Debian, please refer to
the <a href="../../security">Debian Security Information</a>.</p>

<a class="rss_logo" href="dla">RSS</a>
<toc-add-entry name="DLAS">Recent Advisories</toc-add-entry>

<p>These web pages include a condensed archive of security advisories posted to
the <a href="https://lists.debian.org/debian-lts-announce/">\
debian-lts-announce</a> list.

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/lts/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (titles only)" href="dla">
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (summaries)" href="dla-long">
:#rss#}
<p>The latest Debian LTS security advisories are also available in
<a href="dla">RDF format</a>. We also offer a
<a href="dla-long">second file</a> that includes the first paragraph
of the corresponding advisory so you can see in it what the advisory is
about.</p>

#include "$(ENGLISHDIR)/lts/security/index.include"
<p>Older security advisories are also available:
<:= get_past_sec_list(); :>

<!-- This section a copy from /security/dsa.wml. TODO: create and include file -->
<toc-add-entry name="infos">Sources of Security Information</toc-add-entry>

<ul>
<li><a href="https://security-tracker.debian.org/">Debian Security Tracker</a>
primary source for all security related information, search options</li>

<li><a href="https://security-tracker.debian.org/tracker/data/json">JSON list</a>
  contains CVE description, package name, Debian bug number, package versions with fix, no DSA included
</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DSA/list">DSA list</a>
  contains DSA including date, related CVE's numbers, package versions with fix
</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DLA/list">DLA list</a>
  contains DLA including date, related CVE's numbers, package versions with fix
</li>

<li><a href="https://lists.debian.org/debian-security-announce/">
DSA announcements</a></li>
<li><a href="https://lists.debian.org/debian-lts-announce/">
DLA announcements</a></li>

<li><a href="oval">Oval files</a></li>

<li>Lookup a DSA (uppercase is important)<br>
e.g. <tt>https://security-tracker.debian.org/tracker/DSA-3814</tt>
</li>

<li>Lookup a DLA ( -1 is important)<br>
e.g. <tt>https://security-tracker.debian.org/tracker/DLA-867-1</tt>
</li>

<li>Lookup a CVE<br>
e.g. <tt>https://security-tracker.debian.org/tracker/CVE-2017-6827</tt>
</li>
</ul>

<ul>
<li> <a href="https://lts-team.pages.debian.net/wiki/FAQ">Debian LTS FAQ</a>. Your question may well be answered there already!
<li>
<a href="https://lts-team.pages.debian.net/wiki/Contact">Debian LTS team contact information</a>
</ul>
<define-tag description>LTS security update</define-tag>
<define-tag moreinfo></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22885">CVE-2021-22885</a>

    <p>There is a possible information disclosure/unintended method
    execution vulnerability in Action Pack when using the
    `redirect_to` or `polymorphic_url` helper with untrusted user
    input.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22904">CVE-2021-22904</a>

    <p>There is a possible DoS vulnerability in the Token Authentication
    logic in Action Controller. Impacted code uses
    `authenticate_or_request_with_http_token` or
    `authenticate_with_http_token` for request authentication.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2:4.2.7.1-1+deb9u5.</p>

<p>We recommend that you upgrade your rails packages.</p>

<p>For the detailed security status of rails please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/rails">https://security-tracker.debian.org/tracker/rails</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2655.data"
# $Id: $

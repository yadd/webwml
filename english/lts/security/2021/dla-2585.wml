<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>libupnp, the portable SDK for UPnP Devices allows remote attackers to
cause a denial of service (crash) via a crafted SSDP message due to a
NULL pointer dereference in the functions FindServiceControlURLPath
and FindServiceEventURLPath in genlib/service_table/service_table.c.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1:1.6.19+git20160116-1.2+deb9u1.</p>

<p>We recommend that you upgrade your libupnp packages.</p>

<p>For the detailed security status of libupnp please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libupnp">https://security-tracker.debian.org/tracker/libupnp</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2585.data"
# $Id: $

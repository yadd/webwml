<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability in XStream, a Java library to serialize objects to and
from XML, may allow a remote attacker to execute commands of the host
only by manipulating the processed input stream.
<p><b>Note</b>: the XStream project recommends to setup its security framework
with a whitelist limited to the minimal required types, rather than
relying on the black list (which got updated to address this
vulnerability). The project is also phasing out maintainance of the
black list, see <a href="https://x-stream.github.io/security.html">https://x-stream.github.io/security.html</a> .</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.4.11.1-1+deb9u3.</p>

<p>We recommend that you upgrade your libxstream-java packages.</p>

<p>For the detailed security status of libxstream-java please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libxstream-java">https://security-tracker.debian.org/tracker/libxstream-java</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2704.data"
# $Id: $

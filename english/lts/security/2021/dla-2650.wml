<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The Qualys Research Labs reported several vulnerabilities in Exim, a mail
transport agent, which could result in local privilege escalation and
remote code execution.</p>

<p>Details can be found in the Qualys advisory at
<a href="https://www.qualys.com/2021/05/04/21nails/21nails.txt">https://www.qualys.com/2021/05/04/21nails/21nails.txt</a></p>


<p>For Debian 9 stretch, these problems have been fixed in version
4.89-2+deb9u8.</p>

<p>We recommend that you upgrade your exim4 packages.</p>

<p>For the detailed security status of exim4 please refer to its security
tracker page at: <a href="https://security-tracker.debian.org/tracker/exim4">https://security-tracker.debian.org/tracker/exim4</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2650.data"
# $Id: $

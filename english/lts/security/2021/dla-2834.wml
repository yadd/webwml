<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Out-of-bounds read for an incomplete URI with an IPv6 address containing
an embedded IPv4 address has been fixed in uriparser, a library to parse 
Uniform Resource Identifiers (URIs).</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.8.4-1+deb9u2.</p>

<p>We recommend that you upgrade your uriparser packages.</p>

<p>For the detailed security status of uriparser please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/uriparser">https://security-tracker.debian.org/tracker/uriparser</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2834.data"
# $Id: $

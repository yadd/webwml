<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in LibreCAD, an application
for computer aided design (CAD) in two dimensions. An attacker could
trigger code execution through malicious .dwg and .dxf files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21898">CVE-2021-21898</a>

    <p>A code execution vulnerability exists in the
    dwgCompressor::decompress18() functionality of LibreCad
    libdxfrw. A specially-crafted .dwg file can lead to an
    out-of-bounds write.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21899">CVE-2021-21899</a>

    <p>A code execution vulnerability exists in the
    dwgCompressor::copyCompBytes21 functionality of LibreCad
    libdxfrw. A specially-crafted .dwg file can lead to a heap buffer
    overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21900">CVE-2021-21900</a>

    <p>A code execution vulnerability exists in the dxfRW::processLType()
    functionality of LibreCad libdxfrw. A specially-crafted .dxf file
    can lead to a use-after-free vulnerability.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.1.2-1+deb9u2.</p>

<p>We recommend that you upgrade your librecad packages.</p>

<p>For the detailed security status of librecad please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/librecad">https://security-tracker.debian.org/tracker/librecad</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2838.data"
# $Id: $

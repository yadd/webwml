<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Version 3.9.6-11+deb7u1 and 3.9.6-11+deb7u2 introduced changes that
resulted in libtiff writing out invalid tiff files when the compression
scheme in use relies on codec-specific TIFF tags embedded in the image.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.9.6-11+deb7u3.</p>

<p>We recommend that you upgrade your tiff3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-610-2.data"
# $Id: $

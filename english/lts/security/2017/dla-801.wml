<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Tobias Stoeckmann discovered a vulnerability in the libXpm library
that could cause a malicious attacker to execute arbitrary code
via a specially crafted XPM file.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:3.5.10-1+deb7u1.</p>

<p>We recommend that you upgrade your libxpm packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-801.data"
# $Id: $

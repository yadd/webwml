<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in qemu-kvm, a full
virtualization solution for Linux hosts on x86 hardware with x86 guests
based on the Quick Emulator(Qemu).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14167">CVE-2017-14167</a>

    <p>Incorrect validation of multiboot headers could result in the
    execution of arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15038">CVE-2017-15038</a>

    <p>When using 9pfs qemu-kvm is vulnerable to an information
    disclosure issue. It could occur while accessing extended attributes
    of a file due to a race condition. This could be used to disclose
    heap memory contents of the host.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.2+dfsg-6+deb7u24.</p>

<p>We recommend that you upgrade your qemu-kvm packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1128.data"
# $Id: $

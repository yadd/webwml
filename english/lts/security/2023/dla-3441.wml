<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a series of heap overflow and integer
overflow vulnerabilities in Sofia-SIP, a building block for creating VoIP/SIP
and instant messaging applications.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32307">CVE-2023-32307</a>

    <p>Sofia-SIP is an open-source SIP User-Agent library, compliant with the
    IETF RFC3261 specification. Referring to
    <a href="https://github.com/freeswitch/sofia-sip/security/advisories/GHSA-8599-x7rq-fr54">GHSA-8599-x7rq-fr54</a>
    several other potential heap-over-flow and integer-overflow in
    <code>stun_parse_attr_error_code</code> and
    <code>stun_parse_attr_uint32</code> were found because the lack of
    attributes length check when Sofia-SIP handles STUN packets.  The previous
    patch of
    <a href="https://github.com/freeswitch/sofia-sip/security/advisories/GHSA-8599-x7rq-fr54">GHSA-8599-x7rq-fr54</a>
    fixed the vulnerability when attr_type did not match the enum value, but
    there are also vulnerabilities in the handling of other valid cases. The
    OOB read and integer-overflow made by attacker may lead to crash, high
    consumption of memory or even other more serious consequences. These issue
    have been addressed in version 1.13.15. Users are advised to upgrade.</li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1.12.11+20110422.1-2.1+deb10u4.</p>

<p>We recommend that you upgrade your sofia-sip packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3441.data"
# $Id: $

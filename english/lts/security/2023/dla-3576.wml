<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A buffer overflow can occur when calculating the quantile value using the
Statistics Library of GSL (GNU Scientific Library). Processing a
maliciously crafted input data for gsl_stats_quantile_from_sorted_data of
the library may lead to unexpected application termination or arbitrary
code execution.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.5+dfsg-6+deb10u1.</p>

<p>We recommend that you upgrade your gsl packages.</p>

<p>For the detailed security status of gsl please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/gsl">https://security-tracker.debian.org/tracker/gsl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3576.data"
# $Id: $

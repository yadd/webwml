<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Security issues were discovered in python-reportlab, a Python library
for generating PDFs and graphics, which could lead to remote code
execution or authorization bypass.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19450">CVE-2019-19450</a>

    <p>Ravi Prakash Giri discovered a remote code execution vulnerability
    via crafted XML document where <code>&lt;unichar code="</code> is followed by
    arbitrary Python code.</p>

    <p>This issue is similar to <a href="https://security-tracker.debian.org/tracker/CVE-2019-17626">CVE-2019-17626</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28463">CVE-2020-28463</a>

    <p>Karan Bamal discovered a Server-side Request Forgery (SSRF)
    vulnerability via <code>&lt;img&gt;</code> tags.
    New settings <code>trustedSchemes</code> and <code>trustedHosts</code>
    have been added as part of the fix/mitigation: they can be used to
    specify an explicit allowlist for remote sources.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
3.5.13-1+deb10u2.</p>

<p>We recommend that you upgrade your python-reportlab packages.</p>

<p>For the detailed security status of python-reportlab please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python-reportlab">https://security-tracker.debian.org/tracker/python-reportlab</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3590.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in otrs2, the Open-Source Ticket
Request System, which could lead to impersonation, denial of service,
information disclosure, or execution of arbitrary code.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11358">CVE-2019-11358</a>

    <p>A Prototype Pollution vulnerability was discovered in OTRS' embedded
    jQuery 3.2.1 copy, which could allow sending drafted messages as
    wrong agent.</p>

    <p>This vulnerability is also known as OSA-2020-05.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12248">CVE-2019-12248</a>

    <p>Matthias Terlinde discovered that when an attacker sends a malicious
    email to an OTRS system and a logged in agent user later quotes it,
    the email could cause the browser to load external image resources.</p>

    <p>A new configuration setting <code>Ticket::Frontend::BlockLoadingRemoteContent</code>
    has been added as part of the fix.  It controls whether external
    content should be loaded, and it is disabled by default.</p>

    <p>This vulnerability is also known as OSA-2019-08.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12497">CVE-2019-12497</a>

    <p>Jens Meister discovered that in the customer or external frontend,
    personal information of agents, like Name and mail address in
    external notes, could be disclosed.</p>

    <p>New configuration settings <code>Ticket::Frontend::CustomerTicketZoom###DisplayNoteFrom</code>
    has been added as part of the fix.  It controls if agent information
    should be displayed in external note sender field, or be substituted
    with a different generic name.  Another option named
    <code>Ticket::Frontend::CustomerTicketZoom###DefaultAgentName</code> can then
    be used to define the generic agent name used in the latter case.
    By default, previous behavior is preserved, in which agent
    information is divulged in the external note From field, for the
    sake of backwards compatibility.</p>

    <p>This vulnerability is also known as OSA-2019-09.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12746">CVE-2019-12746</a>

    <p>A user logged into OTRS as an agent might unknowingly disclose their
    session ID by sharing the link of an embedded ticket article with
    third parties.  This identifier can be then potentially abused in
    order to impersonate the agent user.</p>

    <p>This vulnerability is also known as OSA-2019-10.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13458">CVE-2019-13458</a>

    <p>An attacker who is logged into OTRS as an agent user with
    appropriate permissions can leverage OTRS tags in templates in order
    to disclose hashed user passwords.</p>

    <p>This vulnerability is also known as OSA-2019-12.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16375">CVE-2019-16375</a>

    <p>An attacker who is logged into OTRS as an agent or customer user
    with appropriate permissions can create a carefully crafted string
    containing malicious JavaScript code as an article body.  This
    malicious code is executed when an agent compose an answer to the
    original article.</p>

    <p>This vulnerability is also known as OSA-2019-13.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18179">CVE-2019-18179</a>

    <p>An attacker who is logged into OTRS as an agent is able to list
    tickets assigned to other agents, which are in the queue where
    attacker doesn't have permissions.</p>

    <p>This vulnerability is also known as OSA-2019-14.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18180">CVE-2019-18180</a>

    <p>OTRS can be put into an endless loop by providing filenames with
    overly long extensions.  This applies to the PostMaster (sending in
    email) and also upload (attaching files to mails, for example).</p>

    <p>This vulnerability is also known as OSA-2019-15.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1765">CVE-2020-1765</a>

    <p>Sebastian Renker and Jonas Becker discovered an improper control of
    parameters, which allows the spoofing of the From fields in several
    screens, namely AgentTicketCompose, AgentTicketForward,
    AgentTicketBounce and AgentTicketEmailOutbound.</p>

    <p>This vulnerability is also known as OSA-2020-01.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1766">CVE-2020-1766</a>

    <p>Anton Astaf'ev discovered that due to improper handling of uploaded
    images, it is possible — in very unlikely and rare conditions — to
    force the agents browser to execute malicious JavaScript from a
    special crafted SVG file rendered as inline jpg file.</p>

    <p>This vulnerability is also known as OSA-2020-02.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1767">CVE-2020-1767</a>

    <p>Agent A is able to save a draft (i.e., for customer reply).  Then
    Agent B can open the draft, change the text completely and send it
    in the name of Agent A.  For the customer it will not be visible
    that the message was sent by another agent.</p>

    <p>This vulnerability is also known as OSA-2020-03.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1769">CVE-2020-1769</a>

    <p>Martin Møller discovered that in the login screens (in agent and
    customer interface), Username and Password fields use autocomplete,
    which might be considered as security issue.</p>

    <p>A new configuration setting <code>DisableLoginAutocomplete</code> has been
    added as part of the fix.  It controls whether to disable
    autocompletion in the login forms, by setting the
    <code>autocomplete="off"</code> attribute to the login input fields.  Note that
    some browsers ignore it by default (usually it can be changed in the
    browser configuration).</p>

    <p>This vulnerability is also known as OSA-2020-06.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1770">CVE-2020-1770</a>

    <p>Matthias Terlinde discovered that the support bundle generated files
    could contain sensitive information, such as user credentials.</p>

    <p>This vulnerability is also known as OSA-2020-07.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1771">CVE-2020-1771</a>

    <p>Christoph Wuetschne discovered that an attacker is able craft an
    article with a link to the customer address book with malicious
    content (JavaScript).  When agent opens the link, JavaScript code is
    executed due to the missing parameter encoding.</p>

    <p>This vulnerability is also known as OSA-2020-08.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1772">CVE-2020-1772</a>

    <p>Fabian Henneke discovered that it is possible to craft Lost Password
    requests with wildcards in the Token value, which allows an attacker
    to retrieve valid Token(s), generated by users which already
    requested new passwords.</p>

    <p>This vulnerability is also known as OSA-2020-09.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1773">CVE-2020-1773</a>

    <p>Fabian Henneke discovered that an attacker with the ability to
    generate session IDs or password reset tokens, either by being able
    to authenticate or by exploiting <a href="https://security-tracker.debian.org/tracker/CVE-2020-1772">CVE-2020-1772</a>, may be able to
    predict other users session IDs, password reset tokens and
    automatically generated passwords.</p>

    <p>The fix adds <code>libmath-random-secure-perl</code> to otrs2's
    Depends: field.</p>

    <p>This vulnerability is also known as OSA-2020-10.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1774">CVE-2020-1774</a>

    <p>When a user downloads PGP or S/MIME keys/certificates, exported file
    has same name for private and public keys.  It is therefore possible
    to mix them and to send private key to the third-party instead of
    public key.</p>

    <p>This vulnerability is also known as OSA-2020-11.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1776">CVE-2020-1776</a>

    <p>When an agent user is renamed or set to invalid the session
    belonging to the user is keept active.  The session can not be used
    to access ticket data in the case the agent is invalid.</p>

    <p>This vulnerability is also known as OSA-2020-13.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11022">CVE-2020-11022</a>

    <p>Masato Kinugawa discovered a Potential XSS vulnerability in OTRS'
    embedded jQuery 3.2.1's htmlPrefilter and related methods.</p>

    <p>The fix requires patching embedded copies of fullcalendar (3.4.0),
    fullcalendar-scheduler (1.6.2) and spectrum (1.8.0).</p>

    <p>This vulnerability is also known as OSA-2020-14.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11023">CVE-2020-11023</a>

    <p>Masato Kinugawa discovered a Potential XSS vulnerability in OTRS'
    embedded jQuery 3.2.1 copy when appending HTML containing option
    elements.</p>

    <p>This vulnerability is also known as OSA-2020-14.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21252">CVE-2021-21252</a>

    <p>Erik Krogh Kristensen and Alvaro Muñoz from the GitHub Security Lab
    team discovered a Regular Expression Denial of Service (ReDoS)
    vulnerability in OTRS' embedded jQuery-validate 1.16.0 copy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21439">CVE-2021-21439</a>

    <p>A Denial of Service (DoS) attack can be performed when an email
    contains specially designed URL in the body.  It can lead to the
    high CPU usage and cause low quality of service, or in extreme case
    bring the system to a halt.</p>

    <p>This vulnerability is also known as OSA-2021-09 or ZSA-2021-03.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21440">CVE-2021-21440</a>

    <p>Julian Droste and Mathias Terlinde discovered that the Generated
    Support Bundles contains private S/MIME and PGP keys when the parent
    directory is not hidden.  Furthermore, secrets and PIN for the keys
    are not masked properly.</p>

    <p>This vulnerability is also known as OSA-2021-10 or ZSA-2021-08.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21441">CVE-2021-21441</a>

    <p>There is a Cross-Site Scripting (XSS) vulnerability in the ticket
    overview screens.  It is possible to collect various information by
    having an e-mail shown in the overview screen.  An attack can be
    performed by sending specially crafted e-mail to the system, which
    does not require any user interaction.</p>

    <p>This vulnerability is also known as OSA-2021-11 or ZSA-2021-06.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21443">CVE-2021-21443</a>

    <p>Agents are able to list customer user emails without required
    permissions in the bulk action screen.</p>

    <p>This vulnerability is also known as OSA-2021-13 or ZSA-2021-09.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36091">CVE-2021-36091</a>

    <p>Agents are able to list appointments in the calendars without
    required permissions.</p>

    <p>This vulnerability is also known as OSA-2021-14 or ZSA-2021-10.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36100">CVE-2021-36100</a>

    <p>Rayhan Ahmed and Maxime Brigaudeau discovered that a specially
    crafted string in the system configuration allows execution of
    arbitrary system command.</p>

    <p>The fix 1/ removes configurable system commands from generic agents;
    2/ removes the <code>MIME-Viewer###…</code> settings (the system command in
    SysConfig option <code>MIME-Viewer</code> is now only configurable via
    Kernel/Config.pm); 3/ removes dashboard widget support for execution
    of system commands; and 4/ deactivates support for execution of
    configurable system commands from Sendmail and PostMaster pre-filter
    configurations.</p>

    <p>This vulnerability is also known as OSA-2022-03 or ZSA-2022-02.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41182">CVE-2021-41182</a>

    <p>Esben Sparre Andreasen discovered an XSS vulnerability in the
    <code>altField</code> option of the Datepicker widget in OTRS' embedded
    jQuery-UI 1.12.1 copy.</p>

    <p>This vulnerability is also known as ZSA-2022-01.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41183">CVE-2021-41183</a>

    <p>Esben Sparre Andreasen discovered an XSS vulnerability in the
    <code>*Text</code> options of the Datepicker widget in OTRS' embedded jQuery-UI
    1.12.1 copy.</p>

    <p>This vulnerability is also known as ZSA-2022-01.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41184">CVE-2021-41184</a>

    <p>Esben Sparre Andreasen discovered an XSS vulnerability in the <code>of</code>
    option of the <code>.position()</code> util in OTRS' embedded jQuery-UI 1.12.1
    copy.</p>

    <p>This vulnerability is also known as ZSA-2022-01.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4427">CVE-2022-4427</a>

    <p>Tim Püttmanns discovered an SQL injection vulnerability in
    <code>Kernel::System::Ticket::TicketSearch</code>, which can be exploited using
    the web service operation <q>TicketSearch</q>.</p>

    <p>This vulnerability is also known as ZSA-2022-07.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38060">CVE-2023-38060</a>

    <p>Tim Püttmanns discovered an Improper Input Validation vulnerability
    in the ContentType parameter for attachments on TicketCreate or
    TicketUpdate operations.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
6.0.16-2+deb10u1.</p>

<p>We recommend that you upgrade your otrs2 packages.</p>

<p>For the detailed security status of otrs2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/otrs2">https://security-tracker.debian.org/tracker/otrs2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3551.data"
# $Id: $

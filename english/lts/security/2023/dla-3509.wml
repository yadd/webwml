<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the domain check in libmail-dkim-perl, a Perl module to
cryptographically identify the sender of email, compares i and d tags case
sensitive when t=s is set on the DKIM key which causes spurious fails of
legitimate messages.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.54-1+deb10u1.</p>

<p>We recommend that you upgrade your libmail-dkim-perl packages.</p>

<p>For the detailed security status of libmail-dkim-perl please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libmail-dkim-perl">https://security-tracker.debian.org/tracker/libmail-dkim-perl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3509.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in runc, the Open Container
Project runtime, which is often used with virtualization environments
such as Docker. Malicious Docker images or OCI bundles could breach
isolation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16884">CVE-2019-16884</a>

    <p>runc, as used in Docker and other products, allows AppArmor and
    SELinux restriction bypass because libcontainer/rootfs_linux.go
    incorrectly checks mount targets, and thus a malicious Docker
    image can mount over a /proc directory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19921">CVE-2019-19921</a>

    <p>runc has Incorrect Access Control leading to Escalation of
    Privileges, related to libcontainer/rootfs_linux.go. To exploit
    this, an attacker must be able to spawn two containers with custom
    volume-mount configurations, and be able to run custom
    images. (This vulnerability does not affect Docker due to an
    implementation detail that happens to block the attack.)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30465">CVE-2021-30465</a>

    <p>runc allows a Container Filesystem Breakout via Directory
    Traversal. To exploit the vulnerability, an attacker must be able
    to create multiple containers with a fairly specific mount
    configuration. The problem occurs via a symlink-exchange attack
    that relies on a race condition.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29162">CVE-2022-29162</a>

    <p>`runc exec --cap` created processes with non-empty inheritable
    Linux process capabilities, creating an atypical Linux environment
    and enabling programs with inheritable file capabilities to
    elevate those capabilities to the permitted set during
    execve(2). This bug did not affect the container security sandbox
    as the inheritable set never contained more capabilities than were
    included in the container's bounding set.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27561">CVE-2023-27561</a>

    <p><a href="https://security-tracker.debian.org/tracker/CVE-2019-19921">CVE-2019-19921</a> was re-introduced by the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2021-30465">CVE-2021-30465</a>.</p></li>

</ul>

<p>For Debian 10 buster, this problem has been fixed in version
1.0.0~rc6+dfsg1-3+deb10u2.</p>

<p>We recommend that you upgrade your runc packages.</p>

<p>For the detailed security status of runc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/runc">https://security-tracker.debian.org/tracker/runc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3369.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update fixes multiple vulnerabilities concerning the <code>urlparse</code> module as
well as vulnerabilities concerning the <code>heapq</code>, <code>hmac</code>, <code>plistlib</code> and <code>ssl</code> modules.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23336">CVE-2021-23336</a>

    <p>Python was vulnerable to Web Cache Poisoning via <code>urlparse.parse_qsl</code> and
    <code>urlparse.parse_qs</code> by using a vector called parameter cloaking. When the
    attacker can separate query parameters using a semicolon (<code>;</code>), they can
    cause a difference in the interpretation of the request between the proxy
    (running with default configuration) and the server. This can result in
    malicious requests being cached as completely safe ones, as the proxy would
    usually not see the semicolon as a separator, and therefore would not
    include it in a cache key of an unkeyed parameter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0391">CVE-2022-0391</a>

    <p>The <code>urlparse</code> module helps break Uniform Resource Locator (URL) strings into
    components. The issue involves how the urlparse method does not sanitize
    input and allows characters like <code>'\r'</code> and <code>'\n'</code> in the URL path.  This flaw
    allows an attacker to input a crafted URL, leading to injection attacks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-48560">CVE-2022-48560</a>

    <p>A use-after-free exists in Python via <code>heappushpop</code> in <code>heapq</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-48565">CVE-2022-48565</a>

    <p>An XML External Entity (XXE) issue was discovered in Python.  The <code>plistlib</code>
    module no longer accepts entity declarations in XML plist files to avoid
    XML vulnerabilities.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-48566">CVE-2022-48566</a>

    <p>An issue was discovered in <code>compare_digest</code> in <code>Lib/hmac.py</code> in Python.
    Constant-time-defeating optimisations were possible in the accumulator
    variable in <code>hmac.compare_digest</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24329">CVE-2023-24329</a>

    <p>An issue in the <code>urlparse</code> component of Python allows attackers to bypass
    blocklisting methods by supplying a URL that starts with blank characters.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-40217">CVE-2023-40217</a>

    <p>The issue primarily affects servers written in Python (such as HTTP
    servers) that use TLS client authentication. If a TLS server-side socket is
    created, receives data into the socket buffer, and then is closed quickly,
    there is a brief window where the <code>SSLSocket</code> instance will detect the socket
    as <q>not connected</q> and won't initiate a handshake, but buffered data will
    still be readable from the socket buffer.  This data will not be
    authenticated if the server-side TLS peer is expecting client certificate
    authentication, and is indistinguishable from valid TLS stream data. Data
    is limited in size to the amount that will fit in the buffer. (The TLS
    connection cannot directly be used for data exfiltration because the
    vulnerable code path requires that the connection be closed on
    initialization of the <code>SSLSocket</code>.)</p>


<p>For Debian 10 buster, these problems have been fixed in version
2.7.16-2+deb10u3.</p>

<p>We recommend that you upgrade your python2.7 packages.</p>

<p>For the detailed security status of python2.7 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python2.7">https://security-tracker.debian.org/tracker/python2.7</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3575.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in libapache2-mod-auth-openidc,
an OpenID Connect Relying Party implementation for Apache.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20479">CVE-2019-20479</a>

    <p>Insufficient validatation of URLs beginning with a slash and backslash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32785">CVE-2021-32785</a>

    <p>Crash when using an unencrypted Redis cache.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32786">CVE-2021-32786</a>

    <p>Open Redirect vulnerability in the logout functionality.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32791">CVE-2021-32791</a>

    <p>AES GCM encryption in used static IV and AAD.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32792">CVE-2021-32792</a>

    <p>XSS vulnerability when using OIDCPreservePost.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28625">CVE-2023-28625</a>

    <p>NULL pointer dereference with OIDCStripCookies.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.3.10.2-1+deb10u2.</p>

<p>We recommend that you upgrade your libapache2-mod-auth-openidc packages.</p>

<p>For the detailed security status of libapache2-mod-auth-openidc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libapache2-mod-auth-openidc">https://security-tracker.debian.org/tracker/libapache2-mod-auth-openidc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3409.data"
# $Id: $

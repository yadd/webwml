<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Letian Yuan discovered a flaw in Apache Axis 1.x, a SOAP implementation written
in Java. It may not have been obvious that looking up a service through
<q>ServiceFactory.getService</q> allows potentially dangerous lookup mechanisms such
as LDAP. When passing untrusted input to this API method, this could expose the
application to DoS, SSRF and even attacks leading to remote code execution.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.4-28+deb10u1.</p>

<p>We recommend that you upgrade your axis packages.</p>

<p>For the detailed security status of axis please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/axis">https://security-tracker.debian.org/tracker/axis</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3622.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that syslog-ng, a system logging daemon, had integer
overflow and buffer out-of-bounds issues, which could allow a remote
attacker to cause Denial of Service via crafted syslog input.</p>

<p>For Debian 10 buster, this problem has been fixed in version
3.19.1-5+deb10u1.</p>

<p>We recommend that you upgrade your syslog-ng packages.</p>

<p>For the detailed security status of syslog-ng please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/syslog-ng">https://security-tracker.debian.org/tracker/syslog-ng</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3348.data"
# $Id: $

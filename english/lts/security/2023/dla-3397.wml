<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential denial of service vulnerabilty
in connman, a command-line network manager designed for use on embedded
devices.</p>

<p>Network-adjacent attackers operating a crafted DHCP server could have caused
a stack-based buffer overflow, resulting in a denial of service through
terminating the connman process.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28488">CVE-2023-28488</a>

    <p>client.c in gdhcp in ConnMan through 1.41 could be used by
    network-adjacent attackers (operating a crafted DHCP server) to cause a
    stack-based buffer overflow and denial of service, terminating the connman
    process.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1.36-2.1~deb10u4.</p>

<p>We recommend that you upgrade your connman packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3397.data"
# $Id: $

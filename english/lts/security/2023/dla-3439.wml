<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential arbitrary code execution
vulnerability in <code>libwebp</code>, a library to support the WebP image
compression format.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1999">CVE-2023-1999</a></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
0.6.1-2+deb10u2.</p>

<p>We recommend that you upgrade your libwebp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3439.data"
# $Id: $

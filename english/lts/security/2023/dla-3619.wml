<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Batik is a toolkit for applications or applets that want to use images
in the Scalable Vector Graphics (SVG) format for various purposes,
such as viewing, generation or manipulation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11987">CVE-2020-11987</a>

    <p>A server-side request forgery was found,
    caused by improper input validation by the NodePickerPanel.
    By using a specially-crafted argument, an attacker could exploit
    this vulnerability to cause the underlying server to make
    arbitrary GET requests.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38398">CVE-2022-38398</a>

    <p>A Server-Side Request Forgery (SSRF) vulnerability
    was found that allows an attacker to load a url thru the jar
    protocol.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38648">CVE-2022-38648</a>

    <p>A Server-Side Request Forgery (SSRF) vulnerability
    was found that allows an attacker to fetch external resources.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40146">CVE-2022-40146</a>

    <p>A Server-Side Request Forgery (SSRF) vulnerability
    was found that allows an attacker to access files using a Jar url.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44729">CVE-2022-44729</a>

    <p>A Server-Side Request Forgery (SSRF) vulnerability
    was found. A malicious SVG could trigger loading external resources
    by default, causing resource consumption or in some
    cases even information disclosure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44730">CVE-2022-44730</a>

    <p>A Server-Side Request Forgery (SSRF) vulnerability
    was found. A malicious SVG can probe user profile / data and send
    it directly as parameter to a URL.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.10-2+deb10u3.</p>

<p>We recommend that you upgrade your batik packages.</p>

<p>For the detailed security status of batik please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/batik">https://security-tracker.debian.org/tracker/batik</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3619.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were found in libx11, the X11 client-side
library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-43785">CVE-2023-43785</a>

    <p>Gregory James Duck discovered an out of bounds memory access in
    _XkbReadKeySyms, which could result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-43786">CVE-2023-43786</a>

    <p>Yair Mizrahi found an infinite recursion in PutSubImage when
    parsing a crafted file, which would result in stack exhaustion
    and denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-43787">CVE-2023-43787</a>

    <p>Yair Mizrahi discovered an integer overflow in XCreateImage
    when parsing crafted input, which would result in a small buffer
    allocation leading into a buffer overflow. This could result
    in denial of service or potentially in arbitrary code execution.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2:1.6.7-1+deb10u4.</p>

<p>We recommend that you upgrade your libx11 packages.</p>

<p>For the detailed security status of libx11 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libx11">https://security-tracker.debian.org/tracker/libx11</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3602.data"
# $Id: $

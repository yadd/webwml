<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security issues were found in Curl, an easy-to-use client-side URL
transfer library and command line tool.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28321">CVE-2023-28321</a>

    <p>Hiroki Kurosawa found that curl could mismatch hostnames with
    wildcards when using its own name matching function.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38546">CVE-2023-38546</a>

    <p>It was discovered that under some circumstances libcurl was
    susceptible to cookie injection.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
7.64.0-4+deb10u7.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>For the detailed security status of curl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/curl">https://security-tracker.debian.org/tracker/curl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3613.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A potential authorisation bypass vulnerability was discovered in Apache
Zookeeper, a co-ordination service for reliable distributed applications.</p>

<p>Specifically, if SASL Quorum Peer authentication was enabled via
<code>quorum.auth.enableSasl</code>, authorisation was performed by verifying
that the instance part in the SASL authentication ID was listed in the
<code>zoo.cfg</code> server list. However, this value is optional, and, if
missing (such as in <code>eve@EXAMPLE.COM</code>), the authorisation check will
be skipped. As a result, an arbitrary endpoint could join the cluster and begin
propagating counterfeit changes to the leader, essentially giving it complete
read-write access to the data tree.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-44981">CVE-2023-44981</a>

    <p>Authorization Bypass Through User-Controlled Key vulnerability in Apache ZooKeeper. If SASL Quorum Peer authentication is enabled in ZooKeeper (quorum.auth.enableSasl=true), the authorization is done by verifying that the instance part in SASL authentication ID is listed in zoo.cfg server list. The instance part in SASL auth ID is optional and if it's missing, like 'eve@EXAMPLE.COM', the authorization check will be skipped. As a result an arbitrary endpoint could join the cluster and begin propagating counterfeit changes to the leader, essentially giving it complete read-write access to the data tree. Quorum Peer authentication is not enabled by default. Users are recommended to upgrade to version 3.9.1, 3.8.3, 3.7.2, which fixes the issue. Alternately ensure the ensemble election/quorum communication is protected by a firewall as this will mitigate the issue. See the documentation for more details on correct cluster administration.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
3.4.13-2+deb10u1.</p>

<p>We recommend that you upgrade your zookeeper packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3624.data"
# $Id: $

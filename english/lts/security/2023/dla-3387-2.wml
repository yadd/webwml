<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A regression was reported that the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2021-3802">CVE-2021-3802</a> broken mounting
allow-listed mount option/value pairs, for example errors=remount-ro.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.8.1-4+deb10u2.</p>

<p>We recommend that you upgrade your udisks2 packages.</p>

<p>For the detailed security status of udisks2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/udisks2">https://security-tracker.debian.org/tracker/udisks2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3387-2.data"
# $Id: $

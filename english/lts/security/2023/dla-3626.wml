<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Potential freeing of an uninitialized pointer in kadm_rpc_xdr.c
was fixed in krb5, the MIT implementation of the Kerberos network
authentication protocol.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.17-3+deb10u6.</p>

<p>We recommend that you upgrade your krb5 packages.</p>

<p>For the detailed security status of krb5 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/krb5">https://security-tracker.debian.org/tracker/krb5</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3626.data"
# $Id: $

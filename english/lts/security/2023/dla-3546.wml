<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue in the <tt>opendmarc</tt> DMARC
email filter system. A vulnerability allowed attackers to inject authentication
results to provide false information about the domain that originated an email
message. This was caused by incorrect parsing and interpretation of SPF/DKIM
authentication results.</p>


<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12272">CVE-2020-12272</a>

    <p>OpenDMARC through 1.3.2 and 1.4.x allows attacks that inject
    authentication results to provide false information about the domain that
    originated an e-mail message. This is caused by incorrect parsing and
    interpretation of SPF/DKIM authentication results, as demonstrated by the
    example.net(.example.com substring.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1.3.2-6+deb10u3.</p>

<p>We recommend that you upgrade your opendmarc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3546.data"
# $Id: $

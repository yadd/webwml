<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities have been fixed in sqlite (V2) which which might
allow local users to obtain sensitive information, cause a denial of
service (application crash), or have unspecified other impact.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6153">CVE-2016-6153</a>

<p>sqlite improperly implemented the temporary directory search algorithm,
which might allow local users to obtain sensitive information, cause a
denial of service (application crash), or have unspecified other impact
by leveraging use of the current working directory for temporary files.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8740">CVE-2018-8740</a>

<p>Databases whose schema is corrupted using a CREATE TABLE AS statement
could cause a NULL pointer dereference,</p>


<p>For Debian 10 buster, these problems have been fixed in version
2.8.17-15+deb10u1.</p>

<p>We recommend that you upgrade your sqlite packages.</p>

<p>For the detailed security status of sqlite please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sqlite">https://security-tracker.debian.org/tracker/sqlite</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3431.data"
# $Id: $

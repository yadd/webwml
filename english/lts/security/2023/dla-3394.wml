<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A flaw was found in Asterisk, an Open Source Private Branch Exchange. A
buffer overflow vulnerability affects users that use PJSIP DNS resolver.
This vulnerability is related to <a href="https://security-tracker.debian.org/tracker/CVE-2022-24793">CVE-2022-24793</a>. The difference is that
this issue is in parsing the query record `parse_query()`, while the issue
in <a href="https://security-tracker.debian.org/tracker/CVE-2022-24793">CVE-2022-24793</a> is in `parse_rr()`. A workaround is to disable DNS
resolution in PJSIP config (by setting `nameserver_count` to zero) or use
an external resolver implementation instead.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1:16.28.0~dfsg-0+deb10u3.</p>

<p>We recommend that you upgrade your asterisk packages.</p>

<p>For the detailed security status of asterisk please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/asterisk">https://security-tracker.debian.org/tracker/asterisk</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3394.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in tiff, a library and tools
providing support for the Tag Image File Format (TIFF).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2908">CVE-2023-2908</a>

    <p>NULL pointer dereference in tif_dir.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3316">CVE-2023-3316</a>

    <p>NULL pointer dereference in TIFFClose()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3618">CVE-2023-3618</a>

    <p>Buffer overflow in tiffcrop</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-25433">CVE-2023-25433</a>

    <p>Buffer overflow in tiffcrop</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-26965">CVE-2023-26965</a>

    <p>Use after free in tiffcrop</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-26966">CVE-2023-26966</a>

    <p>Buffer overflow in uv_encode()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-40745">CVE-2023-40745</a>

    <p>Integer overflow in tiffcp</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41175">CVE-2023-41175</a>

    <p>Integer overflow in raw2tiff</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
4.1.0+git191117-2~deb10u8.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>For the detailed security status of tiff please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tiff">https://security-tracker.debian.org/tracker/tiff</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3513.data"
# $Id: $

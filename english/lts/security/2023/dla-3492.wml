<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilties have been found in yajl, a JSON parser / small
validating JSON generator# written in ANSI C, which potentially can
cause memory corruption or DoS.</p>

<p>The <a href="https://security-tracker.debian.org/tracker/CVE-20117-16516">CVE-20117-16516</a> had been addressed already in DLA-3478, however the
fix has been found to be incomplete as it missed an additional memory
leak.  This update fixes that problem.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16516">CVE-2017-16516</a>

  <p>When a crafted JSON file is supplied to yajl, the process might
  crash with a SIGABRT in the yajl_string_decode function in
  yajl_encode.c. This results potentially in a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24795">CVE-2022-24795</a>

  <p>The 1.x branch and the 2.x branch of `yajl` contain an integer
  overflow which leads to subsequent heap memory corruption when dealing
  with large (~2GB) inputs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-33460">CVE-2023-33460</a>

  <p>There's a memory leak in yajl 2.1.0 with use of yajl_tree_parse
  function, which potentially cause out-of-memory in server and cause
  crash.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.1.0-3+deb10u2.</p>

<p>We recommend that you upgrade your yajl packages.</p>

<p>For the detailed security status of yajl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/yajl">https://security-tracker.debian.org/tracker/yajl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3492.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>menglong2234 discovered NULL pointer exceptions in net-snmp, a suite of
Simple Network Management Protocol applications, which could could
result in debian of service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44792">CVE-2022-44792</a>

    <p>A remote attacker (with write access) could trigger a NULL
    dereference while handling ipDefaultTTL via a crafted UDP packet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44793">CVE-2022-44793</a>

    <p>A remote attacker (with write access) could trigger a NULL
    dereference while handling ipv6IpForwarding via a crafted UDP
    packet.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
5.7.3+dfsg-5+deb10u4.</p>

<p>We recommend that you upgrade your net-snmp packages.</p>

<p>For the detailed security status of net-snmp please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/net-snmp">https://security-tracker.debian.org/tracker/net-snmp</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3270.data"
# $Id: $

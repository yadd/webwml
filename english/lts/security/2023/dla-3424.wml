<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Martin Wennberg discovered that python-ipaddress, a Backport of Python 3's
ipaddress module, improperly computed hash values in the
<code>IPv4Interface</code> and <code>IPv6Interface</code> classes, which might
allow a remote attacker to cause a denial of service if an application is
affected by the performance of a dictionary containing
<code>IPv4Interface</code> or </code>IPv6Interface objects</code>.  The
attacker can use this flaw to cause many dictionary entries to be created.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.0.17-1+deb10u1.</p>

<p>We recommend that you upgrade your python-ipaddress packages.</p>

<p>For the detailed security status of python-ipaddress please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python-ipaddress">https://security-tracker.debian.org/tracker/python-ipaddress</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3424.data"
# $Id: $

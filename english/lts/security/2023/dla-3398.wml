<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been found in cURL, an easy-to-use
client-side URL transfer library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27533">CVE-2023-27533</a>

    <p>A vulnerability in input validation exists in curl during
    communication using the TELNET protocol may allow an attacker to pass on
    maliciously crafted user name and <q>telnet options</q> during server
    negotiation. The lack of proper input scrubbing allows an attacker to send
    content or perform option negotiation without the application's intent.
    This vulnerability could be exploited if an application allows user input,
    thereby enabling attackers to execute arbitrary code on the system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27535">CVE-2023-27535</a>

    <p>An authentication bypass vulnerability exists in libcurl in the FTP
    connection reuse feature that can result in wrong credentials being used
    during subsequent transfers. Previously created connections are kept in a
    connection pool for reuse if they match the current setup. However, certain
    FTP settings such as CURLOPT_FTP_ACCOUNT, CURLOPT_FTP_ALTERNATIVE_TO_USER,
    CURLOPT_FTP_SSL_CCC, and CURLOPT_USE_SSL were not included in the
    configuration match checks, causing them to match too easily. This could
    lead to libcurl using the wrong credentials when performing a transfer,
    potentially allowing unauthorized access to sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27536">CVE-2023-27536</a>

    <p>An authentication bypass vulnerability exists in libcurl in the
    connection reuse feature which can reuse previously established connections
    with incorrect user permissions due to a failure to check for changes in
    the CURLOPT_GSSAPI_DELEGATION option. This vulnerability affects
    krb5/kerberos/negotiate/GSSAPI transfers and could potentially result in
    unauthorized access to sensitive information. The safest option is to not
    reuse connections if the CURLOPT_GSSAPI_DELEGATION option has been changed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27538">CVE-2023-27538</a>

    <p>An authentication bypass vulnerability exists in libcurl where it
    reuses a previously established SSH connection despite the fact that an SSH
    option was modified, which should have prevented reuse. libcurl maintains a
    pool of previously used connections to reuse them for subsequent transfers
    if the configurations match. However, two SSH settings were omitted from
    the configuration check, allowing them to match easily, potentially leading
    to the reuse of an inappropriate connection.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
7.64.0-4+deb10u6.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>For the detailed security status of curl please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/curl">https://security-tracker.debian.org/tracker/curl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3398.data"
# $Id: $

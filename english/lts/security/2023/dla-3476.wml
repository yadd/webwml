<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in cups, the Common UNIX Printing System(tm).
Due to a use-after-free bug an attacker could cause a denial-of-service.
In case of having access to the log files, an attacker could also
exfiltrate private keys or other sensitive information from the cups
daemon.</p>


<p>For Debian 10 buster, this problem has been fixed in version
2.2.10-6+deb10u8.</p>

<p>We recommend that you upgrade your cups packages.</p>

<p>For the detailed security status of cups please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cups">https://security-tracker.debian.org/tracker/cups</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3476.data"
# $Id: $

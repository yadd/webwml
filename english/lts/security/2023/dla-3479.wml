<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two denial of service vulnerabilities have been discovered in
golang-yaml.v2, a library which provides YAML support for the Go
language.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4235">CVE-2021-4235</a>

    <p>Due to unbounded alias chasing, a maliciously crafted YAML file can
    cause the system to consume significant system resources. If parsing
    user input, this may be used as a denial of service vector.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3064">CVE-2022-3064</a>

    <p>Parsing malicious or large YAML documents can consume excessive
    amounts of CPU or memory.</p>

<p>Thanks to Scarlett Moore for working on preparing this update.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.2.2-1+deb10u1.</p>

<p>We recommend that you upgrade your golang-yaml.v2 packages.</p>

<p>For the detailed security status of golang-yaml.v2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/golang-yaml.v2">https://security-tracker.debian.org/tracker/golang-yaml.v2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3479.data"
# $Id: $

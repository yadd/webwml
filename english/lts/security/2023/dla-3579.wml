<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in elfutils, a collection of utilities to handle
ELF objects.
Due to missing bound checks and reachable asserts, an attacker can
use crafted elf files to trigger application crashes that result in
denial-of-services.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.176-1.1+deb10u1.</p>

<p>We recommend that you upgrade your elfutils packages.</p>

<p>For the detailed security status of elfutils please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/elfutils">https://security-tracker.debian.org/tracker/elfutils</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3579.data"
# $Id: $

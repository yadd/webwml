<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Exim, a mail transport agent,
which could result in remote code execution if the SPA/NTLM authenticators
are used.</p>

<p>For Debian 10 buster, these problems have been fixed in version
4.92-8+deb10u8.</p>

<p>We recommend that you upgrade your exim4 packages.</p>

<p>For the detailed security status of exim4 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/exim4">https://security-tracker.debian.org/tracker/exim4</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3599.data"
# $Id: $

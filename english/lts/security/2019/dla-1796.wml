<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple vulnerabilities have been discovered in jruby, Java
implementation of the Ruby programming language.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000074">CVE-2018-1000074</a>

    <p>Deserialization of Untrusted Data vulnerability in owner command
    that can result in code execution. This attack appear to be
    exploitable via victim must run the `gem owner` command on a gem
    with a specially crafted YAML file</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000075">CVE-2018-1000075</a>

    <p>an infinite loop caused by negative size vulnerability in ruby gem
    package tar header that can result in a negative size could cause an
    infinite loop</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000076">CVE-2018-1000076</a>

    <p>Improper Verification of Cryptographic Signature vulnerability in
    package.rb that can result in a mis-signed gem could be installed,
    as the tarball would contain multiple gem signatures.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000077">CVE-2018-1000077</a>

    <p>Improper Input Validation vulnerability in ruby gems specification
    homepage attribute that can result in a malicious gem could set an
    invalid homepage URL</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000078">CVE-2018-1000078</a>

    <p>Cross Site Scripting (XSS) vulnerability in gem server display of
    homepage attribute that can result in XSS. This attack appear to be
    exploitable via the victim must browse to a malicious gem on a
    vulnerable gem server</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8321">CVE-2019-8321</a>

    <p>Gem::UserInteraction#verbose calls say without escaping, escape
    sequence injection is possible</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8322">CVE-2019-8322</a>

    <p>The gem owner command outputs the contents of the API response
    directly to stdout. Therefore, if the response is crafted, escape
    sequence injection may occur</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8323">CVE-2019-8323</a>

    <p>Gem::GemcutterUtilities#with_response may output the API response to
    stdout as it is. Therefore, if the API side modifies the response,
    escape sequence injection may occur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8324">CVE-2019-8324</a>

    <p>A crafted gem with a multi-line name is not handled correctly.
    Therefore, an attacker could inject arbitrary code to the stub line
    of gemspec</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8325">CVE-2019-8325</a>

    <p>Gem::CommandManager#run calls alert_error without escaping, escape
    sequence injection is possible. (There are many ways to cause an
    error.)</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.5.6-9+deb8u1.</p>

<p>We recommend that you upgrade your jruby packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1796.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several security issues have been corrected in multiple demuxers and
decoders of the libav multimedia library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-8542">CVE-2014-8542</a>

    <p>libavcodec/utils.c omitted a certain codec ID during enforcement of
    alignment, which allowed remote attackers to cause a denial of ervice
    (out-of-bounds access) or possibly have unspecified other impact via
    crafted JV data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-1207">CVE-2015-1207</a>

    <p>Double-free vulnerability in libavformat/mov.c allowed remote
    attackers to cause a denial of service (memory corruption and crash)
    via a crafted .m4a file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7863">CVE-2017-7863</a>

    <p>libav had an out-of-bounds write caused by a heap-based buffer
    overflow related to the decode_frame_common function in
    libavcodec/pngdec.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7865">CVE-2017-7865</a>

    <p>libav had an out-of-bounds write caused by a heap-based buffer
    overflow related to the ipvideo_decode_block_opcode_0xA function in
    libavcodec/interplayvideo.c and the avcodec_align_dimensions2
    function in libavcodec/utils.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14169">CVE-2017-14169</a>

    <p>In the mxf_read_primer_pack function in libavformat/mxfdec.c in, an
    integer signedness error might have occured when a crafted file,
    claiming a large <q>item_num</q> field such as 0xffffffff, was provided.
    As a result, the variable <q>item_num</q> turned negative, bypassing the
    check for a large value.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14223">CVE-2017-14223</a>

    <p>In libavformat/asfdec_f.c a DoS in asf_build_simple_index() due to
    lack of an EOF (End of File) check might have caused huge CPU
    consumption. When a crafted ASF file, claiming a large <q>ict</q> field in
    the header but not containing sufficient backing data, was provided,
    the for loop would have consumed huge CPU and memory resources, since
    there was no EOF check inside the loop.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
6:11.12-1~deb8u5.</p>

<p>We recommend that you upgrade your libav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1654.data"
# $Id: $

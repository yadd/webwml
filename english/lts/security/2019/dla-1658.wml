<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A couple of vulnerabilities have been discovered in phpmyadmin, MySQL web
administration tool.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19968">CVE-2018-19968</a>

    <p>An attacker can exploit phpMyAdmin before 4.8.4 to leak the contents of a
    local file because of an error in the transformation feature. The attacker
    must have access to the phpMyAdmin Configuration Storage tables, although
    these can easily be created in any database to which the attacker has
    access. An attacker must have valid credentials to log in to phpMyAdmin;
    this vulnerability does not allow an attacker to circumvent the login
    system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19970">CVE-2018-19970</a>

    <p>A XSS vulnerability was found in the navigation tree, where an attacker can
    deliver a payload to a user through a crafted database/table name.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4:4.2.12-2+deb8u4.</p>

<p>We recommend that you upgrade your phpmyadmin packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1658.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several security issues were fixed in libav, a multimedia library for
processing audio and video files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17127">CVE-2017-17127</a>

    <p>The vc1_decode_frame function in libavcodec/vc1dec.c allows remote
    attackers to cause a denial of service (NULL pointer dereference
    and application crash) via a crafted file.<br />
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-19130">CVE-2018-19130</a> is a duplicate of this vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18245">CVE-2017-18245</a>

    <p>The mpc8_probe function in libavformat/mpc8.c allows remote
    attackers to cause a denial of service (heap-based buffer
    over-read) via a crafted audio file on 32-bit systems.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19128">CVE-2018-19128</a>

    <p>Heap-based buffer over-read in decode_frame in libavcodec/lcldec.c
    allows an attacker to cause denial-of-service via a crafted avi
    file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14443">CVE-2019-14443</a>

    <p>Division by zero in range_decode_culshift in libavcodec/apedec.c
    allows remote attackers to cause a denial of service (application
    crash), as demonstrated by avconv.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17542">CVE-2019-17542</a>

    <p>Heap-based buffer overflow in vqa_decode_chunk because of an
    out-of-array access in vqa_decode_init in libavcodec/vqavideo.c.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
6:11.12-1~deb8u9.</p>

<p>We recommend that you upgrade your libav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2021.data"
# $Id: $

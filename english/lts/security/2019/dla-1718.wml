<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two vulnerabilities were discovered in SQLALchemy, a Python SQL
Toolkit and Object Relational Mapper.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7164">CVE-2019-7164</a>

    <p>SQLAlchemy allows SQL Injection via the order_by parameter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7548">CVE-2019-7548</a>

    <p>SQLAlchemy has SQL Injection when the group_by parameter can be controlled.</p></li>

</ul>

<p>The SQLAlchemy project warns that these security fixes break the
seldom-used text coercion feature.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.9.8+dfsg-0.1+deb8u1.</p>

<p>We recommend that you upgrade your sqlalchemy packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1718.data"
# $Id: $

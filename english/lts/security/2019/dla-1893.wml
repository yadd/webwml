<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues have been found in cups, the Common UNIX Printing System(tm).</p>

<p>Basically both CVEs (<a href="https://security-tracker.debian.org/tracker/CVE-2019-8675">CVE-2019-8675</a> and <a href="https://security-tracker.debian.org/tracker/CVE-2019-8696">CVE-2019-8696</a>) are about
stack-buffer-overflow in two functions of libcup. One happens in
asn1_get_type() the other one in asn1_get_packed().</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.7.5-11+deb8u5.</p>

<p>We recommend that you upgrade your cups packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1893.data"
# $Id: $

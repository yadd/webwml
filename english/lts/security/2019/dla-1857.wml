<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Vulnerabilities have been discovered in nss, the Mozilla Network
Security Service library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11719">CVE-2019-11719</a>:

<p>Out-of-bounds read when importing curve25519 private key</p>

    <p>When importing a curve25519 private key in PKCS#8format with leading
    0x00 bytes, it is possible to trigger an out-of-bounds read in the
    Network Security Services (NSS) library. This could lead to
    information disclosure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11729">CVE-2019-11729</a>:

<p>Empty or malformed p256-ECDH public keys may trigger a
    segmentation fault</p>

    <p>Empty or malformed p256-ECDH public keys may trigger a segmentation
    fault due values being improperly sanitized before being copied into
    memory and used.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2:3.26-1+debu8u5.</p>

<p>We recommend that you upgrade your nss packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1857.data"
# $Id: $

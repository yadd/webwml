<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>debian-security-support, the Debian security support coverage checker,
has been updated in jessie.</p>

<p>This marks the end of life of the Enigmail package in jessie. After many
months of work to try backporting the various changes and fixes required
to ensure a secure Enigmail package compatible with the newest version
of Thunderbird now shipped in jessie, it is the LTS team's opinion that
the changes are too intrusive to ensure the stability of the
distribution as a whole.</p>

<p>While Enigmail is still available on addons.mozilla.org, we do not
recommend that package is used, as downloads and executes arbitrary
binaries from the internet without the user's knowledge. It also doesn't
respect's Debian commitment to free software, in that it is not
buildable from source.</p>

<p>We instead recommend you upgrade workstations needing Enigmail to Debian
stretch where proper updates are available.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>For Debian 8 <q>Lenny</q>, these issues have been fixed in debian-security-support version 2019.02.01~deb8u1</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1657.data"
# $Id: $

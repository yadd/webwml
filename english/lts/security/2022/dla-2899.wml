<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The Qualys Research Labs discovered a local privilege escalation in
PolicyKit's pkexec.</p>

<p>Details can be found in the Qualys advisory at
<a href="https://www.qualys.com/2022/01/25/cve-2021-4034/pwnkit.txt">https://www.qualys.com/2022/01/25/cve-2021-4034/pwnkit.txt</a></p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.105-18+deb9u2.</p>

<p>We recommend that you upgrade your policykit-1 packages.</p>

<p>For the detailed security status of policykit-1 please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/policykit-1">https://security-tracker.debian.org/tracker/policykit-1</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2899.data"
# $Id: $

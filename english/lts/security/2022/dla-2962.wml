<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in pjproject, is a free and
open source multimedia communication library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32686">CVE-2021-32686</a>

    <p>A race condition between callback and destroy, due to the accepted
    socket having no group lock. Second, the SSL socket
    parent/listener may get destroyed during handshake. s. They cause
    crash, resulting in a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37706">CVE-2021-37706</a>

    <p>An incoming STUN message contains an ERROR-CODE attribute, the
    header length is not checked before performing a subtraction
    operation, potentially resulting in an integer underflow scenario.
    This issue affects all users that use STUN. A malicious actor
    located within the victim’s network may forge and send a specially
    crafted UDP (STUN) message that could remotely execute arbitrary
    code on the victim’s machine</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41141">CVE-2021-41141</a>

    <p>In various parts of PJSIP, when error/failure occurs, it is found
    that the function returns without releasing the currently held
    locks. This could result in a system deadlock, which cause a
    denial of service for the users.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43299">CVE-2021-43299</a>

    <p>Stack overflow in PJSUA API when calling pjsua_player_create. An
    attacker-controlled <q>filename</q> argument may cause a buffer
    overflow since it is copied to a fixed-size stack buffer without
    any size validation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43300">CVE-2021-43300</a>

    <p>Stack overflow in PJSUA API when calling pjsua_recorder_create. An
    attacker-controlled <q>filename</q> argument may cause a buffer
    overflow since it is copied to a fixed-size stack buffer without
    any size validation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43301">CVE-2021-43301</a>

    <p>Stack overflow in PJSUA API when calling pjsua_playlist_create. An
    attacker-controlled <q>file_names</q> argument may cause a buffer
    overflow since it is copied to a fixed-size stack buffer without
    any size validation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43302">CVE-2021-43302</a>

    <p>Read out-of-bounds in PJSUA API when calling
    pjsua_recorder_create. An attacker-controlled <q>filename</q> argument
    may cause an out-of-bounds read when the filename is shorter than
    4 characters.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43303">CVE-2021-43303</a>

    <p>Buffer overflow in PJSUA API when calling pjsua_call_dump. An
    attacker-controlled <q>buffer</q> argument may cause a buffer overflow,
    since supplying an output buffer smaller than 128 characters may
    overflow the output buffer, regardless of the <q>maxlen</q> argument
    supplied</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43804">CVE-2021-43804</a>

    <p>An incoming RTCP BYE message contains a reason's length, this
    declared length is not checked against the actual received packet
    size, potentially resulting in an out-of-bound read access. A
    malicious actor can send a RTCP BYE message with an invalid reason
    length</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43845">CVE-2021-43845</a>

    <p>if incoming RTCP XR message contain block, the data field is not
    checked against the received packet size, potentially resulting in
    an out-of-bound read access</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21722">CVE-2022-21722</a>

    <p>it is possible that certain incoming RTP/RTCP packets can
    potentially cause out-of-bound read access. This issue affects
    all users that use PJMEDIA and accept incoming RTP/RTCP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21723">CVE-2022-21723</a>

    <p>Parsing an incoming SIP message that contains a malformed
    multipart can potentially cause out-of-bound read access. This
    issue affects all PJSIP users that accept SIP multipart.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23608">CVE-2022-23608</a>

    <p>When in a dialog set (or forking) scenario, a hash key shared by
    multiple UAC dialogs can potentially be prematurely freed when one
    of the dialogs is destroyed . The issue may cause a dialog set to
    be registered in the hash table multiple times (with different
    hash keys) leading to undefined behavior such as dialog list
    collision which eventually leading to endless loop</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24754">CVE-2022-24754</a>

    <p>There is a stack-buffer overflow vulnerability which only impacts
    PJSIP users who accept hashed digest credentials (credentials with
    data_type `PJSIP_CRED_DATA_DIGEST`).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24764">CVE-2022-24764</a>

     <p>A stack buffer overflow vulnerability that affects PJSUA2 users
     or users that call the API `pjmedia_sdp_print(),
     pjmedia_sdp_media_print()`</p>


<p>For Debian 9 stretch, these problems have been fixed in version
2.5.5~dfsg-6+deb9u3.</p>

<p>We recommend that you upgrade your pjproject packages.</p>

<p>For the detailed security status of pjproject please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pjproject">https://security-tracker.debian.org/tracker/pjproject</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2962.data"
# $Id: $

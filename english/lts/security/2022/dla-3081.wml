<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was discovered in open-vm-tools, an open source
implementation of VMware Tools, allowing an unprivileged local guest
user to escalate their privileges as root user in the virtual machine.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2:10.3.10-1+deb10u3.</p>

<p>We recommend that you upgrade your open-vm-tools packages.</p>

<p>For the detailed security status of open-vm-tools please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/open-vm-tools">https://security-tracker.debian.org/tracker/open-vm-tools</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3081.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The update for mariadb-10.3 released as DLA-3114 introduced a bug in the
mariadb-server-10.3 package, that could cause installation failures when
installing or updating plugin packages.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1:10.3.36-0+deb10u2.</p>

<p>We recommend that you upgrade your mariadb-10.3 packages.</p>

<p>For the detailed security status of mariadb-10.3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mariadb-10.3">https://security-tracker.debian.org/tracker/mariadb-10.3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3114-2.data"
# $Id: $

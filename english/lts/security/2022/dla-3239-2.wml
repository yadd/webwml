<define-tag description>LTS regression update</define-tag>
<define-tag moreinfo>
<p>In rare conditions, the previous git update released as DLA-3239-1
could generate a segmentation fault, which prevented its availability
on armhf architecture. This update addresses this issue. For reference
the original advisory text follows.</p>

<p>Multiple issues were found in Git, a distributed revision control
system. An attacker may cause other local users into executing
arbitrary commands, leak information from the local filesystem, and
bypass restricted shell.
<p><b>Note</b>: Due to new security checks, access to repositories owned and
accessed by different local users may now be rejected by Git; in case
changing ownership is not practical, git displays a way to bypass
these checks using the new <q>safe.directory</q> configuration entry.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24765">CVE-2022-24765</a>

    <p>Git is not checking the ownership of directories in a local
    multi-user system when running commands specified in the local
    repository configuration.  This allows the owner of the repository
    to cause arbitrary commands to be executed by other users who
    access the repository.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29187">CVE-2022-29187</a>

    <p>An unsuspecting user could still be affected by the issue reported
    in <a href="https://security-tracker.debian.org/tracker/CVE-2022-24765">CVE-2022-24765</a>, for example when navigating as root into a
    shared tmp directory that is owned by them, but where an attacker
    could create a git repository.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39253">CVE-2022-39253</a>

    <p>Exposure of sensitive information to a malicious actor. When
    performing a local clone (where the source and target of the clone
    are on the same volume), Git copies the contents of the source's
    `$GIT_DIR/objects` directory into the destination by either
    creating hardlinks to the source contents, or copying them (if
    hardlinks are disabled via `--no-hardlinks`). A malicious actor
    could convince a victim to clone a repository with a symbolic link
    pointing at sensitive information on the victim's machine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39260">CVE-2022-39260</a>

    <p>`git shell` improperly uses an `int` to represent the number of
    entries in the array, allowing a malicious actor to intentionally
    overflow the return value, leading to arbitrary heap
    writes. Because the resulting array is then passed to `execv()`,
    it is possible to leverage this attack to gain remote code
    execution on a victim machine.</p></li>

</ul>

<p>For Debian 10 buster, this problem has been fixed in version
1:2.20.1-2+deb10u6.</p>

<p>We recommend that you upgrade your git packages.</p>

<p>For the detailed security status of git please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/git">https://security-tracker.debian.org/tracker/git</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3239-2.data"
# $Id: $

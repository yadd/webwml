<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple file parsing vulnerabilities have been fixed in libraw. They are
concerned with the dng and x3f formats. </p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35530">CVE-2020-35530</a>

    <p>There is an out-of-bounds write vulnerability within the "<code>new_node()</code>"
    function (<code>src/x3f/x3f_utils_patched.cpp</code>) that can be triggered via a
    crafted X3F file. Reported by github user 0xfoxone.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35531">CVE-2020-35531</a>

    <p>An out-of-bounds read vulnerability exists within the
    <code>get_huffman_diff()</code> function (<code>src/x3f/x3f_utils_patched.cpp</code>) when
    reading data from an image file. Reported by github user GirlElecta.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35532">CVE-2020-35532</a>

    <p>An out-of-bounds read vulnerability exists within the
    "<code>simple_decode_row()</code>" function (<code>src/x3f/x3f_utils_patched.cpp</code>) which
    can be triggered via an image with a large <code>row_stride</code> field.
    Reported by github user GirlElecta.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35533">CVE-2020-35533</a>

    <p>An out-of-bounds read vulnerability exists within the
    "<code>LibRaw::adobe_copy_pixel()</code>" function (<code>src/decoders/dng.cpp</code>) when
    reading data from the image file. Reported by github user GirlElecta.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
0.19.2-2+deb10u1.</p>

<p>We recommend that you upgrade your libraw packages.</p>

<p>For the detailed security status of libraw please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libraw">https://security-tracker.debian.org/tracker/libraw</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3113.data"
# $Id: $

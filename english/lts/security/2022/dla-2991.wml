<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The Twisted Web HTTP 1.1 server, located in the twisted.web.http module, parsed
several HTTP request constructs more leniently than permitted by RFC 7230. This
non-conformant parsing can lead to desync if requests pass through multiple
HTTP parsers, potentially resulting in HTTP request smuggling.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
16.6.0-2+deb9u3.</p>

<p>We recommend that you upgrade your twisted packages.</p>

<p>For the detailed security status of twisted please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/twisted">https://security-tracker.debian.org/tracker/twisted</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2991.data"
# $Id: $

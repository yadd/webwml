<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Sandipan Roy discovered two vulnerabilities in InfoZIP's unzip program,
a de-archiver for .zip files, which could result in denial of service
or potentially the execution of arbitrary code.</p>

<p>For Debian 10 buster, these problems have been fixed in version
6.0-23+deb10u3.</p>

<p>We recommend that you upgrade your unzip packages.</p>

<p>For the detailed security status of unzip please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/unzip">https://security-tracker.debian.org/tracker/unzip</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3118.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security vulnerabilities were discovered in Jetty, a Java servlet engine
and webserver.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2047">CVE-2022-2047</a>

    <p>In Eclipse Jetty the parsing of the authority segment of an http scheme
    URI, the Jetty HttpURI class improperly detects an invalid input as a
    hostname. This can lead to failures in a Proxy scenario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2048">CVE-2022-2048</a>

    <p>In Eclipse Jetty HTTP/2 server implementation, when encountering an invalid
    HTTP/2 request, the error handling has a bug that can wind up not properly
    cleaning up the active connections and associated resources. This can lead
    to a Denial of Service scenario where there are no enough resources left to
    process good requests.</p>


<p>For Debian 10 buster, these problems have been fixed in version
9.4.16-0+deb10u2.</p>

<p>We recommend that you upgrade your jetty9 packages.</p>

<p>For the detailed security status of jetty9 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/jetty9">https://security-tracker.debian.org/tracker/jetty9</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3079.data"
# $Id: $

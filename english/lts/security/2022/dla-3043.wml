<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An issue was discovered in Pidgin before 2.14.9. A remote attacker
who can spoof DNS responses can redirect a client connection to a
malicious server. The client will perform TLS certificate
verification of the malicious domain name instead of the original
XMPP service domain, allowing the attacker to take over control
over the XMPP connection and to obtain user credentials and all
communication content.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.12.0-1+deb9u1.</p>

<p>We recommend that you upgrade your pidgin packages.</p>

<p>For the detailed security status of pidgin please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pidgin">https://security-tracker.debian.org/tracker/pidgin</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3043.data"
# $Id: $

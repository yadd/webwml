<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in htmldoc, an HTML processor that
generates indexed HTML, PS, and PDF.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0534">CVE-2022-0534</a>

      <p>A crafted GIF file could lead to a stack out-of-bounds read,
      which could result in a crash (segmentation fault).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43579">CVE-2021-43579</a>

      <p>Converting an HTML document, which links to a crafted BMP file,
      could lead to a stack-based buffer overflow, which could result
      in remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40985">CVE-2021-40985</a>

      <p>A crafted BMP image could lead to a buffer overflow, which could
      cause a denial of service.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
1.8.27-8+deb9u2.</p>

<p>We recommend that you upgrade your htmldoc packages.</p>

<p>For the detailed security status of htmldoc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/htmldoc">https://security-tracker.debian.org/tracker/htmldoc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2928.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Various vulnerabilities leading to denial of service or possible unspecified
other impacts were discovered in sam2p, an utility to convert raster images to
EPS, PDF, and other formats.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12578">CVE-2018-12578</a>

    <p>A heap-buffer-overflow in bmp_compress1_row. Thanks to Peter Szabo for
    providing a fix.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12601">CVE-2018-12601</a>

    <p>A heap-buffer-overflow in function ReadImage, in file input-tga.ci. Thanks
    to Peter Szabo for providing a fix.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.49.2-3+deb8u3.</p>

<p>We recommend that you upgrade your sam2p packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1463.data"
# $Id: $

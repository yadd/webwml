<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that 389-ds-base (the 389 Directory Server) is vulnerable
to search queries with malformed values in the do_search() function
(servers/slapd/search.c). Attackers could leverage this vulnerability by
sending crafted queries in a loop to cause DoS.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.3.3.5-4+deb8u4.</p>

<p>We recommend that you upgrade your 389-ds-base packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1554.data"
# $Id: $

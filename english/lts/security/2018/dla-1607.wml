<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in Samba, a SMB/CIFS file,
print, and login server for Unix. The Common Vulnerabilities and
Exposures project identifies the following issues:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14629">CVE-2018-14629</a>

    <p>Florian Stuelpner discovered that Samba is vulnerable to
    infinite query recursion caused by CNAME loops, resulting in
    denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16851">CVE-2018-16851</a>

    <p>Garming Sam of the Samba Team and Catalyst discovered a NULL pointer
    dereference vulnerability in the Samba AD DC LDAP server allowing a
    user able to read more than 256MB of LDAP entries to crash the Samba
    AD DC's LDAP server.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2:4.2.14+dfsg-0+deb8u11.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1607.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Ilja Van Sprundel discovered a buffer overflow vulnerability in ppp,
the Point-to-Point Protocol daemon. When receiving an EAP Request
message in client mode, an attacker was able to overflow the rhostname
array by providing a very long name. This issue is also mitigated by
Debian's hardening build flags.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.4.6-3.1+deb8u1.</p>

<p>We recommend that you upgrade your ppp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2097.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue in the <a
href="https://jupyter.org/">Jupyter</a> interactive notebook system where a
maliciously-crafted link could redirect the browser to a malicious/spoofed
website.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26215">CVE-2020-26215</a>

    <p>Jupyter Notebook before version 6.1.5 has an Open redirect
    vulnerability. A maliciously crafted link to a notebook server could
    redirect the browser to a different website. All notebook servers are
    technically affected, however, these maliciously crafted links can only be
    reasonably made for known notebook server hosts. A link to your notebook
    server may appear safe, but ultimately redirect to a spoofed server on the
    public internet. The issue is patched in version 6.1.5.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
4.2.3-4+deb9u2.</p>

<p>We recommend that you upgrade your jupyter-notebook packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2477.data"
# $Id: $

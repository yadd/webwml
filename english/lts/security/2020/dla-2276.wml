<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The following CVEs were reported against src:mailman.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12108">CVE-2020-12108</a>

    <p>/options/mailman in GNU Mailman before 2.1.31 allows Arbitrary
    Content Injection.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15011">CVE-2020-15011</a>

    <p>GNU Mailman before 2.1.33 allows arbitrary content injection via
    the Cgi/private.py private archive login page.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:2.1.23-1+deb9u6.</p>

<p>We recommend that you upgrade your mailman packages.</p>

<p>For the detailed security status of mailman please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mailman">https://security-tracker.debian.org/tracker/mailman</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2276.data"
# $Id: $

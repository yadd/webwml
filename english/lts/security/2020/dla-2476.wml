<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A buffer overflow was discovered in Brotli, a generic-purpose lossless
compression suite.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.5.2+dfsg-2+deb9u1.</p>

<p>We recommend that you upgrade your brotli packages.</p>

<p>For the detailed security status of brotli please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/brotli">https://security-tracker.debian.org/tracker/brotli</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2476.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service, or information
leak.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3874">CVE-2019-3874</a>

    <p>Kernel buffers allocated by the SCTP network protocol were not
    limited by the memory cgroup controller.  A local user could
    potentially use this to evade container memory limits and to cause
    a denial of service (excessive memory use).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19448">CVE-2019-19448</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-19813">CVE-2019-19813</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-19816">CVE-2019-19816</a>

    <p><q>Team bobfuzzer</q> reported bugs in Btrfs that could lead to a
    use-after-free or heap buffer overflow, and could be triggered by
    crafted filesystem images.  A user permitted to mount and access
    arbitrary filesystems could use these to cause a denial of service
    (crash or memory corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10781">CVE-2020-10781</a>

    <p>Luca Bruno of Red Hat discovered that the zram control file
    /sys/class/zram-control/hot_add was readable by all users.  On a
    system with zram enabled, a local user could use this to cause a
    denial of service (memory exhaustion).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12888">CVE-2020-12888</a>

    <p>It was discovered that the PCIe Virtual Function I/O (vfio-pci)
    driver allowed users to disable a device's memory space while it
    was still mapped into a process.  On some hardware platforms,
    local users or guest virtual machines permitted to access PCIe
    Virtual Functions could use this to cause a denial of service
    (hardware error and crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14314">CVE-2020-14314</a>

    <p>A bug was discovered in the ext4 filesystem that could lead to an
    out-of-bound read.  A local user permitted to mount and access
    arbitrary filesystem images could use this to cause a denial of
    service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14331">CVE-2020-14331</a>

    <p>A bug was discovered in the VGA console driver's soft-scrollback
    feature that could lead to a heap buffer overflow.  On a system
    with a custom kernel that has CONFIG_VGACON_SOFT_SCROLLBACK
    enabled, a local user with access to a console could use this to
    cause a denial of service (crash or memory corruption) or possibly
    for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14356">CVE-2020-14356</a>

    <p>A bug was discovered in the cgroup subsystem's handling of socket
    references to cgroups.  In some cgroup configurations, this could
    lead to a use-after-free.  A local user might be able to use this
    to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14385">CVE-2020-14385</a>

    <p>A bug was discovered in XFS, which could lead to an extended
    attribute (xattr) wrongly being detected as invalid.  A local user
    with access to an XFS filesystem could use this to cause a denial
    of service (filesystem shutdown).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14386">CVE-2020-14386</a>

    <p>Or Cohen discovered a bug in the packet socket (AF_PACKET)
    implementation which could lead to a heap buffer overflow.  A
    local user with the CAP_NET_RAW capability (in any user namespace)
    could use this to cause a denial of service (crash or memory
    corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14390">CVE-2020-14390</a>

    <p>Minh Yuan discovered a bug in the framebuffer console driver's
    scrollback feature that could lead to a heap buffer overflow.  On
    a system using framebuffer consoles, a local user with access to a
    console could use this to cause a denial of service (crash or
    memory corruption) or possibly for privilege escalation.</p>

    <p>The scrollback feature has been disabled for now, as no other fix
    was available for this issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16166">CVE-2020-16166</a>

    <p>Amit Klein reported that the random number generator used by the
    network stack might not be re-seeded for long periods of time,
    making e.g. client port number allocations more predictable.  This
    made it easier for remote attackers to carry out some network-based
    attacks such as DNS cache poisoning or device tracking.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25212">CVE-2020-25212</a>

    <p>A bug was discovered in the NFSv4 client implementation that could
    lead to a heap buffer overflow.  A malicious NFS server could use
    this to cause a denial of service (crash or memory corruption) or
    possibly to execute arbitrary code on the client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25284">CVE-2020-25284</a>

    <p>It was discovered that the Rados block device (rbd) driver allowed
    tasks running as uid 0 to add and remove rbd devices, even if they
    dropped capabilities.  On a system with the rbd driver loaded,
    this might allow privilege escalation from a container with a task
    running as root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25285">CVE-2020-25285</a>

    <p>A race condition was discovered in the hugetlb filesystem's sysctl
    handlers, that could lead to stack corruption.  A local user
    permitted to write to hugepages sysctls could use this to cause a
    denial of service (crash or memory corruption) or possibly for
    privilege escalation.  By default only the root user can do this.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25641">CVE-2020-25641</a>

    <p>The syzbot tool found a bug in the block layer that could lead to
    an infinite loop.  A local user with access to a raw block device
    could use this to cause a denial of service (unbounded CPU use and
    possible system hang).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26088">CVE-2020-26088</a>

    <p>It was discovered that the NFC (Near Field Communication) socket
    implementation allowed any user to create raw sockets.  On a
    system with an NFC interface, this allowed local users to evade
    local network security policy.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.19.146-1~deb9u1.  This update additionally fixes Debian bugs
<a href="https://bugs.debian.org/966846">#966846</a>, <a href="https://bugs.debian.org/966917">#966917</a>, and <a href="https://bugs.debian.org/968567">#968567</a>; and includes many more bug fixes from
stable updates 4.19.133-4.19.146 inclusive.</p>

<p>We recommend that you upgrade your linux-4.19 packages.</p>

<p>For the detailed security status of linux-4.19 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux-4.19">https://security-tracker.debian.org/tracker/linux-4.19</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2385.data"
# $Id: $

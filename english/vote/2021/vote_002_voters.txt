-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    1           93sam	Steve McIntyre
    2             abe	Axel Beckert
    3        abhijith	Abhijith PA
    4        achernya	Alexander Chernyakhovsky
    5       adrianorg	Adriano Rafael Gomes
    6            adsb	Adam D. Barratt
    7             agi	Alberto Gonzalez Iniesta
    8        agmartin	Agustín Martín Domingo
    9             agx	Guido Guenther
   10              ah	Andreas Henriksson
   11           alexm	Alex Muntada
   12        amacater	Andrew Martin Adrian Cater
   13           amaya	Amaya Rodrigo Sastre
   14        ametzler	Andreas Metzler
   15             ana	Ana Beatriz Guerrero López
   16         anarcat	Antoine Beaupré
   17            andi	Andreas B. Mundt
   18        andrewsh	Andrej Shadura
   19        angdraug	Dmitry Borodaenko
   20           angel	Angel Abad
   21          anibal	Anibal Monsalve Salazar
   22          ansgar	Ansgar
   23             apo	Markus Koschany
   24         apoikos	Apollon Oikonomopoulos
   25             ari	Ari Pollak
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   26            aron	Aron Xu
   27             asb	Andrew Starr-Bochicchio
   28         aurel32	Aurelien Jarno
   29           aviau	Alexandre Viau
   30             awm	Andrew McMillan
   31       awoodland	Alan Woodland
   32              az	Alexander Zangerl
   33      babelouest	Nicolas Mora
   34     balasankarc	Balasankar Chelamattathu
   35        ballombe	Bill Allombert
   36             bam	Brian May
   37             bap	Barak A. Pearlmutter
   38           bartm	Bart Martens
   39             bas	Bas Zoetekouw
   40          bbaren	Benjamin Barenblat
   41         bblough	William Blough
   42           bdale	Bdale Garbee
   43          bengen	Hilko Bengen
   44          bernat	Vincent Bernat
   45            beuc	Sylvain Beucler
   46           biebl	Michael Biebl
   47          birger	Birger Schacht
   48           blade	Eduard Bloch
   49           bluca	Luca Boccassi
   50           bootc	Chris Boot
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   51         bremner	David Bremner
   52         broonie	Mark Brown
   53            bunk	Adrian Bunk
   54           byang	Boyuan Yang
   55            bzed	Bernd Zeimetz
   56        calculus	Jerome Georges Benoit
   57        capriott	Andrea Capriotti
   58          carnil	Salvatore Bonaccorso
   59             cas	Craig Sanders
   60        cascardo	Thadeu Cascardo
   61          cbiedl	Christoph Biedl
   62       chronitis	Gordon Ball
   63        cjwatson	Colin Watson
   64           cklin	Chuan-kai Lin
   65           clint	Clint Adams
   66             cmb	Chris Boyle
   67        codehelp	Neil Williams
   68          colint	Colin Tuckley
   69         coucouf	Aurélien Couderc
   70          crusoe	Michael Robin Crusoe
   71          csmall	Craig Small
   72         ctaylor	Christopher Taylor
   73             cts	Christian T. Steigies
   74           curan	Kai Wasserbäch
   75           cwryu	Changwoo Ryu
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   76          czchen	ChangZhuo Chen
   77             dai	Daisuke Higuchi
   78           dannf	Dann Frazier
   79       directhex	Jo Shields
   80             dkg	Daniel Kahn Gillmor
   81       dktrkranz	Luca Falavigna
   82           dlehn	David I. Lehn
   83        dleidert	Daniel Leidert
   84         dmartin	Dale Martin
   85             dmn	Damyan Ivanov
   86             dod	Dominique Dumont
   87         dogsleg	Lev Lamberov
   88             dom	Dominic Hargreaves
   89             don	Don Armstrong
   90          donald	Donald Norwood
   91         donkult	David Kalnischkies
   92        dsilvers	Daniel Silverstone
   93          ebourg	Emmanuel Bourg
   94             edd	Dirk Eddelbuettel
   95         edmonds	Robert Edmonds
   96          eevans	Eric Evans
   97        ehashman	Elana Hashman
   98          elbrus	Paul Mathijs Gevers
   99             ema	Emanuele Rocca
  100          enrico	Enrico Zini
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  101        eppesuig	Giuseppe Sacco
  102        eriberto	Joao Eriberto Mota Filho
  103            eric	Eric Dorland
  104           eriks	Erik Schanze
  105           eriol	Daniele Tricoli
  106            etbe	Russell Coker
  107          fabian	Fabian Greffrath
  108             faw	Felipe Augusto van de Wiel
  109        federico	Federico Ceratto
  110          fgeyer	Felix Geyer
  111         filippo	Filippo Giunchedi
  112         florian	Florian Ernst
  113         fpeters	Frederic Peters
  114         frankie	Francesco Lovergine
  115           fuddl	Bruno Kleinert
  116              fw	Florian Weimer
  117  gareuselesinge	Enrico Tassi
  118         gaudenz	Gaudenz Steinlin
  119           georg	Georg Faerber
  120        georgesk	Georges Khaznadar
  121            gewo	Gert Wollny
  122          ginggs	Graham Inggs
  123             gio	Giovanni Mascellani
  124         giovani	Giovani Augusto Ferreira
  125           gladk	Anton Gladky
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  126          glondu	Stéphane Glondu
  127          gniibe	NIIBE Yutaka
  128              gq	Alexander GQ Gerasiov
  129          gregoa	Gregor Herrmann
  130         guilhem	Guilhem Moulin
  131         guillem	Guillem Jover
  132        gunnarhj	Gunnar Ingemar Hjalmarsson
  133          gusnan	Andreas Rönnquist
  134            guus	Guus Sliepen
  135           gwolf	Gunnar Wolf
  136        hartmans	Sam Hartman
  137           hefee	Sandro Knauß
  138         helmutg	Helmut Grohne
  139         hertzog	Raphaël Hertzog
  140      hlieberman	Harlan Lieberman-Berg
  141         hoexter	Sven Hoexter
  142          holger	Holger Levsen
  143        holmgren	Magnus Holmgren
  144          hsteoh	Hwei Sheng Teoh
  145             hug	Philipp Hug
  146      hvhaugwitz	Hannes von Haugwitz
  147            ianw	Ian Wienand
  148             ijc	Ian James Campbell
  149        iliastsi	Ilias Tsitsimpis
  150        indiebio	Bernelle Verster
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  151       infinity0	Ximin Luo
  152       intrigeri	Intrigeri
  153        ishikawa	Ishikawa Mutsumi
  154          iustin	Iustin Pop
  155           ivodd	Ivo De Decker
  156             iwj	Ian Jackson
  157             jak	Julian Andres Klode
  158         jaldhar	Jaldhar H. Vyas
  159           jandd	Jan Dittberner
  160          jaqque	John Robinson
  161          jathan	Jonathan Bustillos
  162          jbicha	Jeremy Bicha
  163             jcc	Jonathan Cristopher Carter
  164            jcfp	Jeroen Ploemen
  165        jcristau	Julien Cristau
  166             jdg	Julian Gilbey
  167             jeb	James Bromberger
  168          jelmer	Jelmer Vernooij
  169             jjr	Jeffrey Ratcliffe
  170       jmaslibre	Josué Abarca
  171             jmm	Moritz Muehlenhoff
  172            jmtd	Jonathan Dowland
  173             jmw	Jonathan Wiltshire
  174           joerg	Joerg Jaspert
  175           jordi	Jordi Mallach
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  176           josue	Josué Ortega
  177         joussen	Mario Joussen
  178          jpuydt	Julien Puydt
  179        jredrejo	José L. Redrejo Rodríguez
  180          jrtc27	Jessica Clarke
  181              js	Jonas Smedegaard
  182       jvalleroy	James Valleroy
  183           kamal	Kamal Mostafa
  184          kartik	Kartik Mistry
  185            kees	Kees Cook
  186          kenhys	HAYASHI Kentaro
  187            kibi	Cyril Brulebois
  188        kilobyte	Adam Borowski
  189       kitterman	Scott Kitterman
  190            knok	Takatsugu Nokubi
  191           kobla	Ondřej Kobližek
  192          kolter	Emmanuel Bouthenot
  193           krait	Christopher Knadle
  194           krala	Antonin Kral
  195         kreckel	Richard Kreckel
  196      kritzefitz	Sven Bartscher
  197            kula	Marcin Kulisz
  198           lamby	Chris Lamb
  199           lange	Thomas Lange
  200         larjona	Laura Arjona Reina
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  201        lavamind	Jerome Charaoui
  202        lawrencc	Christopher Lawrence
  203      lazyfrosch	Markus Frosch
  204         lbrenta	Ludovic Brenta
  205         lechner	Felix Lechner
  206          leepen	Mark Hindley
  207         legoktm	Kunal Mehta
  208         lenharo	Daniel Lenharo de Souza
  209             leo	Carsten Leonhardt
  210        lfaraone	Luke Faraone
  211        lfilipoz	Luca Filipozzi
  212        lightsey	John Lightsey
  213          lkajan	Laszlo Kajan
  214         lmamane	Lionel Elie Mamane
  215         lopippo	Filippo Rusconi
  216           lucab	Luca Bruno
  217           lucas	Lucas Nussbaum
  218           lumin	Mo Zhou
  219         lyknode	Baptiste Beauplat
  220         madduck	Martin F. Krafft
  221            mafm	Manuel A. Fernandez Montecelo
  222             mak	Matthias Klumpp
  223            manu	Emmanuel Kasper Kasprzyk
  224        marillat	Christian Marillat
  225           mattb	Matthew Brown
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  226         matthew	Matthew Vernon
  227          mattia	Mattia Rizzolo
  228            maxx	Martin Wuertele
  229         mbehrle	Mathias Behrle
  230           mbuck	Martin Buck
  231              md	Marco d'Itri
  232           mehdi	Mehdi Dogguy
  233            mejo	Jonas Meurer
  234          merker	Karsten Merker
  235          merkys	Andrius Merkys
  236           metal	Marcelo Jorge Vieira
  237             mez	Martin Meredith
  238             mfv	Matteo F. Vescovi
  239           micah	Micah Anderson
  240           micha	Micha Lenk
  241             mih	Michael Hanke
  242            mika	Michael Prokop
  243           milan	Milan Kupcevic
  244        mjeanson	Michael Jeanson
  245           mjg59	Matthew Garrett
  246         mollydb	Molly de Blanc
  247           mones	Ricardo Mones Lastra
  248           moray	Moray Allan
  249           morph	Sandro Tosi
  250           mpitt	Martin Pitt
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  251          mstone	Michael Stone
  252     mtecknology	Michael Lustfield
  253           murat	Murat Demirten
  254            mwei	Ming-ting Yao Wei
  255            myon	Christoph Berg
  256    natureshadow	Dominik George
  257          nbreen	Nicholas Breen
  258           neilm	Neil McGovern
  259           nicoo	Nicoo
  260           nijel	Michal Cihar
  261          nirgal	Jean-Michel Vourgère
  262           noahm	Noah Meyerhans
  263            noel	Noèl Köthe
  264         noodles	Jonathan McDowell
  265        nthykier	Niels Thykier
  266           ntyni	Niko Tyni
  267            odyx	Didier Raboud
  268           ohura	Makoto OHURA
  269           olasd	Nicolas Dandrimont
  270         olebole	Ole Streicher
  271            olek	Olek Wojnar
  272          ondrej	Ondrej Sury
  273         onlyjob	Dmitry Smirnov
  274             orv	Benda Xu
  275           osamu	Osamu Aoki
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  276            otto	Otto Kekäläinen
  277    paddatrapper	Kyle Robbertze
  278        paravoid	Faidon Liambotis
  279          paride	Paride Legovini
  280            paul	Paul Slootman
  281         paulliu	Ying-Chun Liu
  282         paultag	Paul Richards Tagliamonte
  283             pdm	Milan Zamazal
  284             peb	Pierre-Elliott Bécue
  285            pere	Petter Reinholdtsen
  286             pgt	Pierre Gruet
  287           philh	Philip Hands
  288            phls	Paulo Henrique de Lima Santana
  289             pik	Paul Cannon
  290            pini	Gilles Filippini
  291           piotr	Piotr Ożarowski
  292             pjb	Phil Brooke
  293           pkern	Philipp Kern
  294          plessy	Charles Plessy
  295          pmhahn	Philipp Matthias Hahn
  296           pollo	Louis-Philippe Véronneau
  297          pollux	Pierre Chifflier
  298        porridge	Marcin Owsiany
  299         praveen	Praveen Arimbrathodiyil
  300        preining	Norbert Preining
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  301        pronovic	Kenneth J. Pronovici
  302        pvaneynd	Peter Van Eynde
  303             ras	Russell Stuart
  304         rbalint	Balint Reczey
  305          rbasak	Robie Basak
  306         reichel	Joachim Reichel
  307          rhonda	Rhonda D'Vine
  308          richih	Richard Michael Hartmann
  309           risko	Risko Gergely
  310         rlaager	Richard Laager
  311        rmayorga	Rene Mayorga
  312            roam	Peter Pentchev
  313          roland	Roland Rosenfeld
  314            rosh	Roger Shimizu
  315        rousseau	Ludovic Rousseau
  316           rover	Roberto Lumbreras
  317             rra	Russ Allbery
  318     rvandegrift	Ross Vandegrift
  319       samueloph	Samuel Henrique
  320        santiago	Santiago Ruano Rincón
  321         sanvila	Santiago Vila
  322            sasa	Attila Szalay
  323         sathieu	Mathieu Parent
  324          sbadia	Sebastien Badia
  325        schultmc	Michael C. Schultheiss
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  326             seb	Sebastien Delafond
  327       sebastien	Sébastien Villemot
  328         serpent	Tomasz Rybak
  329           sesse	Steinar H. Gunderson
  330             sez	Serafeim Zanikolas
  331              sf	Stefan Fritsch
  332         sharkey	Eric Sharkey
  333        siretart	Reinhard Tartler
  334          sjoerd	Sjoerd Simons
  335             sjr	Simon Richter
  336           skitt	Stephen Kitt
  337           slomo	Sebastian Dröge
  338            smcv	Simon McVittie
  339           smurf	Matthias Urlichs
  340       spwhitton	Sean Whitton
  341       sramacher	Sebastian Ramacher
  342            srud	Sruthi Chandran
  343          ssgelm	Stephen Gelman
  344      stapelberg	Michael Stapelberg
  345        stappers	Geert Stappers
  346          steele	David Steele
  347        stefanor	Stefano Rivera
  348  stephanlachnit	Stephan Lachnit
  349           steve	Steve Kostecke
  350         stevenc	Steven Chamberlain
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  351       sthibault	Samuel Thibault
  352          stuart	Stuart Prescott
  353           sudip	Sudip Mukherjee
  354            sune	Sune Vuorela
  355       sunweaver	Mike Gabriel
  356           swt2c	Scott Talbert
  357       sylvestre	Sylvestre Ledru
  358            tach	Taku Yasui
  359          taffit	David Prévot
  360          takaki	Takaki Taniguchi
  361           taowa	Taowa
  362          tassia	Tássia Camões Araújo
  363             tbm	Martin Michlmayr
  364         tehnick	Boris Pek
  365        terceiro	Antonio Terceiro
  366          tfheen	Tollef Fog Heen
  367              tg	Thorsten Glaser
  368         thansen	Tobias Hansen
  369             thb	Thaddeus H. Black
  370         thibaut	Thibaut Jean-Claude Paumard
  371           thijs	Thijs Kinkhorst
  372             thk	Thomas Koch
  373           tiago	Tiago Bortoletto Vaz
  374          tianon	Tianon Gravi
  375          tijuca	Carsten Schoenert
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  376           tille	Andreas Tille
  377            timo	Timo Jyrinki
  378            tina	Martina Ferrari
  379            tiwe	Timo Weingärtner
  380        tjhukkan	Teemu Hukkanen
  381        tmancill	Tony Mancill
  382            tobi	Tobias Frost
  383            toni	Toni Mueller
  384             tpo	Tomas Pospisek
  385         treinen	Ralf Treinen
  386        tvincent	Thomas Vincent
  387          tweber	Thomas Weber
  388           tytso	Theodore Y. Ts'o
  389         tzafrir	Tzafrir Cohen
  390            ucko	Aaron M. Ucko
  391          uhoreg	Hubert Chathi
  392        ukleinek	Uwe Kleine-König
  393          ulrike	Ulrike Uhlig
  394       ultrotter	Guido Trotter
  395        umlaeute	IOhannes m zmölnig
  396         unit193	Unit 193  
  397         vagrant	Vagrant Cascadian
  398         vasudev	Vasudev Sathish Kamath
  399           vicho	Javier Merino Cacho
  400            vivi	Vincent Prat
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  401         vlegout	Vincent Legout
  402          vorlon	Steve Langasek
  403          vvidic	Valentin Vidic
  404          wagner	Hanno Wagner
  405        weinholt	Göran Weinholt
  406           wferi	Ferenc Wágner
  407          wijnen	Bas Wijnen
  408          wookey	Wookey
  409          wouter	Wouter Verhelst
  410            wrar	Andrey Rahmatullin
  411             xam	Max Vozeler
  412          xluthi	Xavier Lüthi
  413            yadd	Xavier Guimard
  414             yoh	Yaroslav Halchenko
  415            zack	Stefano Zacchiroli
  416            zeha	Christian Hofstaedtler
  417            zhsj	Shengjing Zhu
  418            zigo	Thomas Goirand
  419          zlatan	Zlatan Todoric
  420       zugschlus	Marc Haber

<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that jackson-databind, a Java library used to parse
JSON and other data formats, did not properly validate user input
before attempting deserialization. This allowed an attacker providing
maliciously crafted input to perform code execution, or read arbitrary
files on the server.</p>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 2.8.6-1+deb9u6.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.9.8-3+deb10u1.</p>

<p>We recommend that you upgrade your jackson-databind packages.</p>

<p>For the detailed security status of jackson-databind please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/jackson-databind">\
https://security-tracker.debian.org/tracker/jackson-databind</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4542.data"
# $Id: $

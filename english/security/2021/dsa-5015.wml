<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Andrew Bartlett discovered that Samba, a SMB/CIFS file, print, and login
server for Unix, may map domain users to local users in an undesired
way. This could allow a user in an AD domain to potentially become root
on domain members.</p>

<p>A new parameter <q>min domain uid</q> (default 1000) has been added to
specify the minimum uid allowed when mapping a local account to a domain
account.</p>

<p>Further details and workarounds can be found in the upstream advisory
<a href="https://www.samba.org/samba/security/CVE-2020-25717.html">\
https://www.samba.org/samba/security/CVE-2020-25717.html</a></p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 2:4.9.5+dfsg-5+deb10u2. Additionally the update mitigates
<a href="https://security-tracker.debian.org/tracker/CVE-2020-25722">CVE-2020-25722</a>. Unfortunately the changes required to fix additional
CVEs affecting Samba as an AD-compatible domain controller are too
invasive to be backported. Thus users using Samba as an AD-compatible
domain controller are encouraged to migrate to Debian bullseye. From
this point onwards AD domain controller setups are no longer supported
in Debian oldstable.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>For the detailed security status of samba please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/samba">https://security-tracker.debian.org/tracker/samba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5015.data"
# $Id: $

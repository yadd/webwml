<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in BIND, a DNS server
implementation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8619">CVE-2020-8619</a>

    <p>It was discovered that an asterisk character in an empty non    terminal can cause an assertion failure, resulting in denial
    of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8622">CVE-2020-8622</a>

    <p>Dave Feldman, Jeff Warren, and Joel Cunningham reported that a
    truncated TSIG response can lead to an assertion failure, resulting
    in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8623">CVE-2020-8623</a>

    <p>Lyu Chiy reported that a flaw in the native PKCS#11 code can lead
    to a remotely triggerable assertion failure, resulting in denial
    of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8624">CVE-2020-8624</a>

    <p>Joop Boonen reported that update-policy rules of type <q>subdomain</q>
    are enforced incorrectly, allowing updates to all parts of the zone
    along with the intended subdomain.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 1:9.11.5.P4+dfsg-5.1+deb10u2.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>For the detailed security status of bind9 please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/bind9">https://security-tracker.debian.org/tracker/bind9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4752.data"
# $Id: $

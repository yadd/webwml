<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities have been discovered in PDNS Recursor, a resolving
name server; a traffic amplification attack against third party
authoritative name servers (NXNSAttack) and insufficient validation of
NXDOMAIN responses lacking an SOA.</p>

<p>The version of pdns-recursor in the oldstable distribution (stretch) is
no longer supported. If these security issues affect your setup, you
should upgrade to the stable distribution (buster).</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 4.1.11-1+deb10u1.</p>

<p>We recommend that you upgrade your pdns-recursor packages.</p>

<p>For the detailed security status of pdns-recursor please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pdns-recursor">\
https://security-tracker.debian.org/tracker/pdns-recursor</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4691.data"
# $Id: $

<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was found in the EC2 credentials API of Keystone, the
OpenStack identity service: Any user authenticated within a limited
scope (trust/oauth/application credential) could create an EC2 credential
with an escalated permission, such as obtaining <q>admin</q> while
the user is on a limited <q>viewer</q> role.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 2:14.2.0-0+deb10u1.</p>

<p>We recommend that you upgrade your keystone packages.</p>

<p>For the detailed security status of keystone please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/keystone">\
https://security-tracker.debian.org/tracker/keystone</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4679.data"
# $Id: $

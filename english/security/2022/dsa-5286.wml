<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Greg Hudson discovered integer overflow flaws in the PAC parsing in
krb5, the MIT implementation of Kerberos, which may result in remote
code execution (in a KDC, kadmin, or GSS or Kerberos application server
process), information exposure (to a cross-realm KDC acting
maliciously), or denial of service (KDC or kadmind process crash).</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 1.18.3-6+deb11u3.</p>

<p>We recommend that you upgrade your krb5 packages.</p>

<p>For the detailed security status of krb5 please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/krb5">https://security-tracker.debian.org/tracker/krb5</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5286.data"
# $Id: $

<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities have been discovered in the Apache HTTP server:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44224">CVE-2021-44224</a>

    <p>When operating as a forward proxy, Apache was depending on the setup
    suspectible to denial of service or Server Side Request forgery.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44790">CVE-2021-44790</a>

    <p>A buffer overflow in mod_lua may result in denial of service or
    potentially the execution of arbitrary code.</p></li>

</ul>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 2.4.38-3+deb10u7.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2.4.52-1~deb11u2.</p>

<p>We recommend that you upgrade your apache2 packages.</p>

<p>For the detailed security status of apache2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/apache2">\
https://security-tracker.debian.org/tracker/apache2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5035.data"
# $Id: $

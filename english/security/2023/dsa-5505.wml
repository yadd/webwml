<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Matteo Memelli reported an out-of-bounds read flaw when parsing CDP
addresses in lldpd, an implementation of the IEEE 802.1ab (LLDP)
protocol. A remote attacker can take advantage of this flaw to cause a
denial of service via a specially crafted CDP PDU packet.</p>

<p>For the oldstable distribution (bullseye), this problem has been fixed
in version 1.0.11-1+deb11u2.</p>

<p>For the stable distribution (bookworm), this problem has been fixed in
version 1.0.16-1+deb12u1.</p>

<p>We recommend that you upgrade your lldpd packages.</p>

<p>For the detailed security status of lldpd please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/lldpd">\https://security-tracker.debian.org/tracker/lldpd</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5505.data"
# $Id: $

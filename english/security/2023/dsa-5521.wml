<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in the Tomcat
servlet and JSP engine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28709">CVE-2023-28709</a>

    <p>Denial of Service. If non-default HTTP connector settings were used such
    that the maxParameterCount could be reached using query string parameters
    and a request was submitted that supplied exactly maxParameterCount
    parameters in the query string, the limit for uploaded request parts could
    be bypassed with the potential for a denial of service to occur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41080">CVE-2023-41080</a>

    <p>Open redirect. If the ROOT (default) web application is configured to use
    FORM authentication then it is possible that a specially crafted URL could
    be used to trigger a redirect to an URL of the attackers choice.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-42795">CVE-2023-42795</a>

    <p>Information Disclosure. When recycling various internal objects, including
    the request and the response, prior to re-use by the next request/response,
    an error could cause Tomcat to skip some parts of the recycling process
    leading to information leaking from the current request/response to the
    next.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-44487">CVE-2023-44487</a>

    <p>DoS caused by HTTP/2 frame overhead (Rapid Reset Attack)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-45648">CVE-2023-45648</a>

    <p>Request smuggling. Tomcat did not correctly parse HTTP trailer headers. A
    specially crafted, invalid trailer header could cause Tomcat to treat a
    single request as multiple requests leading to the possibility of request
    smuggling when behind a reverse proxy.</p></li>

</ul>

<p>For the stable distribution (bookworm), these problems have been fixed in
version 10.1.6-1+deb12u1.</p>

<p>We recommend that you upgrade your tomcat10 packages.</p>

<p>For the detailed security status of tomcat10 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tomcat10">\
https://security-tracker.debian.org/tracker/tomcat10</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5521.data"
# $Id: $

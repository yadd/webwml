<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The Qualys Research Labs discovered a buffer overflow in the dynamic
loader's processing of the GLIBC_TUNABLES environment variable. An
attacker can exploit this flaw for privilege escalation.</p>

<p>Details can be found in the Qualys advisory at
<a href="https://www.qualys.com/2023/10/03/cve-2023-4911/looney-tunables-local-privilege-escalation-glibc-ld-so.txt">\
https://www.qualys.com/2023/10/03/cve-2023-4911/looney-tunables-local-privilege-escalation-glibc-ld-so.txt</a></p>

<p>For the oldstable distribution (bullseye), this problem has been fixed
in version 2.31-13+deb11u7.</p>

<p>For the stable distribution (bookworm), this problem has been fixed in
version 2.36-9+deb12u3. This update includes fixes for
<a href="https://security-tracker.debian.org/tracker/CVE-2023-4527">CVE-2023-4527</a> and
<a href="https://security-tracker.debian.org/tracker/CVE-2023-4806">CVE-2023-4806</a>
originally planned for the upcoming bookworm point release.</p>

<p>We recommend that you upgrade your glibc packages.</p>

<p>For the detailed security status of glibc please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/glibc">\
https://security-tracker.debian.org/tracker/glibc</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5514.data"
# $Id: $

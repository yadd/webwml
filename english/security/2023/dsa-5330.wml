<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were discovered in Curl, an easy-to-use client-side
URL transfer library, which could result in denial of service or
information disclosure.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 7.74.0-1.3+deb11u5. This update also revises the fix for
<a href="https://security-tracker.debian.org/tracker/CVE-2022-27774">\
CVE-2022-27774</a> released in DSA-5197-1.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>For the detailed security status of curl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5330.data"
# $Id: $

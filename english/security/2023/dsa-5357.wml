<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in git, a fast, scalable,
distributed revision control system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-22490">CVE-2023-22490</a>

    <p>yvvdwf found a data exfiltration vulnerability while performing local
    clone from malicious repository even using a non-local transport.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23946">CVE-2023-23946</a>

    <p>Joern Schneeweisz found a path traversal vulnerability in git-apply
    where a path outside the working tree can be overwritten as the acting
    user.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 1:2.30.2-1+deb11u2.</p>

<p>We recommend that you upgrade your git packages.</p>

<p>For the detailed security status of git please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/git">\
https://security-tracker.debian.org/tracker/git</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5357.data"
# $Id: $

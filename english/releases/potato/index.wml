#use wml::debian::template title="Debian GNU/Linux 2.2 ('potato') Release Information" BARETITLE=yes
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
# $Id$

<p>Debian GNU/Linux 2.2 (a.k.a. Potato) was released on
<:=spokendate ("2000-08-14"):>.  The latest point release of Debian 2.2 is
<current_release_potato>, released on <a href="$(HOME)/News/<current_release_newsurl_potato/>"><current_release_date_potato></a>.</p>

<p><strong>Debian GNU/Linux 2.2 has been obsoleted by
<a href="../woody/">Debian GNU/Linux 3.0 ("woody")</a>.
Security updates are discontinued as of June 30th, 2003.</strong>
Please see
<a href="https://lists.debian.org/debian-devel-announce/2003/debian-devel-announce-200302/msg00010.html">\
the security team survey results</a> for more information.</p>   

<p>For information on the major changes in this release, please refer
to the <a href="releasenotes">Release Notes</a> and to the official
<a href="$(HOME)/News/2000/20000815">press release</a>.</p>

<p>Debian GNU/Linux 2.2 is dedicated to the memory of Joel "Espy" Klecker, a
Debian developer, unbeknownst to most of the Debian Project, was bedridden
and fighting a disease known as Duchenne Muscular Dystrophy during most of
his involvement with Debian. Only now is the Debian Project realizing the
extent of his dedication, and the friendship he bestowed upon us. So as a
show of appreciation, and in memory of his inspirational life, this release
of Debian GNU/Linux is dedicated to him.</p>

<p>Debian GNU/Linux 2.2 is available from the Internet or from CD
vendors, please see the <a href="$(HOME)/distrib/">Distribution
page</a> for more information on getting Debian.</p>

<p>The following architectures were supported in this release:</p>

<ul>
<li><a href="/ports/alpha/">alpha</a>
<li><a href="/ports/arm/">arm</a>
<li><a href="/ports/i386/">i386</a>
<li><a href="/ports/m68k/">m68k</a>
<li><a href="/ports/powerpc/">powerpc</a>
<li><a href="/ports/sparc/">sparc</a>
</ul>

<p>Before installing Debian, please read the
Installation Manual. The Installation Manual for your target
architecture contains instructions and links for all the files you need
to install. You might also be interested in the
installation guide for Debian 2.2, which is an on line tutorial.</p>

<p>If you are using APT, you can use the following lines in your
<code>/etc/apt/sources.list</code> file to be able to access potato
packages:</p>

<pre>
  deb http://archive.debian.org potato main contrib non-free
  deb http://non-us.debian.org/debian-non-US potato/non-US main non-free
</pre>

<p>Read the <code>apt-get</code>(8) and the <code>sources.list</code>(5)
manual pages for more information.</p>

<p>Contrary to our wishes, there are some problems that exist in the
potato release, even though it is declared <em>stable</em>. We've made
<a href="errata">a list of the major known problems</a>, and you can
always report other issues to us.</p>

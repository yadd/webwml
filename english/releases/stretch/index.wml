#use wml::debian::template title="Debian &ldquo;stretch&rdquo; Release Information"
#include "$(ENGLISHDIR)/releases/info"

<p>Debian <current_release_stretch> was
released <a href="$(HOME)/News/<current_release_newsurl_stretch/>"><current_release_date_stretch></a>.
<ifneq "9.0" "<current_release>"
  "Debian 9.0 was initially released on <:=spokendate('2017-06-17'):>."
/>
The release included many major
changes, described in 
our <a href="$(HOME)/News/2017/20170617">press release</a> and 
the <a href="releasenotes">Release Notes</a>.</p>

<p><strong>Debian 9 has been superseded by
<a href="../buster/">Debian 10 (<q>buster</q>)</a>.
Security updates have been discontinued as of <:=spokendate('2020-07-06'):>.
</strong></p>

<p><strong>Stretch also had benefited from Long Term Support (LTS) until
the end of June 2022. The LTS was limited to i386, amd64, armel, armhf and arm64.
All other architectures were no longer supported in stretch.
For more information, please refer to the <a
href="https://wiki.debian.org/LTS">LTS section of the Debian Wiki</a>.
</strong></p>

<p>To obtain and install Debian, see
the installation information page and the
Installation Guide. To upgrade from an older
Debian release, see the instructions in the
<a href="releasenotes">Release Notes</a>.</p>


# Activate the following when LTS period starts.
<p>Architectures supported during Long Term Support:</p>

<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
<li><a href="../../ports/arm64/">64-bit ARM (AArch64)</a>
</ul>

<p>Computer architectures supported at initial release of stretch:</p>
<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/s390x/">IBM System z</a>
<li><a href="../../ports/arm64/">64-bit ARM (AArch64)</a>
<li><a href="../../ports/ppc64el/">POWER Processors</a>
<li><a href="../../ports/mips64el/">64-bit MIPS (little endian)</a>
</ul>


<p>Contrary to our wishes, there may be some problems that exist in the
release, even though it is declared <em>stable</em>. We've made
<a href="errata">a list of the major known problems</a>, and you can always
report other issues to us.</p>

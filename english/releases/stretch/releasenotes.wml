#use wml::debian::template title="Debian 9 -- Release Notes" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"

<if-stable-release release="jessie">
<p>This is a <strong>work-in-progress version</strong> of the Release Notes
for Debian 9, codename stretch, which isn't released yet. The
information presented here might be inaccurate and outdated and is most
likely incomplete.</p>
</if-stable-release>

<p>To find out what's new in Debian 9, see the Release Notes for your
architecture:</p>

<ul>
<li> <a href="amd64/release-notes.en.txt">Release Notes for 64-bit PC (amd64)</a>
<li> <a href="i386/release-notes.en.txt">Release Notes for 32-bit PC (i386)</a>
<li> <a href="armel/release-notes.en.txt">Release Notes for EABI ARM (armel)</a>
<li> <a href="armhf/release-notes.en.txt">Release Notes for Hard Float ABI ARM (armhf)</a>
<li> <a href="mipsel/release-notes.en.txt">Release Notes for MIPS (little endian)</a>
<li> <a href="mips/release-notes.en.txt">Release Notes for MIPS (big endian)</a>
<li> <a href="s390x/release-notes.en.txt">Release Notes for IBM System z</a>
<li> <a href="arm64/release-notes.en.txt">Release Notes for 64-bit ARM (AArch64)</a>
<li> <a href="ppc64el/release-notes.en.txt">Release Notes for POWER Processors</a>
<li> <a href="mips64el/release-notes.en.txt">Release Notes for 64-bit MIPS (little endian)</a>
</ul>

<p>The Release Notes also contain instructions for users who are upgrading from prior
releases.</p>

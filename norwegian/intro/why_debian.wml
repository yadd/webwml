#use wml::debian::template title="Grunner til å velge Debian" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b" maintainer="Hans F. Nordhaug"
# Oversatt til norsk av Tor Slettnes (tor@slett.net)
# Oppdatert av Hans F. Nordhaug <hansfn@gmail.com>, 2008-2023

<div id="toc">
  <ul class="toc">
    <li><a href="#users">Debian for brukere</a></li>
    <li><a href="#devel">Debian for utviklere</a></li>
    <li><a href="#enterprise">Debian i bedriftsmiljø</a></li>
  </ul>
</div>

<p>Det er mange grunner til å velge Debian som operativsystem - som en bruker,
som en utvikler eller som en bedrift. De fleste brukere setter pris på
stabiliteten og den enkle oppdateringsprosessen av både pakker og hele
distribusjonen. Debian er også meget utbredt blant programvare- og
maskinvareutvikler fordi Debian kjører på utallige arkitekturer og enheter,
har en offentlig tilgjengelig feilsporing og andre verktøy til utviklere.
Hvis du planlegger å bruke Debian i et professjonelt miljø, så er det
ytterligere fordeler som LTS-versjoner og cloud-filbilder.</p>


<aside>
<p><span class="far fa-comment-dots fa-3x"></span> For meg har Debian det perfekte
nivået med brukervennlighet og stabilitet. Jeg har brukt flere forskjellige distribusjoner
i årenes løp, men Debian er den eneste som bare fungerer. 
<a href="https://www.reddit.com/r/debian/comments/8r6d0o/why_debian/e0osmxx">NorhamsFinest på Reddit</a></p>
</aside>

<h2><a id="users">Debian for brugere</a></h2>

<dl>
  <dt><strong>Debian er fri programvare.</strong></dt>
  <dd>
    Debian er laget med fri og åpen programvare, og vil alltid være 100%
    <a href="free">fri</a>. Fri for alle til å bruke, endre og distribuere.
    Dette er vårt hovedløfte til <a href="../users">våre brukere</a>. Det er 
    også uten kostnad.
  </dd>
</dl>

<dl>
  <dt><strong>Debian er stabilt og sikkert.</strong></dt>
  <dd>
    Debian er et Linux-basertoperativsystem for et bredt utvalg av enheter, herunder 
    bærbare og stasjonære datamaskiner, og servere. Vi leverer fornuftige standardinnstillinger 
    for alle pakkene og tilbyr sikkerhetsoppdateringer gjennom deres levetid.
  </dd>
</dl>

<dl>
  <dt><strong>Debian har omfattende støtte for maskinvare.</strong></dt>
  <dd>
    Det meste av maskinvare er allerede støttet av Linux-kjernen som
    betyr at Debian også støtter det. Proprietære drivere til maskinvare
    er tilgjengelig når det trengs.
  </dd>
</dl>

<dl>
  <dt><strong>Debian har et fleksibelt installasjonsprogram.</strong></dt>
  <dd>
    Vår <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">Live-CD</a>
    er for alle som vil prøve Debian før de installerer det.
    CD-en inneholder det brukervennlige installasjonsprogrammet Calamares, 
    som gjør det enkelt å installere Debian fra Live-systemet.
    Mer erfarne brukere kan bruke Debian installasjonsprogrammet med flere 
    valg for fininnstilling av installasjonen eller til og med bruke et
    automatisert verktøy for automatisert nettverksinstallasjon.
  </dd>
</dl>

<dl>
  <dt><strong>Debian gir problemfrie oppgraderinger.</strong></dt>
  <dd>
    Det er enkelt å holde operativsystemet oppdatert enten du ønsker å
    oppdatere til en fullstendig ny utgave eller bare oppdatere en enkelt pakke.
  </dd>
</dl>

<dl>
  <dt><strong>Debian er grunnlaget for mange andre distribusjoner.</strong></dt>
  <dd>
    Mange av de mest populære Linux-distribusjonene, slik som Ubuntu, Knoppix, PureOS og Tails,
    er basert på Debian. Vi stiller alle verktøy til rådighet, så alle kan supplere
    programvarepakkerne fra Debians arkiv med deres egne pakker hvis det trengs.
  </dd>
</dl>

<dl>
  <dt><strong>Debian-prosjektet er et fellesskap.</strong></dt>
  <dd>
    Alle kan være del av vårt fellesskap; du trenger ikke være en
    utvikler eller systemadministrator. Debian har en 
    <a href="../devel/constitution">demokratisk styreform</a>. Siden alle
    medlemmer av Debian-prosjektet har samme rettigheter, kan ikke projektet
    kontrolleres av en enkelt virksomhet. Våre utviklere er fra flere enn 60 land,
    og selve Debian er oversatt til flere enn 80 språk.
  </dd>
</dl>


<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Grunnen til Debians status som et 
operativsystem for utviklere, er støtten for et stort antall pakker / masse programvare, som er
viktig for utviklere. Debian anbefales varmt til avanserte programmerere og systemadministratorer. 
<a href="https://fossbytes.com/best-linux-distros-for-programming-developers/">Adarsh Verma på Fossbytes</a></p>
</aside>

<h2><a id="devel">Debian for utviklere</a></h2>

<dl>
  <dt><strong>Mange maskinvarearkitekturer.</strong></dt>
  <dd>
    Det er støtte for en <a href="../ports">lang liste</a> med 
    CPU-arkitekturer, herunder amd64, i386, mange versioner av ARM og MIPS, 
    POWER7, POWER8, IBM System z og RISC-V. Debian er også tilgjengelig for 
    niche-arkitekturer.
  </dd>
</dl>

<dl>
  <dt><strong>IoT og innebygde enheter.</strong></dt>
  <dd>
    Vi støtter et bredt utvalg av enheter, som eksempelvis Raspberry Pi, 
    forskjellige varianter av QNAP, mobile enheter, hjemmeruter og mange 
    Single Board Computers (SBC).
  </dd>
</dl>

<dl>
  <dt><strong>Enormt antall programvarepakker.</strong></dt>
  <dd>
    Debian har et stort antall <a href="$(DISTRIB)/packages">pakker</a> 
    (for øyeblikket i den stabile utgaven: <packages_in_stable> pakker) som bruker 
    <a href="https://manpages.debian.org/unstable/dpkg-dev/deb.5.en.html">deb-formatet</a>.
  </dd>
</dl>

<dl>
  <dt><strong>Forskjellige utgaver.</strong></dt>
  <dd>
    Ut over vår stabile utgave, kan du også få de nyere programvareversjoner ved 
    å bruke utgavene "testing" eller "unstable".
  </dd>
</dl>

<dl>
  <dt><strong>Offentligt tilgjengelig feilsporingssystem.</strong></dt>
  <dd>
    Vårt <a href="../Bugs">feilsporingssystem</a> (BTS) er offentlig 
    tilgjengelig for alle gjennom en nettleser.  Vi skjuler ikke våre 
    programvarefeil, og du kan lett sende inn nye feilrapporter eller 
    bli med i diskusjonen.
  </dd>
</dl>

<dl>
  <dt><strong>Debian fremgangsmåte og utviklerverktøy.</strong></dt>
  <dd>
    Debian tilbyr programvare med høy kvalitet. For å lære mer om våre 
    standarder, les <a href="../doc/debian-policy/">fremgangsmåten</a> som 
    definerer de tekniske kravene for alle pakker inkludert i distribusjonen.
    Vår kontinuerlige integrasjon strategi inkluderer
    Autopkgtest (kjører tester på pakkenr), 
    Piuparts (tester installasjon, oppgradering og fjerning)
    og Lintian (sjekker pakker for inkonsistens og feil).
  </dd>
</dl>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Stabilitet er synonymt med Debian. [...]
Sikkerhet er en av de vigtigste tingene, som Debian tilbyr. 
<a href="https://www.pontikis.net/blog/five-reasons-to-use-debian-as-a-server">Christos Pontikis på pontikis.net</a></p>
</aside>

<h2><a id="enterprise">Debian i bedriftsmiljø</a></h2>

<dl>
  <dt><strong>Debian er pålitelig.</strong></dt>
  <dd>
    Debian beviser dagligt sin pålitelighet i tusenvis av scenarier i den 
    virkelige verden, fra enkeltbrukeres bærbare datamaskiner til 
    superdatamaskoner, børser og bilindustrien. Debian er også populær i den 
    akademiske verden, i videnskapelige miljøer og i den offentlige sektor.
  </dd>
</dl>

<dl>
  <dt><strong>Debian har mange eksperter.</strong></dt>
  <dd>
    Våre pakkevedlikeholdere tar seg ikke bare av Debian-pakking og 
    innarbeidelse av nye oppstrømsversjoner. Ofte er de eksperter på 
    selve programvaren og bidrar derfor direkte til oppstrømsutviklingen.  
  </dd>
</dl>

<dl>
  <dt><strong>Debian er sikker.</strong></dt>
  <dd>
    Debian har sikkerhetsstøtte for sine stabile utgaver. Mange andre 
    distribusjoner og sikkerhetsforskere er avhengige av Debians 
    sikkerhetssporingsverktøy.
  </dd>
</dl>

<dl>
  <dt><strong>Long Term Support.</strong></dt>
  <dd>
    Det er gratis <a href="https://wiki.debian.org/LTS">Long Term Support</a>
    (LTS/langtidsstøtte). Det gir deg utvidet støtte av den 
    stabile utgaven i fem år eller mer. Dessuten er det også et
    <a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a>-initiativ, 
    som utvider støtte av et begrenset antall pakker i mer enn fem år.
  </dd>
</dl>

<dl>
  <dt><strong>Cloud-filbilder.</strong></dt>
  <dd>
    Offisielle cloud-filbilder er tilgjengelige for alle de største platformene.  
    Vi tilbyder også verktøy og konfigurasjon så du kan bygge ditt eget 
    skreddersydde cloud-filbilde. Du kan også bruke Debian i virtuelle 
    maskiner på skrivebordet eller i en container.
  </dd>
</dl>


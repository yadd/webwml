#use wml::debian::template title="Folk: hvem vi er, hva vi gjør" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b" maintainer="Hans F. Nordhaug"

# translators: some text is taken from /intro/about.wml

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#history">Hvordan det hele startet</a>
    <li><a href="#devcont">Utviklere og bidragsytere</a>
    <li><a href="#supporters">Individer og organisasjoner som støtter Debian</a>
    <li><a href="#users">Debian-brukere</a>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Siden mange har spurt;
Debian uttales <span style="white-space: nowrap;">/&#712;de.bi.&#601;n/.</span>
Dette kommer fra navnene på skaperen, Ian Murdock, og hans kone Debra.</p>
</aside>

<h2><a id="history">Hvordan det hele startet</a></h2>

#include "about_sub_history.data"

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span>
<a href="$(DOC)/manuals/project-history/">Les den komplette historien</a></button></p>

<h2><a id="devcont">Utviklere og bidragsytere</a></h2>

#include "about_sub_who.data"

<p>Den komplette listen med offisielle Debian-medlemmer fins på
<a href="https://nm.debian.org/members">nm.debian.org</a>, og
<a href="https://contributors.debian.org">contributors.debian.org</a>
viser en liste over all bidragsytere som jobber med Debian.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span>
<a href="philosophy">Les om filosofien vår</a></button></p>

<h2><a id="supporters">Individer og organisasjoner som støtter Debian</a></h2>

<p>I tillegg til utviklere og bidragsytere, er det mange andre individer
og organisasjoner som er del av Debian-fellesskapet:</p>

<ul>
  <li><a href="https://db.debian.org/machines.cgi">Netthotell- og maskinvaresponsorer</a></li>
  <li><a href="../mirror/sponsors">Filspeilsponsorer</a></li>
  <li><a href="../partners/">Utviklings- og servicepartnere</a></li>
  <li><a href="../consultants">Konsulenter</a></li>
  <li><a href="../CD/vendors">Leverandører av Debian installasjonsmedium</a></li>
  <li><a href="../distrib/pre-installed">Datamaskinleverandører som tilbyr Debian forhåndsinstallert</a></li>
  <li><a href="../events/merchandise">Vareleverandører</a></li>
</ul>

<h2><a id="users">Debian-brukere</a></h2>

<p>Debian brukes av et bredt spekter av organisasjoner, store og små,
i tillegg til mange tusen individer. Se vår
<a href="../users/">Hvem bruker Debian?</a>-side for en liste med
høyt profilerte organisasjoner som har sendt inn korte beskrivelser av
hvordan og om hvorfor de bruker Debian.
</p>

#use wml::debian::translation-check translation="1cd443bbff5c5f8e18b9961d25d41c997d4a4103"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se descubrieron varias vulnerabilidades en Heimdal, una implementación de
Kerberos 5 que aspira a ser compatible con MIT Kerberos.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16860">CVE-2018-16860</a>

    <p>Isaac Boukris y Andrew Bartlett descubrieron que Heimdal era
    propenso a ataques por intermediario («man-in-the-middle») provocados por una comprobación
    incompleta de las sumas de validación («checksum»). En el aviso siguiente de Samba se pueden encontrar
    detalles del problema: <a href="https://www.samba.org/samba/security/CVE-2018-16860.html">\
    https://www.samba.org/samba/security/CVE-2018-16860.html</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12098">CVE-2019-12098</a>

    <p>Se descubrió que la ausencia de verificación de la clave de intercambio
    PA-PKINIT-KX del lado cliente podría permitir que se llevara a cabo un ataque por intermediario («man-in-the-middle»).</p></li>

</ul>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 7.1.0+dfsg-13+deb9u3.</p>

<p>Le recomendamos que actualice los paquetes de heimdal.</p>

<p>Para información detallada sobre el estado de seguridad de heimdal, consulte su
página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/heimdal">\
https://security-tracker.debian.org/tracker/heimdal</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4455.data"

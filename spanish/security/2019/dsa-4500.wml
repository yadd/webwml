#use wml::debian::translation-check translation="3a434572e5a5011fce231866da5376d28082ebe6"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el navegador web Chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5805">CVE-2019-5805</a>

    <p>Se descubrió un problema de «uso tras liberar» en la biblioteca pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5806">CVE-2019-5806</a>

    <p>Wen Xu descubrió un problema de desbordamiento de entero en la biblioteca Angle.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5807">CVE-2019-5807</a>

    <p>TimGMichaud descubrió un problema de corrupción de memoria en la biblioteca
    javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5808">CVE-2019-5808</a>

    <p>cloudfuzzer descubrió un problema de «uso tras liberar» en Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5809">CVE-2019-5809</a>

    <p>Mark Brand descubrió un problema de «uso tras liberar» en Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5810">CVE-2019-5810</a>

    <p>Mark Amery descubrió un problema de revelación de información.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5811">CVE-2019-5811</a>

    <p>Jun Kokatsu descubrió una manera de sortear la funcionalidad de intercambio de recursos de origen
    cruzado.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5813">CVE-2019-5813</a>

    <p>Aleksandar Nikolic descubrió un problema de lectura fuera de límites en la
    biblioteca javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5814">CVE-2019-5814</a>

    <p>@AaylaSecura1138 descubrió una manera de sortear la funcionalidad de intercambio de recursos de
    origen cruzado.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5815">CVE-2019-5815</a>

    <p>Nicolas Grégoire descubrió un problema de desbordamiento de memoria en Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5818">CVE-2019-5818</a>

    <p>Adrian Tolbaru descubrió un problema de valor no inicializado.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5819">CVE-2019-5819</a>

    <p>Svyat Mitin descubrió un error en las herramientas para el programador.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5820">CVE-2019-5820</a>

    <p>pdknsk descubrió un problema de desbordamiento de entero en la biblioteca pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5821">CVE-2019-5821</a>

    <p>pdknsk descubrió otro problema de desbordamiento de entero en la biblioteca pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5822">CVE-2019-5822</a>

    <p>Jun Kokatsu descubrió una manera de sortear la funcionalidad de intercambio de recursos de origen
    cruzado.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5823">CVE-2019-5823</a>

    <p>David Erceg descubrió un error de navegación.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5824">CVE-2019-5824</a>

    <p>leecraso y Guang Gong descubrieron un error en el reproductor multimedia.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5825">CVE-2019-5825</a>

    <p>Genming Liu, Jianyu Chen, Zhen Feng y Jessica Liu descubrieron un
    problema de escritura fuera de límites en la biblioteca javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5826">CVE-2019-5826</a>

    <p>Genming Liu, Jianyu Chen, Zhen Feng y Jessica Liu descubrieron un
    problema de «uso tras liberar».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5827">CVE-2019-5827</a>

    <p>mlfbrown descubrió un problema de lectura fuera de límites en la biblioteca sqlite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5828">CVE-2019-5828</a>

    <p>leecraso y Guang Gong descubrieron un problema de «uso tras liberar».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5829">CVE-2019-5829</a>

    <p>Lucas Pinheiro descubrió un problema de «uso tras liberar».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5830">CVE-2019-5830</a>

    <p>Andrew Krashichkov descubrió un error de credenciales en la funcionalidad de intercambio de
    recursos de origen cruzado.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5831">CVE-2019-5831</a>

    <p>yngwei descubrió un error de mapa («map error») en la biblioteca javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5832">CVE-2019-5832</a>

    <p>Sergey Shekyan descubrió un error en la funcionalidad de intercambio de recursos de origen
    cruzado.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5833">CVE-2019-5833</a>

    <p>Khalil Zhani descubrió un error en la interfaz de usuario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5834">CVE-2019-5834</a>

    <p>Khalil Zhani descubrió un problema de suplantación de URL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5836">CVE-2019-5836</a>

    <p>Omair descubrió un problema de desbordamiento de memoria en la biblioteca Angle.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5837">CVE-2019-5837</a>

    <p>Adam Iawniuk descubrió un problema de revelación de información.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5838">CVE-2019-5838</a>

    <p>David Erceg descubrió un error en los permisos de extensiones.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5839">CVE-2019-5839</a>

    <p>Masato Kinugawa descubrió errores de implementación en Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5840">CVE-2019-5840</a>

    <p>Eliya Stein y Jerome Dangu descubrieron una manera de sortear el bloqueador de ventanas emergentes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5842">CVE-2019-5842</a>

    <p>BUGFENSE descubrió un problema de «uso tras liberar» en Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5847">CVE-2019-5847</a>

    <p>m3plex descubrió un error en la biblioteca javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5848">CVE-2019-5848</a>

    <p>Mark Amery descubrió un problema de revelación de información.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5849">CVE-2019-5849</a>

    <p>Zhen Zhou descubrió una lectura fuera de límites en la biblioteca Skia.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5850">CVE-2019-5850</a>

    <p>Brendon Tiszka descubrió un problema de «uso tras liberar» en el componente de descarga de páginas para navegación sin conexión («offline page
    fetcher»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5851">CVE-2019-5851</a>

    <p>Zhe Jin descubrió un problema de «uso tras envenenamiento» («use-after-poison»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5852">CVE-2019-5852</a>

    <p>David Erceg descubrió un problema de revelación de información.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5853">CVE-2019-5853</a>

    <p>Yngwei y sakura descubrieron un problema de corrupción de memoria.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5854">CVE-2019-5854</a>

    <p>Zhen Zhou descubrió un problema de desbordamiento de entero en la biblioteca pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5855">CVE-2019-5855</a>

    <p>Zhen Zhou descubrió un problema de desbordamiento de entero en la biblioteca pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5856">CVE-2019-5856</a>

    <p>Yongke Wang descubrió un error relacionado con el sistema de archivos: permisos de URI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5857">CVE-2019-5857</a>

    <p>cloudfuzzer descubrió una manera de provocar la caída de Chromium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5858">CVE-2019-5858</a>

    <p>evil1m0 descubrió un problema de revelación de información.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5859">CVE-2019-5859</a>

    <p>James Lee descubrió una manera de lanzar otros navegadores.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5860">CVE-2019-5860</a>

    <p>Se descubrió un problema de «uso tras liberar» en la biblioteca javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5861">CVE-2019-5861</a>

    <p>Robin Linus descubrió un error en la determinación de la ubicación del clic.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5862">CVE-2019-5862</a>

    <p>Jun Kokatsu descubrió un error en la implementación de AppCache.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5864">CVE-2019-5864</a>

    <p>Devin Grindle descubrió un error en la funcionalidad de intercambio de recursos de origen
    cruzado para extensiones.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5865">CVE-2019-5865</a>

    <p>Ivan Fratric descubrió una manera de sortear la funcionalidad de aislamiento de sitios web («site isolation»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5867">CVE-2019-5867</a>

    <p>Lucas Pinheiro descubrió un problema de lectura fuera de límites en la biblioteca
    javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5868">CVE-2019-5868</a>

    <p>banananapenguin descubrió un problema de «uso tras liberar» en la biblioteca
    javascript v8.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 76.0.3809.100-1~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de chromium.</p>

<p>Para información detallada sobre el estado de seguridad de chromium, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4500.data"

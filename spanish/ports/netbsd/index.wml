#use wml::debian::template title="Debian GNU/NetBSD" BARETITLE="yes" NOHEADER="yes"
#use wml::fmt::verbatim
#use wml::debian::translation-check translation="c9a7e0f78250fe2fea728e669907c9ee47374e1c" maintainer="David Moreno Garza"
#############################################################################
<div class="important">
 <p><strong>
 Este trabajo de adaptación está abandonado desde hace tiempo. No ha tenido actualizaciones
 desde octubre de 2002. La información en esta página es a efectos de archivo solamente.
 </strong></p>
</div>

<h1>
Debian GNU/NetBSD
</h1>

<p>
Debian GNU/NetBSD (i386) fue una adaptación del sistema operativo Debian al núcleo
NetBSD y libc (no confundir con las otras adaptaciones de Debian a BSD basadas
en glibc). En el momento en que fue abandonada (en torno a octubre de 2002) estaba una etapa temprana de desarrollo (sin
embargo, podía ser instalado desde cero).
</p>

<p>
También hubo un intento de comenzar una adaptación de Deiban a GNU/NetBSD (alpha), que podría ejecutarse desde un chroot en un sistema NetBSD (alpha) nativo, pero no podía arrancar por sí misma, y usaba la mayoría de las bibliotecas NetBSD nativas.
Se envió a la lista un <a
href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200201/msg00203.html">mensaje
de estado</a>.
</p>

<h2>Noticias históricas</h2>

<dl class="gloss">
  <dt class="new">2002-10-06:</dt>
  <dd>
  <dt class="new">2002-10-06:</dt>
  <dd>
      Ya se dispone de disquetes de instalación experimentales para la
      instalación de un sistema Debian GNU/NetBSD.
  </dd>
  <dt>2002-03-06:</dt>
  <dd>
      Matthew trabajó en <a href="https://packages.debian.org/ifupdown">ifupdown</a>
      para hacerlo funcionar.
  </dd>
  <dt>2002-02-25:</dt>
  <dd>
      Matthew ha informado de que el soporte shadow y PAM funciona en NetBSD
      ahora. <a href="https://packages.debian.org/fakeroot">fakeroot</a>
      parece funcionar en FreeBSD, pero aún tiene algunos temas en NetBSD.
  </dd>
  <dt>2002-02-07:</dt>
  <dd>
      Nathan acaba de <a
      href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00091.html">informar</a>
      de que ha conseguido arrancar Debian GNU/FreeBSD en multiusuario.
      También está trabajando en una istalación sólo de paquetes (usando un
      debootstrap modificado) consiguiendo un tarball considerablemente
      pequeño.
  </dd>
  <dt>2002-02-06:</dt>
  <dd>
      Según Joel, gcc-2.95.4 pasó la mayoría de las pruebas de su suite de
      pruebas y está empaquetado.
  </dd>
  <dt>2002-02-06:</dt>
  <dd>¡X11 funciona en NetBSD!  De nuevo, enhorabuena a Joel Baker
  </dd>
  <dt>2002-02-04:</dt> 
  <dd>Primer paso hacia el archivo Debian/*BSD: <br />
    <a href="mailto:lucifer@lightbearer.com">Joel Baker</a>
    <a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00067.html">
     anunció</a> un archivo <kbd>descarga</kbd>ble para los paquetes de
     FreeBSD y NetBSD.
  </dd>
  <dt>2002-02-03:</dt>
  <dd>¡Debian GNU/NetBSD se aloja
      <a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00043.html">
        a sí misma</a>! Tengan en cuenta que aún se necesita una instalación
        de NetBSD previa.
  </dd>
  <dt>2002-01-30:</dt>
  <dd>Ahora, la migración de Debian GNU/*BSD tiene una página web.</dd>
</dl>

<h2>¿Por qué Debian GNU/NetBSD?</h2>

<ul>
<li>NetBSD corre en hardware no soportado por Linux. La migración de Debian al
núcleo NetBSD incrementa el número de plataformas que pueden correr un sistema
operativo basado en Debian.</li>

<li>El proyecto Debian GNU/Hurd demuestra que Debian no está atado a un núcleo
específico. Sin embargo, el núcleo Hurd está aún relativamente inmaduro. Un
sistema Debian GNU/NetBSD sería usable en nivel de producción.</li>

<li>Las lecciones aprendidas al migrar Debian a NetBSD se pueden usar para
migrar Debian a otros núcleos (como los de <a
href="https://www.freebsd.org/">FreeBSD</a> y <a
href="https://www.openbsd.org/">OpenBSD</a>).</li>

<li>En contraste a otros proyectos como <a href="https://www.finkproject.org/">Fink</a>
o <a href="http://debian-cygwin.sf.net/">Debian GNU/w32</a>, Debian GNU/NetBSD
no existe para proporcionar software extra o un entorno estilo Unix para un
sistema operativo existente (los árboles de ports *BSD son bastante
extensos, y no es argumentable proporcionar un entorno de estilo Unix).
En su lugar, un usuario o administrador acostumbrado a un sistema Debian más
tradicional debería sentirse cómodo inmediatamente con un sistema GNU/NetBSD
y ser competente en un período de tiempo relativamente corto.</li>

<li>No a todo el mundo le gusta el árbol de ports *BSD o el espacio de
usuario de *BSD (esto es una preferencia personal, más que un comentario
sobre calidad). Se han producido distribuciones Linux para proporcionar
ports o un espacio de usuario de estilo *BSD para aquellos a los que les
gusta el entorno de usuario BSD pero también desean usar el núcleo Linux -
Debian GNU/NetBSD es el opuesto lógico de esto, permitiendo que la gente a
la que le gusta el espacio de usuario GNU o un sistema de empaquetado
estilo Linux use el núcleo NetBSD.</li>

<li>Porque podemos.</li>
</ul>

<h2>
Recursos
</h2>

<p>
Hay una lista de correo de Debian GNU/*BSD. La mayoría de las discusiones históricas sobre esta adaptación ocurrieron en esa lista, y son accesibles desde nuestros archivos vía web en 
<url "https://lists.debian.org/debian-bsd/" />.
</p>

## Local variables:
## sgml-default-doctype-name: "HTML"
## End:

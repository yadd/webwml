#use wml::debian::template title="Documentatieproject van Debian - VCS (Versiecontrolesysteem)" MAINPAGE="true"
#use wml::debian::translation-check translation="e6e987deba52309df5b062b1567c8f952d7bd476"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#access">Toegang tot de broncode in Git</a></li>
  <li><a href="#obtaining">Push-rechten verkrijgen</a></li>
  <li><a href="#updates">Automatisch updatemechanisme</a></li>
</ul>


<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Het <a href="ddp">Documentatieproject van Debian</a> slaat zijn webpagina's en de meeste handleidingen op in <a href="https://salsa.debian.org">Salsa</a>, de GitLab-dienst van Debian. Iedereen kan de bronnen vanuit de Salsa-dienst downloaden, maar alleen DDP-leden hebben schrijftoegang en kunnen bestanden bijwerken.</p>
</aside>

<h2><a id="access">Toegang tot de broncode in Git</a></h2>

<p>
Het documentatieproject van Debian bewaart alle inhoud op Salsa, de GitLab-dienst van Debian. Om toegang te krijgen tot individuele bestanden, te controleren op recente wijzigingen en de activiteiten van het project in het algemeen te volgen, gaat u naar de <a href="https://salsa.debian.org/ddp-team/">DDP-opslagplaats</a>.
</p>

<p>
Als u daarvandaan hele handleidingen wilt downloaden, is directe toegang tot Salsa waarschijnlijk een betere optie. In de volgende secties wordt uitgelegd hoe u een Git-opslagplaats (alleen-lezen en lezen-schrijven) naar uw lokale computer kunt klonen en hoe u uw lokale kopie kunt bijwerken. Installeer voordat u begint het pakket <tt><a href="https://packages.debian.org/git">git</a></tt> op uw eigen computer.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://wiki.debian.org/Salsa">De Salsa-documentatie lezen</a></button></p>


<h3>Een Git-opslagplaats anoniem klonen (alleen-lezen)</h3>

<p>
Gebruik dit commando om alle bestanden voor één project te downloaden:
</p>

<pre>
git clone https://salsa.debian.org/ddp-team/release-notes.git
</pre>

<p>
Doe hetzelfde voor elk project dat u naar uw eigen computer wilt downloaden. <strong>Tip:</strong> om de juiste URL voor het commando <code>git clone</code> te vinden, opent u het project in een webbrowser, klikt u op de blauwe knop <em>Clone</em> en kopieert u de URL van <em>Klonen met HTTPS</em> naar uw klembord.
</p>

<h3>Een Git-opslagplaats klonen met push-rechten (uploadrechten - lezen en schrijven)</h3>

<p>
Voordat u op deze manier toegang kunt krijgen tot de Git-server, moet u schrijftoegang krijgen tot de opslagplaats. Raadpleeg <a href="#obtaining">deze sectie</a> voor meer informatie over het aanvragen van push-toestemming.
</p>

<p>
Met schrijftoegang tot Salsa kunt u het volgende commando gebruiken om alle bestanden voor één project te downloaden:
</p>

<pre>
git clone git@salsa.debian.org:ddp-team/release-notes.git
</pre>

<p>Doe hetzelfde voor elk project dat u lokaal wilt klonen.</p>

<h3>Wijzigingen ophalen uit de externe Git-opslagplaats</h3>

<p>
Om uw lokale kopie bij te werken met eventuele wijzigingen van anderen, gaat u naar de map van de betreffende handleiding en voert u dit commando uit:
</p>

<pre>
git pull
</pre>

<h2><a id="obtaining">Push-rechten verkrijgen</a></h2>

<p>
Push-rechten zijn beschikbaar voor iedereen die wil meewerken aan het schrijven van handleidingen, FAQ's, HOWTO's, enz. Over het algemeen vragen we u om eerst een paar nuttige patches in te dienen. Volg daarna deze stappen om schrijftoegang aan te vragen:
</p>

<ol>
  <li>Een account creëren op <a href="https://salsa.debian.org/">Salsa</a> indien u dit niet reeds eerder deed.</li>
  <li>Naar de <a href="https://salsa.debian.org/ddp-team/">opslagplaats van DDP</a> gaan en klikken op <em>Request Access</em> (toegang aanvragen).</li>
  <li>Een e-mail sturen naar <a href="mailto:debian-doc@lists.debian.org">debian-doc@lists.debian.org</a> en ons vertellen hoe u bijgedragen heeft aan het Debian-project.</li>
  <li>Zodra uw aanvraag is goedgekeurd, maakt u deel uit van het DDP-team.</li>
</ol>

<h2><a id="updates">Automatisch updatemechanisme</a></h2>

<p>
Alle handleidingen worden gepubliceerd als webpagina's. Deze worden automatisch gegenereerd op www-master.debian.org als onderdeel van het reguliere proces van herbouwen van de website, dat elke vier uur plaatsvindt. Tijdens dit proces worden de nieuwste pakketversies uit het archief gedownload, wordt elke handleiding opnieuw gebouwd en worden alle bestanden overgebracht naar de onderliggende map <code>doc/manuals/</code> van de website.
</p>

<p>De documentatiebestanden die door het updatescript gegenereerd worden zijn te vinden op
<a href="manuals/">https://www.debian.org/doc/manuals/</a>.</p>

<p>De logbestanden die door het updateproces worden gegenereerd zijn te vinden op
<url "https://www-master.debian.org/build-logs/webwml/" />
(de naam van het script is <code>7doc</code> en het wordt uitgevoerd als onderdeel van de crontaak
<code>often</code>).</p>

# <p>Merk op dat dit proces de map <code>/doc/manuals/</code> opnieuw aanmaakt.
# De inhoud van de map <code>/doc/</code> wordt ofwel gegenereerd vanuit
# <a href="/devel/website/desc">webwml</a> ofwel vanuit andere scripts,
# zoals die welke bepaalde handleidingen uit hun pakketten halen.</p>

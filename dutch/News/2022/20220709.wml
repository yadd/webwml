#use wml::debian::translation-check translation="4c0e547d89fc6a0f2307d782bbce0a6ac7d74bc2"
<define-tag pagetitle>Debian 11 is bijgewerkt: 11.4 werd uitgebracht</define-tag>
<define-tag release_date>2022-07-09</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.4</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de vierde update aan van zijn
stabiele distributie Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie door een
bijgewerkte Debian-spiegelserver te gebruiken.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
plaatsen.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst van spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Oplossingen voor diverse problemen</h2>

<p>Deze update van stable, de stabiele release, voegt een paar belangrijke
correcties toe aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction apache2 "Nieuwe bovenstroomse stabiele release; oplossen probleem met smokkelen van HTTP-verzoeken [CVE-2022-26377], oplossen van problemen van lezen buiten de grenzen [CVE-2022-28330 CVE-2022-28614 CVE-2022-28615], oplossen problemen van denial of service [CVE-2022-29404 CVE-2022-30522], mogelijk probleem van lezen buiten de grenzen [CVE-2022-30556], mogelijk probleem met het omzeilen van authenticatie op basis van IP [CVE-2022-31813]">
<correction base-files "Updaten van /etc/debian_version voor tussenrelease 11.4">
<correction bash "Oplossing voor lezen 1-byte bufferoverloop, waardoor beschadigde multibyte-tekens worden veroorzaakt in commandosubstituties">
<correction clamav "Nieuwe bovenstroomse stabiele release; beveiligingsoplossingen [CVE-2022-20770 CVE-2022-20771 CVE-2022-20785 CVE-2022-20792 CVE-2022-20796]">
<correction clementine "Toevoegen ontbrekende vereiste van libqt5sql5-sqlite">
<correction composer "Probleem met code-injectie oplossen [CVE-2022-24828]; updaten GitHub-tokenpatroon">
<correction cyrus-imapd "Ervoor zorgen dat alle mailboxen een veld <q>uniqueid</q> hebben, waardoor upgrades naar versie 3.6 worden opgelost">
<correction dbus-broker "Oplossen probleem van bufferoverloop [CVE-2022-31212]">
<correction debian-edu-config "Post van het lokale netwerk aanvaarden die gezonden wordt naar root@&lt;mijn-netwerk-namen&gt;; host- en service-principals enkel aanmaken in Kerberos als deze nog niet bestaan; ervoor zorgen dat libsss-sudo geïnstalleerd is op mobiele werkstations; naamgeving en zichtbaarheid van afdrukwachtrijen oplossen; krb5i ondersteunen op schijfloze werkstations; squid: DNSv4-opzoekingen verkiezen boven DNSv6">
<correction debian-installer "Hercompilatie tegen proposed-updates; Linux kernel ABI verhogen naar 16; enkele armel netboot-doelen herstellen (openrd)">
<correction debian-installer-netboot-images "Hercompilatie tegen proposed-updates; Linux kernel ABI verhogen naar 16; enkele armel netboot-doelen herstellen (openrd)">
<correction distro-info-data "Toevoegen van Ubuntu 22.10, Kinetic Kudu">
<correction docker.io "docker.service rangschikken na containerd.service om afsluiten van containers op te lossen; expliciet het containerd socket-pad doorgeven aan dockerd om ervoor te zorgen dat het niet zelf containerd opstart">
<correction dpkg "dpkg-deb: oplossen van onverwachte bestandseinde-condities bij uitpakken .deb; libdpkg: source:* virtuele velden niet beperken tot geïnstalleerde pakketten; Dpkg::Source::Package::V2: altijd de permissies voor bovenstroomse tar-ballen herstellen (regressie van DSA-5147-1]">
<correction freetype "Oplossen probleem van bufferoverloop [CVE-2022-27404]; oplossing voor crashes [CVE-2022-27405 CVE-2022-27406]">
<correction fribidi "Oplossen probleem van bufferoverloop [CVE-2022-25308 CVE-2022-25309]; oplossing voor crash [CVE-2022-25310]">
<correction ganeti "Nieuwe bovenstroomse release; oplossen verschillende opwaarderingsproblemen; oplossen live-migratie met QEMU 4 en <q>security_model</q> van <q>user</q> of <q>pool</q>">
<correction geeqie "Reparatie voor Ctrl-klik binnen een blokselectie">
<correction gnutls28 "Oplossen SSSE3 SHA384 rekenfout; oplossen probleem van null pointer dereference [CVE-2021-4209]">
<correction golang-github-russellhaering-goxmldsig "Oplossen van probleem van null pointer dereference veroorzaakt door bewerkte XML-handtekeningen [CVE-2020-7711]">
<correction grunt "Oplossen van probleem van padoverschrijding [CVE-2022-0436]">
<correction hdmi2usb-mode-switch "udev: achtervoegsel toevoegen aan /dev/video apparaat-nodes om ze ondubbelzinnig te maken; udev-regels verplaatsen naar prioriteit 70, zodat deze na 60-persistent-v4l.rules komen">
<correction hexchat "Toevoegen van ontbrekende vereiste python3-cffi-backend">
<correction htmldoc "Oplossen van oneindige lus [CVE-2022-24191], problemen met overloop van gehele getallen [CVE-2022-27114] en probleem met stapelbufferoverloop [CVE-2022-28085]">
<correction knot-resolver "Oplossen van mogelijke fout in bewering in NSEC3-grenssituatie [CVE-2021-40083]">
<correction libapache2-mod-auth-openidc "Nieuwe bovenstroomse stabiele release; oplossen van probleem met open omleiding [CVE-2021-39191]; oplossen van crash bij herladen / herstarten">
<correction libintl-perl "gettext_xs.pm echt installeren">
<correction libsdl2 "Vermijden buiten de grenzen te lezen bij het laden van een misvormd BMP-bestand [CVE-2021-33657], en tijdens de conversie van YUV naar RGB">
<correction libtgowt "Nieuwe bovenstroomse stabiele release om recentere telegram-desktop te ondersteunen">
<correction linux "Nieuwe bovenstroomse stabiele release; verhogen van ABI naar 16">
<correction linux-signed-amd64 "Nieuwe bovenstroomse stabiele release; verhogen van ABI naar 16">
<correction linux-signed-arm64 "Nieuwe bovenstroomse stabiele release; verhogen van ABI naar 16">
<correction linux-signed-i386 "Nieuwe bovenstroomse stabiele release; verhogen van ABI naar 16">
<correction logrotate "Vergrendeling overslaan als het statusbestand voor de hele wereld leesbaar is [CVE-2022-1348]; configuratie-ontleding strenger maken om ontleden van vreemde bestanden zoals core dumps te voorkomen">
<correction lxc "Updaten van standaard GPG-sleutelserver als reparatie voor het maken van containers met het <q>download</q>-sjabloon">
<correction minidlna "HTTP-verzoeken valideren om te beschermen tegen DNS rebinding-aanvallen [CVE-2022-26505]">
<correction mutt "Probleem met uudecode-bufferoverloop oplossen [CVE-2022-1328]">
<correction nano "Oplossen van verschillende bugs, waaronder oplossingen voor crashes">
<correction needrestart "cgroup-detectie voor services en gebruikerssessies op de hoogte stellen van cgroup v2">
<correction network-manager "Nieuwe bovenstroomse stabiele release">
<correction nginx "Reparatie voor crash wanneer libnginx-mod-http-lua geladen wordt en init_worker_by_lua* gebruikt wordt; beperken van aanval met inhoudsverwarring in het protocol van de toepassingslaag in de Mail-module [CVE-2021-3618]">
<correction node-ejs "Oplossing voor probleem met sjablooninjectie aan de serverkant [CVE-2022-29078]">
<correction node-eventsource "Gevoelige kopregels verwijderen bij doorsturen naar andere oorsprong [CVE-2022-1650]">
<correction node-got "Geen omleiding naar Unix-socket toestaan [CVE-2022-33987]">
<correction node-mermaid "Problemen met cross-site scripting oplossen [CVE-2021-23648 CVE-2021-43861]">
<correction node-minimist "Probleem met prototypepollutie oplossen [CVE-2021-44906]">
<correction node-moment "Oplossen van probleem van padoverschrijding [CVE-2022-24785]">
<correction node-node-forge "Problemen met handtekeningverificatie oplossen [CVE-2022-24771 CVE-2022-24772 CVE-2022-24773]">
<correction node-raw-body "Mogelijk probleem van denial of service in node-express oplossen door node-iconv-lite in plaats van node-iconv te gebruiken">
<correction node-sqlite3 "Probleem van denial of service oplossen [CVE-2022-21227]">
<correction node-url-parse "Oplossen van problemen met het omzeilen van authenticatie [CVE-2022-0686 CVE-2022-0691]">
<correction nvidia-cuda-toolkit "Snapshots van OpenJDK8 gebruiken voor amd64 en ppc64el; de bruikbaarheid van het binaire java-bestand controleren; nsight-compute: de map 'sections' verplaatsen naar een multiarch-locatie; versie-ordening van nvidia-openjdk-8-jre oplossen">
<correction nvidia-graphics-drivers "Nieuwe bovenstroomse release; overschakelen naar de bovenstroomse boom 470; problemen van denial of service oplossen [CVE-2022-21813 CVE-2022-21814]; oplossen van probleem met schrijven buiten de grenzen [CVE-2022-28181], van probleem met lezen buiten de grenzen [CVE-2022-28183], van denial of service-problemen [CVE-2022-28184 CVE-2022-28191 CVE-2022-28192]">
<correction nvidia-graphics-drivers-legacy-390xx "Nieuwe bovenstroomse release; oplossen van problemen met schrijven buiten de grenzen [CVE-2022-28181 CVE-2022-28185]">
<correction nvidia-graphics-drivers-tesla-418 "Nieuwe bovenstroomse stabiele release">
<correction nvidia-graphics-drivers-tesla-450 "Nieuwe bovenstroomse stabiele release; oplossen van problemen met schrijven buiten de grenzen [CVE-2022-28181 CVE-2022-28185], van een probleem van denial of service [CVE-2022-28192]">
<correction nvidia-graphics-drivers-tesla-460 "Nieuwe bovenstroomse stabiele release">
<correction nvidia-graphics-drivers-tesla-470 "Nieuw pakket, overschakelen van ondersteuning voor Tesla naar de bovenstroomse boom 470; oplossen van probleem van schrijven buiten de grenzen [CVE-2022-28181], van probleem van lezen buiten de grenzen [CVE-2022-28183], van denial of service-problemen [CVE-2022-28184 CVE-2022-28191 CVE-2022-28192]">
<correction nvidia-persistenced "Nieuwe bovenstroomse release; overschakelen naar de bovenstroomse boom 470">
<correction nvidia-settings "Nieuwe bovenstroomse release; overschakelen naar de bovenstroomse boom 470">
<correction nvidia-settings-tesla-470 "Nieuw pakket, overschakelen van ondersteuning voor Tesla naar de bovenstroomse boom 470">
<correction nvidia-xconfig "Nieuwe bovenstroomse release">
<correction openssh "seccomp: toevoegen van pselect6_time64 syscall op 32-bits architecturen">
<correction orca "Gebruik met webkitgtk 2.36 oplosssen">
<correction php-guzzlehttp-psr7 "Onjuiste ontleding van kopregels repareren [CVE-2022-24775]">
<correction phpmyadmin "Enkele SQL-zoekopdrachten repareren die een serverfout genereren">
<correction postfix "Nieuwe bovenstroomse stabiele release; door gebruiker ingesteld default_transport niet opheffen in postinst; if-up.d: niet afsluiten met een foutmelding als postfix nog geen mail kan versturen">
<correction procmail "Oplossen van probleem van null pointer dereference">
<correction python-scrapy "Geen authenticatiegegevens meesturen met alle verzoeken [CVE-2021-41125]; bij het doorsturen geen cookies blootgeven over de domeinen heen [CVE-2022-0577]">
<correction ruby-net-ssh "Oplossen van de authenticatie tegenover systemen die OpenSSH 8.8 gebruiken">
<correction runc "seccomp defaultErrnoRet in ere houden; geen overerfbare bevoegdheden instellen [CVE-2022-29162]">
<correction samba "Opstartfout van winbind oplossen wanneer <q>allow trusted domains = no</q> gebruikt wordt; MIT Kerberos-authenticatie repareren; oplossen van probleem met ontsnappen uit gedeelde map via een mkdir race-conditie [CVE-2021-43566]; mogelijk ernstig probleem van gegevenscorruptie als gevolg van vergiftiging van Windows-clientcache; reparatie voor installatie op niet-systemd-systemen">
<correction tcpdump "Updaten van AppArmor-profiel om toegang te verlenen tot *.cap-bestanden, en hanteren van een numeriek achtervoegsel in bestandsnamen dat toegevoegd wordt door -W">
<correction telegram-desktop "Nieuwe bovenstroomse stabiele release met herstel van functionaliteit">
<correction tigervnc "Reparatie van de opstart van GNOME desktop bij het gebruik van tigervncserver@.service; kleurweergave repareren wanneer vncviewer en X11-server een verschillende bytevolgorde gebruiken">
<correction twisted "Oplossing voor probleem met openbaarmaking van informatie bij domeinoverschrijdende omleidingen [CVE-2022-21712], probleem van denial of service tijdens SSH-handdruk [CVE-2022-21716], problemen van smokkelen van HTTP-verzoeken [CVE-2022-24801]">
<correction tzdata "Tijdzonegegevens voor Palestina bijwerken; lijst met schrikkelseconden bijwerken">
<correction ublock-origin "Nieuwe bovenstroomse stabiele release">
<correction unrar-nonfree "Oplossen van probleem van mapoverschrijding [CVE-2022-30333]">
<correction usb.ids "Nieuwe bovenstroomse release; bijwerken van ingesloten gegevens">
<correction wireless-regdb "Nieuwe bovenstroomse release; omleiding verwijderen die door het installatieprogramma is toegevoegd, zodat de bestanden uit het pakket worden gebruikt">
</table>


<h2>Beveiligingsupdates</h2>


<p>Deze revisie voegt de volgende beveiligingsupdates toe aan de stabiele
release. Het beveiligingsteam heeft voor elk van deze updates al een advies
uitgebracht:</p>

<table border=0>
<tr><th>Advies-ID</th>  <th>Pakket</th></tr>
<dsa 2021 4999 asterisk>
<dsa 2021 5026 firefox-esr>
<dsa 2022 5034 thunderbird>
<dsa 2022 5044 firefox-esr>
<dsa 2022 5045 thunderbird>
<dsa 2022 5069 firefox-esr>
<dsa 2022 5074 thunderbird>
<dsa 2022 5086 thunderbird>
<dsa 2022 5090 firefox-esr>
<dsa 2022 5094 thunderbird>
<dsa 2022 5097 firefox-esr>
<dsa 2022 5106 thunderbird>
<dsa 2022 5107 php-twig>
<dsa 2022 5108 tiff>
<dsa 2022 5110 chromium>
<dsa 2022 5111 zlib>
<dsa 2022 5112 chromium>
<dsa 2022 5113 firefox-esr>
<dsa 2022 5114 chromium>
<dsa 2022 5115 webkit2gtk>
<dsa 2022 5116 wpewebkit>
<dsa 2022 5117 xen>
<dsa 2022 5118 thunderbird>
<dsa 2022 5119 subversion>
<dsa 2022 5120 chromium>
<dsa 2022 5121 chromium>
<dsa 2022 5122 gzip>
<dsa 2022 5123 xz-utils>
<dsa 2022 5124 ffmpeg>
<dsa 2022 5125 chromium>
<dsa 2022 5127 linux-signed-amd64>
<dsa 2022 5127 linux-signed-arm64>
<dsa 2022 5127 linux-signed-i386>
<dsa 2022 5127 linux>
<dsa 2022 5128 openjdk-17>
<dsa 2022 5129 firefox-esr>
<dsa 2022 5130 dpdk>
<dsa 2022 5131 openjdk-11>
<dsa 2022 5132 ecdsautils>
<dsa 2022 5133 qemu>
<dsa 2022 5134 chromium>
<dsa 2022 5136 postgresql-13>
<dsa 2022 5137 needrestart>
<dsa 2022 5138 waitress>
<dsa 2022 5139 openssl>
<dsa 2022 5140 openldap>
<dsa 2022 5141 thunderbird>
<dsa 2022 5142 libxml2>
<dsa 2022 5143 firefox-esr>
<dsa 2022 5145 lrzip>
<dsa 2022 5147 dpkg>
<dsa 2022 5148 chromium>
<dsa 2022 5149 cups>
<dsa 2022 5150 rsyslog>
<dsa 2022 5151 smarty3>
<dsa 2022 5152 spip>
<dsa 2022 5153 trafficserver>
<dsa 2022 5154 webkit2gtk>
<dsa 2022 5155 wpewebkit>
<dsa 2022 5156 firefox-esr>
<dsa 2022 5157 cifs-utils>
<dsa 2022 5158 thunderbird>
<dsa 2022 5159 python-bottle>
<dsa 2022 5160 ntfs-3g>
<dsa 2022 5161 linux-signed-amd64>
<dsa 2022 5161 linux-signed-arm64>
<dsa 2022 5161 linux-signed-i386>
<dsa 2022 5161 linux>
<dsa 2022 5162 containerd>
<dsa 2022 5163 chromium>
<dsa 2022 5164 exo>
<dsa 2022 5165 vlc>
<dsa 2022 5166 slurm-wlm>
<dsa 2022 5167 firejail>
<dsa 2022 5168 chromium>
<dsa 2022 5169 openssl>
<dsa 2022 5171 squid>
<dsa 2022 5172 firefox-esr>
<dsa 2022 5174 gnupg2>
</table>


<h2>Verwijderde pakketten</h2>

<p>De volgende pakketten werden verwijderd wegens omstandigheden waarover wij geen controle hebben:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction elog "Onbeheerd; beveiligingsproblemen">
<correction python-hbmqtt "Onbeheerd en defect">

</table>

<h2>Het Debian-installatieprogramma</h2>
<p>Het installatieprogramma werd bijgewerkt om de reparaties die met deze tussenrelease in de stabiele release opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Voorgestelde updates voor de stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informatie over de stabiele distributie (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een vereniging van ontwikkelaars van vrije software
die vrijwillig tijd en moeite steken in het produceren van het volledig vrije
besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
release op &lt;debian-release@lists.debian.org&gt;.</p>

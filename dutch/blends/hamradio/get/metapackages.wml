#use wml::debian::blend title="De metapakketten gebruiken"
#use "../navbar.inc"
#use wml::debian::translation-check translation="2e5bc5857b3caaa669cb2f5806c02eb8a8dc4deb"

<p>Deze doelgroepspecifieke uitgave gebruikt metapakketten als een handige manier om ermee verband houdende softwarepakketten samen te nemen. Elk metapakket dat geïnstalleerd wordt zorgt ervoor dat het pakketbeheersysteem de pakketten installeert die betrekking hebben op de betreffende taak.</p>

<p>De volgende metapakketten worden momenteel onderhouden door deze doelgroepspecifieke uitgave:</p>

<table>
	<tr><th>Taaknaam</th><th>Metapakket</th><th>Beschrijving</th><th>Catalogus</th></tr>
	<tr>
		<td>Antennes</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-antenna"><code>hamradio-antenna</code></a></td>
		<td>Deze taak bevat pakketten die nuttig zijn voor het modelleren van antennes.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/antenna">Link</a></td>
	</tr>
	<tr>
		<td>Datamodi</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-datamodes"><code>hamradio-datamodes</code></a></td>
		<td>Deze taak bevat pakketten die handig zijn voor het gebruik van datamodi, zoals RTTY en SSTV, inclusief zwakke signaalmodi, zoals JT65.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/datamodes">Link</a></td>
	</tr>
	<tr>
		<td>Digitale spraak</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-digitalvoice"><code>hamradio-digitalvoice</code></a></td>
		<td>Deze taak bevat pakketten die nuttig zijn voor het gebruik van digitale spraakmodi op RF en voor internetkoppelingen.
		<td><a href="https://blends.debian.org/hamradio/tasks/digitalvoice">Link</a></td>
	</tr>
	<tr>
		<td>Loggen</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-logging"><code>hamradio-logging</code></a></td>
		<td>Deze taak bevat pakketten die nuttig zijn voor het loggen (ook voor wedstrijden)).</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/logging">Link</a></td>
	</tr>
	<tr>
		<td>Morse</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-morse"><code>hamradio-morse</code></a></td>
		<td>Deze taak bevat pakketten die nuttig zijn voor morse-activiteiten en voor het leren van morse.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/morse">Link</a></td>
	</tr>
	<tr>
		<td>Niet-amateurmodi</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-nonamateur"><code>hamradio-nonamateur</code></a></td>
		<td>Deze taak bevat pakketten die handig zijn voor het luisteren naar niet-amateurmodi zoals AIS en ADS-B.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/nonamateur">Link</a></td>
	</tr>
	<tr>
		<td>Pakketmodi</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-packetmodes"><code>hamradio-packetmodes</code></a></td>
		<td>Deze taak bevat pakketten die nuttig zijn voor het gebruik van AX.25, waaronder IPv4 over AX.25 en APRS.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/packetmodes">Link</a></td>
	</tr>
	<tr>
		<td>Toestelbesturing</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-rigcontrol"><code>hamradio-rigcontrol</code></a></td>
		<td>Deze taak bevat pakketten die nuttig zijn voor het besturen en programmeren van apparaten.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/rigcontrol">Link</a></td>
	</tr>
	<tr>
		<td>Bediening van satelleiten</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-satellite"><code>hamradio-satellite</code></a></td>
		<td>Deze taak bevat pakketten die nuttig zijn voor het bedienen van amateursatellieten.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/satellite">Link</a></td>
	</tr>
	<tr>
		<td>Softwarematige radio</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-sdr"><code>hamradio-sdr</code></a></td>
		<td>Deze taak bevat pakketten die nuttig zijn voor het werken met softwarematige radio.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/sdr">Link</a></td>
	</tr>
	<tr>
		<td>Gereedschap</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-tools"><code>hamradio-tools</code></a></td>
		<td>Deze taak bevat pakketten die nuttig gereedschap bieden voor hamradio.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/tools">Link</a></td>
	</tr>
	<tr>
		<td>Training</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-training"><code>hamradio-training</code></a></td>
		<td>Deze taak bevat pakketten die nuttig zijn bij het trainen voor hamradio-examens.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/training">Link</a></td>
	</tr>
</table>

<p>Om een van de taakmetapakketten te installeren, gebruikt u uw favoriete pakketbeheerprogramma zoals u dat met elk ander Debian-pakket zou doen. Voor
<code>apt-get</code>:</p>

<pre>apt-get install hamradio-&lt;task&gt;</pre>

<p>Indien u de hele doelgroepspecifieke collectie wilt installeren:</p>

<pre>apt-get install hamradio-antenna hamradio-datamodes hamradio-digitalvoice hamradio-logging hamradio-morse hamradio-nonamateur hamradio-packetmodes hamradio-rigcontrol hamradio-satellite hamradio-sdr hamradio-tasks hamradio-tools hamradio-training</pre>


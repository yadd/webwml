#use wml::debian::template title="Debian BTS - verzoekserver" NOHEADER=yes NOCOPYRIGHT=true
#use wml::debian::translation-check translation="349225831b38b6f8237943a18d07bb16ba66a2aa"

<h1><a name="introduction">Inleiding tot de verzoekserver van het bugsysteem</a></h1>

<p>Er is een mailserver die de bugrapporten en indexen op verzoek als platte tekst kan verzenden.</p>

<p>Om deze te gebruiken zend u een e-mailbericht naar
<a href="mailto:request@bugs.debian.org"><code>request@bugs.debian.org</code></a>.
Het <code>Onderwerp</code> van het bericht wordt genegeerd, behalve voor
het genereren van het <code>Subject</code> van het antwoord.</p>

<p>De inhoud die u verstuurt moet een reeks commando's zijn, één per regel.
U zult een antwoord ontvangen dat eruit ziet als een transcriptie van uw
geïnterpreteerd bericht, met een antwoord op elk commando. Er worden naar
niemand meldingen verzonden voor de hier vermelde opdrachten en de e-mail wordt
nergens publiekelijk beschikbaar gemaakt.</p>

<p>Alle tekst op een regel die begint met een hekje <code>#</code> wordt
genegeerd; de server zal de verwerking stoppen wanneer hij een regel tegenkomt
met een <a href="#stopprocessing">afsluitend besturingscommando</a> (
<code>quit</code>, <code>thank you</code>, of twee
koppeltekens zijn gebruikelijke voorbeelden). De server zal ook stoppen wanneer
deze te veel onbekende of slecht opgemaakte commando's tegenkomt. Indien er
geen commando's met succes konden verwerkt worden, zal een hulptekst over de
server toegestuurd worden.</p>

<h1>Mogelijke commando's</h1>

<dl>
<dt><code>send</code> <var>bugnummer</var></dt>
<dt><code>send-detail</code> <var>bugnummer</var></dt>
<dd>
Vraagt het transcript op van het betreffende bugrapport.
<code>send-detail</code> zend ook al de <q>saaie</q> berichten mee in het
transcript, zoals de verschillende automatische ontvangstbevestigingen.
</dd>

<dt><code>index</code> [<code>full</code>]</dt>
<dt><code>index-summary by-package</code></dt>
<dt><code>index-summary by-number</code></dt>
<dd>
Vraagt de volledige index op (met alle details, inclusief afgewerkte en
doorgestuurde rapporten), of de samenvatting, geordend respectievelijk volgens
pakket of nummer.
</dd>

<dt><code>index-maint</code></dt>
<dd>
Vraagt de indexpagina op met de lijst van pakketbeheerders met bugs (open en
recent gesloten) in het volgsysteem.
</dd>

<dt><code>index maint</code> <var>pakketbeheerder</var></dt>
<dd>
Vraagt de indexpagina's van bugs in het systeem op voor de pakketbeheerder
<var>pakketbeheerder</var>. De zoekterm is een exacte overeenkomst.
De bugindex zal in een apart bericht opgestuurd worden.
</dd>

<dt><code>index-packages</code></dt>
<dd>
Vraagt de indexpagina op met de lijst van pakketten met bugs (open en
recent gesloten) in het volgsysteem.
</dd>

<dt><code>index packages</code> <var>pakket</var></dt>
<dd>
Vraagt de indexpagina's van bugs in het systeem op voor het pakket
<var>pakket</var>. De zoekterm is een exacte overeenkomst.
De bugindex zal in een apart bericht opgestuurd worden.
</dd>

<dt><code>getinfo</code> <var>bestandsnaam</var></dt>
<dd>
<p>
Een bestand opvragen met informatie over pakket(ten) en/of
beheerder(s) - beschikbare bestanden zijn:
</p>

  <dl>
  <dt><code>maintainers</code></dt>
  <dd>
  De uniforme lijst van pakketbeheerders, zoals gebruikt door het volgsysteem.
  De lijst wordt afgeleid uit de <code>Packages</code>-bestanden, de
  override-bestanden en de pseudo-packages-bestanden.
  </dd>

  <dt><code>override.</code><var>distributie</var></dt>
  <dt><code>override.</code><var>distributie</var><code>.non-free</code></dt>
  <dt><code>override.</code><var>distributie</var><code>.contrib</code></dt>
  <dt><code>override.experimental</code></dt>
  <dd>
  Informatie over de prioriteit en de sectie van pakketten en over de
  override-waarden voor de pakketbeheerders. Deze informatie wordt gebruikt
  door het proces dat de <code>Packages</code>-bestanden in het FTP-archief
  genereert. Er is informatie beschikbaar over elk van de beschikbare
  belangrijkste distributiebomen, via hun codewoord.
  </dd>

  <dt><code>pseudo-packages.description</code></dt>
  <dt><code>pseudo-packages.maintainers</code></dt>
  <dd>
  Lijst met respectievelijk beschrijvingen en pakketbeheerders van
  pseudo-pakketten.
  </dd>
  </dl>
</dd>

<dt><code>refcard</code></dt>
<dd>
Verzoekt de referentiekaart van de mailservers in gewone ASCII te verzenden.
</dd>

<dt><a name="user"><code>user</code> <var>adres</var></a></dt>
<dd>
Stelt <var>adres</var> in als de <q>user</q> voor alle volgende <code>usertag</code>-commando's.
</dd>

<dt><a name="usertag"><code>usertag</code> <var>bugnummer</var>
    [ <code>+</code> | <code>-</code> | <code>=</code> ] <var>tag</var>
    [ <var>tag</var> ... ]</a></dt>
<dd>
Maakt het mogelijk om per gebruiker tags te definiëren. Het commando
<code>usertag</code> werkt juist zoals het gewone <code>tag</code>-commando,
behalve dat u tags kunt verzinnen die u zelf wilt. Standaard wordt het adres
uit het kopveld <code>Van:</code> of <code>Antwoordadres:</code> van uw
e-mailbericht gebruikt om de gebruiker in te stellen voor
de <code>usertag</code>.
</dd>

<dt><a name="usercategory"><code>usercategory</code>
     <var>categorie-naam</var> [ <code>[hidden]</code> ]</a></dt>
<dd>
<p>
Voegt een <code>usercategory</code> toe, bewerkt deze of verwijdert ze.
Standaard is de gebruikerscategorie (usercategory) zichtbaar, maar indien het
facultatieve argument <code>[hidden]</code> opgegeven werd, zal deze niet
zichtbaar maar nog steeds beschikbaar zijn om naar te verwijzen vanuit andere
definities van gebruikerscategorieën.
</p>

<p>
Dit commando is enigszins speciaal, omdat het bij het toevoegen of bijwerken
van een gebruikerscategorie inhoud vereist die onmiddellijk na het commando
volgt. Indien echter na het commando geen inhoud volgt, zal de
gebruikerscategorie verwijderd worden. De inhoud bestaat uit regels die
beginnen met een willekeurig aantal spaties. Elke categorie moet starten met
een regel met <code>*</code> en facultatief kan deze gevolgd worden door
verschillende selectie-regels die beginnen met <code>+</code>. De volledige
indeling is als volgt:
</p>

<div>
* <var>categorienaam-1</var><br />
* <var>Categorietitel 2</var>
  [ <code>[</code><var>selectie-voorvoegsel</var><code>]</code> ]<br />
&nbsp;+ <var>Selectietitel 1</var> <code>[</code>
        [ <var>volgorde</var><code>:</code> ]
        <var>selectie-1</var> <code>]</code><br />
&nbsp;+ <var>Selectietitel 2</var> <code>[</code>
        [ <var>volgorde</var><code>:</code> ]
        <var>selectie-2</var> <code>]</code><br />
&nbsp;+ <var>Standaard Selectietitel</var> <code>[</code>
        [ <var>volgorde</var>: ] <code>]</code><br />
* <var>categorienaam-3</var><br />
</div>

<p>
De <var>categorienamen</var> welke in het commando en in de inhoud voorkomen,
worden gebruikt om onderlinge verwijzingen te maken, om onnodige uitlijning te
vermijden. De <var>Categorietitels</var> worden gebruik in de samenvatting van
het pakketrapport.
</p>

<p>
Het facultatieve <var>selectie-voorvoegsel</var> wordt voorgevoegd aan elke
<var>selectie</var> bij elk item in het categoriegedeelte. Onder de eerste
overeenkomende <var>selectie</var> wordt de bug weergegeven. De facultatieve
parameter <var>volgorde</var> specificeert de positie bij het weergeven van
de geselecteerde items, wat handig is als je een overeenkomst gebruikt die een
superset van de vorige selecteert, maar die ervoor moet worden weergegeven.
</p>

<p>
De <var>categorienaam</var> <code>normal</code> heeft de bijzondere betekenis
dat het de standaardweergave is, en dus kan men de standaardclassificatie van
een pakket wijzigen door dit te vervangen door een andere gebruikerscategorie
voor de gebruiker <var>pakketnaam</var>@packages.debian.org.
</p>

<p>
Gebruiksvoorbeeld:
</p>

<pre>
    usercategory dpkg-program [hidden]
     * Program
       + dpkg-deb [tag=dpkg-deb]
       + dpkg-query [tag=dpkg-query]
       + dselect [package=dselect]

    usercategory new-status [hidden]
     * Status [pending=]
       + Outstanding with Patch Available [0:pending+tag=patch]
       + Outstanding and Confirmed [1:pending+tag=confirmed]
       + Outstanding and More Information Needed [pending+tag=moreinfo]
       + Outstanding and Forwarded [pending+tag=forwarded]
       + Outstanding but Will Not Fix [pending+tag=wontfix]
       + Outstanding and Unclassified [2:pending]
       + From other Branch [absent]
       + Pending Upload [pending-fixed]
       + Fixed in NMU [fixed]
       + Resolved [done]
       + Unknown Pending Status []

    \# Standaardweergave wijzigen
    usercategory normal
      * new-status
      * severity

    usercategory old-normal
      * status
      * severity
      * classification
</pre>
</dd>

<dt><code>help</code></dt>
<dd>
Verzoekt dit hulpdocument per e-mail in gewone ASCII te verzenden.
</dd>

<dt><a name="stopprocessing"></a><code>quit</code></dt>
<dt><code>stop</code></dt>
<dt><code>thank</code></dt>
<dt><code>thanks</code></dt>
<dt><code>thankyou</code></dt>
<dt><code>thank you</code></dt>
<dt><code>--</code></dt>
<!-- #366093, I blame you! -->
<!-- <dt><code>kthxbye</code></dt> -->
<!-- See... I documented it! -->
<dd>
Stopt met de verwerking op deze plaats in het bericht. Hierna kunt u elke
gewenste tekst invoegen, en die zal genegeerd worden. U kunt hiervan gebruik
maken om commentaarstukken toe te voegen die langer zijn dan die welke geschikt
zijn voor <code>#</code>, bijvoorbeeld ten behoeve van personen die uw bericht
lezen (via de logboeken van het bugvolgsysteem of vanwege een <code>CC</code>
of een <code>BCC</code>).
</dd>

<dt><code>#</code>...</dt>
<dd>
Commentaar van één regel. Het <code>#</code>-teken moet aan het begin van de
regel staan.
</dd>

<dt><code>debug</code> <var>niveau</var></dt>
<dd>
Stelt het foutopsporingsniveau in op <var>niveau</var>, wat een niet-negatief
geheel getal moet zijn. 0 is geen foutopsporing; 1 volstaat gewoonlijk. De
foutopsporingsuitvoer verschijnt in het transcript. Het is waarschijnlijk niet
nuttig voor algemene gebruikers van het bug-systeem.
</dd>

</dl>

<p>Via het WWW is een <a href="server-refcard">referentiekaart</a> over de
mailservers beschikbaar in <code>bug-mailserver-refcard.txt</code> of via
e-mail met het commando <code>refcard</code> (zie hierboven).</p>

<p>Als u bugrapporten wilt manipuleren, moet u het adres
<code>control@bugs.debian.org</code> gebruiken welke een
<a href="server-control">superset van de bovenstaande commando's</a> begrijpt.
Dit wordt beschreven in een ander document dat te vinden is op het
<a href="server-control">WWW</a> in het bestand
<code>bug-maint-mailcontrol.txt</code> of door <code>help</code>
te sturen naar <code>control@bugs.debian.org</code>.</p>

<p>Voor het geval u dit leest als een gewoon tekstbestand of via e-mail: er is
een HTML-versie beschikbaar via de hoofdpagina van de inhoudsopgave voor het
bugsysteem, <code>https://www.debian.org/Bugs/</code>.</p>

<hr />

#use "otherpages.inc"

#use "$(ENGLISHDIR)/Bugs/footer.inc"

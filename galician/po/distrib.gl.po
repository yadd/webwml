# Jorge Barreiro <yortx.barry@gmail.com>, 2012.
# Parodper <parodper@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-05-13 10:11+0200\n"
"Last-Translator: Parodper <parodper@gmail.com>\n"
"Language-Team: Galician <debian-l10n-galician@lists.debian.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 3.0.1\n"

#: ../../english/distrib/search_contents-form.inc:9
#: ../../english/distrib/search_packages-form.inc:8
msgid "Keyword"
msgstr "Palabra clave"

#: ../../english/distrib/search_contents-form.inc:13
msgid "Display"
msgstr "Mostrar"

#: ../../english/distrib/search_contents-form.inc:16
msgid "paths ending with the keyword"
msgstr "rutas que acaben ca palabra clave"

#: ../../english/distrib/search_contents-form.inc:19
msgid "packages that contain files named like this"
msgstr "paquetes que conteñan ficheiros con este nome"

#: ../../english/distrib/search_contents-form.inc:22
msgid "packages that contain files whose names contain the keyword"
msgstr "paquetes que conteñan ficheiros que conteñan a palabra clave no nome"

#: ../../english/distrib/search_contents-form.inc:25
#: ../../english/distrib/search_packages-form.inc:25
msgid "Distribution"
msgstr "Distribución"

#: ../../english/distrib/search_contents-form.inc:27
#: ../../english/distrib/search_packages-form.inc:27
msgid "experimental"
msgstr "experimental"

#: ../../english/distrib/search_contents-form.inc:28
#: ../../english/distrib/search_packages-form.inc:28
msgid "unstable"
msgstr "unstable"

#: ../../english/distrib/search_contents-form.inc:29
#: ../../english/distrib/search_packages-form.inc:29
msgid "testing"
msgstr "testing"

#: ../../english/distrib/search_contents-form.inc:30
#: ../../english/distrib/search_packages-form.inc:30
msgid "stable"
msgstr "stable"

#: ../../english/distrib/search_contents-form.inc:31
#: ../../english/distrib/search_packages-form.inc:31
msgid "oldstable"
msgstr "oldstable"

#: ../../english/distrib/search_contents-form.inc:33
msgid "Architecture"
msgstr "Arquitectura"

#: ../../english/distrib/search_contents-form.inc:38
#: ../../english/distrib/search_packages-form.inc:32
#: ../../english/distrib/search_packages-form.inc:39
msgid "any"
msgstr "calquera"

#: ../../english/distrib/search_contents-form.inc:48
#: ../../english/distrib/search_packages-form.inc:43
msgid "Search"
msgstr "Busca"

#: ../../english/distrib/search_contents-form.inc:49
#: ../../english/distrib/search_packages-form.inc:44
msgid "Reset"
msgstr "Reiniciar"

#: ../../english/distrib/search_packages-form.inc:12
msgid "Search on"
msgstr "Buscar en"

#: ../../english/distrib/search_packages-form.inc:14
msgid "Package names only"
msgstr "Só nos nomes dos paquetes"

#: ../../english/distrib/search_packages-form.inc:16
msgid "Descriptions"
msgstr "Descricións"

#: ../../english/distrib/search_packages-form.inc:18
msgid "Source package names"
msgstr "Nomes dos paquetes de fontes"

#: ../../english/distrib/search_packages-form.inc:21
msgid "Only show exact matches"
msgstr "Mostrar só coincidencias exactas"

#: ../../english/distrib/search_packages-form.inc:34
msgid "Section"
msgstr "Sección"

#: ../../english/distrib/search_packages-form.inc:36
msgid "main"
msgstr "main"

#: ../../english/distrib/search_packages-form.inc:37
msgid "contrib"
msgstr "contrib"

#: ../../english/distrib/search_packages-form.inc:38
msgid "non-free"
msgstr "non-free"

#: ../../english/releases/arches.data:8
msgid "Alpha"
msgstr "Alpha"

#: ../../english/releases/arches.data:9
msgid "64-bit PC (amd64)"
msgstr "PC de 64 bits (amd64)"

#: ../../english/releases/arches.data:10
msgid "ARM"
msgstr "ARM"

#: ../../english/releases/arches.data:11
msgid "64-bit ARM (AArch64)"
msgstr "ARM de 64 bits (AArch64)"

#: ../../english/releases/arches.data:12
msgid "EABI ARM (armel)"
msgstr "EABI ARM (armel)"

#: ../../english/releases/arches.data:13
msgid "Hard Float ABI ARM (armhf)"
msgstr "Hard Float ABI ARM (armhf)"

#: ../../english/releases/arches.data:14
msgid "HP PA-RISC"
msgstr "HP PA-RISC"

#: ../../english/releases/arches.data:15
msgid "Hurd 32-bit PC (i386)"
msgstr "Hurd para PC de 32 bits (i386)"

#: ../../english/releases/arches.data:16
#, fuzzy
#| msgid "64-bit PC (amd64)"
msgid "Hurd 64-bit PC (amd64)"
msgstr "PC de 64 bits (amd64)"

#: ../../english/releases/arches.data:17
msgid "32-bit PC (i386)"
msgstr "PC de 32 bits (i386)"

#: ../../english/releases/arches.data:18
msgid "Intel Itanium IA-64"
msgstr "Intel Itanium IA-64"

#: ../../english/releases/arches.data:19
msgid "kFreeBSD 32-bit PC (i386)"
msgstr "kFreeBSD para PC de 32 bits (i386)"

#: ../../english/releases/arches.data:20
msgid "kFreeBSD 64-bit PC (amd64)"
msgstr "kFreeBSD para PC de 64 bits (amd64)"

#: ../../english/releases/arches.data:21
msgid "Motorola 680x0"
msgstr "Motorola 680x0"

#: ../../english/releases/arches.data:22
msgid "MIPS (big endian)"
msgstr "MIPS (big endian)"

#: ../../english/releases/arches.data:23
msgid "64-bit MIPS (little endian)"
msgstr "MIPS de 64 bits (little endian, extremidade menor)"

#: ../../english/releases/arches.data:24
msgid "MIPS (little endian)"
msgstr "MIPS (little endian)"

#: ../../english/releases/arches.data:25
msgid "PowerPC"
msgstr "PowerPC"

#: ../../english/releases/arches.data:26
msgid "POWER Processors"
msgstr "Procesadores POWER"

#: ../../english/releases/arches.data:27
msgid "RISC-V 64-bit little endian (riscv64)"
msgstr "RISC-V de 64 bits e extremo menor (little endian) (riscv64)"

#: ../../english/releases/arches.data:28
msgid "IBM S/390"
msgstr "IBM S/390"

#: ../../english/releases/arches.data:29
msgid "IBM System z"
msgstr "IBM System z"

#: ../../english/releases/arches.data:30
msgid "SPARC"
msgstr "SPARC"

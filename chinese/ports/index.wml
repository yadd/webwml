#use wml::debian::template title="移植"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::toc
#use wml::debian::translation-check translation="acad0e189af43d486b586d3628b2f3afa4b55523"

<toc-display/>

<toc-add-entry name="intro">介绍</toc-add-entry>
<p>
如大多数人所知，<a href="https://www.kernel.org/">Linux</a>
只是一个内核。而且长期以来，\
Linux 内核只能在从 386 起的\
英特尔 x86 系列机器上运行。
</p>
<p>
但这不再是事实。Linux 内核现\
已移植到庞大数量的架构中，这个列表还在不断增长。\
我们紧随其后，将 Debian 发行版移植到了
这些架构上。一般来说，开始过程比较\
困难（要让 libc 和动态链接器正常工作），\
然后是比较常规的，可能会有点冗长的工作，尝试在新架构下\
重新编译所有的软件包。
</p>
<p>
Debian 是一个操作系统（OS），而不是内核（实际上，它不仅仅\
是一个操作系统，因为它包含数千个应用程序）。因此，\
尽管大多数 Debian 移植都基于 Linux，但也有基于
FreeBSD、NetBSD 和 Hurd 内核的移植。
</p>

<div class="important">
<p>
此页面所述工作正在进行。注意并不是所有的移植都建有\
页面，大多数都在外部站点上。我们正在收集\
所有移植的信息，便于与 Debian 网站一起进行\
镜像。\
更多的移植可能会<a href="https://wiki.debian.org/CategoryPorts">列在</a> wiki 上。
</p>
</div>

<toc-add-entry name="portlist-released">官方移植列表</toc-add-entry>

<p>
这些移植是 Debian 计划官方支持的架构，已被包含于当前官方发布版本，或者\
将被包含于未来的某个发布版本。
</p>

<table class="tabular" summary="">
<tbody>
<tr>
<th>移植</th>
<th>架构</th>
<th>描述</th>
<th>添加于</th>
<th>状态</th>
</tr>
<tr>
<td><a href="amd64/">amd64</a></td>
<td>64 位 PC（amd64）</td>
<td>到 64 位 x86 处理器的移植，同时支持 32 位和 64 位用户空间（userland）。\
该移植支持 AMD 的 64 位 Opteron、Athlon 和 Sempron
处理器，以及支持 Intel 64 的英特尔处理器，包括
Pentium D 及各种 Xeon 和 Core 系列。</td>
<td>4.0</td>
<td><a href="$(HOME)/releases/stable/amd64/release-notes/">已发布</a></td>
</tr>
<tr>
<td><a href="arm/">arm64</a></td>
<td>64 位 ARM（AArch64）</td>
<td>到 64 位 ARM 架构的移植，包含新的 64 位的 v8 指令集\
（名为 AArch64），以支持 Applied Micro X-Gene、AMD Seattle 和 Cavium ThunderX
等处理器。</td>
<td>8</td>
<td><a href="$(HOME)/releases/stable/arm64/release-notes/">已发布</a></td>
</tr>
<tr>
<td><a href="arm/">armel</a></td>
<td>EABI ARM</td>
<td>到使用 Embedded ABI 的 32 位小端序 ARM 架构的移植，\
支持与 v5te 指令集兼容的 ARM CPU。\
此移植不会使用浮点运算单元（FPU）。</td>
<td>5.0</td>
<td><a href="$(HOME)/releases/stable/armel/release-notes/">已发布</a></td>
</tr>
<tr>
<td><a href="arm/">armhf</a></td>
<td>硬浮点 ABI ARM</td>
<td>到 32 位小端序 ARM 架构的移植，适用于支持浮点运算单元\
（FPU），以及其他现代 ARM CPU 特性的板子和设备。\
此移植至少需要支持 Thumb-2 和 VFPv3-D16 浮点\
的 ARMv7 CPU。</td>
<td>7.0</td>
<td><a href="$(HOME)/releases/stable/armhf/release-notes/">已发布</a></td>
</tr>
<tr>
<td><a href="i386/">i386</a></td>
<td>32 位 PC（i386）</td>
<td>到 32 位 x86 处理器的移植。Linux 最初\
是为英特尔（Intel） 386 处理器开发的，缩写由此而来。Debian
支持英特尔（包括所有奔腾\
系列和最近的 32 位模式的 Core Duo 机器）、AMD（K6，所有 Athlon
系列，32 位模式的 Athlon64 系列）、Cyrix 及其他\
制造商制造的所有 IA-32 处理器。</td>
<td>1.1</td>
<td><a href="$(HOME)/releases/stable/i386/release-notes/">已发布</a></td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/mips64el">mips64el</a></td>
<td>MIPS（64 位小端序模式）</td>
<td>
到小端序 N64 ABI 的移植，支持 MIPS64r1 ISA 和\
硬件浮点。
</td>
<td>9</td>
<td><a href="$(HOME)/releases/stable/mips64el/release-notes/">已发布</a></td>
</tr>
<tr>
<td><a href="powerpc/">ppc64el</a></td>
<td>POWER7+，POWER8</td>
<td>到 64 位小端序 POWER 架构的移植，\
使用新的 Open Power ELFv2 ABI。</td>
<td>8</td>
<td><a href="$(HOME)/releases/stable/ppc64el/release-notes/">已发布</a></td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/RISC-V">riscv64</a></td>
<td>RISC-V（64 位小端序）</td>
<td>到 64 位小端序 <a href="https://riscv.org/">RISC-V</a> 架构的移植，\
这是一个自由和开放的指令集架构。</td>
<td>13</td>
<td>测试中</td>
</tr>
<tr>
<td><a href="s390x/">s390x</a></td>
<td>System z</td>
<td>到 IBM System z 大型机的移植，使用 64 位用户空间（userland）。</td>
<td>7.0</td>
<td><a href="$(HOME)/releases/stable/s390x/release-notes/">已发布</a></td>
</tr>
</tbody>
</table>

<toc-add-entry name="portlist-other">其他移植列表</toc-add-entry>

<p>
这些移植可能是正在进行中的移植，计划在未来成为\
官方支持的架构，或者曾经是官方支持的移植，\
但是因为不满足发布标准，或者开发人员对其的\
兴趣减退而不再发布，或者是已停止开发的移植，\
列在这里给对移植的历史感兴趣的人阅读。
</p>

<p>
这些移植当中，仍在活跃维护的移植可以\
在 <url "https://www.ports.debian.org/"> 查看。
</p>

<div class="tip">
<p>
某些移植架构还提供了非官方性质的安装镜像，您可以在
<url "https://cdimage.debian.org/cdimage/ports"/>
这里下载使用。这些镜像由各自对应的 Debian 移植团队进行维护。
</p>
</div>

<table class="tabular" summary="">
<tbody>
<tr>
<th>移植</th>
<th>架构</th>
<th>描述</th>
<th>添加于</th>
<th>删除于</th>
<th>状态</th>
<th>被此移植取代</th>
</tr>
<tr>
<td><a href="alpha/">alpha</a></td>
<td>Alpha</td>
<td>到 64-bit RISC 架构 Alpha 的移植。</td>
<td>2.1</td>
<td>6.0</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="arm/">arm</a></td>
<td>OABI ARM</td>
<td>到使用旧 ABI 的 ARM 架构的移植。
</td>
<td>2.2</td>
<td>6.0</td>
<td>死亡</td>
<td>armel</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20130326061253/http://avr32.debian.net/">avr32</a></td>
<td>Atmel 32 位 RISC</td>
<td>到 Atmel 的 32 位 RISC 架构，AVR32 的移植。</td>
<td>-</td>
<td>-</td>
<td>死亡</td>
<td>-</td>
</tr>
<tr>
<td><a href="hppa/">hppa</a></td>
<td>HP PA-RISC</td>
<td>到惠普的 PA-RISC 架构的移植。
</td>
<td>3.0</td>
<td>6.0</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="hurd/">hurd-i386</a></td>
<td>32 位 PC（i386）</td>
<td>
到 GNU Hurd 操作系统的移植，用于 32 位 x86 处理器。
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="hurd/">hurd-amd64</a></td>
<td>64 位 PC（amd64）</td>
<td>
到 GNU Hurd 操作系统的移植，用于 64 位 x86 处理器。\
它只支持 64 位，而不是 32 位和 64 位。
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="ia64/">ia64</a></td>
<td>Intel Itanium IA-64</td>
<td>到英特尔的\
第一个 64 位架构的移植。注意：不应与用于 Pentium 4 和
Celeron 处理器的最新英特尔 64 位扩展混淆，\
其称为 Intel 64; 对于这些，请参阅 amd64 移植。
</td>
<td>3.0</td>
<td>8</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-amd64</a></td>
<td>64 位 PC（amd64）</td>
<td>到 FreeBSD 内核的移植，使用 glibc。\
作为 Debian 的第一个非 Linux 移植，以技术预览版的形式发布。
</td>
<td>6.0</td>
<td>8</td>
<td><a href="https://lists.debian.org/debian-devel/2023/05/msg00306.html">死亡</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-i386</a></td>
<td>32 位 PC（i386）</td>
<td>到 FreeBSD 内核的移植，使用 glibc。\
作为 Debian 的第一个非 Linux 移植，以技术预览版的形式发布。
</td>
<td>6.0</td>
<td>8</td>
<td><a href="https://lists.debian.org/debian-devel/2023/05/msg00306.html">死亡</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="http://www.linux-m32r.org/">m32</a></td>
<td>M32R</td>
<td>到瑞萨科技（Renesas Technology）32 位 RISC 微处理器的移植。</td>
<td>-</td>
<td>-</td>
<td>死亡</td>
<td>-</td>
</tr>
<tr>
<td><a href="m68k/">m68k</a></td>
<td>Motorola 68k</td>
<td>到 Motorola 68k 系列处理器的移植，特别是
Sun3 系列工作站、Apple Macintosh 个人
电脑及 Atari 和 Amiga 个人电脑。</td>
<td>2.0</td>
<td>4.0</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="mips/">mips</a></td>
<td>MIPS（大端序模式）</td>
<td>到 MIPS 架构的移植，
用于（大端序）SGI 机器。</td>
<td>3.0</td>
<td>11</td>
<td><a href="https://lists.debian.org/debian-release/2019/08/msg00582.html">死亡</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="mips/">mipsel</a></td>
<td>MIPS（小端序模式）</td>
<td>到 MIPS 架构的移植，
用于（小端序）Digital DECstation。
</td>
<td>3.0</td>
<td>13</td>
<td><a href="https://lists.debian.org/debian-devel-announce/2023/09/msg00000.html">死亡</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="netbsd/">netbsd-i386</a></td>
<td>32 位 PC（i386）</td>
<td>
到 NetBSD 内核和 libc 的移植，用于 32 位 x86 处理器。
</td>
<td>-</td>
<td>-</td>
<td>死亡</td>
<td>-</td>
</tr>
<tr>
<td><a href="netbsd/">netbsd-alpha</a></td>
<td>Alpha</td>
<td>
到 NetBSD 内核和 libc 的移植，用于 64 位 Alpha 处理器。
</td>
<td>-</td>
<td>-</td>
<td>死亡</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20150905061423/http://or1k.debian.net/">or1k</a></td>
<td>OpenRISC 1200</td>
<td>到 <a href="https://openrisc.io/">OpenRISC</a> 1200 开源 CPU 的一个移植。</td>
<td>-</td>
<td>-</td>
<td>死亡</td>
<td>-</td>
</tr>
<tr>
<td><a href="powerpc/">powerpc</a></td>
<td>Motorola/IBM PowerPC</td>
<td>到许多
Apple Macintosh PowerMac 机型，以及 CHRP 和 PReP 开放\
架构机器的移植。</td>
<td>2.2</td>
<td>9</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/PowerPCSPEPort">powerpcspe</a></td>
<td>PowerPC Signal Processing Engine</td>
<td>
到低功耗 32 位 FreeScale 和 IBM "e500" CPU 上的“Signal Processing Engine”硬件移植。
</td>
<td>-</td>
<td>-</td>
<td>死亡</td>
<td>-</td>
</tr>
<tr>
<td><a href="s390/">s390</a></td>
<td>S/390 和 zSeries</td>
<td>到 IBM S/390 服务器的移植。</td>
<td>3.0</td>
<td>8</td>
<td>死亡</td>
<td>s390x</td>
</tr>
<tr>
<td><a href="sparc/">sparc</a></td>
<td>Sun SPARC</td>
<td>到 Sun
UltraSPARC 系列工作站，以及它们的一些 sun4 架构\
的继承者的移植。
</td>
<td>2.1</td>
<td>8</td>
<td>死亡</td>
<td>sparc64</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/Sparc64">sparc64</a></td>
<td>64 位 SPARC</td>
<td>
到 64 位 SPARC 处理器的移植。
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/SH4">sh4</a></td>
<td>SuperH</td>
<td>
到日立 SuperH 处理器的移植。还支持开源的
<a href="https://j-core.org/">J-Core</a> 处理器。
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/X32Port">x32</a></td>
<td>64 位 PC，使用 32 位指针</td>
<td>
到 amd64/x86_64 x32 ABI 的移植，它使用 amd64 指令集，但使用\
32 位指针，以利用该指令集的更多的寄存器数量，\
同时又兼有 32 位指针的较小内存和缓存占用。
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
</tbody>
</table>

<div class="note">
<p>以上许多计算机和处理器\
名称是其制造商的商标和注册商标。</p>
</div>

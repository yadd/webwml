#use wml::debian::template title="Где взять Debian"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="e2b3956e193e35640f906e022d18f79d5522a104" maintainer="Lev Lamberov"

<p>Debian <a href="../intro/free">свободно</a> распространяется
через Интернет. Вы можете загрузить всю систему с любого из наших
<a href="ftplist">зеркал</a>.
В <a href="../releases/stable/installmanual">руководстве по установке</a>
содержатся подробные инструкции по установке.
Информацию о выпуске можно найти <a href="../releases/stable/releasenotes">здесь</a>.
</p>

<p>Данная страница содержит варианты установки стабильного выпуска Debian. Если вам нужен тестируемый
   или нестабильный выпуски, обратитесь к <a href="../releases/">странице выпусков</a>.</p>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">Загрузка установочного образа</a></h2>
    <p>В зависимости от вашего соединения с Интернет, вы можете загрузить один из следующих образов:</p>
    <ul>
      <li><a href="netinst"><strong>Небольшой установочный образ</strong></a>:
          Эти маленькие образы могут быть быстро загружены, их следует записать на сменный
          диск. Чтобы использовать эти образы, необходимо подключение к Интернет
          на машине, на которой производится установка Debian.
        <ul class="quicklist downlist">
          <li><a title="Загрузить установщик для обычного 64-битного ПК Intel и AMD"
                 href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">iso-образы
              netinst для 64-битных ПК</a></li>
          <li><a title="Загрузить установщик для обычного 32-битного ПК Intel и AMD"
                 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">iso-образы
              netinst для 32-битных ПК</a></li>
        </ul>
       </li>
       <li><a href="../CD/"><strong>Полные установочные образы</strong></a>
        содержат большее количество пакетов, что упрощает процесс установки на
        машины без соединения с Интернет.
        <ul class="quicklist downlist">
          <li><a title="Загрузить торренты DVD для 64-битного ПК Intel и AMD"
                 href="<stable-images-url/>/amd64/bt-dvd/">торренты для 64-битного ПК (DVD)</a></li>
          <li><a title="Загрузить торренты DVD для обычного 32-битного ПК Intel и AMD"
                 href="<stable-images-url/>/i386/bt-dvd/">торренты для 32-битного ПК (DVD)</a></li>
          <li><a title="Загрузить торренты CD для 64-битного ПК Intel и AMD"
                 href="<stable-images-url/>/amd64/bt-cd/">торренты для 64-битного ПК (CD)</a></li>
          <li><a title="Загрузить торренты CD для обычного 32-битного ПК Intel и AMD"
                 href="<stable-images-url/>/i386/bt-cd/">торренты для 32-битного ПК (CD)</a></li>
        </ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Используйте облачные образы Debian</a></h2>
    <p>Официальный <a href="https://cloud.debian.org/images/cloud/"><strong>облачный образ</strong></a>, собранный облачной командой, может использоваться:</p>
    <ul>
      <li>у вашего поставщика OpenStack в qcow2 или raw форматах.
        <ul class="quicklist downlist">
          <li>64-битный образ для AMD/Intel (<a title="Образ OpenStack для 64-битных архитектур Intel и AMD в формате qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.qcow2">qcow2</a>, <a title="Образ OpenStack для 64-битных архитектур Intel и AMD в формате raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.raw">raw</a>)</li>
          <li>64-битный образ для ARM (<a title="Образ OpenStack для 64-битной архитектуры ARM в формате qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-arm64.qcow2">qcow2</a>, <a title="Образ OpenStack для 64-битной архитектуры ARM в формате raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-arm64.raw">raw</a>)</li>
          <li>64-битный образ для Little Endian PowerPC (<a title="Образ OpenStack для 64-битной архитектуры Little Endian PowerPC в формате qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-ppc64el.qcow2">qcow2</a>, <a title="Образ OpenStack для 64-битной архитектуры Little Endian PowerPC в формате raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-ppc64el.raw">raw</a>)</li>
        </ul>
      </li>
      <li>в Amazon EC2 с помощью идентификаторов образов (AMI IDs) или может быть запущен через AWS Marketplace.
        <ul class="quicklist downlist">
          <li><a title="Amazon Machine Images" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Amazon Machine Images</a></li>
          <li><a title="AWS Marketplace" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS Marketplace</a></li>
        </ul>
      </li>
      <li>в Microsoft Azure через Azure Marketplace.
        <ul class="quicklist downlist">
          <li><a title="Debian 11 в Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 ("Bullseye")</a></li>
          <li><a title="Debian 10 в Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-10?tab=PlansAndPrice">Debian 10 ("Buster")</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">Покупка CD или DVD у одного из поставщиков,
      продающих CD с Debian</a></h2>

   <p>
      Многие поставщики продают дистрибутив менее, чем за 5 долларов плюс цена доставки
      (чтобы узнать, осуществляют ли они международную доставку, посмотрите их web-страницы).
      <br />
      Также вместе с дисками могут поставляться некоторые из
      <a href="../doc/books">книг о Debian</A>.
   </p>

   <p>У этих дисков есть несколько преимуществ:</p>

   <ul>
     <li>Установка с набора CD проще.</li>
     <li>Вы можете установить систему на машину без подключения к Интернет.</li>
         <li>Вы можете установить Debian на любое число компьютеров без
         необходимости скачивать при каждой установке нужные вам пакеты.</li>
     <li>Позднее CD можно использовать для более простого восстановления повреждённой системы Debian.</li>
   </ul>
    <h2><a href="pre-installed">Купить компьютер с предустановленным
      Debian</a></h2>
   <p>Свои преимущества есть и у этого способа:</p>
   <ul>
    <li>Вам не нужно устанавливать Debian.</li>
    <li>Установленная система заранее сконфигурирована под оборудование.</li>
    <li>Продавец может предоставлять техническую поддержку.</li>
   </ul>
  </div>

  <div class="item col50 lastcol">
    <h2><a href="../CD/live/">Попробуйте Debian live до установки</a></h2>
    <p>
      Вы можете попробовать Debian, загрузив live-систему с CD, DVD или USB-карты
      без установки файлов на компьютер. Когда вы будете готовы, можно будет
      запустить прилагаемый установщик (начиная с Debian 10 Buster, используется
      дружественный <a href="https://calamares.io">установщик Calamares</a>).
      Среди предоставляемых образов можно выбрать подходящий по
      размеру, языку и включённым в него пакетам, что может быть очень удобно.
      Прочтите дополнительную <a href="../CD/live#choose_live">информацию об этом методе установки</a>,
      чтобы решить, подходит ли он вам.
    </p>
    <ul class="quicklist downlist">
      <li><a title="Загрузить торренты live-образов для 64-битного ПК Intel и AMD PC"
             href="<live-images-url/>/amd64/bt-hybrid/">торренты live-образов для 64-битного ПК</a></li>
    </ul>
  </div>

</div>

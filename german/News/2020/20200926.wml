#use wml::debian::translation-check translation="cc3aa11466129a6224ab33a305a554cb8d65f63c" maintainer="Erik Pfannenstein"
<define-tag pagetitle>Debian 10 aktualisiert: 10.6 veröffentlicht</define-tag>
<define-tag release_date>2020-09-26</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die sechste Aktualisierung seiner 
Stable-Veröffentlichung Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen. Diese Aktualisierung behebt hauptsächlich 
Sicherheitslücken der Stable-Veröffentlichung sowie einige ernste Probleme. 
Für sie sind bereits separate Sicherheitsankündigungen veröffentlicht worden, auf 
die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Aktualisierung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete 
auffrischt. Es gibt keinen Grund, <codename>-Medien zu entsorgen, da deren 
Pakete nach der Installation mit Hilfe eines aktuellen Debian-Spiegelservers 
auf den neuesten Stand gebracht werden können. 
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird 
nicht viele Pakete auf den neuesten Stand bringen müssen. Die meisten dieser 
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>
Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden. 
</p>

<p>
Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian 
verwiesen wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter: 
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Stable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige 
Korrekturen vor.</p>

<p>
Bitte beachten Sie, dass die Aktualisierungen für die Pakete cargo, rustc und 
rustc-bindgen auf der <q>armel</q>-Architektur wegen 
Kompilierungsproblemen zur Zeit nicht verfügbar sind.
Sie werden nachgereicht, nachdem die Probleme behoben wurden.
</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction arch-test "Gelegentlich fehlschlagende s390x-Erkennung überarbeitet">
<correction asterisk "Absturz beim Aushandeln von T.38 mit einem Declined-Stream [CVE-2019-15297], <q>SIP-Anfrage kann die Adresse einer SIP-Gegenstelle ändern</q> [CVE-2019-18790], <q>AMI-Benutzer konnte Systembefehle ausführen</q> [CVE-2019-18610], Speicherzugriffsfehler in pjsip show history mit IPv6-Gegenstellen behoben">
<correction bacula "<q>Überlange Digest-Zeichenketten erlauben einem bösartigen Client, einen Heap-Überlauf im Speicher des Directors zu verursachen</q> behoben [CVE-2020-11061]">
<correction base-files "/etc/debian_version auf diese Zwischenveröffentlichung aktualisiert">
<correction calamares-settings-debian "Displaymanager-Modul deaktiviert">
<correction cargo "Neue Version der Originalautoren, um künftige Firefox-ESR-Versionen zu unterstützen">
<correction chocolate-doom "Fehlende Validierung nachgereicht [CVE-2020-14983]">
<correction chrony "Symlink-Race beim Schreiben in die PID-Datei verhindern [CVE-2020-14367]; Temperaturerfassung überarbeitet">
<correction debian-installer "Linux-ABI auf 4.19.0-11 aktualisiert">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates">
<correction diaspora-installer "--frozen auf die Bundle-Installation anwenden, um die Gemfile.lock der Originalautoren zu verwenden; Gemfile.lock bei Upgrades nicht ausschließen; bei Upgrades config/oidc_key.pem nicht überscreiben; config/schedule.yml beschreibbar machen">
<correction dojo "Prototype-Pollution in der deepCopy-Methode [CVE-2020-5258] und in der jqMix-Methode [CVE-2020-5259] behoben">
<correction dovecot "Dsync-Sieve-Filter-Sync-Regression behoben; Umgang mit dem Resultat von getpwent in userdb-passwd korrigiert">
<correction facter "Google-GCE-Metadaten-Endpunkt von <q>v1beta1</q> auf <q>v1</q> geändert">
<correction gnome-maps "Problem mit falsch ausgerichtetem Shape-Layer-Rendering behoben">
<correction gnome-shell "LoginDialog: Bei der VT-Umschaltung den Anmeldedialog vor dem Erscheinenlassen zurücksetzen [CVE-2020-17489]">
<correction gnome-weather "Absturz, wenn die konfigurierten Orte ungültig sind, verhindert">
<correction grunt "safeLoad beim Laden von YAML-Dateien verwenden [CVE-2020-7729]">
<correction gssdp "Neue stabile Veröffentlichung der Originalautoren">
<correction gupnp "Neue stabile Veröffentlichung der Originalautoren; <q>CallStranger</q>-Angriff verhindern [CVE-2020-12695]; GSSDP 1.0.5 voraussetzen">
<correction haproxy "logrotate.conf: rsyslog-Helfer statt SysV-Initskript verwenden; Nachrichten zurückweisen, bei denen <q>chunked</q> im Transfer-Encoding fehlt [CVE-2019-18277]">
<correction icinga2 "Symlink-Angriff verhindert [CVE-2020-14004]">
<correction incron "Aufräumen der Zombie-Prozesse überarbeitet">
<correction inetutils "Problem mit Code-Fernausführung behoben [CVE-2020-10188]">
<correction libcommons-compress-java "Dienstblockade beseitigt [CVE-2019-12402]">
<correction libdbi-perl "Speicherkorrumpierung in XS-Funktionen behoben, wenn der Perl-Stack neu alloziert wird [CVE-2020-14392]; Pufferüberlauf bei einem überlangen DBD-Klassennamen behoben [CVE-2020-14393]; NULL-Profil-Dereferenzierung in dbi_profile() behoben [CVE-2019-20919]">
<correction libvncserver "libvncclient: Abspringen, wenn der UNIX-Socketname überzulaufen droht [CVE-2019-20839]; Problem mit Pointer-Aliasing/-Alignment behoben [CVE-2020-14399]; Textchat-Maximallänge begrenzen [CVE-2020-14405]; libvncserver: Fehlende NULL-Zeiger-Überprüfung  nachgetragen [CVE-2020-14397]; Problem mit Zeiger-Aliasing/-Alignment behoben [CVE-2020-14400]; scale: vor dem Verschieben auf 64 Bit umwandeln [CVE-2020-14401]; OOB-Zugriffe (außerhalb der Grenzen) verhindert [CVE-2020-14402 CVE-2020-14403 CVE-2020-14404]">
<correction libx11 "Ganzzahlüberläufe behoben [CVE-2020-14344 CVE-2020-14363]">
<correction lighttpd "Mehrere Benutzerfreundlichkeits- und Sicherheitskorrekturen zurückportiert">
<correction linux "Neue stabile Veröffentlichung der Originalautoren; ABI auf 11 angehoben">
<correction linux-latest "Aktualisierung auf -11 Linux-Kernel-ABI">
<correction linux-signed-amd64 "Neue stabile Veröffentlichung der Originalautoren">
<correction linux-signed-arm64 "Neue stabile Veröffentlichung der Originalautoren">
<correction linux-signed-i386 "Neue stabile Veröffentlichung der Originalautoren">
<correction llvm-toolchain-7 "Neue Version der Originalautoren, um künftige Firefox-ESR-Versionen zu unterstützen; Fehler behoben, welche die rustc-Kompilierung beeinträchtigt haben">
<correction lucene-solr "Sicherheitsproblem in der Handhabung der DataImportHandler-Konfiguration behoben [CVE-2019-0193]">
<correction milkytracker "Heap-Überlauf [CVE-2019-14464], Stack-Überlauf [CVE-2019-14496], Heap-Überlauf [CVE-2019-14497], Use-after-free behoben [CVE-2020-15569]">
<correction node-bl "Anfälligkeit für Über-Lesen behoben [CVE-2020-8244]">
<correction node-elliptic "Signatur-Anpassbarkeit und Überläufe behoben [CVE-2020-13822]">
<correction node-mysql "localInfile-Option zur Kontrolle von LOAD DATA LOCAL INFILE hinzugefügt [CVE-2019-14939]">
<correction node-url-parse "Unzureichende Validierung und Überprüfung von Benutzereingaben korrigiert [CVE-2020-8124]">
<correction npm "Keine Passwörter in Logs anzeigen [CVE-2020-15095]">
<correction orocos-kdl "Explizite Inklusion des Standard-Include-Pfads entfernt, um Probleme mit cmake &lt; 3.16 zu lösen">
<correction postgresql-11 "Neue stabile Veröffentlichung der Originalautoren; sicheren search_path in Logical-Replication-Walsenders und Apply-Workern setzen [CVE-2020-14349]; Contrib-Modulinstallation sicherer machen [CVE-2020-14350]">
<correction postgresql-common "plpgsql nicht löschen, bevor die Erweiterungen getestet sind">
<correction pyzmq "Asyncio: auf POLLOUT beim Sender in can_connect warten">
<correction qt4-x11 "Pufferüberlauf in XBM-Auswertungsroutine behoben [CVE-2020-17507]">
<correction qtbase-opensource-src "Pufferüberlauf in XBM-Auswertungsroutine behoben [CVE-2020-17507]; Ausfall der Zwischenablage behoben, wenn der Zeitgeber nach 50 Tagen umbricht">
<correction ros-actionlib "YAML sicher laden [CVE-2020-10289]">
<correction rustc "Neue Version der Originalautoren, um künftige Firefox-ESR-Versionen zu unterstützen">
<correction rust-cbindgen "Neue Version der Originalautoren, um künftige Firefox-ESR-Versionen zu unterstützen">
<correction ruby-ronn "Umgang mit UTF-8-Inhalt auf Handbuchseiten überarbeitet">
<correction s390-tools "${perl:Depends} mit hartkodierter Perl-Abhängigkeit ersetzt, um die Installation unter debootstrap zu reparieren">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für jede 
davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2020 4662 openjdk-11>
<dsa 2020 4734 openjdk-11>
<dsa 2020 4736 firefox-esr>
<dsa 2020 4737 xrdp>
<dsa 2020 4738 ark>
<dsa 2020 4739 webkit2gtk>
<dsa 2020 4740 thunderbird>
<dsa 2020 4741 json-c>
<dsa 2020 4742 firejail>
<dsa 2020 4743 ruby-kramdown>
<dsa 2020 4744 roundcube>
<dsa 2020 4745 dovecot>
<dsa 2020 4746 net-snmp>
<dsa 2020 4747 icingaweb2>
<dsa 2020 4748 ghostscript>
<dsa 2020 4749 firefox-esr>
<dsa 2020 4750 nginx>
<dsa 2020 4751 squid>
<dsa 2020 4752 bind9>
<dsa 2020 4753 mupdf>
<dsa 2020 4754 thunderbird>
<dsa 2020 4755 openexr>
<dsa 2020 4756 lilypond>
<dsa 2020 4757 apache2>
<dsa 2020 4758 xorg-server>
<dsa 2020 4759 ark>
<dsa 2020 4760 qemu>
<dsa 2020 4761 zeromq3>
<dsa 2020 4762 lemonldap-ng>
<dsa 2020 4763 teeworlds>
<dsa 2020 4764 inspircd>
<dsa 2020 4765 modsecurity>
</table>

<h2>Debian-Installer</h2>

<p>
Der Installer wurde neu gebaut, damit er die Sicherheitskorrekturen enthält, 
die durch diese Zwischenveröffentlichung in Stable eingeflossen sind.
</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert 
haben:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata 
usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier Software, 
die ihre Zeit und Bemühungen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail (auf 
Englisch) an &lt;press@debian.org&gt;, oder kontaktieren das 
Stable-Release-Team (auch auf Englisch) über 
&lt;debian-release@lists.debian.org&gt;.</p>


#use wml::debian::template title="Security Information" GEN_TIME="yes" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::recent_list_security
#use wml::debian::translation-check translation="2edef71b2fd3c6f14c247e0555fea4bf13b2b627" maintainer="Luca Monducci"
#include "$(ENGLISHDIR)/releases/info"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Mantenere sicuro il proprio sistema Debian</a></li>
<li><a href="#DSAS">Avvisi recenti</a></li>
<li><a href="#infos">Fonti di informazioni sulla sicurezza</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian prende molto sul
serio la sicurezza. Gestiamo tutti i problemi di sicurezza portati alla
nostra attenzione e ci assicuriamo che vengano corretti in tempi
ragionevoli.</p>
</aside>

<p>
L'esperienza ha dimostrato che la <q>sicurezza attraverso l'oscurit�</q>
non funziona mai. Pertanto, la divulgazione pubblica consente di risolvere
pi� rapidamente e meglio i problemi di sicurezza. A questo proposito, questa
pagina affronta lo stato di Debian riguardo a varie falle di sicurezza note,
che potrebbero potenzialmente influenzare il sistema operativo Debian.
</p>

<p>
Il progetto Debian coordina molti avvisi di sicurezza con altri fornitori di
software libero e, di conseguenza, questi avvisi vengono pubblicati il giorno
stesso in cui una vulnerabilit� viene resa pubblica.
Per ricevere gli ultimi avvisi di sicurezza Debian, iscriversi alla mailing
list <a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
</p>


# "reasonable timeframe" might be too vague, but we don't have
# accurate statistics. For older (out of date) information and data
# please read:
# https://www.debian.org/News/2004/20040406  [ Year 2004 data ]
# and (older)
# https://people.debian.org/~jfs/debconf3/security/ [ Year 2003 data ]
# https://lists.debian.org/debian-security/2001/12/msg00257.html [ Year 2001]
# If anyone wants to do up-to-date analysis please contact me (jfs)
# and I will provide scripts, data and database schemas.


<p>
Debian partecipa anche alle iniziative di standardizzazione della sicurezza:
</p>

<ul>
<li>I <a href="#DSAS">Debian Security Advisories</a> sono <a
href="cve-compatibility">Compatibili con CVE</a> (vedi l <a
href="crossreferences">riferimenti incrociati</a>).</li>

<li>Debian <a href="oval/">pubblica</a> i propri rapporti sulla sicurezza
utilizzando <a href="https://github.com/CISecurity/OVALRepo">Open
Vulnerability Assessment Language (OVAL)</a></li>
</ul>


<h2><a id="keeping-secure">Mantenere sicuro il proprio sistema Debian</a></h2>

<p>
Il pacchetto <a
href="https://packages.debian.org/stable/admin/unattended-upgrades">unattended-upgrades</a>
� installato con il desktop GNOME e mantiene il computer aggiornato con gli
ultimi aggiornamenti di sicurezza (e di altro tipo) in modo automatico.

La <a href="https://wiki.debian.org/UnattendedUpgrades">pagina del wiki</a>
contiene informazioni pi� dettagliate su come impostare manualmente gli
<tt>unattended-upgrades</tt> (aggiornamenti non presidiati).
</p>

<p>
Per ulteriori informazioni sui problemi di sicurezza in Debian, consultare
le nostre FAQ e la nostra documentazione:
</p>

<p style="text-align:center">
<button type="button"><span class="fas fa-book-open fa-2x"></span>
<a href="faq">FAQ sulla Securezza</a></button>
<button type="button"><span class="fas fa-book-open fa-2x"></span>
<a href="../doc/user-manuals#securing">Securing Debian</a></button>
</p>


<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>


<h2><a id="DSAS">Avvisi recenti</a></h2>

<p>Questi sono i recenti avvisi di sicurezza Debian (DSA) inviati alla lista <a
href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
<br><b>T</b>� il collegamento alle informazioni del <a
href="https://security-tracker.debian.org/tracker">Debian Security
Tracker</a>, il numero DSA rimanda alla mail di annuncio.
</p>

<p>
#include "$(ENGLISHDIR)/security/dsa.list"
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (titles only)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (summaries)" href="dsa-long">
:#rss#}


<h2><a id="infos">Fonti di informazioni sulla sicurezza</a></h2>

<ul>
<li><a href="https://security-tracker.debian.org/">Debian Security Tracker</a>
fonte primaria per tutte le informazioni relative alla sicurezza, opzioni
di ricerca</li>

<li><a href="https://security-tracker.debian.org/tracker/data/json">elenco
in JSON</a> contiene la descrizione di CVE, il nome del pacchetto, il numero
di bug Debian, le versioni del pacchetto con la correzione, non � incluso il
DSA</li>

<li><a
href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DSA/list">Elenco
dei DSA</a> contiene i DSA con la data, i numeri dei CVE correlati, le
versioni dei pacchetti con le correzioni.</li>

<li><a
href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DLA/list">Elenco
dei DLA</a> contiene i DLA con la data, i numeri dei CVE correlati, le
versioni dei pacchetti con le correzioni.</li>

<li><a href="https://lists.debian.org/debian-security-announce/">annunci
DSA</a></li>

<li><a href="https://lists.debian.org/debian-lts-announce/">annunci
DLA</a></li>

<li><a href="oval">Archivi Oval</a></li>

<li>Cercare un DSA (il maiuscolo � importante)<br>
esempio <tt>https://security-tracker.debian.org/tracker/DSA-3814</tt></li>

<li>Cercare un DLA ( -1 � importante)<br>
esempio <tt>https://security-tracker.debian.org/tracker/DLA-867-1</tt></li>

<li>Cercare un CVE<br>
esempio <tt>https://security-tracker.debian.org/tracker/CVE-2017-6827</tt></li>
</ul>

<p>
Gli ultimi avvisi di sicurezza Debian sono disponibili come file <a
href="dsa">RDF</a>. Offriamo anche una <a href="dsa-long">versione
leggermente pi� lunga</a> dei file che include il primo paragrafo
dell'avviso corrispondente.
</p>
